/* Copyright (C) 2020 Institute of Science and Technology Austria (IST Austria)

 This file is part of wrap_2_3 which is released under the terms of GNU GENERAL PUBLIC LICENSE Version 3.
 See file "license.txt" or go to http://www.gnu.org/licenses/gpl-3.0.html for full license details.

 Developers:
 koelsboe, hwagner

*/

# wrap_2_3
## by koelsboe

## What does the software do?
For a given (possibly weighted) set of points in 2D or 3D, it computes the Delaunay triangulation, **Alpha complexes** and **Wrap complexes** (for all radii). In the interactive version, you can display them, show the **persistence diagram** and various statistics, perform **hole manipulations** and other operations, change the point set, change to **Bregman geometry**, and export various kinds of information. A lot of the functionality is also available when starting the program from the command line.

## Overview: Interactive version

**Start** the software with `./wrap_2_3`.

The **main window** opens. It displays the full Wrap complex of the default 2-dimensional point set (in green). Instead/additionally you can display the Delaunay triangulation (in gray) or the Alpha complex for the same radius (in blue) by activating the corresponding checkboxes. You can change the radius value with a slider or a spinbox, or set it to infinity.

You can zoom in the drawing area with the mouse wheel. In 3D, you can rotate the view point by pressing the left or right mouse button and moving the mouse. Doing the same while holding Ctrl will rotate the light source. The view point can be translated using the arrow keys.

Other checkboxes in the main window allow you to display the convex hull of the point set, the union of balls, labels for the input points, or labels for the intervals. By clicking on the corresponding buttons, you can open a window with the persistence diagram, (part of the) interval graph, or statistics plots. You can also show statistics for the current Alpha and Wrap complex, this will make the drawing area smaller.

The **menu bar** of the main window gives you many more options.

In the category **Input** you find options to change the input point set. The easiest to start is clearing the current point set and interactively drawing 2-dimensional points by clicking on the drawing area. Furthermore, you can load points from a file, generate a random point set with the Poisson point process, or load a filtered complex from a file. Also, you can compute the complexes based on another distance measure than the Euclidean metric: the Fisher metric, the Bregman divergence associated with the Shannon entropy, or its conjugate.

In the category **Export** you find options for exporting information about the current complexes to a file: the point set (which can be imported later), the filtration of Alpha or Wrap complexes (which also can be imported later), the simplices, information about the intervals, information about persistence homology, the tri-partition of the simplices in the Alpha filtration, ...

In the category **Operation** you find options to manipulate the current complexes or highlight interesting subsets: you can dynamically change the point set (add/delete/move a point), perform hole manipulations to open or close holes, highlight canonical basis vectors, compute a relaxed version of the Wrap complex ("with threshold"), or highlight a p-tree.

In the category **View** you find options to change the current drawing or to export the current view: you can change the range of radius values, fit the view to the current point set, change the point size, line width, colors, or export the current view to an image.

## Command-line version

A lot of the functionality (but not all) is also available when starting the program from the command line. This is also possible when the QT-library is not available and the software is compiled without it.

Expected input: `./wrap_2_3 -i <input_options> -c <computation_options> -o <output_options> -d <draw_options>`

  (all of them are optional)

 input_options:

1. default point set: empty
2. generate random points with PPP: `-i ppp <dim(2/3)> <window_size(int)> <lambda(double)> <periodic?(0/1)>`
3. load points from file: `-i file <filename>`
4. load filtered complex from file: `-i complex <filename>`

 computation_options:
1. Euclidean Alpha and Wrap complexes: empty (default)
2. Bregman geometries: `-c bregman <"shannon"/"conjugate_shannon"/"fisher">`
3. hole manipulation with basis vectors: `-c hole <"lock"/"fill"/"unlock"/"unfill"> <dim(int)> <"rank"/"below"/"above"> <rank(int)/threshold(double)>`
4. relaxed Wrap complex: `-c relaxed <threshold(double)>`

 output_options:
   1. export full information in one file, default prefix "output2/3": empty
   2. if 1 string provided: interpret as export option (see 3.), default prefix
   3. first string used as prefix for output files, other strings: different export options, can choose multiple, e.g. `-o my_prefix points persistence_barcode tripartition`,
   export options: "points", "full_info", "filtered_complex_wrap", "filtered_complex_alpha", "interval_statistics", "interval_info", "persistence_barcode", "persistence_pairs_wrap", "persistence_pairs_alpha", "boundary_matrix_wrap", "boundary_matrix_alpha", "persistence_matrices_wrap", "persistence_matrices_alpha", "tripartition"

draw_options
  1. do not draw: empty
  2. start interactive window: `-d start`
  3. export image of Alpha or Wrap complex of given radius and possibly also start window: `-d (start) <radius^2(double)/"max_wrap"> <"alpha" and/or "wrap">`


## Interactive version in detail

Interpreting the **visual output**:

* As the Wrap complex is a subset of the Alpha complex, it is always drawn above. The same is true for the Alpha complex and the Delaunay triangulation.
* 3D: To distinguish the bounding triangles of a tetrahedron from triangles which do not bound a tetrahedron, they are drawn differently. The boundary of a tetrahedron is drawn in vivid colors, the exact shading depends on the surface normal. Triangles which do not bound a tetrahedron are drawn in pale colors.
* Persistence Diagram: The axes go from 0 to the maximum critical radius value for the Wrap complex. Finite 0-dim points are drawn in orange, finite 1-dim points are drawn in blue and finite 2-dim points in green. Any infinite points are drawn in red just above the y-axis. Pairs with persistence 0 (points on the diagonal) are not drawn.

**Navigating** the scene:

* Zooming is possible in all windows (drawing area, statistics plot, interval graph, persistence diagram), using the mouse wheel.
* 3D: The scene can be rotated by pressing the left (rotation around z-axis) or right (rotation around y-axis) button and moving the mouse.
* 3D: The light source can be rotated by holding Ctrl, pressing the left (rotation around z-axis) or right (rotation around y-axis) button and moving the mouse.
* 3D: The view can be moved (the object translated) by using the arrow keys (up/down: y-direction, left/right: x-direction).

**Interactive elements** in the main window:

* The value of r (r^2 for weighted points) can be changed with a slider or a spin box. It can also be set to infinty by activating the corresponding checkbox.
* 2D: The Delaunay triangulation can be displayed (in gray color) by activating the corresponding checkbox.
* The Alpha complex (in blue) or the Wrap complex (in green) for the current radius value can be displayed by activating the corresponding checkbox..
* Plots of various statistics over the range of critical values for the Wrap complex can be displayed in a separate window by clicking the corresponding button (above drawing area).
* Statistics for the current subcomplex can be shown/hidden in the main window by clicking the corresponding button.
* 2D: The convex hull (in gray, with black border) or the union of balls (black) are displayed activating the corresponding checkboxes.
* Triangle (2D) / Tetrahedron (3D) labels are displayed by checking the 'Interval labels'-checkbox. The labels correspond to the index of the interval the simplex is contained in. The interval graph and labels are only useful for very small instances!
* Point labels are displayed by checking the 'Point labels'-checkbox. They are useful for debugging purposes or point manipulations.
* The directed graph of intervals (or actually a subgraph) can be displayed by clicking the corresponding button.
    Only intervals containing a maximal simplex (2D: triangle, 3D: tetrahedron) are displayed. Every node corresponds to an interval. There is an arc from interval A to interval B if interval A contains a simplex that is a face of a simplex in interval B.
    Non-singular intervals are displayed in gray color.
* The persistence diagram can be displayed in a separate window by clicking the corresponding button.
* For weighted points, activating the 'Weighted point spheres' displays spheres around the points with radius = sqrt(abs(weight)). They are drawn in red if the weight is negative.
* 2D: By clicking on the drawing plane, a new (unweighted) point is inserted into the current set and the complexes updated.

**Persistence diagram** window:

* Choose between diagram for the Alpha and Wrap complex (same if no hole operation was performed).
* Choose between persistence diagram for original and current filtration (differ if a hole operation was performed).
* Activate the 'highlight subcomplex'-checkbox to display a quadrant anchored on the diagonal corresponding to the homology of the current subcomplex. The persistence points within (corresponding to holes that are born and have not died yet) are highlighted.
* The 'status' persistence diagram contains the original persistence points, but their appearance depends on the status of the corresponding hole in the current subcomplex after any applied hole operation: filled disks for points in the past (hole has died), empty circles for points in the presence (hole is born, has not died), dashed circles for points in the future (hole is not born yet). A subset of the points can be selected for display: only points of a certain dimension, of a certain status, only points directly or recursively dependent on the pair currently chosen in the hole operation window or on the pair manipulated during the last hole operation.

**Point manipulation** window (Operation->Point Manipulation):

* You can delete the last inserted point by pressing the corresponding button.
* Choose whether to add, delete, or move a point of the current point set.
* If the 'recompute'-checkbox is activated, everything will be recomputed for the new point set (for weighted point, this is always done). Otherwise, the complexes will be updated dynamically (before that any hole operation that was performed will be discarded).
* Add point: Choose coordinates of the new point by inserting values in the fields 'x', 'y', (3D: 'z') or clicking on the drawing plane (only 2D).
* Delete point: Choose index of the point that should be deleted. Its coordinates are displayed.
* Move point: Choose index of the point that should be moved, and its new coordinates.
* The currently chosen new point (add, move) is drawn in black, the currently chosen old point (delete, move) is highlighted in orange.
* 2D: Moving the mouse over the drawing plane, the current mouse coordinates are displayed in the point manipulation window.
* Apply the point manipulation by clicking the button 'Apply'.
* Close the point manipulation window and hide the highlighted old and new point by clicking the button 'Exit'.

**Hole operations** window (Operation->Hole Operation):

* Apply hole manipulating operations to the current Alpha and Wrap complex.
* Choose between operations on the full complex (adding/deleting canonical (co)cycles and (co)chains) or recursive operations on the current subcomplex.
* Optionally, cohomological dependences instead of homological dependences (resulting from row reduction instead of column reduction) can be used for the recursive operations.
* Choose the dimension of the hole that should be manipulated, choose one of the 4 operations.
* Choose the persistence pair (hole) that should be manipulated. The pairs are ordered by decreasing persistence. The corresponding point will be highlighted in the persistence diagram (in 'color 2').
* The canonical (co)cycle or (co)chain of the currently chosen pair can be highlighted by activating the corresponding checkbox. Either it is highlighted in the current subcomplex or drawn without the complex. The simplex (birth or death) of the persistence pair is highlighted in 'color 2', the rest of the canonical (co)cycle/chain in 'color 1'. In the persistence diagram the current point is highlighted in 'color 2' and the directly dependent points in 'color 1'.
* Instead of on a single pair, the operation can be applied to all persistence pairs with persistence below or above a given threshold. The corresponding points will be highlighted in the persistence diagram.
* Activate 'print info' to print statistical information about the operation that was executed.
* Apply the operation with the button 'Apply', undo all previous hole operations with 'Undo all' and close the window with 'Close'.

Compute with **different distance measure** (Input->Recompute with...):

* Choose distance measure: Euclidean metric, Fisher metric, Bregman divergence (associated with half the Euclidean norm, the Shannon entropy, or its conjugate).
* Deactivating 'primal balls' computes the Bregman-Delaunay triangulation and its subcomplexes for growing dual balls instead of primal balls (which is the same complex as for the conjugate).
* One should activate 'standard 2-simplex' if the input is such.
* Activating 'weighted Euclidean radii' computes the Euclidean Delaunay triangulation for the weighted points used to compute the Bregman-Delaunay triangulation.
* Activating 'restrict to domain' computes the complexes restricted to the positive quadrant/octant by checking if the circumcenters of the simplices lie inside.

Generate a **random point set** using the Poisson point process (Input->Generate points):

* Choose the dimension, a window size (for simulating in a (size x size (x size))-window), the parameter lambda (point density), and the number of trials.
* The output is stored in a file in the folder "output".
* Optionally, points can be generated on the standard 2-simplex (in 3D) instead of a square/cube domain.
* The points can be generated uniformly for the Fisher metric instead of the Euclidean metric.
* Instead of generating points with the PPP, one could generate a fixed number.
* Optionally, weighted points are generated with random weights sampled uniformly in the given interval.
* The complexes for the last generated set of points are drawn. Disable the option 'Draw' if you generate a lot of points (more than 1000 or so).
* Activate the 'periodic'-checkbox if the generated point set shall be periodic in all directions (drawn on a torus). Otherwise the complexes are computed in R^2/R^3.
  For some point sets (usually, when the window size is too small) the periodic Delaunay triangulation does not exist. These point sets are discarded and the trial is rerun.
* Optionally, hole manipulation statistics can be computed, the filtration can be perturbed, or the complexes can be computed in Bregman geometry instead of Euclidean geometry.

**Statistics**: The following statistics for the current subcomplexes are displayed in the main window (if they are not hidden). In a separete window, plots visualize the statistics over the whole range of critical (for Wrap) radius values.

* number of points (same for Delaunay, Alpha and Wrap complex)
* number of edges and triangles (and tetrahedra) for Delaunay triangulation
* number of edges and triangles (and tetrahedra) for Alpha complex
* number of edges and triangles (and tetrahedra) for Wrap complex
* number of negative critical edges and triangles (and tetrahedra)
* number of positive critical edges (and triangles)
* number of intervals
* 0th and 1st (and 2nd) Betti number

Changing **drawing paramters** (View->Redraw):

* Change the point size in the drawing area (independently for input points and points in the filtration).
* Change the line widh in the drawing area.
* Change the font size for the labels (for points or intervals).
* Choose a maximum value for the statistics plots, persistence diagram, and coloring of the simplices (if -1: maximum critical value for Wrap complex).
* 3D: Choose values for the rotation of the scene, the rotation of the light source, the scale factor of the scene, and values for translating the scene.
* 3D: Disable 'draw lone triangles pale' to draw all triangles in vivid colors.
* 3D: Disable 'filter highlighted items' to draw all highlighted items independent of their filtration value.
* Color the simplices by their Alpha or Wrap radius (2D: color of the Delaunay triangulation changes, 3D: colors of Alpha and Wrap simplices change).
* Activating 'always use radius^2' will use the squared radius in the interactive view and for the persistence computation even if the points are unweighted.

### Menu
**Input**
* Input->Clear: Clear the current set of points (delete all).
* Input->Clear->2/3D: Clear the current set of points and change the underlying dimension (2D <-> 3D).
* Input->Load Points: Load points from a file, containing a list of 2- or 3-dimensional points. Each row stores the coordinates of a point separated by a white space. For weighted points, the first row stores the underlying dimension (2 or 3) and the weight is stored as an additional coordinate. Examples may be found in the folders "data_*".
* Input->Load Points (no weights): Load points from a file, ignoring any weights.
* Input->Load Points (test ...): Load points from a file and perform some test procedure on them (dynamic runtime or hole operations).
* Input->Recompute with ...: Recompute the current complexes using a different distance measure (see above).
* Input->Generate Points: Generate a random point set (see above).
* Input->Import filtered complex: Import a filtered complex from a file and use it for the Alpha complexes (the filtration values can be different from the Delaunay radii). The expected input has the same format that we get from exporting filtered complexes with the software.

**Export**
* Export->Export points: Export the current point set coordinates to a file.
* Export->Export full information: Export the point set, information about the simplices and the filtration, the boundary matrix, and the persistence pairs to a file.
* Export->Export simplices: Export information about the simplices to a file.
* Export->Export filtered complex ...: Export the filtration of Alpha or Wrap complexes to a file. It could be imported later with 'Input->Import filtered complex'.
* Export->Export interval info ...: Export statistics or detailed information about the intervals to a file.
* Export->Export persistence info ...: Export the persistence barcode, persistence pairs, boundary matrix, or reduction matrices for the Wrap or the Alpha complexes to a file.
* Export->Export tri-partition (Alpha): Export the tri-partition of the simplices ordered by the Delaunay radius to a file.

**Operation**
* Operation->Point manipulation: Open point manipulation window (see above).
* Operation->Hole operation: Open hole operation window (see above).
* Operation->Undo hole operations: Undo all hole operations that were performed.
* Operation->Recompute with threshold: Recompute the intervals with a given imprecision threshold to compute a relaxed version of the Wrap complex.
* Operation->Highlight differences: Highlight the simplices that do not appear in a given simplicial complex. The expected input has the same format that we get with 'Export->Export simplices'.
* Operation->Highlight p-tree: Highlight the simplices of the p-tree (the death simplices) for a given dimension.

**View**
* View->Set radius range: Change the possible range of radius values for the slider.
* View->Fit radius range (Alpha): The range of values for the radius slider is set to the critical values of the Alpha complex.
* View->Fit radius range (Wrap): The range of values for the radius slider is set to the critical values of the Wrap complex.
* View->Fit view: Fit the view to the current set of points.
* View->Set view: Set the minimum and maximum coordinates that are displayed in the drawing area.
* View->Redraw: Redraw the current scene with the given parameters (see above).
* View->Change colors: Change colors of the various kinds of simplices.
* View->Export PNG/SVG-image: The current view is exported as an image file (2D: as SVG (vector graphics) or PNG, 3D: as PNG) and stored in a desired location.
* View->Export persistence diagram: The persistence diagram is exported as an image file in SVG-format.

## How to run the software

### Docker
* Install Docker on your system (see e.g. [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)).
* Go into the *docker*-folder of the repository: `cd docker`
* Build the Docker-image: `sudo ./build_docker_image.sh`
* Run: `sudo ./run_wrap_2_3.sh`

### Linux
* It only works on Linux. If you have another operating system, install a virtual machine (see below).
* Download the repository. Extract. Go into the folder.
* Install necessary libraries: `sudo apt-get install cmake make g++ gcc libmpfr-dev libeigen3-dev libboost-all-dev libqt5svg5-dev` (maybe also libmpfr4, libmpfr4-dbg, libgmp3-dev, libgmp-dev)
* Install CGAL:
    * We need the version that is provided with the repository. It is CGAL-4.12, but 2 files were modified due to bugs (Regular_Triangulation2.h, Regular_Triangulation3.h).
    * if already installed, remove: `sudo apt-get remove libcgal-dev libcgal-qt5-dev`
    * go into the CGAL-4.12 folder in the repository
    * `cmake .`
    * `make`
* Run:
    * go into the repository folder
    * get permission: `chmod 744 wrap_2_3`
    * for Ubuntu>16: create symbolic link to libmpfr4: `sudo ln -s /usr/lib/x86_64-linux-gnu/libmpfr.so.6 /usr/lib/x86_64-linux-gnu/libmpfr.so.4`
    * if error "libCGAL.so.13: cannot open shared object file": `export LD_LIBRARY_PATH=/home/yourname/CGAL-4.12/lib` (path to CGAL-directory)
    * run `./wrap_2_3`
    * maybe need to create folder "output" in same folder as repository, some of the output might be stored there

## How to compile the software

* follow the steps of "How to run" first
* install qt5: `sudo apt-get install qt5-default qtscript5-dev`
* go into folder "compile"
* delete "CMakeCache.txt", folder "CMakeFiles", everything with "moc", only leave one important file: "CMakeLists.txt"
* `cmake -DCGAL_DIR="../CGAL-4.12" .`
* `make`
* If you do not need the interactive version or do not have QT5 installed:
    * in "CMakeLists.txt": uncomment the line `set(USE_QT OFF)`
    * in "../src/basics.h": uncomment the line `#define NOQT5`
* go up one directory
* run `./wrap_2_3`

If it does not work and you get error messages with "automoc", "vtable": install older version of CMake
* if already installed: `sudo apt-get remove cmake`
* install cmake-3.5.1
    * download from https://cmake.org/files/v3.5/: "cmake-3.5.1-Linux-x86_64.sh"
    * open containing folder, terminal, `sudo sh ./cmake-3.5.1-Linux-x86_64.sh --prefix=/usr` , press enter many times until questions `y`, `n`
    * create link `sudo ln -s /usr/bin/cmake /usr/local/bin/cmake`
    * check `cmake --version`, should give version 3.5.1


## How to set up virtual machine

* install Virtual Box: https://www.virtualbox.org/wiki/Downloads (6.0.14)
* download Ubuntu from http://releases.ubuntu.com/18.04/: ubuntu-18.04.3-desktop-amd64.iso-file
* create Virtual machine: Ubuntu 64-bit, RAM ("Memory size") 2GB, create a virtual hard disk now: File size not too small (>10GB), VDI dynamically allocated
* install Ubuntu on Virtual machine: start, select iso-file, install, minimal, do not download updates, erase disk and install (no worries if you use virtual machine!), wait, restart
* (guest additions for virtual box (for convenience, not necessary): Devices->Insert Guest Additions CD image ...)
    * (create Shared Folder: Devices->Shared Folder, choose folder with project files or your home folder, make permanent, enable auto-mount)
    * (Devices->Drag and Drop->Bidirectional, Devices->Shared Clipboard->Bidirectional, restart)


last update: 12.05.2021
