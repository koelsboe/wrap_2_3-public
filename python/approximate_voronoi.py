import math
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib as mpl
import csv
import os

filename="fisher2_2_20"
filename="sphere_20"
filename="paper_sphere_1000"
filename="thesis/uniform_points2_2_100"
domain="Simplex"
domain="Square"
discretization=1000

#input_file = ("../data_export/bregman_examples/"+filename+".txt")
input_file = ("../output/bregman_paper/approximate_voronoi/"+filename+".txt")
points = np.loadtxt(input_file, delimiter = ' ', skiprows=1)

def dist2(point1,point2,typ="Euclidean",domain="Square"):
    point1 = np.array(point1)
    point2 = np.array(point2)
    if typ == "Euclidean":
        return np.sum((point1-point2)*(point1-point2))
    elif typ == "KL_primal":
        return np.sum((point1*np.log(point1)-point1*np.log(point2)-point1+point2))
    elif typ == "KL_dual":
        return np.sum((point2*np.log(point2)-point2*np.log(point1)-point2+point1))
    elif typ == "Fisher":
        #transform x->sqrt(2x)
        point1 = np.sqrt(2.*point1)
        point2 = np.sqrt(2.*point2)
        if domain=="Square":
            return np.sum((point1-point2)*(point1-point2))
        else: #standard simplex
            return 2.*(math.acos(np.sum(point1*point2)/2.))^2;
    elif typ == "KLweightedEuclidean_primal":
        weight1 = 0.5*np.sum(point1*point1)-(np.sum(point1*np.log(point1)-point1))
        return np.sum((point1-point2)*(point1-point2))-weight1
    else:
        return -1
        
def simplex2plane(x,y,z):
    return np.array([y+0.5*z,math.sqrt(3)/2.*z])
def plane2simplex(u,v):
    z = 2./math.sqrt(3)*v
    y = u-v/math.sqrt(3)
    eps=1e-12
    return np.array([max(eps,1-y-z),max(eps,y),max(eps,z)])
    
def approximate_voronoi(points,discretization,typ="Euclidean",domain="Square",outputfile=""):
    if domain=="Square":
        pixel_borders = np.linspace(0,2,discretization+1)
        pixel_centers_x = np.array((pixel_borders[1:]+pixel_borders[:-1])/2.)
        pixel_centers_y = pixel_centers_x
    else:
        plane_points = np.array([simplex2plane(point[0],point[1],point[2]) for point in points])
        pixel_borders_x = np.linspace(0,1,discretization+1)
        pixel_centers_x = np.array((pixel_borders_x[1:]+pixel_borders_x[:-1])/2.)
        pixel_borders_y = np.linspace(0,math.sqrt(3)/2.,int(discretization*math.sqrt(3)/2.)+1)
        pixel_centers_y =  np.array((pixel_borders_y[1:]+pixel_borders_y[:-1])/2.)
    voronoi_labels = np.zeros((len(pixel_centers_x),len(pixel_centers_y)))
    border_values = {}
    border_tolerance = 0.05
    for xi,x in enumerate(pixel_centers_x):
        for yi,y in enumerate(pixel_centers_y):
            if domain=="Square" or (y<=math.sqrt(3)*x and y<=math.sqrt(3)*(1.-x)):
                best_dist = -1.
                best_labels = []
                borderpoint = False
                bordervalue = -1
                
                for i,point in enumerate(points):
                    if domain=="Square":
                        dist = dist2(point,[x,y],typ)
                    else:
                        dist = dist2(point,plane2simplex(x,y),typ)
                    if(abs(dist-best_dist)<best_dist*border_tolerance):
                        borderpoint = True
                        bordervalue = (dist+best_dist)/2.
                        best_labels.append(i)
                    if(dist<best_dist or best_dist<0):
                        best_dist = dist
                        best_labels = [i]
                        borderpoint = False
                voronoi_labels[xi,yi]=best_labels[0]
                if borderpoint:
                    best_labels.sort()
                    key = " ".join(map(str, best_labels))
                    if key in border_values:
                        border_values[key]=border_values[key]+[bordervalue]
                    else:
                        border_values[key]=[bordervalue]
    if outputfile!="":
        f = open(outputfile+"_labels.txt",'w') 
        for line in voronoi_labels:
            f.write(", ".join(map(str,map(int,(line)))))
            f.write("\n")
        f2 = open(outputfile+"_bordervalues.txt",'w') 
        for key in border_values:
            f2.write(key+", ")
            f2.write(str(min(border_values[key])))
            f2.write(", ")
            f2.write(str(max(border_values[key])))
            f2.write("\n")        

for typ in ["Euclidean","KL_primal","KL_dual","Fisher"]:                   
    approximate_voronoi(points,discretization,typ,domain,"../output/bregman_paper/approximate_voronoi/"+filename+"_"+str(discretization)+"_"+typ)