import math
import numpy as np 
import matplotlib.pyplot as plt
import csv
import os

def count_inversions(filename1, filename2):
    datafile = open(filename1, 'r')
    datareader = csv.reader(datafile, delimiter = ',')
    next(datareader) #skiprow
    simplices1 = []
    
    for row in datareader:
        simplices1.append((float(row[1]),row[3]))
        
    datafile = open(filename2, 'r')
    datareader = csv.reader(datafile, delimiter = ',')
    next(datareader) #skiprow
    simplices2 = []
    for row in datareader:
        simplices2.append((float(row[1]),row[3]))
        
    simplices1.sort()
    simplices2.sort()
    ordered_pairs_of_simplices1 = []
    reverse_ordered_pairs_of_simplices2 = []
    
    for i in range(len(simplices1)):
        for j in range(len(simplices1)-i-1):
            s1 = simplices1[i]
            s2 = simplices1[j+i+1]
            if(s1[0]!=s2[0]):
                ordered_pairs_of_simplices1.append((s1[1],s2[1]))
    for i in range(len(simplices2)):
        for j in range(len(simplices2)-i-1):
            s1 = simplices2[i]
            s2 = simplices2[j+i+1]
            if(s1[0]!=s2[0]):
                reverse_ordered_pairs_of_simplices2.append((s2[1],s1[1]))
    
    ordered_pairs_of_simplices1 = set(ordered_pairs_of_simplices1)
    reverse_ordered_pairs_of_simplices2 = set(reverse_ordered_pairs_of_simplices2)
    inversions = ordered_pairs_of_simplices1.intersection(reverse_ordered_pairs_of_simplices2)
    num_pairs = (len(simplices1)*(len(simplices1)-1))/2.
    print len(inversions), num_pairs, len(inversions)/num_pairs
    return len(inversions), num_pairs, len(inversions)/num_pairs

example = "square" #8459354 17775703.0 0.475894202328
#example = "simplex" #8308791 17787630.0 0.467110626879
count_inversions("../output/bregman_paper/paper/"+example+"_simplices/simplices_KL_primal.txt","../output/bregman_paper/paper/"+example+"_simplices/simplices_KLweightedEuclidean_primal.txt")    