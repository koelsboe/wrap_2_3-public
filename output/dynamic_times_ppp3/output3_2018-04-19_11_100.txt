POISSON POINT PROCESS
# trials, dim, window size,     lambda, periodic
     100,   3,          11,          1,        0

TRIAL, 0
TIME FOR NEWLY COMPUTING COMPLEXES, 0.753476
Elapsed time: 0.788191
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.10504
TIME FOR NEWLY COMPUTING COMPLEXES, 0.779844
Elapsed time: 0.811555
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.063524
TIME FOR NEWLY COMPUTING COMPLEXES, 0.841517
Elapsed time: 0.875457

TRIAL, 1
TIME FOR NEWLY COMPUTING COMPLEXES, 0.73158
Elapsed time: 0.765496
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.136623
TIME FOR NEWLY COMPUTING COMPLEXES, 0.78734
Elapsed time: 0.820786
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.142465
TIME FOR NEWLY COMPUTING COMPLEXES, 0.754396
Elapsed time: 0.783709

TRIAL, 2
TIME FOR NEWLY COMPUTING COMPLEXES, 0.700938
Elapsed time: 0.735939
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.130932
TIME FOR NEWLY COMPUTING COMPLEXES, 0.809187
Elapsed time: 0.845561
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.187914
TIME FOR NEWLY COMPUTING COMPLEXES, 0.792591
Elapsed time: 0.819835

TRIAL, 3
TIME FOR NEWLY COMPUTING COMPLEXES, 0.791202
Elapsed time: 0.826226
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.094712
TIME FOR NEWLY COMPUTING COMPLEXES, 0.711995
Elapsed time: 0.738842
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.168897
TIME FOR NEWLY COMPUTING COMPLEXES, 0.741828
Elapsed time: 0.782377

TRIAL, 4
TIME FOR NEWLY COMPUTING COMPLEXES, 0.760421
Elapsed time: 0.798114
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.27232
TIME FOR NEWLY COMPUTING COMPLEXES, 0.754208
Elapsed time: 0.789395
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.189953
TIME FOR NEWLY COMPUTING COMPLEXES, 0.739219
Elapsed time: 0.770269

TRIAL, 5
TIME FOR NEWLY COMPUTING COMPLEXES, 0.818366
Elapsed time: 0.852996
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.179769
TIME FOR NEWLY COMPUTING COMPLEXES, 0.764301
Elapsed time: 0.801736
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.096639
TIME FOR NEWLY COMPUTING COMPLEXES, 0.906387
Elapsed time: 0.941497

TRIAL, 6
TIME FOR NEWLY COMPUTING COMPLEXES, 0.760302
Elapsed time: 0.798659
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.268319
TIME FOR NEWLY COMPUTING COMPLEXES, 0.779772
Elapsed time: 0.820324
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.243081
TIME FOR NEWLY COMPUTING COMPLEXES, 0.846888
Elapsed time: 0.8911

TRIAL, 7
TIME FOR NEWLY COMPUTING COMPLEXES, 0.94316
Elapsed time: 0.995821
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.224517
TIME FOR NEWLY COMPUTING COMPLEXES, 0.854738
Elapsed time: 0.894319
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.122047
TIME FOR NEWLY COMPUTING COMPLEXES, 0.794366
Elapsed time: 0.823854

TRIAL, 8
TIME FOR NEWLY COMPUTING COMPLEXES, 0.813363
Elapsed time: 0.853997
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.234189
TIME FOR NEWLY COMPUTING COMPLEXES, 0.711046
Elapsed time: 0.746738
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.189884
TIME FOR NEWLY COMPUTING COMPLEXES, 0.767112
Elapsed time: 0.813761

TRIAL, 9
TIME FOR NEWLY COMPUTING COMPLEXES, 0.832112
Elapsed time: 0.864983
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.332152
TIME FOR NEWLY COMPUTING COMPLEXES, 0.793901
Elapsed time: 0.837872
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.182819
TIME FOR NEWLY COMPUTING COMPLEXES, 0.770564
Elapsed time: 0.805842

TRIAL, 10
TIME FOR NEWLY COMPUTING COMPLEXES, 0.768921
Elapsed time: 0.799364
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.170888
TIME FOR NEWLY COMPUTING COMPLEXES, 0.744146
Elapsed time: 0.777052
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.211285
TIME FOR NEWLY COMPUTING COMPLEXES, 0.815491
Elapsed time: 0.847235

TRIAL, 11
TIME FOR NEWLY COMPUTING COMPLEXES, 0.953437
Elapsed time: 0.990468
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.321021
TIME FOR NEWLY COMPUTING COMPLEXES, 0.768473
Elapsed time: 0.801415
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.173021
TIME FOR NEWLY COMPUTING COMPLEXES, 0.819351
Elapsed time: 0.856789

TRIAL, 12
TIME FOR NEWLY COMPUTING COMPLEXES, 0.807036
Elapsed time: 0.845577
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.197653
TIME FOR NEWLY COMPUTING COMPLEXES, 0.838866
Elapsed time: 0.871331
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.476633
TIME FOR NEWLY COMPUTING COMPLEXES, 0.812256
Elapsed time: 0.84794

TRIAL, 13
TIME FOR NEWLY COMPUTING COMPLEXES, 0.822472
Elapsed time: 0.856934
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.094978
TIME FOR NEWLY COMPUTING COMPLEXES, 0.80824
Elapsed time: 0.846796
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.11142
TIME FOR NEWLY COMPUTING COMPLEXES, 0.859849
Elapsed time: 0.904462

TRIAL, 14
TIME FOR NEWLY COMPUTING COMPLEXES, 0.795303
Elapsed time: 0.82705
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.239922
TIME FOR NEWLY COMPUTING COMPLEXES, 1.05701
Elapsed time: 1.09501
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.294892
TIME FOR NEWLY COMPUTING COMPLEXES, 0.888327
Elapsed time: 0.923494

TRIAL, 15
TIME FOR NEWLY COMPUTING COMPLEXES, 0.851546
Elapsed time: 0.895547
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.267881
TIME FOR NEWLY COMPUTING COMPLEXES, 0.90785
Elapsed time: 0.955578
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.127043
TIME FOR NEWLY COMPUTING COMPLEXES, 0.801749
Elapsed time: 0.836799

TRIAL, 16
TIME FOR NEWLY COMPUTING COMPLEXES, 0.901564
Elapsed time: 0.934364
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.1269
TIME FOR NEWLY COMPUTING COMPLEXES, 0.873528
Elapsed time: 0.916459
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.32088
TIME FOR NEWLY COMPUTING COMPLEXES, 0.895122
Elapsed time: 0.92956

TRIAL, 17
TIME FOR NEWLY COMPUTING COMPLEXES, 0.782334
Elapsed time: 0.823246
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.230953
TIME FOR NEWLY COMPUTING COMPLEXES, 0.8681
Elapsed time: 0.904562
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.247396
TIME FOR NEWLY COMPUTING COMPLEXES, 0.851614
Elapsed time: 0.896397

TRIAL, 18
TIME FOR NEWLY COMPUTING COMPLEXES, 1.19133
Elapsed time: 1.24152
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.248158
TIME FOR NEWLY COMPUTING COMPLEXES, 0.802679
Elapsed time: 0.834246
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.082318
TIME FOR NEWLY COMPUTING COMPLEXES, 1.01663
Elapsed time: 1.08746

TRIAL, 19
TIME FOR NEWLY COMPUTING COMPLEXES, 1.11194
Elapsed time: 1.16086
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.305689
TIME FOR NEWLY COMPUTING COMPLEXES, 0.949846
Elapsed time: 0.983899
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.270891
TIME FOR NEWLY COMPUTING COMPLEXES, 0.760968
Elapsed time: 0.79685

TRIAL, 20
TIME FOR NEWLY COMPUTING COMPLEXES, 1.04837
Elapsed time: 1.07945
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.135642
TIME FOR NEWLY COMPUTING COMPLEXES, 0.865054
Elapsed time: 0.907105
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.072687
TIME FOR NEWLY COMPUTING COMPLEXES, 0.867411
Elapsed time: 0.914415

TRIAL, 21
TIME FOR NEWLY COMPUTING COMPLEXES, 0.794662
Elapsed time: 0.835394
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.169854
TIME FOR NEWLY COMPUTING COMPLEXES, 0.851041
Elapsed time: 0.889563
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.174324
TIME FOR NEWLY COMPUTING COMPLEXES, 0.882039
Elapsed time: 0.929824

TRIAL, 22
TIME FOR NEWLY COMPUTING COMPLEXES, 0.859235
Elapsed time: 0.906649
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.213429
TIME FOR NEWLY COMPUTING COMPLEXES, 0.937214
Elapsed time: 0.978164
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.199051
TIME FOR NEWLY COMPUTING COMPLEXES, 0.934071
Elapsed time: 0.982937

TRIAL, 23
TIME FOR NEWLY COMPUTING COMPLEXES, 0.946103
Elapsed time: 0.988216
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.317318
TIME FOR NEWLY COMPUTING COMPLEXES, 0.740316
Elapsed time: 0.779834
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.190139
TIME FOR NEWLY COMPUTING COMPLEXES, 0.959737
Elapsed time: 1.00384

TRIAL, 24
TIME FOR NEWLY COMPUTING COMPLEXES, 0.791253
Elapsed time: 0.831142
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.19943
TIME FOR NEWLY COMPUTING COMPLEXES, 0.798065
Elapsed time: 0.830674
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.165302
TIME FOR NEWLY COMPUTING COMPLEXES, 0.757982
Elapsed time: 0.789224

TRIAL, 25
TIME FOR NEWLY COMPUTING COMPLEXES, 0.740932
Elapsed time: 0.780141
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.134939
TIME FOR NEWLY COMPUTING COMPLEXES, 0.801916
Elapsed time: 0.848363
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.177766
TIME FOR NEWLY COMPUTING COMPLEXES, 0.779777
Elapsed time: 0.816329

TRIAL, 26
TIME FOR NEWLY COMPUTING COMPLEXES, 0.718276
Elapsed time: 0.756738
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.098208
TIME FOR NEWLY COMPUTING COMPLEXES, 0.783734
Elapsed time: 0.817591
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.172308
TIME FOR NEWLY COMPUTING COMPLEXES, 0.813826
Elapsed time: 0.866425

TRIAL, 27
TIME FOR NEWLY COMPUTING COMPLEXES, 0.750454
Elapsed time: 0.793435
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.181246
TIME FOR NEWLY COMPUTING COMPLEXES, 0.732933
Elapsed time: 0.772407
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.099324
TIME FOR NEWLY COMPUTING COMPLEXES, 0.829994
Elapsed time: 0.880197

TRIAL, 28
TIME FOR NEWLY COMPUTING COMPLEXES, 0.739691
Elapsed time: 0.780203
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.123157
TIME FOR NEWLY COMPUTING COMPLEXES, 0.806957
Elapsed time: 0.846388
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.094854
TIME FOR NEWLY COMPUTING COMPLEXES, 0.746382
Elapsed time: 0.780954

TRIAL, 29
TIME FOR NEWLY COMPUTING COMPLEXES, 0.82501
Elapsed time: 0.861786
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.112438
TIME FOR NEWLY COMPUTING COMPLEXES, 0.761866
Elapsed time: 0.795376
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.108834
TIME FOR NEWLY COMPUTING COMPLEXES, 0.801651
Elapsed time: 0.836143

TRIAL, 30
TIME FOR NEWLY COMPUTING COMPLEXES, 0.719199
Elapsed time: 0.75742
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.102754
TIME FOR NEWLY COMPUTING COMPLEXES, 0.724175
Elapsed time: 0.765031
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.084876
TIME FOR NEWLY COMPUTING COMPLEXES, 0.768558
Elapsed time: 0.810838

TRIAL, 31
TIME FOR NEWLY COMPUTING COMPLEXES, 0.843238
Elapsed time: 0.892734
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.140461
TIME FOR NEWLY COMPUTING COMPLEXES, 0.761244
Elapsed time: 0.803385
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.224441
TIME FOR NEWLY COMPUTING COMPLEXES, 0.756909
Elapsed time: 0.796908

TRIAL, 32
TIME FOR NEWLY COMPUTING COMPLEXES, 0.768481
Elapsed time: 0.80646
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.191278
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755626
Elapsed time: 0.795467
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.108063
TIME FOR NEWLY COMPUTING COMPLEXES, 0.787841
Elapsed time: 0.826707

TRIAL, 33
TIME FOR NEWLY COMPUTING COMPLEXES, 0.770328
Elapsed time: 0.809962
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.072091
TIME FOR NEWLY COMPUTING COMPLEXES, 0.775609
Elapsed time: 0.814428
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.112999
TIME FOR NEWLY COMPUTING COMPLEXES, 0.864253
Elapsed time: 0.900831

TRIAL, 34
TIME FOR NEWLY COMPUTING COMPLEXES, 0.894254
Elapsed time: 0.936284
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.185418
TIME FOR NEWLY COMPUTING COMPLEXES, 0.851776
Elapsed time: 0.889533
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.193461
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755916
Elapsed time: 0.79106

TRIAL, 35
TIME FOR NEWLY COMPUTING COMPLEXES, 0.729948
Elapsed time: 0.7707
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.230159
TIME FOR NEWLY COMPUTING COMPLEXES, 0.877929
Elapsed time: 0.917125
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.114118
TIME FOR NEWLY COMPUTING COMPLEXES, 0.753761
Elapsed time: 0.791854

TRIAL, 36
TIME FOR NEWLY COMPUTING COMPLEXES, 0.785456
Elapsed time: 0.822727
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.16312
TIME FOR NEWLY COMPUTING COMPLEXES, 0.74491
Elapsed time: 0.778526
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.187392
TIME FOR NEWLY COMPUTING COMPLEXES, 0.824991
Elapsed time: 0.863095

TRIAL, 37
TIME FOR NEWLY COMPUTING COMPLEXES, 0.777187
Elapsed time: 0.812631
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.239927
TIME FOR NEWLY COMPUTING COMPLEXES, 0.79097
Elapsed time: 0.82799
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.102939
TIME FOR NEWLY COMPUTING COMPLEXES, 0.778928
Elapsed time: 0.821039

TRIAL, 38
TIME FOR NEWLY COMPUTING COMPLEXES, 0.74279
Elapsed time: 0.780219
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.214585
TIME FOR NEWLY COMPUTING COMPLEXES, 0.729192
Elapsed time: 0.766211
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.397968
TIME FOR NEWLY COMPUTING COMPLEXES, 0.75454
Elapsed time: 0.788361

TRIAL, 39
TIME FOR NEWLY COMPUTING COMPLEXES, 0.744841
Elapsed time: 0.782816
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.204183
TIME FOR NEWLY COMPUTING COMPLEXES, 0.727389
Elapsed time: 0.768644
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.152903
TIME FOR NEWLY COMPUTING COMPLEXES, 0.739825
Elapsed time: 0.777141

TRIAL, 40
TIME FOR NEWLY COMPUTING COMPLEXES, 0.770941
Elapsed time: 0.816273
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.035891
TIME FOR NEWLY COMPUTING COMPLEXES, 0.758619
Elapsed time: 0.793928
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.182224
TIME FOR NEWLY COMPUTING COMPLEXES, 0.808186
Elapsed time: 0.847476

TRIAL, 41
TIME FOR NEWLY COMPUTING COMPLEXES, 0.804767
Elapsed time: 0.839846
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.110009
TIME FOR NEWLY COMPUTING COMPLEXES, 0.75536
Elapsed time: 0.79942
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.25373
TIME FOR NEWLY COMPUTING COMPLEXES, 0.761682
Elapsed time: 0.798961

TRIAL, 42
TIME FOR NEWLY COMPUTING COMPLEXES, 0.749168
Elapsed time: 0.784103
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.257343
TIME FOR NEWLY COMPUTING COMPLEXES, 0.789311
Elapsed time: 0.828406
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.128531
TIME FOR NEWLY COMPUTING COMPLEXES, 0.765472
Elapsed time: 0.808416

TRIAL, 43
TIME FOR NEWLY COMPUTING COMPLEXES, 0.784943
Elapsed time: 0.825732
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.12238
TIME FOR NEWLY COMPUTING COMPLEXES, 0.784959
Elapsed time: 0.826531
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.119972
TIME FOR NEWLY COMPUTING COMPLEXES, 0.763745
Elapsed time: 0.813857

TRIAL, 44
TIME FOR NEWLY COMPUTING COMPLEXES, 0.708757
Elapsed time: 0.748248
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.137119
TIME FOR NEWLY COMPUTING COMPLEXES, 0.697761
Elapsed time: 0.735539
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.113596
TIME FOR NEWLY COMPUTING COMPLEXES, 0.709574
Elapsed time: 0.745876

TRIAL, 45
TIME FOR NEWLY COMPUTING COMPLEXES, 0.742476
Elapsed time: 0.779101
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.146107
TIME FOR NEWLY COMPUTING COMPLEXES, 0.730144
Elapsed time: 0.767027
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.162904
TIME FOR NEWLY COMPUTING COMPLEXES, 0.723072
Elapsed time: 0.760097

TRIAL, 46
TIME FOR NEWLY COMPUTING COMPLEXES, 0.727119
Elapsed time: 0.762215
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.154639
TIME FOR NEWLY COMPUTING COMPLEXES, 0.793943
Elapsed time: 0.833352
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.225604
TIME FOR NEWLY COMPUTING COMPLEXES, 0.749647
Elapsed time: 0.790965

TRIAL, 47
TIME FOR NEWLY COMPUTING COMPLEXES, 0.733838
Elapsed time: 0.768134
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.161012
TIME FOR NEWLY COMPUTING COMPLEXES, 0.769855
Elapsed time: 0.80854
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.079965
TIME FOR NEWLY COMPUTING COMPLEXES, 0.750014
Elapsed time: 0.789636

TRIAL, 48
TIME FOR NEWLY COMPUTING COMPLEXES, 0.774443
Elapsed time: 0.812398
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.117277
TIME FOR NEWLY COMPUTING COMPLEXES, 0.78654
Elapsed time: 0.826172
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.282569
TIME FOR NEWLY COMPUTING COMPLEXES, 0.757915
Elapsed time: 0.79767

TRIAL, 49
TIME FOR NEWLY COMPUTING COMPLEXES, 0.724674
Elapsed time: 0.766243
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.102159
TIME FOR NEWLY COMPUTING COMPLEXES, 0.69542
Elapsed time: 0.730405
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.279818
TIME FOR NEWLY COMPUTING COMPLEXES, 0.726199
Elapsed time: 0.760559

TRIAL, 50
TIME FOR NEWLY COMPUTING COMPLEXES, 0.78068
Elapsed time: 0.822532
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.172473
TIME FOR NEWLY COMPUTING COMPLEXES, 0.759614
Elapsed time: 0.797722
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.235655
TIME FOR NEWLY COMPUTING COMPLEXES, 0.817784
Elapsed time: 0.85711

TRIAL, 51
TIME FOR NEWLY COMPUTING COMPLEXES, 0.87247
Elapsed time: 0.912822
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.252472
TIME FOR NEWLY COMPUTING COMPLEXES, 0.836854
Elapsed time: 0.879909
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.133748
TIME FOR NEWLY COMPUTING COMPLEXES, 0.83594
Elapsed time: 0.875602

TRIAL, 52
TIME FOR NEWLY COMPUTING COMPLEXES, 0.85679
Elapsed time: 0.914671
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.352248
TIME FOR NEWLY COMPUTING COMPLEXES, 0.941362
Elapsed time: 0.983867
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.233923
TIME FOR NEWLY COMPUTING COMPLEXES, 0.91453
Elapsed time: 0.95224

TRIAL, 53
TIME FOR NEWLY COMPUTING COMPLEXES, 0.777778
Elapsed time: 0.819794
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.145229
TIME FOR NEWLY COMPUTING COMPLEXES, 1.03525
Elapsed time: 1.08001
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.265646
TIME FOR NEWLY COMPUTING COMPLEXES, 1.18468
Elapsed time: 1.23482

TRIAL, 54
TIME FOR NEWLY COMPUTING COMPLEXES, 1.21045
Elapsed time: 1.25967
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.147216
TIME FOR NEWLY COMPUTING COMPLEXES, 1.4913
Elapsed time: 1.56416
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.286363
TIME FOR NEWLY COMPUTING COMPLEXES, 1.83612
Elapsed time: 1.90133

TRIAL, 55
TIME FOR NEWLY COMPUTING COMPLEXES, 1.47908
Elapsed time: 1.52282
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.3683
TIME FOR NEWLY COMPUTING COMPLEXES, 0.827583
Elapsed time: 0.868296
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.235558
TIME FOR NEWLY COMPUTING COMPLEXES, 0.921284
Elapsed time: 0.963988

TRIAL, 56
TIME FOR NEWLY COMPUTING COMPLEXES, 0.731773
Elapsed time: 0.772726
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.199403
TIME FOR NEWLY COMPUTING COMPLEXES, 0.821345
Elapsed time: 0.86394
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.278894
TIME FOR NEWLY COMPUTING COMPLEXES, 0.729078
Elapsed time: 0.777833

TRIAL, 57
TIME FOR NEWLY COMPUTING COMPLEXES, 0.751749
Elapsed time: 0.792822
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.188636
TIME FOR NEWLY COMPUTING COMPLEXES, 0.801544
Elapsed time: 0.843673
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.227587
TIME FOR NEWLY COMPUTING COMPLEXES, 0.777884
Elapsed time: 0.817276

TRIAL, 58
TIME FOR NEWLY COMPUTING COMPLEXES, 0.744042
Elapsed time: 0.787984
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.1594
TIME FOR NEWLY COMPUTING COMPLEXES, 0.774031
Elapsed time: 0.81204
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.231515
TIME FOR NEWLY COMPUTING COMPLEXES, 0.825465
Elapsed time: 0.888547

TRIAL, 59
TIME FOR NEWLY COMPUTING COMPLEXES, 0.807136
Elapsed time: 0.847057
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.348871
TIME FOR NEWLY COMPUTING COMPLEXES, 0.831212
Elapsed time: 0.878078
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.194836
TIME FOR NEWLY COMPUTING COMPLEXES, 0.765416
Elapsed time: 0.807058

TRIAL, 60
TIME FOR NEWLY COMPUTING COMPLEXES, 0.801887
Elapsed time: 0.843759
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.068593
TIME FOR NEWLY COMPUTING COMPLEXES, 0.843575
Elapsed time: 0.892527
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.148551
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755057
Elapsed time: 0.795317

TRIAL, 61
TIME FOR NEWLY COMPUTING COMPLEXES, 0.796747
Elapsed time: 0.834326
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.11813
TIME FOR NEWLY COMPUTING COMPLEXES, 0.774228
Elapsed time: 0.814816
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.150285
TIME FOR NEWLY COMPUTING COMPLEXES, 0.851636
Elapsed time: 0.891149

TRIAL, 62
TIME FOR NEWLY COMPUTING COMPLEXES, 0.850841
Elapsed time: 0.897154
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.215304
TIME FOR NEWLY COMPUTING COMPLEXES, 0.760421
Elapsed time: 0.803193
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.27389
TIME FOR NEWLY COMPUTING COMPLEXES, 0.761605
Elapsed time: 0.798527

TRIAL, 63
TIME FOR NEWLY COMPUTING COMPLEXES, 0.832744
Elapsed time: 0.867495
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.256725
TIME FOR NEWLY COMPUTING COMPLEXES, 0.769683
Elapsed time: 0.805327
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.164236
TIME FOR NEWLY COMPUTING COMPLEXES, 0.908556
Elapsed time: 0.951957

TRIAL, 64
TIME FOR NEWLY COMPUTING COMPLEXES, 0.860776
Elapsed time: 0.90821
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.396587
TIME FOR NEWLY COMPUTING COMPLEXES, 0.866243
Elapsed time: 0.907962
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.126621
TIME FOR NEWLY COMPUTING COMPLEXES, 0.792821
Elapsed time: 0.841801

TRIAL, 65
TIME FOR NEWLY COMPUTING COMPLEXES, 0.851106
Elapsed time: 0.896294
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.21688
TIME FOR NEWLY COMPUTING COMPLEXES, 0.827575
Elapsed time: 0.882312
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.119844
TIME FOR NEWLY COMPUTING COMPLEXES, 0.758787
Elapsed time: 0.802656

TRIAL, 66
TIME FOR NEWLY COMPUTING COMPLEXES, 0.76381
Elapsed time: 0.804919
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.164761
TIME FOR NEWLY COMPUTING COMPLEXES, 0.750645
Elapsed time: 0.787941
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.118521
TIME FOR NEWLY COMPUTING COMPLEXES, 0.793639
Elapsed time: 0.840667

TRIAL, 67
TIME FOR NEWLY COMPUTING COMPLEXES, 0.88452
Elapsed time: 0.929916
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.145074
TIME FOR NEWLY COMPUTING COMPLEXES, 0.776525
Elapsed time: 0.821966
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.180616
TIME FOR NEWLY COMPUTING COMPLEXES, 0.807492
Elapsed time: 0.849879

TRIAL, 68
TIME FOR NEWLY COMPUTING COMPLEXES, 0.83419
Elapsed time: 0.879759
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.193571
TIME FOR NEWLY COMPUTING COMPLEXES, 0.798683
Elapsed time: 0.83763
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.295042
TIME FOR NEWLY COMPUTING COMPLEXES, 0.791722
Elapsed time: 0.833371

TRIAL, 69
TIME FOR NEWLY COMPUTING COMPLEXES, 0.782976
Elapsed time: 0.828245
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.184385
TIME FOR NEWLY COMPUTING COMPLEXES, 0.732522
Elapsed time: 0.775409
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.17447
TIME FOR NEWLY COMPUTING COMPLEXES, 0.713732
Elapsed time: 0.751842

TRIAL, 70
TIME FOR NEWLY COMPUTING COMPLEXES, 0.765597
Elapsed time: 0.801957
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.189134
TIME FOR NEWLY COMPUTING COMPLEXES, 0.813009
Elapsed time: 0.851805
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.258581
TIME FOR NEWLY COMPUTING COMPLEXES, 0.7934
Elapsed time: 0.832897

TRIAL, 71
TIME FOR NEWLY COMPUTING COMPLEXES, 0.837838
Elapsed time: 0.880635
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.088556
TIME FOR NEWLY COMPUTING COMPLEXES, 0.915188
Elapsed time: 0.966007
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.177618
TIME FOR NEWLY COMPUTING COMPLEXES, 0.811404
Elapsed time: 0.848884

TRIAL, 72
TIME FOR NEWLY COMPUTING COMPLEXES, 0.927219
Elapsed time: 0.971892
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.250702
TIME FOR NEWLY COMPUTING COMPLEXES, 0.906482
Elapsed time: 0.950537
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.175655
TIME FOR NEWLY COMPUTING COMPLEXES, 0.797685
Elapsed time: 0.842398

TRIAL, 73
TIME FOR NEWLY COMPUTING COMPLEXES, 0.777416
Elapsed time: 0.815194
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.133257
TIME FOR NEWLY COMPUTING COMPLEXES, 0.791938
Elapsed time: 0.840125
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.216897
TIME FOR NEWLY COMPUTING COMPLEXES, 0.806711
Elapsed time: 0.842998

TRIAL, 74
TIME FOR NEWLY COMPUTING COMPLEXES, 0.965164
Elapsed time: 1.0096
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.198928
TIME FOR NEWLY COMPUTING COMPLEXES, 0.856312
Elapsed time: 0.899835
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.261901
TIME FOR NEWLY COMPUTING COMPLEXES, 0.814062
Elapsed time: 0.854283

TRIAL, 75
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755084
Elapsed time: 0.793784
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.116877
TIME FOR NEWLY COMPUTING COMPLEXES, 0.817447
Elapsed time: 0.852247
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.174398
TIME FOR NEWLY COMPUTING COMPLEXES, 0.780112
Elapsed time: 0.822697

TRIAL, 76
TIME FOR NEWLY COMPUTING COMPLEXES, 0.877688
Elapsed time: 0.924066
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.191014
TIME FOR NEWLY COMPUTING COMPLEXES, 0.864419
Elapsed time: 0.915744
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.298572
TIME FOR NEWLY COMPUTING COMPLEXES, 0.837573
Elapsed time: 0.882738

TRIAL, 77
TIME FOR NEWLY COMPUTING COMPLEXES, 0.80674
Elapsed time: 0.845412
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.083915
TIME FOR NEWLY COMPUTING COMPLEXES, 0.907743
Elapsed time: 0.956386
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.149729
TIME FOR NEWLY COMPUTING COMPLEXES, 0.810952
Elapsed time: 0.852737

TRIAL, 78
TIME FOR NEWLY COMPUTING COMPLEXES, 0.812521
Elapsed time: 0.852675
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.096162
TIME FOR NEWLY COMPUTING COMPLEXES, 0.809047
Elapsed time: 0.853375
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.253748
TIME FOR NEWLY COMPUTING COMPLEXES, 0.812614
Elapsed time: 0.854163

TRIAL, 79
TIME FOR NEWLY COMPUTING COMPLEXES, 0.787624
Elapsed time: 0.823873
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.115314
TIME FOR NEWLY COMPUTING COMPLEXES, 0.8097
Elapsed time: 0.848463
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.162204
TIME FOR NEWLY COMPUTING COMPLEXES, 0.751809
Elapsed time: 0.788596

TRIAL, 80
TIME FOR NEWLY COMPUTING COMPLEXES, 0.73753
Elapsed time: 0.781671
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.141609
TIME FOR NEWLY COMPUTING COMPLEXES, 0.72777
Elapsed time: 0.770061
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.255605
TIME FOR NEWLY COMPUTING COMPLEXES, 0.820022
Elapsed time: 0.860742

TRIAL, 81
TIME FOR NEWLY COMPUTING COMPLEXES, 0.774629
Elapsed time: 0.812048
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.200848
TIME FOR NEWLY COMPUTING COMPLEXES, 0.898164
Elapsed time: 0.936966
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.203677
TIME FOR NEWLY COMPUTING COMPLEXES, 0.829817
Elapsed time: 0.875294

TRIAL, 82
TIME FOR NEWLY COMPUTING COMPLEXES, 0.769087
Elapsed time: 0.817682
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.230364
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755716
Elapsed time: 0.800377
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.15567
TIME FOR NEWLY COMPUTING COMPLEXES, 0.754918
Elapsed time: 0.796619

TRIAL, 83
TIME FOR NEWLY COMPUTING COMPLEXES, 0.870974
Elapsed time: 0.915342
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.079547
TIME FOR NEWLY COMPUTING COMPLEXES, 0.78424
Elapsed time: 0.831729
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.098509
TIME FOR NEWLY COMPUTING COMPLEXES, 0.91913
Elapsed time: 0.964762

TRIAL, 84
TIME FOR NEWLY COMPUTING COMPLEXES, 0.752115
Elapsed time: 0.795462
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.10781
TIME FOR NEWLY COMPUTING COMPLEXES, 0.799209
Elapsed time: 0.85247
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.15535
TIME FOR NEWLY COMPUTING COMPLEXES, 0.756251
Elapsed time: 0.80003

TRIAL, 85
TIME FOR NEWLY COMPUTING COMPLEXES, 0.770965
Elapsed time: 0.815512
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.286458
TIME FOR NEWLY COMPUTING COMPLEXES, 0.792045
Elapsed time: 0.832373
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.070732
TIME FOR NEWLY COMPUTING COMPLEXES, 0.798707
Elapsed time: 0.839452

TRIAL, 86
TIME FOR NEWLY COMPUTING COMPLEXES, 0.753707
Elapsed time: 0.792863
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.142555
TIME FOR NEWLY COMPUTING COMPLEXES, 0.774147
Elapsed time: 0.818366
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.129367
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755665
Elapsed time: 0.800067

TRIAL, 87
TIME FOR NEWLY COMPUTING COMPLEXES, 0.791953
Elapsed time: 0.840397
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.150701
TIME FOR NEWLY COMPUTING COMPLEXES, 0.793298
Elapsed time: 0.843279
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.250445
TIME FOR NEWLY COMPUTING COMPLEXES, 0.772569
Elapsed time: 0.807563

TRIAL, 88
TIME FOR NEWLY COMPUTING COMPLEXES, 0.755715
Elapsed time: 0.794411
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.166954
TIME FOR NEWLY COMPUTING COMPLEXES, 0.786226
Elapsed time: 0.826164
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.12843
TIME FOR NEWLY COMPUTING COMPLEXES, 0.800635
Elapsed time: 0.843729

TRIAL, 89
TIME FOR NEWLY COMPUTING COMPLEXES, 0.732506
Elapsed time: 0.771974
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.228541
TIME FOR NEWLY COMPUTING COMPLEXES, 0.762821
Elapsed time: 0.803455
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.098366
TIME FOR NEWLY COMPUTING COMPLEXES, 0.748301
Elapsed time: 0.788994

TRIAL, 90
TIME FOR NEWLY COMPUTING COMPLEXES, 0.850068
Elapsed time: 0.891453
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.205925
TIME FOR NEWLY COMPUTING COMPLEXES, 0.82699
Elapsed time: 0.867722
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.227592
TIME FOR NEWLY COMPUTING COMPLEXES, 0.764445
Elapsed time: 0.805444

TRIAL, 91
TIME FOR NEWLY COMPUTING COMPLEXES, 0.746941
Elapsed time: 0.78161
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.184522
TIME FOR NEWLY COMPUTING COMPLEXES, 0.748621
Elapsed time: 0.788202
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.103487
TIME FOR NEWLY COMPUTING COMPLEXES, 0.796423
Elapsed time: 0.83883

TRIAL, 92
TIME FOR NEWLY COMPUTING COMPLEXES, 0.888963
Elapsed time: 0.931552
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.204512
TIME FOR NEWLY COMPUTING COMPLEXES, 0.943879
Elapsed time: 0.989216
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.124031
TIME FOR NEWLY COMPUTING COMPLEXES, 0.891343
Elapsed time: 0.93179

TRIAL, 93
TIME FOR NEWLY COMPUTING COMPLEXES, 0.867504
Elapsed time: 0.910651
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.203436
TIME FOR NEWLY COMPUTING COMPLEXES, 0.852113
Elapsed time: 0.896889
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.181991
TIME FOR NEWLY COMPUTING COMPLEXES, 0.913471
Elapsed time: 0.96034

TRIAL, 94
TIME FOR NEWLY COMPUTING COMPLEXES, 0.840855
Elapsed time: 0.87609
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.102704
TIME FOR NEWLY COMPUTING COMPLEXES, 0.752586
Elapsed time: 0.793297
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.158595
TIME FOR NEWLY COMPUTING COMPLEXES, 0.832789
Elapsed time: 0.875229

TRIAL, 95
TIME FOR NEWLY COMPUTING COMPLEXES, 0.844574
Elapsed time: 0.89085
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.124826
TIME FOR NEWLY COMPUTING COMPLEXES, 0.982726
Elapsed time: 1.02743
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.15807
TIME FOR NEWLY COMPUTING COMPLEXES, 0.782517
Elapsed time: 0.822688

TRIAL, 96
TIME FOR NEWLY COMPUTING COMPLEXES, 0.89817
Elapsed time: 0.947713
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.222662
TIME FOR NEWLY COMPUTING COMPLEXES, 0.850707
Elapsed time: 0.895567
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.171261
TIME FOR NEWLY COMPUTING COMPLEXES, 0.802152
Elapsed time: 0.842365

TRIAL, 97
TIME FOR NEWLY COMPUTING COMPLEXES, 0.785188
Elapsed time: 0.828577
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.221566
TIME FOR NEWLY COMPUTING COMPLEXES, 0.776657
Elapsed time: 0.8133
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.155491
TIME FOR NEWLY COMPUTING COMPLEXES, 0.761397
Elapsed time: 0.804903

TRIAL, 98
TIME FOR NEWLY COMPUTING COMPLEXES, 0.790932
Elapsed time: 0.843092
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.170428
TIME FOR NEWLY COMPUTING COMPLEXES, 0.862956
Elapsed time: 0.899493
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.214201
TIME FOR NEWLY COMPUTING COMPLEXES, 0.810613
Elapsed time: 0.85579

TRIAL, 99
TIME FOR NEWLY COMPUTING COMPLEXES, 0.793586
Elapsed time: 0.832212
ADD RANDOM POINT
TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, 0.09313
TIME FOR NEWLY COMPUTING COMPLEXES, 0.813781
Elapsed time: 0.860157
DELETE POINT 0
TIME FOR UPDATING COMPLEXES FOR POINT DELETION, 0.207506
TIME FOR NEWLY COMPUTING COMPLEXES, 0.821475
Elapsed time: 0.865155
