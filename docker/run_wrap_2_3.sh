#!/bin/bash

volume_source="$(pwd)"
volume_target="/home/Documents"
output_rel_path="output"

mkdir -p "${volume_source}/${output_rel_path}"

cmd="echo 'START WRAP_2_3!'; \
     export LD_LIBRARY_PATH=/home/wrap_2_3-public/CGAL-4.12/lib; \
     cd ${volume_target}/${output_rel_path}; \
"

xhost +local: #enable opening of windows

cmd1="$cmd \
    chmod 744 ../../wrap_2_3-public/wrap_2_3
    ./../../wrap_2_3-public/wrap_2_3; \
    /bin/bash \
  "

docker run \
  -v ${volume_source}:${volume_target} \
  -e DISPLAY=$DISPLAY \
  -e XDG_RUNTIME_DIR=/tmp \
  -v /tmp/.X11-unix/:/tmp/.X11-unix:ro \
  -v /tmp/initfile:/tmp/initfile \
  --device /dev/dri/ \
  -it \
  wrap_2_3 \
  /bin/bash -c "${cmd1}"
