//wrap.h
//author: koelsboe

#ifndef WRAP_H
#define WRAP_H

#include "filtered_complex.h"

//intervals of given (Delaunay) radius function, in the face graph related simplices with same radius are collapsed to a single node
class Interval{
    
    private:
        int index; //index in list of intervals
        int max_dim; //maximal dimension of interval simplices
        std::vector<int> interval_simplices; //list of simplices (indices) of this interval, simplex with maximal dim comes first
        std::set<int> predecessors; //indices of predecessor intervals (intervals which contain a simplex that is a facet of a simplex in this interval)
        bool deleted; //if interval was deleted by point manipulation, should not be considered for anything, will be overwritten when new intervals are added
        
        int first_critical_descendant; //for non-singular interval: critical descendant of smallest radius, non-singular interval has same radius, -1 if not in Wrap complex or singular interval      
        std::set<int> other_critical_descendants; //for non-singular interval: all critical descendants except first, for singular: all critical descendants (without nested)

    public:   
        //constructors
        Interval(){deleted=true;max_dim=-1;index=-1;} //empty
        Interval(Simplex* simplex, int index_){
            index = index_;
            max_dim = simplex->getDim();
            interval_simplices.push_back(simplex->getIndex()); 
            first_critical_descendant=-1;
            deleted=false;
        }
        
        //get basic information
        int getIndex(){return index;}
        bool isSingular(){return (interval_simplices.size())==1;} //singular interval contains exactly one simplex
        bool isDeleted(){return deleted;}
        int getSize(){return interval_simplices.size();}
        std::vector<int> getSimplices(){return interval_simplices;}
        int getMaxDim(){return max_dim;}  
        int getMaxSimplexIndex(){if(interval_simplices.size()>0){return interval_simplices[0];}else{return -1;};}
        Simplex* getMaxSimplex(std::vector<Simplex>* simplices){
            return &(simplices->at(interval_simplices[0]));
        }
        std::set<int> getPredecessors(){return predecessors;}
        int getFirstCriticalDescendant(){return first_critical_descendant;}
        std::set<int>* getOtherCriticalDescendants(){return &other_critical_descendants;}
        //get filtration value of interval simplices in Wrap complex
        exact getAlpha2Wrap(std::vector<exact>* filtration_values){
            return filtration_values->at(getMaxSimplexIndex());
        } 
        double getAlpha2Wrap_double(std::vector<exact>* filtration_values){
            return CGAL::to_double(getAlpha2Wrap(filtration_values));
        } 
        //get minimum dimension of interval simplices
        int getMinDim(std::vector<Simplex>* simplices){
            int min_dim = getMaxDim();
            for(int i=1; i<interval_simplices.size(); i++){
                if((simplices->at(interval_simplices[i])).getDim()<min_dim){
                    min_dim = (simplices->at(interval_simplices[i])).getDim();
                }
            }
            return min_dim;
        }
        //get all critical descendants
        std::vector<int> getCriticalDescendants(){
            std::vector<int> descendants(other_critical_descendants.begin(),other_critical_descendants.end());
            if(first_critical_descendant>=0){
                descendants.push_back(first_critical_descendant);
            }
            return descendants;
        }
        
        //add information
        void addSimplex(int simplex_index){interval_simplices.push_back(simplex_index);}       
        void addPredecessor(int predecessor_index){
            if(predecessor_index<0){
                std::cerr << "ERROR (Interval::addPredecessor)" << std::endl;
                printShort();
            }
            predecessors.insert(predecessor_index);
        }
        
        //add critical descendant when setting wrap radii in lower sets (setAlpha2WrapRecursively), for non-singular intervals check if alpha2 improved
        std::pair<bool,bool> addCriticalDescendant(int interval_index, exact descendant_alpha2, std::vector<bool>* in_complex, std::vector<exact>* filtration_values){
            bool already_added = false;
            bool new_first = false;
            if(interval_index == first_critical_descendant){
                //this critical descendant was already found as first
                already_added = true;
            }else{
                exact alpha2_wrap = getAlpha2Wrap(filtration_values);
                if(!isSingular() && (first_critical_descendant==-1 || descendant_alpha2<alpha2_wrap)){
                    //new first critical descendant found
                    if(first_critical_descendant>=0){
                        other_critical_descendants.insert(first_critical_descendant);
                    }
                    first_critical_descendant = interval_index;
                    for(int i=0; i<interval_simplices.size();i++){
                        filtration_values->at(interval_simplices[i])=descendant_alpha2;
                        in_complex->at(interval_simplices[i])=true;
                    }
                    new_first = true;
                }else{
                    std::pair<std::set<int>::iterator,bool> out = other_critical_descendants.insert(interval_index);
                    if(out.second==false){
                        //this critical descendant was already found
                        already_added = true;
                    }
                    //else, new critical descendant added
                }
            }
            return std::make_pair(already_added,new_first);
        }
        //delete a critical descendant after point manipulation, update filtration values
        bool deleteCriticalDescendant(int interval_index, std::vector<Interval>* intervals,std::vector<bool>* in_complex, std::vector<exact>* filtration_values,std::vector<Simplex>* simplices){
            if(interval_index==first_critical_descendant){
                //first critical descendant deleted, need to find new first and update Wrap radius
                if(other_critical_descendants.size()==0){
                    //this was the only critical descendant
                    first_critical_descendant=-1;
                    for(int i=0; i<interval_simplices.size();i++){
                        in_complex->at(interval_simplices[i])=false;
                        filtration_values->at(interval_simplices[i])=-1;
                    }
                }else{
                    //find new first critical descendant (with smallest alpha2)
                    std::set<int>::iterator best_it = other_critical_descendants.begin();
                    exact best_alpha2 = (intervals->at(*best_it)).getAlpha2Wrap(filtration_values);
                    std::set<int>::iterator it = other_critical_descendants.begin();
                    ++it;
                    for(; it != other_critical_descendants.end(); ++it){
                        exact other_alpha2 = (intervals->at(*it)).getAlpha2Wrap(filtration_values);
                        if(other_alpha2<best_alpha2){
                            best_it = it;
                            best_alpha2 = other_alpha2;
                        }
                    }
                    //update
                    first_critical_descendant = (*best_it);
                    for(int i=0; i<interval_simplices.size();i++){
                        filtration_values->at(interval_simplices[i])=best_alpha2;
                        in_complex->at(interval_simplices[i])=true;
                    }
                    other_critical_descendants.erase(best_it);
                }
                return true;
            }else{
                other_critical_descendants.erase(interval_index);
                return false;
            }
        }
        
        //get direct descendants of this interval (a simplex of this interval is face of a simplex in the descendant interval)
        std::set<int> getDescendantIntervals(int interval_index, std::vector<Interval>* intervals, std::vector<int>* interval_indices, std::vector<Simplex>* simplices){
            std::set<int> descendant_intervals;
            Simplex max_simplex = simplices->at(getMaxSimplexIndex());
            //intervals of cofacets are descendants
            std::vector<int> cofacets_ind = max_simplex.getCofacetIndices();
            for(int i=0; i<cofacets_ind.size(); i++){
                int descendant_interval_index = interval_indices->at(cofacets_ind[i]);
                //descendant interval if not the same
                if(descendant_interval_index!=interval_index){
                    descendant_intervals.insert(descendant_interval_index);
                }
            }
            //if not singular, intervals of cofacets of interval faces are descendants
            if(!isSingular()){
                for(int i=1; i<interval_simplices.size(); i++){
                    Simplex interval_simplex = simplices->at(interval_simplices[i]);
                    std::vector<int> interval_simplex_cofacets_ind = interval_simplex.getCofacetIndices();
                    for(int i=0; i<interval_simplex_cofacets_ind.size(); i++){
                        int descendant_interval_index = interval_indices->at(interval_simplex_cofacets_ind[i]);
                        //descendant interval if not the same
                        if(descendant_interval_index!=interval_index){
                            descendant_intervals.insert(descendant_interval_index);   
                        }
                    }
                }
            }
            return descendant_intervals;
        }
        
        //clear deleted intervals from list of predecessors
        void clearDeletedPredecessors(std::set<int>* deleted_intervals){
            for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end();){
                if(deleted_intervals->find((*it))!=deleted_intervals->end()){
                    it = predecessors.erase(it);
                }else{
                    ++it;
                }
            }
        }
        
        //recursively get all singular intervals in upper set of this interval
        std::set<int> getSingularUpperSetRecursively(std::vector<Interval>* intervals){
            std::set<int> singular_upper_set; //initialize set to store all singular intervals in upper set
            if(first_critical_descendant>=0){
                singular_upper_set.insert(first_critical_descendant);
                //recursively get upper set of critical descendant and insert
                std::set<int> upper_set_of_descendant = (intervals->at(first_critical_descendant)).getSingularUpperSetRecursively(intervals);
                singular_upper_set.insert(upper_set_of_descendant.begin(),upper_set_of_descendant.end());
            }
            for(std::set<int>::iterator it = other_critical_descendants.begin(); it != other_critical_descendants.end(); ++it){
                singular_upper_set.insert(*it); //insert direct critical descendant
                //recursively get upper set of critical descendants and insert
                std::set<int> upper_set_of_descendant = (intervals->at(*it)).getSingularUpperSetRecursively(intervals);
                singular_upper_set.insert(upper_set_of_descendant.begin(),upper_set_of_descendant.end());
            }
            return singular_upper_set;
        }
        
        //clear list of critical descendants
        void clearDescendants(){
            first_critical_descendant = -1;
            other_critical_descendants.clear();
        }
         
        //whether nonsingular interval is in 2 or more overlapping lower sets
        bool inOverlap(){
            if(!isSingular() && other_critical_descendants.size()>0){
                return true;
            }else{
                return false;
            }
        }
        
        //delete references to deleted critical descendants (and update first_critical_descendant and alpha2 if necessary), go recursively to predecessors, stop recursion at critical intervals (except first call) or if nothing was deleted (this predecessor was already found)
        //why do we go to predecessors of critical boundary intervals: maybe were not critical before
        int deleteOldReferencesFromLowerSetRecursively(std::set<int>* deleted_intervals, std::vector<Interval>* intervals, bool root_critical, Filtration* filtration, std::vector<bool>* in_complex, std::vector<exact>* filtration_values, std::vector<Simplex>* simplices, std::vector<bool>* critical, std::vector<bool>* included, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter, int count){
            if(!isSingular() || root_critical){
                count ++;
                bool deletion_performed = false;
                //delete old references from other_critical_descendants
                for(std::set<int>::iterator it = other_critical_descendants.begin(); it != other_critical_descendants.end(); ){
                    //check if descendant was deleted
                    if(deleted_intervals->find(*it)!=deleted_intervals->end()){
                        //delete reference
                        it = other_critical_descendants.erase(it);
                        deletion_performed = true;
                    }else{
                        //go to next
                        ++it;
                    }
                }
                //check if first_critical_descendant was deleted
                if(deleted_intervals->find(first_critical_descendant)!=deleted_intervals->end()){
                    //delete and update: find new first_critical_descendant, update alpha2, update in filtration
                    exact old_alpha2 = getAlpha2Wrap(filtration_values);
                    deleteCriticalDescendant(first_critical_descendant,intervals,in_complex,filtration_values,simplices);
                    deletion_performed = true;
                    //remove simplices from filtration 
                    for(int i=0; i<interval_simplices.size(); i++){
                        filtration->deleteSimplex(interval_simplices[i],old_alpha2,simplices,critical,included,inclusion_values_and_counter);
                    }
                    //re-insert with new value to filtration
                    if(in_complex->at(getMaxSimplexIndex())){
                        for(int i=0; i<interval_simplices.size(); i++){
                            filtration->insertElement(filtration_values->at(interval_simplices[i]),interval_simplices[i],simplices,critical,included,inclusion_values_and_counter);
                        }
                    }
                }

                //recursively go to predecessors
                if(deletion_performed || root_critical){
                    for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end(); ++it){
                        count = (intervals->at(*it)).deleteOldReferencesFromLowerSetRecursively(deleted_intervals,intervals,false,filtration,in_complex,filtration_values,simplices,critical,included,inclusion_values_and_counter,count);
                    }
                }
            }
            return count;
        }
        
        //recursively get all intervals with nested lower sets if singular
        std::set<int> getNestedIntervalsFull(std::vector<std::vector<int> >* nested_intervals, std::vector<Interval>* intervals){
            std::vector<int> nested_intervals_this = nested_intervals->at(index); //get directly nested lower sets
            std::set<int> nested_intervals_full = std::set<int>(nested_intervals_this.begin(),nested_intervals_this.end()); 
            if(isSingular()){
                //recursively get other nested lower sets
                for(std::vector<int>::iterator it = nested_intervals_this.begin(); it != nested_intervals_this.end(); ++it){
                    std::set<int> more_nested_intervals = (intervals->at(*it)).getNestedIntervalsFull(nested_intervals,intervals); //full!
                    nested_intervals_full.insert(more_nested_intervals.begin(),more_nested_intervals.end());
                }
            }
            return nested_intervals_full;
        }
        
        //compute all points in the full lower set of this interval if singular, intervals in exclusive lower set (without nested) were already computed
        std::set<int> getPointsLowerSetFull(std::vector<std::vector<int> >* lowerset_intervals, std::vector<std::vector<int> >* nested_intervals, std::vector<Interval>* intervals, std::vector<Simplex>* simplices){
            std::set<int> points_lowerset;
            if(isSingular()){
                //get points of critical simplex
                std::vector<int> points_crit_simplex = getMaxSimplex(simplices)->getVertices();
                points_lowerset.insert(points_crit_simplex.begin(),points_crit_simplex.end());
                //get points of other simplices in exclusive lower set
                std::vector<int> lowerset_intervals_this = lowerset_intervals->at(index);
                for(std::vector<int>::iterator it = lowerset_intervals_this.begin(); it != lowerset_intervals_this.end(); ++it){
                    Interval lowerset_interval = intervals->at(*it);
                    std::vector<int> lowerset_interval_simplices = lowerset_interval.getSimplices();
                    for(std::vector<int>::iterator it2 = lowerset_interval_simplices.begin(); it2 != lowerset_interval_simplices.end(); ++it2){
                        std::vector<int> points_lowerset_simplex = (simplices->at(*it2)).getVertices();
                        points_lowerset.insert(points_lowerset_simplex.begin(),points_lowerset_simplex.end());
                    }
                }
                //recursively get points in nested lower sets
                std::vector<int> nested_intervals_this = nested_intervals->at(index);
                for(std::vector<int>::iterator it = nested_intervals_this.begin(); it != nested_intervals_this.end(); ++it){
                    std::set<int> nested_points = (intervals->at(*it)).getPointsLowerSetFull(lowerset_intervals,nested_intervals,intervals,simplices);
                    points_lowerset.insert(nested_points.begin(),nested_points.end());
                }
            }
            return points_lowerset;
        }
        
        //compute diameter = diameter of maximal simplex
        double getDiameter(std::vector<DataPoint>* data_points, int periodic_size, std::vector<Simplex>* simplices){
            return getMaxSimplex(simplices)->getDiameter(data_points,periodic_size);
        }
        
        
        void print(std::vector<exact>* filtration_values, std::vector<Simplex>* simplices){
            if(!deleted){
                std::cout << "interval " << index << ": size " << getSize() << " dim " << getMaxDim() <<  " alpha2 " << getAlpha2Wrap_double(filtration_values);
                if(getAlpha2Wrap_double(filtration_values)>=0){
                    std::cout << " alpha " << std::sqrt(std::abs(getAlpha2Wrap_double(filtration_values)));
                }
                std::cout << " simplex_index " << getMaxSimplexIndex();
                std::cout << " predecessors ";
                for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end(); ++it){
                    std::cout << (*it) << " ";
                }
                std::cout << std::endl;
                if(!isSingular()){
                    std::cout << "first critical descendant " << first_critical_descendant << std::endl;
                }
                std::cout << " other_critical_descendants ";
                for(std::set<int>::iterator it = other_critical_descendants.begin(); it != other_critical_descendants.end(); ++it){
                    std::cout << (*it) << " ";
                }
                std::cout << std::endl;
                for(std::vector<int>::iterator it = interval_simplices.begin(); it != interval_simplices.end(); ++it){
                    (simplices->at(*it)).print();
                }
                std::cout << std::endl;
            }else{
                std::cout << " deleted" << std::endl;
            }
        }
        
        void printShort(){
            if(!deleted){
                std::cout << "interval " << index << ": size " << getSize() << " dim " << getMaxDim();
                std::cout << " simplex_index " << getMaxSimplexIndex();
                std::cout << std::endl;
            }else{
                std::cout << "deleted interval" << std::endl;
            }
        }
};

//Wrap complex for a given set of points, constructed on basis of Alpha complex
class Wrap : public Filtered_Complex
{      
    private:
        std::vector<exact>* filtration_values_alpha; //Delaunay squared radius for every simplex (filtration value in Alpha complex)
        std::vector<std::vector<int> >* interval_faces; //list of faces that are in same interval, for each simplex that is maximal in its interval (computed in Alpha complex)
        
        std::vector<Interval> intervals; //list of intervals (set of related simplices with same value of Delaunay radius function (minimum alpha^2 to appear in Alpha complex))
        std::vector<int> interval_indices; //interval indices of simplices
        
        std::vector<int> free_indices_intervals; //manipulating points and thereby deleting simplices might result in unused indices in the lists of intervals
        
        //statistics for all filtration values   
        std::vector<std::vector<int> > num_crit_simplices_neg_filtration; //number of negative critical simplices of different dimension (1,2,..), filtered
        std::vector<std::vector<int> > num_crit_simplices_pos_filtration; //number of positive critical simplices of different dimension (1,2,..), filtered
        
        bool descendants_computed; //whether direct critical descendants of non-singular intervals were computed (necessary for dynamic update)
        
    public:  
        
        //constructors
        Wrap(){name="Wrap";name_uppercase ="WRAP";}; //empty
        //compute Wrap complex with information provided by Alpha complex (Delaunay radii, criticality, what is in same interval)
        Wrap(std::vector<exact>* filtration_values_alpha_, std::vector<bool>* critical_, std::vector<std::vector<int> >* interval_faces_, bool dim3_, bool weighted_, int periodic_size, int num_data_points, std::vector<DataPoint>* data_points_, std::vector<Simplex>* simplices_, bool printstats); 
        
        void clear(); //clear complex
        
        bool wereDescendantsComputed(){return descendants_computed;}
        
        std::vector<Interval>* getIntervals(){return &intervals;}
        int getIntervalDim(int interval_index){return intervals[interval_index].getMaxDim();}
        int getIntervalSize(int interval_index){return intervals[interval_index].getSize();}
        std::vector<int>* getIntervalIndices(){return &interval_indices;}
        int getIntervalIndex(int simplex_index){return interval_indices[simplex_index];}
        
        std::pair<int,std::vector<int> > createInterval(Simplex* simplex); //create interval of given simplex, start with higher-dim simplices, lower-dim simplices and references to predecessors are taken care of later, return index and list of deleted intervals (now in same interval as this one)
        void computeIntervalPredecessors(int interval_index); //compute predecessors for given interval
        void addIntervalAsPredecessor(int interval_index); //add given interval as predecessor to its descendants
        
        int computeFiltrationValuesWrap(bool compute_descendants); //compute filtration values for Wrap simplices, optionally also compute critical descendants
        void computeFiltration(bool holeOperation); //compute filtration: simplices sorted by wrap filtration value
        void storeUnadapted(); //store unadapted filtration before any hole operations
        
        //updates for point manipulation
        void updateListSizes(); //update size of lists after change of simplices number
        void updateFiltrationValuesAlpha(std::vector<exact>* filtration_values_alpha_){filtration_values_alpha=filtration_values_alpha_;}
        void updateIntervalFaces(std::vector<std::vector<int> >* interval_faces_){interval_faces = interval_faces_;}
        int updateFiltrationValuesInLowerSetsOf(std::vector<int>* considered_intervals); //update lower sets of new intervals, add new critical intervals as descendants, update filtration value if new first_critical_descendant, update in filtration then
        int updateFiltrationValuesInLowerSetOfNonCriticalBoundary(int interval_index); //update lower sets of (now) non-singular boundary intervals (with old critical descendants)
        std::vector<int> deleteSimplex(Simplex* simplex); //delete simplex from its interval and from wrap filtration, return indices of deleted simplices
        void clearDeletedPredecessors(std::set<int>* considered_intervals, std::set<int>* deleted_intervals); //clear predecessor references to deleted intervals in all considered intervals
        int deleteOldReferencesFromLowerSet(int interval_index,std::set<int>* deleted_intervals); //if interval is non-singular delete references to deleted critical descendants (and update first_critical_descendant and filtration value if necessary), recursively go to non-singular predecessors
        void addFreeIndicesIntervals(std::set<int>* new_indices){free_indices_intervals.insert(free_indices_intervals.end(),new_indices->begin(),new_indices->end());}
        
        //statistics
        void computeFiltrationStatistics(bool printstats, bool after_hole_operation); ///compute statistics (# simplices, ...) for whole filtration (for every filtration value)
        void computeIntervalStatisticsPaper(); //compute and print statistics for all intervals in filtration order, also print statistics about full complex and overlapping lower sets, for PPP paper
        
        int getNumIntervals(){return intervals.size()-free_indices_intervals.size();}      
        int getNumCriticalEdgesNeg(){if(filtration_index_greater_than_alpha2==0 || num_crit_simplices_neg_filtration.size()<=0){return 0;}else{return num_crit_simplices_neg_filtration[0][filtration_index_greater_than_alpha2-1];}}
        int getNumCriticalEdgesPos(){if(filtration_index_greater_than_alpha2==0 || num_crit_simplices_pos_filtration.size()<=0){return 0;}else{return num_crit_simplices_pos_filtration[0][filtration_index_greater_than_alpha2-1];}}
        int getNumCriticalTrianglesNeg(){if(filtration_index_greater_than_alpha2==0 || num_crit_simplices_neg_filtration.size()<=1){return 0;}else{return num_crit_simplices_neg_filtration[1][filtration_index_greater_than_alpha2-1];}}
        int getNumCriticalTrianglesPos(){if(filtration_index_greater_than_alpha2==0 || num_crit_simplices_pos_filtration.size()<=1){return 0;}else{return num_crit_simplices_pos_filtration[1][filtration_index_greater_than_alpha2-1];}}
        int getNumCriticalTetrahedra(){if(filtration_index_greater_than_alpha2==0 || num_crit_simplices_neg_filtration.size()<=2){return 0;}else{return num_crit_simplices_neg_filtration[2][filtration_index_greater_than_alpha2-1];}}
        
        std::vector<int>* getNumCriticalEdgesNegFiltration(){return &num_crit_simplices_neg_filtration[0];}
        std::vector<int>* getNumCriticalEdgesPosFiltration(){return &num_crit_simplices_pos_filtration[0];}
        std::vector<int>* getNumCriticalTrianglesNegFiltration(){return &num_crit_simplices_neg_filtration[1];}       
        std::vector<int>* getNumCriticalTrianglesPosFiltration(){return &num_crit_simplices_pos_filtration[1];}
        std::vector<int>* getNumCriticalTetrahedraFiltration(){return &num_crit_simplices_neg_filtration[2];}  
        
        int getNumCriticalEdgesPosFull(){if(getNumCriticalEdgesPosFiltration()->size()==0){return 0;}else{return getNumCriticalEdgesPosFiltration()->back();}}
        int getNumCriticalEdgesNegFull(){if(getNumCriticalEdgesNegFiltration()->size()==0){return 0;}else{return getNumCriticalEdgesNegFiltration()->back();}}
        int getNumCriticalTrianglesPosFull(){if(getNumCriticalTrianglesPosFiltration()->size()==0){return 0;}else{return getNumCriticalTrianglesPosFiltration()->back();}}
        int getNumCriticalTrianglesNegFull(){if(getNumCriticalTrianglesNegFiltration()->size()==0){return 0;}else{return getNumCriticalTrianglesNegFiltration()->back();}}
        int getNumCriticalTetrahedraFull(){if(getNumCriticalTetrahedraFiltration()->size()==0){return 0;}else{return getNumCriticalTetrahedraFiltration()->back();}}
        
        //print
        void printIntervals(); //print detailed information about intervals (set of related simplices with same alpha value)
        void printOutput(); //print points, filtration and persistence pairs as output
        void printFull(bool header, std::ostream& output); //print info about full complex
        
    private:
        int setWrapAlpha2Recursively(int root_interval_index, int current_interval_index, exact alpha2, bool compute_descendants, bool update, bool stop_when_already_found, int count); //update filtration value for given interval and its lower set recursively, return number of recursive calls
        
        //hole operations for full complex (disregard side effects)
        void lockOrFillCycleFullComplex(bool lock, PersistencePair persistence_pair, exact inclusion_alpha2, bool printinfo, bool printstats); //lock (fill) cycle for full complex by including canonical cycle (chain) of the birth (death) simplex, wrap adaptation: include first critical descendants of these simplices and corresponding lower sets
        void unlockOrUnfillCycleFullComplex(bool lock, PersistencePair persistence_pair, bool printinfo, bool printstats); //unlock (unfill) cycle for full complex by excluding canonical cochain (cocycle) of birth simplex (death), wrap adaptation: exclude critical intervals in upper sets, keep wrap structure
        bool setIncludedLowerSetRecursively(int interval_index, exact inclusion_alpha2, bool printinfo); //include given interval and its lower set earlier in filtration, for hole manipulation
        int adaptNonCritialIntervalsAfterExcludingCritical(); //adapt filtration values (exclude or include with new value) of non critical intervals that are in lower set of excluded critical intervals
};

#endif // WRAP_H