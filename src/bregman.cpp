//bregman.cpp
//author: koelsboe

#include "bregman.h"

//clear list of points
void Bregman::clearPoints()
{
    bregman_points.clear();
}

//Legendre type function determined by type
exact Bregman::bregF(DataPoint x, bool conjugate)
{
    if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
        double sum = 0;
        for(int i=0; i<x.getDim(); i++){
            //the expression is 0 for x_i=0
            if(x.getCoordinate(i)>0){
                double xi = x.getCoordinate_double(i);
                sum = sum + xi*std::log(xi)-xi;
            }
        }
        return sum;
    }else if((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")){
        if(!on_standard_simplex){
            double sum = 0;
            for(int i=0; i<x.getDim(); i++){
                double xi = x.getCoordinate_double(i);
                sum = sum + std::min(1e13,std::exp(xi));
            }
            return sum;
        }else{ 
            //use hwagners implementation with softmax: E*(x*) instead of E*(x)
            auto BR = BregmanFunctions::KL_simp_conj();
            Eigen::Vector3d a{x.getX_double(),x.getY_double(),x.getZ_double()};
            return BR.F(a);
            /*double sum = 1.;
            for(int i=0; i<x.getDim(); i++){
                double xi = x.getCoordinate_double(i);
                sum = sum -1./(x.getDim()*1.)*std::log(xi);
            }
            return sum;*/
        }
    }else{
        //halfEuclidean
        exact sum = 0;
        for(int i=0; i<x.getDim(); i++){
            exact xi = x.getCoordinate(i);
            sum = sum + xi*xi;
        }
        return sum/2.;
    }
}

//gradient of F (or F*)
std::vector<exact> Bregman::bregGradF(DataPoint x, bool conjugate)
{
    std::vector<exact> grad;
    if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
        if(!on_standard_simplex){
            for(int i=0; i<x.getDim(); i++){
                if(x.getCoordinate(i)>0){
                    grad.push_back(std::log(x.getCoordinate_double(i)));
                }else{
                    grad.push_back(-1e12); //-inf
                }
            }
        }else{
            //see bregman_tools.h (hwagners implementation)
            double sum_log = 0;
            for(int i=0; i<x.getDim(); i++){
                sum_log += std::log(x.getCoordinate_double(i));
            }
            for(int i=0; i<x.getDim(); i++){
                grad.push_back(std::log(x.getCoordinate_double(i))-sum_log/(1.*x.getDim()));
            }
            //make it lie EXACTLY on plane through origin
            grad[x.getDim()-1]=0.;
            for(int i=0; i<x.getDim()-1;i++){
                grad[x.getDim()-1]+=-grad[i];
            }
        }
    }else if((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")){
        if(!on_standard_simplex){
            for(int i=0; i<x.getDim(); i++){
                grad.push_back(std::min(std::exp(x.getCoordinate_double(i)),1e13));
            }
        }else{
            //softmax (see bregman_tools.h hwagners implementation)
            double sum_exp = 0.;
            for(int i=0; i<x.getDim();i++){
                sum_exp += std::exp(x.getCoordinate_double(i));
            }
            for(int i=0; i<x.getDim(); i++){
                double xi = x.getCoordinate_double(i);
                grad.push_back(std::exp(xi)/sum_exp);
            }
            //enforce that points lie on plane, sum=1
            grad[x.getDim()-1]=1.;
            for(int i=0; i<x.getDim()-1;i++){
                grad[x.getDim()-1]+=-grad[i];
            }
        }
    }else{
        //for halfEuclidean, gradF(x)=x
        grad = x.getCoordinates();
    }
    return grad;
}

exact Bregman::bregDiv(DataPoint x, DataPoint y)
{
    if(x.getDim()!=y.getDim()){
        return -1;
    }
    exact out= bregF(x,false) - bregF(y,false);
    std::vector<exact> grad = bregGradF(y,false);
    for(int i=0; i<x.getDim(); i++){
        out -= (x.getCoordinate(i)-y.getCoordinate(i))*grad[i];
    }
    return out;
}

//compute smallest circumradius (not caring about emptiness) for a simplex using numerical methods, return radius and circumcenter
std::pair<double,vec> Bregman::compute_circumradius_bregman(Simplex* simplex, bool conjugate)
{
    if(simplex->getDim()==0){
        if(!dim3){
            return std::make_pair(0.,Eigen::Vector2d(bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double()));
        }else{
            return std::make_pair(0.,Eigen::Vector3d(bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double(),bregman_points.at(simplex->getVertex(0)).getZ_double()));
        }
    }else if(simplex->getDim()==1){
        if(!dim3){
            Eigen::Vector2d a{bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double()};
            Eigen::Vector2d b{bregman_points.at(simplex->getVertex(1)).getX_double(),bregman_points.at(simplex->getVertex(1)).getY_double()};
            if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
                return smallest_circumsphere(a,b, BregmanFunctions::KL());
            }else if ((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")){
                return smallest_circumsphere(a,b, BregmanFunctions::KL_conj()); 
            }else{
                return smallest_circumsphere(a,b, BregmanFunctions::EUCLSQ());
            }
        }else{
            Eigen::Vector3d a{bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double(),bregman_points.at(simplex->getVertex(0)).getZ_double()};
            Eigen::Vector3d b{bregman_points.at(simplex->getVertex(1)).getX_double(),bregman_points.at(simplex->getVertex(1)).getY_double(),bregman_points.at(simplex->getVertex(1)).getZ_double()};
            if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
                return smallest_circumsphere(a,b, BregmanFunctions::KL());
            }else if (((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")) && !on_standard_simplex){
                return smallest_circumsphere(a,b, BregmanFunctions::KL_conj()); 
            }else if (((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")) && on_standard_simplex){
                return smallest_circumsphere(a,b, BregmanFunctions::KL_simp_conj()); 
            }else{
                return smallest_circumsphere(a,b, BregmanFunctions::EUCLSQ());
            }
        }
    }else if(simplex->getDim()==2){
        if(!dim3){
            Eigen::Vector2d a{bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double()};
            Eigen::Vector2d b{bregman_points.at(simplex->getVertex(1)).getX_double(),bregman_points.at(simplex->getVertex(1)).getY_double()};
            Eigen::Vector2d c{bregman_points.at(simplex->getVertex(2)).getX_double(),bregman_points.at(simplex->getVertex(2)).getY_double()};
            std::vector<vec> Q = {a,b,c};
            if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
                return smallest_circumsphere(Q, BregmanFunctions::KL());
            }else if ((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")){
                return smallest_circumsphere(Q, BregmanFunctions::KL_conj());
            }else{
                return smallest_circumsphere(Q, BregmanFunctions::EUCLSQ());
            }

        }else{
            Eigen::Vector3d a{bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double(),bregman_points.at(simplex->getVertex(0)).getZ_double()};
            Eigen::Vector3d b{bregman_points.at(simplex->getVertex(1)).getX_double(),bregman_points.at(simplex->getVertex(1)).getY_double(),bregman_points.at(simplex->getVertex(1)).getZ_double()};
            Eigen::Vector3d c{bregman_points.at(simplex->getVertex(2)).getX_double(),bregman_points.at(simplex->getVertex(2)).getY_double(),bregman_points.at(simplex->getVertex(2)).getZ_double()};
            std::vector<vec> Q = {a,b,c};
            if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
                return smallest_circumball_by_convex_minimization(Q, BregmanFunctions::KL());
            }else if (((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")) && !on_standard_simplex){
                return smallest_circumball_by_convex_minimization(Q, BregmanFunctions::KL_conj()); 
            }else if (((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")) && on_standard_simplex){
                return smallest_circumball_by_convex_minimization(Q, BregmanFunctions::KL_simp_conj()); 
            }else{
                return smallest_circumball_by_convex_minimization(Q, BregmanFunctions::EUCLSQ());
            }
        }
    }else if(simplex->getDim()==3){
        Eigen::Vector3d a{bregman_points.at(simplex->getVertex(0)).getX_double(),bregman_points.at(simplex->getVertex(0)).getY_double(),bregman_points.at(simplex->getVertex(0)).getZ_double()};
        Eigen::Vector3d b{bregman_points.at(simplex->getVertex(1)).getX_double(),bregman_points.at(simplex->getVertex(1)).getY_double(),bregman_points.at(simplex->getVertex(1)).getZ_double()};
        Eigen::Vector3d c{bregman_points.at(simplex->getVertex(2)).getX_double(),bregman_points.at(simplex->getVertex(2)).getY_double(),bregman_points.at(simplex->getVertex(2)).getZ_double()};
        Eigen::Vector3d d{bregman_points.at(simplex->getVertex(3)).getX_double(),bregman_points.at(simplex->getVertex(3)).getY_double(),bregman_points.at(simplex->getVertex(3)).getZ_double()};
        std::vector<vec> Q = {a,b,c,d};
        if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
            return smallest_circumsphere(Q, BregmanFunctions::KL());
        }else if ((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon")){
            return smallest_circumsphere(Q, BregmanFunctions::KL_conj()); 
        }else{
            return smallest_circumsphere(Q, BregmanFunctions::EUCLSQ());
        }
    }
}

//check whether any neighboring vertex is inside smallest enclosing ball
bool Bregman::isNeighborInside(std::pair<double,vec> circumsphere_r_c, Simplex* simplex, bool conjugate)
{
    bool neighbor_inside = false;
    
    divergence_fun_t div;
    if((!conjugate && type=="Shannon") || (conjugate && type=="conjugateShannon")){
        div = make_bregman_divergence(BregmanFunctions::KL());
    }else if((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon") && !on_standard_simplex){
        div = make_bregman_divergence(BregmanFunctions::KL_conj()); 
    }else if((!conjugate && type=="conjugateShannon") || (conjugate && type=="Shannon") && on_standard_simplex){
        div = make_bregman_divergence(BregmanFunctions::KL_simp_conj()); 
    }else{
        div = make_bregman_divergence(BregmanFunctions::EUCLSQ());
    }
        
    std::vector<int> cofacets = simplex->getCofacetIndices();
    for(int j=0; j<cofacets.size(); j++){
        Simplex cofacet = simplices_ptr->at(cofacets[j]);
        std::vector<int> cofacet_vertices = cofacet.getVertices();
        int neighbor_ind = -1;
        for(int k=0; k<cofacet_vertices.size(); k++){
            if(!simplex->hasVertex(cofacet_vertices[k])){
                neighbor_ind = cofacet_vertices[k];
                break;
            }
        }
        double div_neighbor = 0;
        if(!dim3){
            Eigen::Vector2d p{bregman_points.at(neighbor_ind).getX_double(),bregman_points.at(neighbor_ind).getY_double()};
            div_neighbor = div(p, circumsphere_r_c.second);
        }else{
            Eigen::Vector3d p{bregman_points.at(neighbor_ind).getX_double(),bregman_points.at(neighbor_ind).getY_double(),bregman_points.at(neighbor_ind).getZ_double()};
            div_neighbor = div(p, circumsphere_r_c.second);
        }
        if(div_neighbor<circumsphere_r_c.first){
            neighbor_inside = true;
            break;
        }
    }
    return neighbor_inside;
}

//compute value and index of smallest cofacet
std::pair<exact,int> Bregman::getSmallestCofacetValue(std::vector<exact>* numeric_filtration_values, Simplex* simplex)
{
    exact smallest_value = -1;
    int smallest_cofacet = -1;
    for(int j=0; j<simplex->getCofacetIndices().size(); j++){
        int cofacet_index = simplex->getCofacetIndices()[j];
        if((smallest_cofacet<0 || smallest_value>numeric_filtration_values->at(cofacet_index)) && numeric_filtration_values->at(cofacet_index)>=0){
            smallest_value = numeric_filtration_values->at(cofacet_index);
            smallest_cofacet = cofacet_index;
        }
    }
    return std::make_pair(smallest_value,smallest_cofacet);
}

//compute geodesic distance between points lying on sphere of radius sqrt(2)
double Bregman::sphericalDistance2(CPoint3 x, CPoint3 y)
{
    return 2.*std::pow(std::acos((CGAL::to_double(x.x())*CGAL::to_double(y.x())+CGAL::to_double(x.y())*CGAL::to_double(y.y())+CGAL::to_double(x.z())*CGAL::to_double(y.z()))/2.),2);
}

bool Bregman::computeBregmanDelaunay(std::vector<DataPoint> *new_points, bool weighted_euclidean_radii, int num_data_points_, int periodic_size_, bool dim3_, bool compute_persistence, bool printstats)
{
    
    clear();
    dim3 = dim3_;
    exact outlier_threshold = 1e10;
    
    //transform points to the necessary domain
    if((new_points->size())>0){
        double min_domain = 0;
        if(type == "Shannon"){
        //for Shannon entropy, point coordinates must be >0
            //find minimum coordinate values
            exact min_x = new_points->at(0).getX();
            exact min_y = new_points->at(0).getY();
            exact min_z;
            if(dim3){
                min_z = new_points->at(0).getZ();
            }
            for(int i=1; i<new_points->size(); i++){
                if(new_points->at(i).getX()<min_x){
                    min_x = new_points->at(i).getX();
                }
                if(new_points->at(i).getY()<min_y){
                    min_y = new_points->at(i).getY();
                }
                if(dim3){
                    if(new_points->at(i).getZ()<min_z){
                        min_z = new_points->at(i).getZ();
                    }
                }
            }
            if(min_x<=min_domain || min_y<=min_domain || (min_z<=min_domain  && dim3)){
                for(int i=0; i<new_points->size(); i++){
                    if(min_x<=min_domain){
                        new_points->at(i).setX(new_points->at(i).getX()-min_x+min_domain+1e-6);
                    }
                    if(min_y<=min_domain){
                        new_points->at(i).setY(new_points->at(i).getY()-min_y+min_domain+1e-6);
                    }
                    if(dim3 && min_z<=min_domain){
                        new_points->at(i).setZ(new_points->at(i).getZ()-min_z+min_domain+1e-6);
                    }
                }
                double x_translation, y_translation, z_translation = 0;
                if(min_x<=min_domain){
                    x_translation=CGAL::to_double(-min_x+min_domain+1e-6);
                }
                if(min_y<=min_domain){
                    y_translation=CGAL::to_double(-min_y+min_domain+1e-6);
                }
                if(dim3 && min_z<=min_domain){
                    z_translation=CGAL::to_double(-min_z+min_domain+1e-6);
                }
                std::cout << "translated input points by +(" << x_translation << ", " << y_translation << ", " << z_translation << ")" << std::endl;
                
            }
        }
    }
    
    input_points = new_points;
    
    //compute weighted Euclidean points corresponding to Bregman geometry
    computeBregmanPoints(new_points);
    
    //TEST
    //std::cout << "BREGMAN POINTS" << std::endl; 
    //for(int i=0; i<bregman_points.size(); i++){
    //    bregman_points[i].print(); 
    //}
   
    //store properties
    //compute complexes in R^2/R^3 or periodic domain
    //(periodic triangulation, no boundary, domain has size periodic_size^2/3, point set repeated in all directions)
    periodic_size = periodic_size_;
    bool periodic=false;
    if(periodic_size>0){
        periodic=true;
    }   
    data_points = &bregman_points; 
    num_data_points = num_data_points_;
    weighted = true;
    
    //compute Bregman-Delaunay triangulation
    bool valid_delaunay = false;
    if(on_standard_simplex){
        valid_delaunay = computeDelaunayTriangulationOnStandardSimplex(false,false);
        
        //ALTERNATIVE //transform to 2d points
        /*std::vector<DataPoint> data_points_2d;
        for(int i=0; i<bregman_points.size(); i++){
            DataPoint data_point = bregman_points[i];
            std::vector<exact> coordinates_2d;
            coordinates_2d.push_back(data_point.getCoordinate(0)-data_point.getCoordinate(1)/2.-data_point.getCoordinate(2)/2.);
            coordinates_2d.push_back(std::sqrt(3)/2.*data_point.getCoordinate(2)-std::sqrt(3)/2.*data_point.getCoordinate(1));
            data_points_2d.push_back(DataPoint(coordinates_2d,data_point.getWeight()));
        }
        data_points= &data_points_2d;
        valid_delaunay = computeDelaunayTriangulation(false);
        data_points = &bregman_points;*/
    }else{
        valid_delaunay = computeDelaunayTriangulation(false);
    }
    if(!valid_delaunay){
        return false;
    }
    
    //use Euclidean Delaunay radii of weighted points
    if(weighted_euclidean_radii){
        //compute Alpha complex and intervals
        computeAlphaComplex();
        //compute Wrap complex
        computeWrapAndPersistence(compute_persistence,printstats);

    //radius computation for bregman geometry
    }else{
        std::vector<exact>  numeric_filtration_values(simplices_ptr->size(),0); 

        //compute Alpha filtration values
        int count_nan=0;
        int maxdim = 2+(dim3&&!on_standard_simplex);
        for(int d=maxdim; d>=0; d--){
            for(int i=0; i<simplices_ptr->size(); i++){
                Simplex simplex = simplices_ptr->at(i);
                if(simplex.getDim()==d){
                    if(d==0){
                        numeric_filtration_values[i]=0;
                        continue;
                    }
                    
                    //-> use this as temporary radii to compute valid filtration if radius computation does not work
                    /*if(type=="conjugateShannon"&&!primal&&on_standard_simplex){
                        numeric_filtration_values[i]=(simplices_ptr->size()*d)+i;
                        continue;
                    }*/

                    //simplex.print(); //TEST 
                    std::pair<double, vec> circumsphere_r_c = compute_circumradius_bregman(&simplex,!primal);
                    //std::cout << " value, " << circumsphere_r_c.first << std::endl; //TEST

                    if(d==maxdim){
                                
                        //for maximal simplices radius of circumsphere defines Alpha radius
                        if((on_standard_simplex || !dim3) && std::isnan(CGAL::to_double(circumsphere_r_c.first))){
                            //std::cout << "NaN circumradius" << std::endl; //TEST
                            count_nan++;
                            //Assign biggest edge value
                            exact longest_edge_radius = -1;
                            std::vector<int> faces = simplex.getFacetIndices();
                            for(int k=0; k<faces.size(); k++){
                                exact edge_radius = compute_circumradius_bregman(&(simplices_ptr->at(faces[k])),!primal).first;
                                if(edge_radius>longest_edge_radius){
                                    longest_edge_radius = edge_radius;
                                }
                            }
                            numeric_filtration_values[i]=longest_edge_radius;
                        }else if(std::isnan(CGAL::to_double(circumsphere_r_c.first))){
                            numeric_filtration_values[i] = outlier_threshold; 
                        }else{
                            numeric_filtration_values[i] = circumsphere_r_c.first;
                        }
                        continue; 
                    }

                    //check if any neighboring vertex inside circumsphere (dual Bregman ball)
                    bool neighbor_inside = isNeighborInside(circumsphere_r_c,&simplex,!primal);
                    //if circumsphere is not empty, cofacet with smallest filtration value determines filtration value
                    //also if we got nan value (for triangle in 3d) we take smallest value of cofacets
                    if(neighbor_inside||std::isnan(CGAL::to_double(circumsphere_r_c.first))){
                        if(std::isnan(CGAL::to_double(circumsphere_r_c.first))){
                            //std::cout << "NaN circumradius" << std::endl; //TEST 
                            count_nan++;
                        }
                        numeric_filtration_values[i]=getSmallestCofacetValue(&numeric_filtration_values,&simplex).first;
                        //std::cout << " smallest cofacet value " << numeric_filtration_values[i] << std::endl; //TEST 
                    }else{
                        numeric_filtration_values[i] = circumsphere_r_c.first;
                    }
                    //std::cout << " value " << numeric_filtration_values[i] << std::endl; //TEST 
                }
            }
        }

        if(printstats){
            std::cout << "num_NaN_values, " << count_nan << std::endl;
        }

        bool weighted_ = false;
        bool relative_threshold = false;
        bool use_value_of_min_simplex = true;
        double prescribed_threshold = -1;
        exact threshold = computeIntervalsFromNoisyFiltration(&numeric_filtration_values,prescribed_threshold,weighted_,use_value_of_min_simplex,relative_threshold,true,compute_persistence,printstats,false); 
    }
        
    return true;
}

//compute weighted Euclidean points corresponding to Bregman geometry
void Bregman::computeBregmanPoints(std::vector<DataPoint> *new_points)
{
    bregman_points.clear();
    
    //compute points for which weighted Delaunay mosaic is the Bregman-Delaunay mosaic for given points
    for(int i=0; i<new_points->size(); i++){
        DataPoint data_point = new_points->at(i);
        
        std::vector<exact> transformed_point;
        exact weight=0;
        //compute coordinates and weights for Bregman points
        if(primal){
            transformed_point = data_point.getCoordinates();
        }else{ //for dual balls go to conjugate space
            transformed_point = bregGradF(data_point,false);
        }
        for(int i=0; i<transformed_point.size(); i++){
            weight += 2.*0.5*transformed_point[i]*transformed_point[i];
        }
        weight += -2.*bregF(transformed_point,!primal);
        DataPoint bregman_point(transformed_point,weight);
        bregman_points.push_back(bregman_point);
    }
}

bool Bregman::computeWithFisherMetric(std::vector<DataPoint> *new_points, int num_data_points_, int periodic_size_, bool dim3_, bool compute_persistence, bool printstats)
{
    clear();
    dim3 = dim3_;
    
    type = "fisher";
    
    //transform points to the necessary domain
    if((new_points->size())>0){
        //point coordinates must be >0
        //find minimum coordinate values
        exact min_x = new_points->at(0).getX();
        exact min_y = new_points->at(0).getY();
        exact min_z;
        if(dim3){
            min_z = new_points->at(0).getZ();
        }
        for(int i=1; i<new_points->size(); i++){
            if(new_points->at(i).getX()<min_x){
                min_x = new_points->at(i).getX();
            }
            if(new_points->at(i).getY()<min_y){
                min_y = new_points->at(i).getY();
            }
            if(dim3){
                if(new_points->at(i).getZ()<min_z){
                    min_z = new_points->at(i).getZ();
                }
            }
        }
        if(min_x<=0 || min_y<=0 || (min_z<=0  && dim3)){
            for(int i=0; i<new_points->size(); i++){
                if(min_x<=0){
                    new_points->at(i).setX(new_points->at(i).getX()-min_x+1);
                }
                if(min_y<=0){
                    new_points->at(i).setY(new_points->at(i).getY()-min_y+1);
                }
                if(dim3 && min_z<=0){
                    new_points->at(i).setZ(new_points->at(i).getZ()-min_z+1);
                }
            }
            double x_translation, y_translation, z_translation = 0;
            if(min_x<=0){
                x_translation=CGAL::to_double(-min_x+1);
            }
            if(min_y<=0){
                y_translation=CGAL::to_double(-min_y+1);
            }
            if(dim3 && min_z<=0){
                z_translation=CGAL::to_double(-min_z+1);
            }
            std::cout << "translated input points by +(" << x_translation << ", " << y_translation << ", " << z_translation << ")" << std::endl;

        }
    }
    
    input_points = new_points;
    
    //Fisher transformation of points
    bregman_points.clear();
    for(int i=0; i<new_points->size(); i++){
        DataPoint data_point = new_points->at(i);
        //take sqrt of coordinates
        std::vector<exact> sqrt_data_point;
        for(int j=0; j<data_point.getDim(); j++){
            //transform x->sqrt(2x)
            sqrt_data_point.push_back(std::sqrt(2.*data_point.getCoordinate_double(j)));
        }
        DataPoint fisher_point(sqrt_data_point,0);
        bregman_points.push_back(fisher_point);
    }
    
    if(!on_standard_simplex){
        bool valid =  setPoints(&bregman_points, num_data_points_, periodic_size_, dim3_, false, compute_persistence, printstats);

        
        
        return valid;
    }else{
        clear();
    
        dim3 = dim3_;
        //compute complexes in R^2/R^3 or periodic domain
        //(periodic triangulation, no boundary, domain has size periodic_size^2/3, point set repeated in all directions)
        periodic_size = periodic_size_;
        bool periodic=false;
        if(periodic_size>0){
            periodic=true;
        }   

        data_points = &bregman_points; 
        num_data_points = num_data_points_;
        weighted = false;

        //compute Delaunay triangulation   

        double time1 = getCPUTime();
        bool valid_delaunay = computeDelaunayTriangulationOnStandardSimplex(true,false);
        if(!valid_delaunay){
            return false;
        }
        if(printstats){
            std::cout << " Time (Total), " << getCPUTime()-time1 << std::endl;
            std::cout << " # points, " << num_data_points << std::endl;
            std::cout << "COMPUTE ALPHA COMPLEX" << std::endl;
            time1 = getCPUTime();
        }
        
        std::vector<exact>  numeric_filtration_values(simplices_ptr->size(),0);
    
        //compute Alpha filtration values: geodesic sizes on sphere of radius sqrt(2)
        for(int d=2; d>=0; d--){
            for(int i=0; i<simplices_ptr->size(); i++){
                Simplex simplex = simplices_ptr->at(i);
                if(simplex.getDim()==d){
                    //simplex.print(); //TEST
                    if(d==0){
                        numeric_filtration_values[i]=0;
                        
                    }else if(d==2){
                        //compute circumcenter of triangle in R^3
                        std::vector<CPoint3> tri_points;
                        tri_points.push_back(CPoint3(data_points->at(simplex.getVertex(0)).getX(),data_points->at(simplex.getVertex(0)).getY(),data_points->at(simplex.getVertex(0)).getZ()));
                        tri_points.push_back(CPoint3(data_points->at(simplex.getVertex(1)).getX(),data_points->at(simplex.getVertex(1)).getY(),data_points->at(simplex.getVertex(1)).getZ()));
                        tri_points.push_back(CPoint3(data_points->at(simplex.getVertex(2)).getX(),data_points->at(simplex.getVertex(2)).getY(),data_points->at(simplex.getVertex(2)).getZ()));
                        
                        CPoint3 euclidean_tricenter = CGAL::circumcenter(tri_points[0],tri_points[1],tri_points[2]);
                        
                        //scale (origin,tricenter)-vector to radius of sphere
                        CGAL::Vector_3<Kernel> vect_origin_tricenter = CGAL::Vector_3<Kernel>(CPoint3(0.,0.,0.),euclidean_tricenter);
                        CGAL::Vector_3<Kernel> vect_spherical_tricenter = vect_origin_tricenter*(std::sqrt(2./CGAL::to_double(vect_origin_tricenter.squared_length())));
                        CPoint3 spherical_tricenter = CPoint3(vect_spherical_tricenter.x(),vect_spherical_tricenter.y(),vect_spherical_tricenter.z());
                        
                        //spherical circumradius is spherical distance of spherical center to any vertex
                        numeric_filtration_values[i]=sphericalDistance2(spherical_tricenter,tri_points[0]);
                        
                    }else if(d==1){
                        //radius of edge on sphere: half length of arc, sphere of radius sqrt(2)
                        double edge_radius2 = (1./4.)*sphericalDistance2((bregman_points.at(simplex.getVertex(0))).toCPoint3(),(bregman_points.at(simplex.getVertex(1))).toCPoint3());
                        
                        CPoint3 a = bregman_points.at(simplex.getVertex(0)).toCPoint3();
                        CPoint3 b = bregman_points.at(simplex.getVertex(1)).toCPoint3();
                        CPoint3 midpoint = CGAL::midpoint(a,b);
                        CGAL::Vector_3<Kernel> vect_origin_midpoint = CGAL::Vector_3<Kernel>(CPoint3(0.,0.,0.),midpoint);
                        CGAL::Vector_3<Kernel> vect_spherical_center = vect_origin_midpoint*(std::sqrt(2./CGAL::to_double(vect_origin_midpoint.squared_length())));
                        CPoint3 spherical_center = CPoint3(vect_spherical_center.x(),vect_spherical_center.y(),vect_spherical_center.z());
                        
                        //TEST
                        //std::cout << "edge spherical center " << spherical_center.x() << " " << spherical_center.y() << " " << spherical_center.z() << std::endl;
                        //for(int k=0; k<2; k++){
                        //    std::cout << " dist2 to vertex " << sphericalDistance2(spherical_center,bregman_points[simplex.getVertex(k)].toCPoint3()) << std::endl;
                        //}
                        
                        //check if any neighboring vertex inside circumsphere
                        int neighbor_inside = -1;
    
                        std::vector<int> cofacets = simplex.getCofacetIndices();
                        for(int j=0; j<cofacets.size(); j++){
                            Simplex cofacet = simplices_ptr->at(cofacets[j]);
                            std::vector<int> cofacet_vertices = cofacet.getVertices();
                            int neighbor_ind = -1;
                            for(int k=0; k<cofacet_vertices.size(); k++){
                                if(!simplex.hasVertex(cofacet_vertices[k])){
                                    neighbor_ind = cofacet_vertices[k];
                                    break;
                                }
                            }
                            double dist_neighbor2 = sphericalDistance2(spherical_center,bregman_points[neighbor_ind].toCPoint3());
                            if(dist_neighbor2<edge_radius2){
                                neighbor_inside = cofacet.getIndex();
                                break;
                            }
                        }
                        
                        //if circumsphere is not empty, cofacet with smallest filtration value determines filtration value
                        if(neighbor_inside>=0){
                            numeric_filtration_values[i]= numeric_filtration_values[neighbor_inside];
                        }else{
                            numeric_filtration_values[i] = edge_radius2;
                        }
                    }
                }
            }
        }
    
        bool weighted_ = false;
        bool relative_threshold = false;
        bool use_value_of_min_simplex = true;
        double prescribed_threshold = -1;
        exact threshold = computeIntervalsFromNoisyFiltration(&numeric_filtration_values,prescribed_threshold,weighted_,use_value_of_min_simplex,relative_threshold,true,compute_persistence,printstats,false); 

        return true;
    }
}

//compute circumcenters of maximum simplices in Bregman-Delaunay triangulation, either restrict to domain 
//(delete all simplices with circumcenter outside of domain (positive quadrant/orthant or standard simplex)) or print as output
void Bregman::computeCircumcentersMaxSimplicesBregman(bool restrict_to_domain, bool print)
{
    if(print){
        std::cout << "simplex_index, vertices, circumcenter, euclidean_circumcenter" << std::endl;
    }
    Kernel kernel;
    //iterate over all maximum simplices, check if circumcenter outside domain, if so delete with interval simplices
    std::vector<int> simplices_to_delete;
    for(int i=0; i<simplices_ptr->size(); i++){
        Simplex simplex = simplices_ptr->at(i);
        if(!simplex.isDeleted() && simplex.getDim()==(2+(dim3&&!on_standard_simplex))){
            std::vector<exact> circumcenter;
            std::vector<exact> euclidean_circumcenter; //compare with euclidean circumcenter for transformed points
            if(!dim3){
                //circumcenter of triangle in 2d
                std::vector<weighted_point> tri_points;
                DataPoint p=data_points->at(simplex.getVertex(0));
                tri_points.push_back(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()));
                p=data_points->at(simplex.getVertex(1));
                tri_points.push_back(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()));
                p=data_points->at(simplex.getVertex(2));
                tri_points.push_back(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()));
                Kernel::Construct_weighted_circumcenter_2 compute_center_object = kernel.construct_weighted_circumcenter_2_object();
                CPoint2 center = compute_center_object(tri_points[0],tri_points[1],tri_points[2]);
                euclidean_circumcenter.push_back(center.x()); 
                euclidean_circumcenter.push_back(center.y());
            }else{
                if(!on_standard_simplex){
                    //circumcenter of tetrahedron in 3d
                    std::vector<weighted_point3> tetra_points;
                    DataPoint p=data_points->at(simplex.getVertex(0));
                    tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    p=data_points->at(simplex.getVertex(1));
                    tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    p=data_points->at(simplex.getVertex(2));
                    tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    p=data_points->at(simplex.getVertex(3));
                    tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    Kernel::Construct_weighted_circumcenter_3 compute_center_object = kernel.construct_weighted_circumcenter_3_object();
                    CPoint3 center = compute_center_object(tetra_points[0],tetra_points[1],tetra_points[2],tetra_points[3]);
                    euclidean_circumcenter.push_back(center.x()); 
                    euclidean_circumcenter.push_back(center.y());
                    euclidean_circumcenter.push_back(center.z());
                }else{
                    //circumcenter of triangle in 3d
                    std::vector<weighted_point3> tri_points;
                    DataPoint p=data_points->at(simplex.getVertex(0));
                    tri_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    p=data_points->at(simplex.getVertex(1));
                    tri_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    p=data_points->at(simplex.getVertex(2));
                    tri_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                    Kernel::Construct_weighted_circumcenter_3 compute_center_object = kernel.construct_weighted_circumcenter_3_object();
                    CPoint3 center = compute_center_object(tri_points[0],tri_points[1],tri_points[2]);
                    euclidean_circumcenter.push_back(center.x()); 
                    euclidean_circumcenter.push_back(center.y());
                    euclidean_circumcenter.push_back(center.z());
                }
            }
            if(type=="fisher"){
            //Fisher geometry
                if(!on_standard_simplex){
                    /*std::vector<DataPoint> fisher_points;
                    for(int j=0; j<simplex.getDim()+1; j++){
                        DataPoint p = input_points->at(simplex.getVertex(j));
                        std::vector<exact> sqrt_data_point;
                        for(int k=0; k<p.getDim(); k++){
                            //transform x->sqrt(2x)
                            sqrt_data_point.push_back(std::sqrt(2.*p.getCoordinate_double(k)));
                        }
                        DataPoint fisher_point(sqrt_data_point,0);
                        fisher_points.push_back(fisher_point);
                    }*/
                    if(!dim3){
                        std::vector<CPoint2> cpoints;
                        for(int j=0; j<simplex.getDim()+1; j++){
                            DataPoint fisher_point = bregman_points[simplex.getVertex(j)];
                            cpoints.push_back(CPoint2(fisher_point.getX(),fisher_point.getY()));
                        }
                        CPoint2 center = CGAL::circumcenter(cpoints[0],cpoints[1],cpoints[2]);
                        circumcenter.push_back(center.x()); 
                        circumcenter.push_back(center.y());
                    }else{
                        std::vector<CPoint3> cpoints;
                        for(int j=0; j<simplex.getDim()+1; j++){
                            DataPoint fisher_point = bregman_points[simplex.getVertex(j)];
                            cpoints.push_back(CPoint3(fisher_point.getX(),fisher_point.getY(),fisher_point.getZ()));
                        }
                        CPoint3 center = CGAL::circumcenter(cpoints[0],cpoints[1],cpoints[2],cpoints[3]);
                        circumcenter.push_back(center.x()); 
                        circumcenter.push_back(center.y());
                        circumcenter.push_back(center.z());
                    }
                }else{
                    //compute circumcenter of triangle in R^3
                    std::vector<CPoint3> tri_points;
                    tri_points.push_back(CPoint3(data_points->at(simplex.getVertex(0)).getX(),data_points->at(simplex.getVertex(0)).getY(),data_points->at(simplex.getVertex(0)).getZ()));
                    tri_points.push_back(CPoint3(data_points->at(simplex.getVertex(1)).getX(),data_points->at(simplex.getVertex(1)).getY(),data_points->at(simplex.getVertex(1)).getZ()));
                    tri_points.push_back(CPoint3(data_points->at(simplex.getVertex(2)).getX(),data_points->at(simplex.getVertex(2)).getY(),data_points->at(simplex.getVertex(2)).getZ()));
                    CPoint3 euclidean_tricenter = CGAL::circumcenter(tri_points[0],tri_points[1],tri_points[2]);

                    //scale (origin,tricenter)-vector to radius of sphere
                    CGAL::Vector_3<Kernel> vect_origin_tricenter = CGAL::Vector_3<Kernel>(CPoint3(0.,0.,0.),euclidean_tricenter);
                    CGAL::Vector_3<Kernel> vect_spherical_tricenter = vect_origin_tricenter*(std::sqrt(2./CGAL::to_double(vect_origin_tricenter.squared_length())));
                    CPoint3 spherical_tricenter = CPoint3(vect_spherical_tricenter.x(),vect_spherical_tricenter.y(),vect_spherical_tricenter.z());
                    circumcenter.push_back(spherical_tricenter.x());
                    circumcenter.push_back(spherical_tricenter.y());
                    circumcenter.push_back(spherical_tricenter.z());
                }
            }else if(type == "Shannon"){
                std::pair<double, vec> circumsphere_r_c = compute_circumradius_bregman(&simplex,!primal);
                if(std::isnan(circumsphere_r_c.first)){
                    //circumcenter.push_back(-1);circumcenter.push_back(-1);circumcenter.push_back(-1);
                    circumcenter.push_back(-1);circumcenter.push_back(-1);
                    if(dim3){
                        circumcenter.push_back(-1);
                    }
                }else if(!primal){
                    //conjugate back
                    if(on_standard_simplex){
                        auto BR = BregmanFunctions::KL_simp_conj();
                        circumsphere_r_c.second = BR.F_prime(circumsphere_r_c.second);
                    }else{
                        auto BR = BregmanFunctions::KL();
                        circumsphere_r_c.second = BR.F_conj_prime(circumsphere_r_c.second);
                    }
                    for(int i=0; i<circumsphere_r_c.second.size(); i++){
                        circumcenter.push_back(circumsphere_r_c.second[i]);
                    }
                }else{
                    for(int i=0; i<circumsphere_r_c.second.size(); i++){
                        circumcenter.push_back(circumsphere_r_c.second[i]);
                    }
                }
            }else{
                return; //no culling in other cases
            }
            if(print){
                std::cout << i << ",";
                std::vector<int> vertices = simplex.getVertices();
                for(int j=0; j<vertices.size(); j++){
                    std::cout  << " " << vertices[j];
                }
                std::cout << ",";
                for(int j=0; j<circumcenter.size(); j++){
                    std::cout  << " " << circumcenter[j];
                }
                std::cout << ",";
                for(int j=0; j<euclidean_circumcenter.size(); j++){
                    std::cout  << " " << euclidean_circumcenter[j];
                }
                std::cout << std::endl;
            }
            if(restrict_to_domain){
                bool outside = false;
                for(int j=0; j<circumcenter.size(); j++){
                    if(circumcenter[j]<=0){
                        outside = true;
                        break;
                    }
                }
                if(outside){
                    simplices_to_delete.push_back(simplex.getIndex());
                    if(print){
                        std::cout << "DELETE "; simplex.print();
                    }
                }
            }
        }
    }
    
    if(restrict_to_domain){
        //problem: inaccuracies may cause that a simplex should be deleted but not one of its cofaces -> do not delete it 
        Wrap* wrap = getWrapComplex();
        std::vector<Interval>* intervals = wrap->getIntervals();
        bool check=true;
        while(check){
            check= false;
            for(std::vector<int>::iterator it = simplices_to_delete.begin(); it!=simplices_to_delete.end();){
                bool not_delete = false;
                Simplex simplex = simplices_ptr->at(*it);
                Interval interval = (intervals->at(wrap->getIntervalIndex(simplex.getIndex())));
                std::vector<int> interval_simplices = interval.getSimplices();
                std::set<int> max_interval_cofaces;
                for(int i=0; i<interval_simplices.size(); i++){
                    Simplex interval_simplex = simplices_ptr->at(interval_simplices[i]);
                    std::set<int> cofaces = getCofacesIndices(interval_simplices[i],simplices_ptr);
                    for(std::set<int>::iterator it2 = cofaces.begin(); it2!=cofaces.end(); ++it2){
                        if((simplices_ptr->at(*it2).getDim()==2&&(!dim3||on_standard_simplex))||(simplices_ptr->at(*it2).getDim()==3&&(dim3&&!on_standard_simplex))){
                            max_interval_cofaces.insert(*it2);
                        }
                    }
                }
                for(std::set<int>::iterator it3 = max_interval_cofaces.begin(); it3!=max_interval_cofaces.end(); ++it3){
                    if(std::find(simplices_to_delete.begin(),simplices_to_delete.end(),*it3)==simplices_to_delete.end()){
                        if(print){
                            std::cout << "COFACE "  << *it3 << " "; 
                            simplices_ptr->at(*it3).print();
                        }
                        check=true;
                        not_delete=true;
                        break;
                    }
                }
                if(not_delete){
                    if(print){
                        std::cout << "NOT DELETE " << *it << " "; 
                        simplex.print();
                    }
                    it = simplices_to_delete.erase(it); 
                }else{
                   ++it;
                }
            }
        }
            
        deleteSimplicesWithIntervals(&simplices_to_delete);
    }
}