//alpha_complex.cpp
//author: koelsboe

#include "basics.h"
#include "alpha_complex.h"

//clear
void Alpha_Complex::clear(){
    
    clear0();
    
    simplices.clear();
    data_points_simplex_indices.clear();
    critical.clear();
    interval_faces.clear();
    free_simplex_indices.clear();
    
    wrap.clear();
}

//build Alpha complex from Delaunay triangulation (computed with CGAL) for given points
bool Alpha_Complex::setPoints(std::vector<DataPoint> *new_data_points, int num_data_points_, int periodic_size_, bool dim3_, bool weighted_, bool compute_persistence, bool printstats)
{
    //std::cout << "My CGAL library is " << CGAL_VERSION_NR << " (1MMmmb1000)" << std::endl; 
    
    clear();
    
    dim3 = dim3_;
    //compute complexes in R^2/R^3 or periodic domain
    //(periodic triangulation, no boundary, domain has size periodic_size^2/3, point set repeated in all directions)
    periodic_size = periodic_size_;
    bool periodic=false;
    if(periodic_size>0){
        periodic=true;
    }   
    
    data_points = new_data_points; 
    num_data_points = num_data_points_;
    weighted = weighted_;
    
    //compute Delaunay triangulation   
    
    double time1 = getCPUTime();
    
    bool valid_delaunay = computeDelaunayTriangulation(printstats);
    if(!valid_delaunay){
        return false;
    }
    
    if(printstats){
        std::cout << " Time (Total), " << getCPUTime()-time1 << std::endl;
        std::cout << " # points, " << num_data_points << std::endl;
        std::cout << "COMPUTE ALPHA COMPLEX" << std::endl;
        time1 = getCPUTime();
    }
    
    //double time_after_del = getCPUTime(); 
    
    //compute Alpha complex and intervals
    
    computeAlphaComplex();
    
    if(printstats){
        std::cout << " Time, " << getCPUTime()-time1 << std::endl;
    }
    
    //compute Wrap complex
    //double time1_wrap = getCPUTime(); 
    
    computeWrapAndPersistence(compute_persistence,printstats);
    
    double time2 = getCPUTime();
    
    
    if(compute_persistence){ 
        std::cout << "TIME FOR COMPUTING COMPLEXES AND PERSISTENCE, " << time2-time1 << std::endl;
    }else{
        //std::cout << "TIME FOR COMPUTING WRAP, " << time2-time1_wrap << std::endl; 
        //std::cout << "TIME FOR COMPUTING ALPHA AND WRAP, " << time2-time_after_del << std::endl; 
        std::cout << "TIME FOR NEWLY COMPUTING COMPLEXES, " << time2-time1 << std::endl;
    }
    
    //TEST 
    //wrap.printIntervals(); 
    //std::cout << "ALPHA FILTRATION" << std::endl;
    //filtration_current.print(simplices_ptr);
    
    
    
    return true;
}

//compute Delaunay triangulation for given point set
bool Alpha_Complex::computeDelaunayTriangulation(std::vector<DataPoint> *new_data_points, int num_data_points_, int periodic_size_, bool dim3_, bool weighted_, bool printstats)
{
    clear();
    
    dim3 = dim3_;
    //compute complexes in R^2/R^3 or periodic domain
    //(periodic triangulation, no boundary, domain has size periodic_size^2/3, point set repeated in all directions)
    periodic_size = periodic_size_;
    bool periodic=false;
    if(periodic_size>0){
        periodic=true;
    }   
    
    data_points = new_data_points; 
    num_data_points = num_data_points_;
    weighted = weighted_;
    
    //compute Delaunay triangulation   
    
    double time1 = getCPUTime();
    
    return computeDelaunayTriangulation(printstats);
}

//compute Delaunay triangulation for current point set, with CGAL, store simplices
bool Alpha_Complex::computeDelaunayTriangulation(bool printstats)
{
    bool periodic=false;
    if(periodic_size>0){
        periodic=true;
    }  
    
    int tri_dimension = 2+dim3; //intrinsic dimension of the triangulation, can be lower than dimension of points (e.g. standard 2-simplex in 3d)
    
    double time1 = getCPUTime();
    
   //clear old data
    delaunay_tri.clear(); //data structure for Delaunay triangulation (computed with CGAL)
    periodic_delaunay_tri.clear(); //data structure for periodic Delaunay triangulation (computed with CGAL)
    delaunay_tri3.clear(); //3-dim Delaunay triangulation  (computed with CGAL)
    periodic_delaunay_tri3.clear(); //3-dim periodic Delaunay triangulation  (computed with CGAL)
    weighted_delaunay_tri.clear(); //weighted Delaunay triangulation
    weighted_delaunay_tri3.clear(); //weighted 3-dim Delaunay triangulation
    vertex_handles.clear();
    vertex_handles_periodic.clear();
    vertex_handles3.clear();
    vertex_handles_periodic3.clear();
    vertex_handles_weighted.clear();
    vertex_handles_weighted3.clear();
    
    if(!dim3){ //2-dimensional
        //normal, in R^2
        if(!periodic){
            if(!weighted){
                //create list of points that CGAL can use
                std::vector<std::pair<CPoint2,int> > cpoints;    
                for (int i=0; i<data_points->size(); i++)
                {
                    //every point contains CGAL 2D-point with index
                    DataPoint p=data_points->at(i);
                    if(!p.isDeleted()){
                        cpoints.push_back( std::make_pair(CPoint2(p.getX(),p.getY()),i) );
                    }
                };
                delaunay_tri.insert(cpoints.begin(),cpoints.end()); //compute new triangulation
                vertex_handles = std::vector<Delaunay_triangulation::Vertex_handle>(data_points->size()); //initialize list of vertex handles
            }else{
                //weighted
                std::vector<std::pair<weighted_point,int>> cpoints_weighted;
                for(int i=0; i<data_points->size(); i++){
                    DataPoint p = data_points->at(i);
                    if(!p.isDeleted()){
                        cpoints_weighted.push_back( std::make_pair(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()),i));
                    }
                }
                
                weighted_delaunay_tri.insert(cpoints_weighted.begin(),cpoints_weighted.end()); 
                //in CGAL4.12 it does not work to insert all points at once, because spatial sorting does not work
                //pull https://github.com/CGAL/cgal/pull/3122/files fixed the issue
                //for(int i=0; i<cpoints_weighted.size(); i++){
                //    weighted_delaunay_tri.insert(cpoints_weighted.begin()+i,cpoints_weighted.begin()+i+1);
                //}
                
                vertex_handles_weighted = std::vector<weighted_Delaunay_triangulation::Vertex_handle>(data_points->size()); //initialize list of vertex handles
            }
        //periodic 2-dim
        }else{ 
            //set domain for periodic triangulation: square of size periodic_size^2
            periodic_Delaunay_triangulation::Iso_rectangle domain(0,0,periodic_size,periodic_size); 
            periodic_delaunay_tri.set_domain(domain);
            //create list of points that CGAL can use
            std::vector<std::pair<periodic_point,int> > cpoints;    
            for (int i=0; i<data_points->size(); i++)
            {
                //every point contains CGAL 2D-point with index
                DataPoint p=data_points->at(i);
                if(!p.isDeleted()){
                    cpoints.push_back(std::make_pair(periodic_point(p.getX(),p.getY()),i));    
                }
            }      
            periodic_delaunay_tri.insert(cpoints.begin(),cpoints.end()); //insert points = compute
            periodic_delaunay_tri.convert_to_1_sheeted_covering(); //convert to 1-sheeted covering, originally 9-sheeted, 9 copies of everything
            //check if triangulation is valid, if Delaunay triangulation could be computed on torus (in 1 sheet, not always possible)
            if(!periodic_delaunay_tri.is_valid() || !periodic_delaunay_tri.is_triangulation_in_1_sheet()){
                std::cerr << "ERROR (Alpha_Complex::setPoints): No periodic Delaunay triangulation!" << std::endl;
                data_points->clear();
                return false;
            }
            vertex_handles_periodic = std::vector<periodic_Delaunay_triangulation::Vertex_handle>(data_points->size()); //initialize list of vertex handles
        }
    }else{ //3-dimensional
        if(!periodic){
            //create list of points that CGAL can use
            if(!weighted){
                std::vector<std::pair<CPoint3,int> > cpoints;    
                for (int i=0; i<data_points->size(); i++){
                    //every point contains CGAL 3D-point with index
                    DataPoint p=data_points->at(i);
                    if(!p.isDeleted()){
                        cpoints.push_back( std::make_pair(CPoint3(p.getX(),p.getY(),p.getZ()),i) );
                    }
                }
                delaunay_tri3.insert(cpoints.begin(),cpoints.end()); //compute new triangulation
                vertex_handles3 = std::vector<Delaunay_triangulation3::Vertex_handle>(data_points->size()); //initialize list of vertex handles
                tri_dimension = delaunay_tri3.dimension();
            }else{
                std::vector<std::pair<weighted_point3,int> > cpoints_weighted;    
                for (int i=0; i<data_points->size(); i++){
                    DataPoint p=data_points->at(i);
                    if(!p.isDeleted()){
                        cpoints_weighted.push_back( std::make_pair(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()),i));
                    }
                }
                weighted_delaunay_tri3.insert(cpoints_weighted.begin(),cpoints_weighted.end()); //compute new triangulation
                vertex_handles_weighted3 = std::vector<weighted_Delaunay_triangulation3::Vertex_handle>(data_points->size()); //initialize list of vertex handles
                tri_dimension = weighted_delaunay_tri3.dimension();
            }
        //3-dim periodic
        }else{  
            //set domain for periodic triangulation: cube of size periodic_size^3
            periodic_Delaunay_triangulation3::Iso_cuboid domain(0,0,0,periodic_size,periodic_size,periodic_size); 
            periodic_delaunay_tri3.set_domain(domain);
            //create list of points that CGAL can use
            std::vector<std::pair<periodic_point3,int> > cpoints; 
            for (int i=0; i<data_points->size(); i++){
                //every point contains CGAL 3D-point with index
                DataPoint p=data_points->at(i);
                //cpoints.push_back( std::make_pair(periodic_point3(p->getX(),p->getY(),p->getZ()),p->getIndex()) );   
                if(!p.isDeleted()){
                    periodic_Delaunay_triangulation3::Vertex_handle vert = periodic_delaunay_tri3.insert(periodic_point3(p.getX(),p.getY(),p.getZ()));
                    vert->info()=i;
                }
            }      
            //http://cgal-discuss.949826.n4.nabble.com/How-to-insert-points-with-info-in-Periodic-3-Delaunay-triangulation-3-td4431410.html
            //periodic_delaunay_tri3.insert(cpoints.begin(),cpoints.end()); //inserting all points at once does not work for 3d periodic!!
            periodic_delaunay_tri3.convert_to_1_sheeted_covering(); //convert to 1-sheeted covering, originally 27-sheeted, 27 copies of everything
            //check if triangulation is valid, if Delaunay triangulation could be computed on torus (in 1 sheet, not always possible)
            if(!periodic_delaunay_tri3.is_valid() || !periodic_delaunay_tri3.is_triangulation_in_1_sheet()){
                std::cerr << "ERROR (Alpha_Complex::setPoints): No periodic Delaunay triangulation!" << std::endl;
                data_points->clear();
                return false;
            } 
            vertex_handles_periodic3 = std::vector<periodic_Delaunay_triangulation3::Vertex_handle>(data_points->size()); //initialize list of vertex handles
        }
    }
    if(printstats){
        std::cout << "COMPUTE DELAUNAY TRIANGULATION " << std::endl;
        std::cout << " Time (CGAL), " << getCPUTime()-time1 << std::endl;
    }
    
    //create list of Delaunay simplices stored in own data structure
    //-------------------------------------------------------------------------
    simplices.clear();
    
    //store empty simplex, put into filtration
    simplices.push_back(Simplex(0)); 
    filtration_current.appendElement(-1,0);
    
    //store points (which are not hidden)
    data_points_simplex_indices = std::vector<int>(data_points->size(),-1);
    if(!dim3){
        if(!periodic){
            //iterate through non-hidden vertices, add to list of simplices, store vertex handles
            if(!weighted){
                for(Delaunay_triangulation::Finite_vertices_iterator vit = delaunay_tri.finite_vertices_begin(); vit != delaunay_tri.finite_vertices_end(); ++vit){
                    int simplex_index = simplices.size();
                    Simplex point(simplex_index,vit->info());
                    data_points_simplex_indices[vit->info()]=simplex_index;
                    vertex_handles[vit->info()]=vit;
                    simplices.push_back(point);
                }
            }else{
                //weighted
                for(weighted_Delaunay_triangulation::Finite_vertices_iterator vit = weighted_delaunay_tri.finite_vertices_begin(); vit != weighted_delaunay_tri.finite_vertices_end(); ++vit){                   
                    int simplex_index = simplices.size();
                    Simplex point(simplex_index,vit->info());
                    data_points_simplex_indices[vit->info()]=simplex_index;
                    vertex_handles_weighted[vit->info()]=vit;
                    simplices.push_back(point);
                }
            }
        }else{
            for(periodic_Delaunay_triangulation::Finite_vertices_iterator vit = periodic_delaunay_tri.finite_vertices_begin(); vit != periodic_delaunay_tri.finite_vertices_end(); ++vit){
                int simplex_index = simplices.size();
                Simplex point(simplex_index,vit->info());
                data_points_simplex_indices[vit->info()]=simplex_index;
                vertex_handles_periodic[vit->info()]=vit;
                simplices.push_back(point);
            }
        }
    }else{
        if(!periodic){
            if(!weighted){
                for(Delaunay_triangulation3::Finite_vertices_iterator vit = delaunay_tri3.finite_vertices_begin(); vit != delaunay_tri3.finite_vertices_end(); ++vit){
                    int simplex_index = simplices.size();
                    Simplex point(simplex_index,vit->info());
                    data_points_simplex_indices[vit->info()]=simplex_index;
                    vertex_handles3[vit->info()]=vit;
                    simplices.push_back(point);
                }
            }else{
                for(weighted_Delaunay_triangulation3::Finite_vertices_iterator vit = weighted_delaunay_tri3.finite_vertices_begin(); vit != weighted_delaunay_tri3.finite_vertices_end(); ++vit){
                    int simplex_index = simplices.size();
                    Simplex point(simplex_index,vit->info());
                    data_points_simplex_indices[vit->info()]=simplex_index;
                    vertex_handles_weighted3[vit->info()]=vit;
                    simplices.push_back(point);
                }
            }
        }else{
            for(periodic_Delaunay_triangulation3::Finite_vertices_iterator vit = periodic_delaunay_tri3.finite_vertices_begin(); vit != periodic_delaunay_tri3.finite_vertices_end(); ++vit){
                int simplex_index = simplices.size();
                Simplex point(simplex_index,vit->info());
                data_points_simplex_indices[vit->info()]=simplex_index;
                vertex_handles_periodic3[vit->info()]=vit;
                simplices.push_back(point);
            }
        }
    }
    //points which are not part of Delaunay triangulation are "hidden"
    for(int i=0; i<data_points->size(); i++){
        if(data_points_simplex_indices[i]<0){
            data_points->at(i).setHidden(true);
        }
    }
    
    std::vector<Simplex> edges0; 
    
    if(!dim3 || (tri_dimension==2 && !periodic)){ //2-dimensional (either in R^2 or degenerated triangulation 3d (e.g. standard simplex))
        //by iterating through Delaunay triangles create list of edges (index of triangle saved), not unique yet     
        
        //in R^2
        if(!dim3){
            if(!periodic){
                if(!weighted){

                    for(Delaunay_triangulation::Finite_faces_iterator fit = delaunay_tri.finite_faces_begin();
                        fit != delaunay_tri.finite_faces_end(); ++fit) {
                        Delaunay_triangulation::Face_handle face = fit; //CGAL face

                        //get vertex indices
                        std::vector<int> vertices;
                        for(int i=0; i<3; i++){ 
                            vertices.push_back(face->vertex(i)->info()); 
                        }
                        std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

                        //save triangle
                        Simplex triangle(simplices.size(),vertices[0],vertices[1],vertices[2]);
                        simplices.push_back(triangle);

                        //save 3 edges together with index of triangle to list edges0
                        for(int i = 0; i<3; i++){
                            int a = vertices[i%3]; //first edge vertex
                            int b = vertices[(i+1)%3]; //second edge vertex
                            Simplex edge(edges0.size(),a,b);
                            edge.addCofacetIndex(triangle.getIndex());
                            edges0.push_back(edge);
                        }
                    }
                }else{
                    //weighted
                    for(weighted_Delaunay_triangulation::Finite_faces_iterator fit = weighted_delaunay_tri.finite_faces_begin();
                        fit != weighted_delaunay_tri.finite_faces_end(); ++fit) {
                        weighted_Delaunay_triangulation::Face_handle face = fit; //CGAL face

                        //get vertex indices
                        std::vector<int> vertices;
                        for(int i=0; i<3; i++){ 
                            vertices.push_back(face->vertex(i)->info()); 
                        }
                        std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

                        //save triangle
                        Simplex triangle(simplices.size(),vertices[0],vertices[1],vertices[2]);
                        simplices.push_back(triangle);

                        //save 3 edges together with index of triangle to list edges0
                        for(int i = 0; i<3; i++){
                            int a = vertices[i%3]; //first edge vertex
                            int b = vertices[(i+1)%3]; //second edge vertex
                            Simplex edge(edges0.size(),a,b);
                            edge.addCofacetIndex(triangle.getIndex());
                            edges0.push_back(edge);
                        }
                    }
                }
            //periodic
            }else{
                for(periodic_Delaunay_triangulation::Finite_faces_iterator fit = periodic_delaunay_tri.finite_faces_begin();
                    fit != periodic_delaunay_tri.finite_faces_end(); ++fit) {
                    periodic_Delaunay_triangulation::Face_handle face = fit; //CGAL face

                    //get vertex indices and their offsets
                    std::vector<std::pair<int,periodic_point_offset> > vertices_with_offsets;
                    for(int i=0; i<3; i++){ 
                        vertices_with_offsets.push_back(std::make_pair(face->vertex(i)->info(),periodic_delaunay_tri.periodic_point(face,i)));
                    }
                    //vertex indices should be sorted for all simplices
                    //offsets need to be sorted together with respective indices!!
                    std::sort(vertices_with_offsets.begin(),vertices_with_offsets.end());

                    //new triangle
                    Simplex triangle(simplices.size(),vertices_with_offsets[0].first,vertices_with_offsets[1].first,vertices_with_offsets[2].first);

                    //store vertex offsets (if any vertex is not in original domain but in neighboring copy)
                    for(int i=0; i<3; i++){ 
                        periodic_point_offset ppoint = vertices_with_offsets[i].second;
                        if(ppoint.second.x()>0 || ppoint.second.y()>0){ //if vertex has any offset
                            Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0);
                            triangle.setOffset(i,off);
                        }   
                    }

                    //save triangle
                    simplices.push_back(triangle);

                    //save 3 edges together with index of triangle to list edges0
                    for(int i = 0; i<3; i++){
                        int a = vertices_with_offsets[i%3].first; //first edge vertex
                        int b = vertices_with_offsets[(i+1)%3].first; //second edge vertex
                        Simplex edge(edges0.size(),a,b);
                        edge.addCofacetIndex(triangle.getIndex());
                        //set vertex offsets
                        periodic_point_offset ppoint0, ppoint1;
                        if(a<b){ //vertices[a] is first vertex
                            ppoint0 = vertices_with_offsets[i%3].second;
                            ppoint1 = vertices_with_offsets[(i+1)%3].second;   
                        }else{ //vertices[b] is first vertex
                            ppoint1 = vertices_with_offsets[i%3].second;
                            ppoint0 = vertices_with_offsets[(i+1)%3].second;                   
                        } 
                        if(ppoint0.second.x()>0 || ppoint0.second.y()>0){ //if first vertex has offset
                            Offset off = Offset(ppoint0.second.x()>0, ppoint0.second.y()>0);
                            edge.setOffset(0,off);
                        }
                        if(ppoint1.second.x()>0 || ppoint1.second.y()>0){ //if second vertex has offset
                            Offset off = Offset(ppoint1.second.x()>0, ppoint1.second.y()>0);
                            edge.setOffset(1,off);
                        }  
                        edges0.push_back(edge);
                    }
                }
            }
        //degenerate triangulation in R^3
        }else{
            if(!weighted) {
                for(Delaunay_triangulation3::Finite_facets_iterator fit = delaunay_tri3.finite_facets_begin();
                    fit != delaunay_tri3.finite_facets_end(); ++fit) {
                      
                    //get vertex indices (stored in corresponding cell), facet is (cell,index)-pair
                    Delaunay_triangulation3::Cell_handle facet = fit->first; 
                    int index = fit->second;
                    std::vector<int> vertices;
                    for(int i=0; i<3; i++){ 
                        vertices.push_back(facet->vertex(delaunay_tri3.vertex_triple_index(index, i))->info()); 
                    }
                    std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

                    //save triangle
                    Simplex triangle(simplices.size(),vertices[0],vertices[1],vertices[2]);
                    simplices.push_back(triangle);

                    //save 3 edges together with index of triangle to list edges0
                    for(int i = 0; i<3; i++){
                        int a = vertices[i%3]; //first edge vertex
                        int b = vertices[(i+1)%3]; //second edge vertex
                        Simplex edge(edges0.size(),a,b);
                        edge.addCofacetIndex(triangle.getIndex());
                        edges0.push_back(edge);
                    }
                }
            }else{
                //weighted
                for(weighted_Delaunay_triangulation3::Finite_facets_iterator fit = weighted_delaunay_tri3.finite_facets_begin();
                    fit != weighted_delaunay_tri3.finite_facets_end(); ++fit) {

                    //get vertex indices (stored in corresponding cell), facet is (cell,index)-pair
                    weighted_Delaunay_triangulation3::Cell_handle facet = fit->first; 
                    int index = fit->second;
                    std::vector<int> vertices;
                    for(int i=0; i<3; i++){ 
                        vertices.push_back(facet->vertex(weighted_delaunay_tri3.vertex_triple_index(index, i))->info()); 
                    }
                    std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

                    //save triangle
                    Simplex triangle(simplices.size(),vertices[0],vertices[1],vertices[2]);
                    simplices.push_back(triangle);

                    //save 3 edges together with index of triangle to list edges0
                    for(int i = 0; i<3; i++){
                        int a = vertices[i%3]; //first edge vertex
                        int b = vertices[(i+1)%3]; //second edge vertex
                        Simplex edge(edges0.size(),a,b);
                        edge.addCofacetIndex(triangle.getIndex());
                        edges0.push_back(edge);
                    }
                }
            }
        }
    }else{ //3-dim case
        std::vector<Simplex> triangles0;
        //by iterating through Delaunay tetrahedra create list of triangles (index of tetrahedron saved), not unique yet    
        if(!periodic){
            if(!weighted){
                for(Delaunay_triangulation3::Finite_cells_iterator cit = delaunay_tri3.finite_cells_begin();
                        cit != delaunay_tri3.finite_cells_end(); ++cit){

                    Delaunay_triangulation3::Cell_handle cell = cit; //CGAL cell (tetrahedron)

                    //get vertex indices             
                    std::vector<int> vertices;
                    for(int i=0; i<4; i++){
                        vertices.push_back(cell->vertex(i)->info());
                    }
                    std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

                    //new tetrahedron
                    Simplex tetrahedron(simplices.size(),vertices[0],vertices[1],vertices[2],vertices[3]);
                    simplices.push_back(tetrahedron);

                    //save 4 triangles together with index of tetrahedron to list triangles0
                    for(int i=0; i<4; i++){
                        //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                        std::vector<int> triangle_vertices;
                        for(int j=0; j<4; j++){
                            if(j!=3-i){
                                triangle_vertices.push_back(vertices[j]);
                            }
                        }                   
                        Simplex triangle(triangles0.size(),triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                        triangle.addCofacetIndex(tetrahedron.getIndex());
                        triangles0.push_back(triangle);
                    }
                }   
            }else{
                //weighted
                for(weighted_Delaunay_triangulation3::Finite_cells_iterator cit = weighted_delaunay_tri3.finite_cells_begin();
                        cit != weighted_delaunay_tri3.finite_cells_end(); ++cit){

                    weighted_Delaunay_triangulation3::Cell_handle cell = cit; //CGAL cell (tetrahedron)

                    //get vertex indices             
                    std::vector<int> vertices;
                    for(int i=0; i<4; i++){
                        vertices.push_back(cell->vertex(i)->info());
                    }
                    std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

                    //new tetrahedron
                    Simplex tetrahedron(simplices.size(),vertices[0],vertices[1],vertices[2],vertices[3]);
                    //save tetrahedron (except for points on 2-simplex)
                    simplices.push_back(tetrahedron);

                    //save 4 triangles together with index of tetrahedron to list triangles0
                    for(int i=0; i<4; i++){
                        //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                        std::vector<int> triangle_vertices;
                        for(int j=0; j<4; j++){
                            if(j!=3-i){
                                triangle_vertices.push_back(vertices[j]);
                            }
                        }                   
                        Simplex triangle(triangles0.size(),triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                        triangle.addCofacetIndex(tetrahedron.getIndex());
                        triangles0.push_back(triangle);
                    }
                }
            }
        //periodic
        }else{
            
            for(periodic_Delaunay_triangulation3::Finite_cells_iterator cit = periodic_delaunay_tri3.finite_cells_begin();
                    cit != periodic_delaunay_tri3.finite_cells_end(); ++cit){
                
                periodic_Delaunay_triangulation3::Cell_handle cell = cit; //CGAL cell (tetrahedron)
                
                //get vertex indices and their offsets          
                std::vector<int> vertices;
                std::vector<std::pair<int,periodic_point_offset3> > vertices_with_offsets;
                for(int i=0; i<4; i++){
                    vertices_with_offsets.push_back(std::make_pair(cell->vertex(i)->info(),periodic_delaunay_tri3.periodic_point(cell,i)));
                }
                //vertex indices should be sorted for all simplices
                //offsets need to be sorted together with respective indices!!
                std::sort(vertices_with_offsets.begin(),vertices_with_offsets.end());          
                
                //new tetrahedron
                Simplex tetrahedron(simplices.size(),vertices_with_offsets[0].first,vertices_with_offsets[1].first,vertices_with_offsets[2].first,vertices_with_offsets[3].first);              
                
                //store vertex offsets (if any vertex is not in original domain but in neighboring copy)
                for(int i=0; i<4; i++){ 
                    periodic_point_offset3 ppoint = vertices_with_offsets[i].second;
                    if(ppoint.second.x()>0 || ppoint.second.y()>0 || ppoint.second.z()>0){ //if vertex has any offset
                        Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0, ppoint.second.z()>0);
                        tetrahedron.setOffset(i,off);
                    }  
                }
                
                //save tetrahedron 
                simplices.push_back(tetrahedron);
                
                //save 4 triangles together with index of tetrahedron to list triangles0
                for(int i=0; i<4; i++){
                    //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                    std::vector<std::pair<int,periodic_point_offset3> > triangle_vertices;
                    for(int j=0; j<4; j++){
                        if(j!=3-i){
                            triangle_vertices.push_back(vertices_with_offsets[j]);
                        }
                    }                   
                    Simplex triangle(triangles0.size(),triangle_vertices[0].first,triangle_vertices[1].first,triangle_vertices[2].first);
                    triangle.addCofacetIndex(tetrahedron.getIndex());
                    
                    //set vertex offsets
                    for(int j=0; j<3; j++){
                        periodic_point_offset3 ppoint = triangle_vertices[j].second;
                        Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0, ppoint.second.z()>0);
                        triangle.setOffset(j,off);
                    }
                    triangles0.push_back(triangle);
                }
            }          
        }
        //sort list of (not unique) triangles
        //for periodic triangulation: triangles with less offset vertices first          
        std::sort(triangles0.begin(),triangles0.end()); 
           
        //create unique list of triangles with all incident tetrahedra
        for(int i=0; i<triangles0.size(); i++){
            Simplex triangle = triangles0[i];
            std::vector<Offset> vertex_offsets;
            for(int j=0; j<3; j++){
                vertex_offsets.push_back(triangle.getOffset(j));
            }
                       
            //check if there are more incident tetrahedra (every triangle has one or two, exactly 2 for periodic triangulation) 
            bool checknext=true;
            while((i+1)<triangles0.size() && checknext){
                //if next list element is same triangle
                if(triangle.sameSimplex(triangles0[i+1])){
                
                    int tetrahedron_ind = triangles0[i+1].getCofacetIndices()[0];
                    triangle.addCofacetIndex(tetrahedron_ind); //save second incident tetrahedron
                    if(periodic){
                    //check if the periodic triangulation is valid, triangle can appear with different offsets, but always needs to have same geometry,
                    //offsets must differ the same way for all vertices, same shift e.g. triangle (010 110 000) + x -> (011 111 001) is allowed
                        std::vector<Offset> other_vertex_offsets;
                        for(int j=0; j<3; j++){
                            other_vertex_offsets.push_back(triangles0[i+1].getOffset(j));
                        }
                        int shift_x = vertex_offsets[0].getX() - other_vertex_offsets[0].getX();
                        int shift_y = vertex_offsets[0].getY() - other_vertex_offsets[0].getY();
                        int shift_z = vertex_offsets[0].getZ() - other_vertex_offsets[0].getZ();
                        for(int j=1; j<3; j++){
                            bool incompatible = false;
                            if(shift_x != (vertex_offsets[j].getX() - other_vertex_offsets[j].getX())){
                                incompatible = true;
                            }
                            if(shift_y != (vertex_offsets[j].getY() - other_vertex_offsets[j].getY())){
                                incompatible = true;
                            }
                            if(shift_z != (vertex_offsets[j].getZ() - other_vertex_offsets[j].getZ())){
                                incompatible = true;
                            }
                            if(incompatible){
                                //triangles with same vertices have incompatible offsets
                                std::cerr << "ERROR (Alpha_Complex::setPoints): triangle appears with incompatible offsets (different geometry) in different tetrahedra" << std::endl;
                                data_points->clear();
                                simplices.clear();
                                return false;
                            }
                        }
                    }
                    i++; //go to next triangle
                }else{
                    checknext=false;
                }         
            }
            if(triangle.getCofacetIndices().size()>2){
                std::cerr << "ERROR (Alpha_Complex::setPoints): triangle has more than two incident tetrahedra" << std::endl;
                //triangulation was not valid, triangle appears with incompatible offsets (different geometry) in different tetrahedra
                data_points->clear();
                simplices.clear();
                return false;
            }
            int triangle_index = simplices.size();
            triangle.setIndex(triangle_index);
            simplices.push_back(triangle); //save triangle
            
            //save triangle index in incident tetrahedra
            std::vector<int> inci_tetrahedra = triangle.getCofacetIndices();
            for(std::vector<int>::iterator it2 = inci_tetrahedra.begin(); it2 < inci_tetrahedra.end(); ++it2){
                simplices[*it2].addFacetIndex(triangle_index);
            }
            
            //save 3 edges together with index of triangle to list edges0
            for(int i = 0; i<3; i++){
                int a = triangle.getVertex(i%3); //first edge vertex
                int b = triangle.getVertex((i+1)%3); //second edge vertex
                Simplex edge(edges0.size(),a,b);
                edge.addCofacetIndex(triangle_index);  
                //set vertex offsets
                if(a<b){ //vertices[a] is first vertex
                    edge.setOffset(0,triangle.getOffset(i%3)); 
                    edge.setOffset(1,triangle.getOffset((i+1)%3)); 
                }else{ //vertices[b] is first vertex
                    edge.setOffset(0,triangle.getOffset((i+1)%3)); 
                    edge.setOffset(1,triangle.getOffset(i%3));                   
                } 
                edges0.push_back(edge);
            }
        }
    }
    
    //sort list of (not unique) edges
    //for periodic triangulation: edges with less offset vertices first
    std::sort(edges0.begin(),edges0.end());
    
    std::vector<Simplex> edges1; //final list of all edges, will be stored in list of simplices later

    //create unique list of edges with all incident triangles
    for(int i=0; i<edges0.size(); i++){
        Simplex edge = edges0[i];
        std::vector<Offset> vertex_offsets;
        for(int j=0; j<2; j++){
            vertex_offsets.push_back(edge.getOffset(j));
        }
        //2-dim: every edge has one or two incident triangles, exactly 2 for periodic triangulation
        //3-dim: every edge has 1-3 incident triangles
        
        //check if there are more incident triangles (in degenerate cases there can be even more than 3!!) 
        bool checknext=true;
        while((i+1)<edges0.size() && checknext){
            //if next list element is same edge
            if(edge.sameSimplex(edges0[i+1])){
                
                int triangle_ind = edges0[i+1].getCofacetIndices()[0];
                edge.addCofacetIndex(triangle_ind); //also save this incident triangle
                if(periodic){
                //check if the periodic triangulation is valid, edge can appear with different offsets, but always needs to have same geometry,
                //offsets must differ the same way for all vertices, same shift e.g. edge (010 110) + x -> (011 111) is allowed
                    std::vector<Offset> other_vertex_offsets;
                    for(int j=0; j<2; j++){
                        other_vertex_offsets.push_back(edges0[i+1].getOffset(j));
                    }
                    int shift_x = vertex_offsets[0].getX() - other_vertex_offsets[0].getX();
                    int shift_y = vertex_offsets[0].getY() - other_vertex_offsets[0].getY();
                    int shift_z = vertex_offsets[0].getZ() - other_vertex_offsets[0].getZ();
                    for(int j=1; j<2; j++){
                        bool incompatible = false;
                        if(shift_x != (vertex_offsets[j].getX() - other_vertex_offsets[j].getX())){
                            incompatible = true;
                        }
                        if(shift_y != (vertex_offsets[j].getY() - other_vertex_offsets[j].getY())){
                            incompatible = true;
                        }
                        if(shift_z != (vertex_offsets[j].getZ() - other_vertex_offsets[j].getZ())){
                            incompatible = true;
                        }
                        if(incompatible){
                            //edges with same vertices have incompatible offsets
                            std::cerr << "ERROR (Alpha_Complex::setPoints): edge appears with incompatible offsets (different geometry) in different triangles" << std::endl;
                            data_points->clear();
                            simplices.clear();
                            return false;
                        }
                    }          
                }
                i++; //go to next edge
            }else{
                checknext=false;
            }         
        }
        if(!dim3 && edge.getCofacetIndices().size()>2){
            std::cerr << "ERROR (Alpha_Complex::setPoints): edge has more than two incident triangles (dim2)" << std::endl;
            //triangulation was not valid, edge appears with incompatible offsets (different geometry) in different triangles
            data_points->clear();
            simplices.clear();
            return false;
        }
        
        edges1.push_back(edge); //save edge
    }

    //sort edges by index of first incident triangle (not by vertex indices)
    std::sort(edges1.begin(),edges1.end(),sortPSimplicesByCofacets());
    //store edges in list of simplices, save all relations
    for(int i=0; i<edges1.size(); i++){
        Simplex edge = edges1[i];
        int edge_index = simplices.size();
        edge.setIndex(edge_index);
        std::vector<int> edge_vertices = edge.getVertices();
        //store relation with facets (points)
        for(int j=0; j<edge_vertices.size(); j++){
            edge.addFacetIndex(data_points_simplex_indices[edge_vertices[j]]);
            simplices[data_points_simplex_indices[edge_vertices[j]]].addCofacetIndex(edge_index);
        }
        //store relation with cofacets (triangles)
        std::vector<int> inci_triangles_ind = edge.getCofacetIndices();
        for(std::vector<int>::iterator it2 = inci_triangles_ind.begin(); it2 < inci_triangles_ind.end(); ++it2){
            int triangle_ind = *it2;
            simplices[triangle_ind].addFacetIndex(edge_index);
        }
        simplices.push_back(edge);
    } 
    
    //store pointer to list of simplices
    simplices_ptr = &simplices;
    
    return true;
}

//compute Delaunay triangulation on standard simplex transformed to 3d weighted points
bool Alpha_Complex::computeDelaunayTriangulationOnStandardSimplex(bool fisher_metric, bool printstats)
{
    if(periodic_size>0 || !dim3){
        std::cout << "Error(Alpha_Complex::computeDelaunayTriangulationOnStandardSimplex): The input points are not on standard simplex." << std::endl;
    }

    double time1 = getCPUTime();
    
    //clear old data
    delaunay_tri.clear(); //data structure for Delaunay triangulation (computed with CGAL)
    periodic_delaunay_tri.clear(); //data structure for periodic Delaunay triangulation (computed with CGAL)
    delaunay_tri3.clear(); //3-dim Delaunay triangulation  (computed with CGAL)
    periodic_delaunay_tri3.clear(); //3-dim periodic Delaunay triangulation  (computed with CGAL)
    weighted_delaunay_tri.clear(); //weighted Delaunay triangulation
    weighted_delaunay_tri3.clear(); //weighted 3-dim Delaunay triangulation
    vertex_handles.clear();
    vertex_handles_periodic.clear();
    vertex_handles3.clear();
    vertex_handles_periodic3.clear();
    vertex_handles_weighted.clear();
    vertex_handles_weighted3.clear();
    
    std::vector<std::pair<weighted_point3,int> > cpoints_weighted;    
    for (int i=0; i<data_points->size(); i++){
        DataPoint p=data_points->at(i);
        if(!p.isDeleted()){
            cpoints_weighted.push_back( std::make_pair(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()),i));
        }
    }
    if(fisher_metric){
        //add dummy point at origin for Fisher-Delaunay on standard simplex = eight-sphere, we need simplices on convex hull
        cpoints_weighted.push_back(std::make_pair(weighted_point3(CPoint3(0,0,0),0),-2));
    }
    
    weighted_delaunay_tri3.insert(cpoints_weighted.begin(),cpoints_weighted.end()); //compute new triangulation
    vertex_handles_weighted3 = std::vector<weighted_Delaunay_triangulation3::Vertex_handle>(data_points->size()); //initialize list of vertex handles
    int tri_dimension = weighted_delaunay_tri3.dimension();
    
    if(printstats){
        std::cout << "COMPUTE DELAUNAY TRIANGULATION ON STANDARD SIMPLEX";
        if(fisher_metric){
            std::cout << " (FISHER METRIC)";
        }
        std::cout << std::endl;
        std::cout << " Time (CGAL), " << getCPUTime()-time1 << std::endl;
    }
    
    //create list of Delaunay simplices stored in own data structure
    //-------------------------------------------------------------------------
    simplices.clear();
    
    //store empty simplex, put into filtration
    simplices.push_back(Simplex(0)); 
    filtration_current.appendElement(-1,0);
    
    //store points (which are not hidden)
    data_points_simplex_indices = std::vector<int>(data_points->size(),-1);
    for(weighted_Delaunay_triangulation3::Finite_vertices_iterator vit = weighted_delaunay_tri3.finite_vertices_begin(); vit != weighted_delaunay_tri3.finite_vertices_end(); ++vit){
        int simplex_index = simplices.size();
        if(vit->info()!=-2){ //do not store dummy point (used for computing Delaunay on eight-sphere for Fisher metric)
            Simplex point(simplex_index,vit->info());
            data_points_simplex_indices[vit->info()]=simplex_index;
            vertex_handles_weighted3[vit->info()]=vit;
            simplices.push_back(point);
        }
    }
    //points which are not part of Delaunay triangulation are "hidden"
    for(int i=0; i<data_points->size(); i++){
        if(data_points_simplex_indices[i]<0){
            data_points->at(i).setHidden(true);
        }
    }
    
    //by iterating through Delaunay triangles create list of edges (index of triangle saved), not unique yet  
    std::vector<Simplex> edges0; 
    if(tri_dimension==2){ //degenerated triangulation in R^3 (e.g. standard simplex in Euclidean space)  
        for(weighted_Delaunay_triangulation3::Finite_facets_iterator fit = weighted_delaunay_tri3.finite_facets_begin();
            fit != weighted_delaunay_tri3.finite_facets_end(); ++fit) {

            //get vertex indices (stored in corresponding cell), facet is (cell,index)-pair
            weighted_Delaunay_triangulation3::Cell_handle facet = fit->first; 
            int index = fit->second;
            std::vector<int> vertices;
            for(int i=0; i<3; i++){ 
                vertices.push_back(facet->vertex(weighted_delaunay_tri3.vertex_triple_index(index, i))->info()); 
            }
            std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

            //save triangle
            Simplex triangle(simplices.size(),vertices[0],vertices[1],vertices[2]);
            simplices.push_back(triangle);

            //save 3 edges together with index of triangle to list edges0
            for(int i = 0; i<3; i++){
                int a = vertices[i%3]; //first edge vertex
                int b = vertices[(i+1)%3]; //second edge vertex
                Simplex edge(edges0.size(),a,b);
                edge.addCofacetIndex(triangle.getIndex());
                edges0.push_back(edge);
            }
        }
    }else{ //3-dim case
        std::vector<Simplex> triangles0;
        //by iterating through Delaunay tetrahedra create list of triangles (index of tetrahedron saved), not unique yet 
        int tetrahedron_index = 0;
        for(weighted_Delaunay_triangulation3::Finite_cells_iterator cit = weighted_delaunay_tri3.finite_cells_begin();
                cit != weighted_delaunay_tri3.finite_cells_end(); ++cit){

            weighted_Delaunay_triangulation3::Cell_handle cell = cit; //CGAL cell (tetrahedron)

            //get vertex indices             
            std::vector<int> vertices;
            for(int i=0; i<4; i++){
                vertices.push_back(cell->vertex(i)->info());
            }
            std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices

            //new tetrahedron
            Simplex tetrahedron(tetrahedron_index,vertices[0],vertices[1],vertices[2],vertices[3]);
            //save tetrahedron (except for points on 2-simplex)
            if(!on_standard_simplex){
                simplices.push_back(tetrahedron);
            }

            //save 4 triangles together with index of tetrahedron to list triangles0
            for(int i=0; i<4; i++){
                //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                std::vector<int> triangle_vertices;
                for(int j=0; j<4; j++){
                    if(j!=3-i){
                        triangle_vertices.push_back(vertices[j]);
                    }
                }                   
                Simplex triangle(triangles0.size(),triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                triangle.addCofacetIndex(tetrahedron_index);
                triangles0.push_back(triangle);
            }
            tetrahedron_index++;
        }
        
        //sort list of (not unique) triangles
        //for periodic triangulation: triangles with less offset vertices first          
        std::sort(triangles0.begin(),triangles0.end());
        
        //create unique list of triangles with all incident tetrahedra
        for(int i=0; i<triangles0.size(); i++){
            Simplex triangle = triangles0[i];
            std::vector<Offset> vertex_offsets;
            for(int j=0; j<3; j++){
                vertex_offsets.push_back(triangle.getOffset(j));
            }
                       
            //check if there are more incident tetrahedra (every triangle has one or two, exactly 2 for periodic triangulation) 
            bool checknext=true;
            while((i+1)<triangles0.size() && checknext){
                //if next list element is same triangle
                if(triangle.sameSimplex(triangles0[i+1])){
                
                    int tetrahedron_ind = triangles0[i+1].getCofacetIndices()[0];
                    triangle.addCofacetIndex(tetrahedron_ind); //save second incident tetrahedron
                    i++; //go to next triangle
                }else{
                    checknext=false;
                }         
            }
            //on standard simplex: only take boundary triangles, delete cofacet pointers
            if(on_standard_simplex){
                if(triangle.getCofacetIndices().size()>1){
                    continue;
                }else{
                    triangle.deleteCofacetIndices();
                }
                if(fisher_metric){
                    //ignore triangles with dummy vertex used in Fisher metric construction
                    if(triangle.getVertex(0)==-2 || triangle.getVertex(1)==-2 || triangle.getVertex(2) ==-2){
                        continue;
                    }
                }
            }
            if(triangle.getCofacetIndices().size()>2){
                std::cerr << "ERROR (Alpha_Complex::setPoints): triangle has more than two incident tetrahedra" << std::endl;
                //triangulation was not valid, triangle appears with incompatible offsets (different geometry) in different tetrahedra
                data_points->clear();
                simplices.clear();
                return false;
            }
            int triangle_index = simplices.size();
            triangle.setIndex(triangle_index);
            simplices.push_back(triangle); //save triangle
            
            //save triangle index in incident tetrahedra
            std::vector<int> inci_tetrahedra = triangle.getCofacetIndices();
            for(std::vector<int>::iterator it2 = inci_tetrahedra.begin(); it2 < inci_tetrahedra.end(); ++it2){
                simplices[*it2].addFacetIndex(triangle_index);
            }
            
            //save 3 edges together with index of triangle to list edges0
            for(int i = 0; i<3; i++){
                int a = triangle.getVertex(i%3); //first edge vertex
                int b = triangle.getVertex((i+1)%3); //second edge vertex
                Simplex edge(edges0.size(),a,b);
                edge.addCofacetIndex(triangle_index);  
                //set vertex offsets
                if(a<b){ //vertices[a] is first vertex
                    edge.setOffset(0,triangle.getOffset(i%3)); 
                    edge.setOffset(1,triangle.getOffset((i+1)%3)); 
                }else{ //vertices[b] is first vertex
                    edge.setOffset(0,triangle.getOffset((i+1)%3)); 
                    edge.setOffset(1,triangle.getOffset(i%3));                   
                } 
                edges0.push_back(edge);
            }
        }
       
    }
    
    //sort list of (not unique) edges
    //for periodic triangulation: edges with less offset vertices first
    std::sort(edges0.begin(),edges0.end());
    
    std::vector<Simplex> edges1; //final list of all edges, will be stored in list of simplices later

    //create unique list of edges with all incident triangles
    for(int i=0; i<edges0.size(); i++){
        Simplex edge = edges0[i];
        std::vector<Offset> vertex_offsets;
        for(int j=0; j<2; j++){
            vertex_offsets.push_back(edge.getOffset(j));
        }
        //2-dim: every edge has one or two incident triangles, exactly 2 for periodic triangulation
        //3-dim: every edge has 1-3 incident triangles
        
        //check if there are more incident triangles (in degenerate cases there can be even more than 3!!) 
        bool checknext=true;
        while((i+1)<edges0.size() && checknext){
            //if next list element is same edge
            if(edge.sameSimplex(edges0[i+1])){
                
                int triangle_ind = edges0[i+1].getCofacetIndices()[0];
                edge.addCofacetIndex(triangle_ind); //also save this incident triangle
                i++; //go to next edge
            }else{
                checknext=false;
            }         
        }
        if(!dim3 && edge.getCofacetIndices().size()>2){
            std::cerr << "ERROR (Alpha_Complex::setPoints): edge has more than two incident triangles (dim2)" << std::endl;
            //triangulation was not valid, edge appears with incompatible offsets (different geometry) in different triangles
            data_points->clear();
            simplices.clear();
            return false;
        }
        
        edges1.push_back(edge); //save edge
    }

    //sort edges by index of first incident triangle (not by vertex indices)
    std::sort(edges1.begin(),edges1.end(),sortPSimplicesByCofacets());
    //store edges in list of simplices, save all relations
    for(int i=0; i<edges1.size(); i++){
        Simplex edge = edges1[i];
        int edge_index = simplices.size();
        edge.setIndex(edge_index);
        std::vector<int> edge_vertices = edge.getVertices();
        //store relation with facets (points)
        for(int j=0; j<edge_vertices.size(); j++){
            edge.addFacetIndex(data_points_simplex_indices[edge_vertices[j]]);
            simplices[data_points_simplex_indices[edge_vertices[j]]].addCofacetIndex(edge_index);
        }
        //store relation with cofacets (triangles)
        std::vector<int> inci_triangles_ind = edge.getCofacetIndices();
        for(std::vector<int>::iterator it2 = inci_triangles_ind.begin(); it2 < inci_triangles_ind.end(); ++it2){
            int triangle_ind = *it2;
            simplices[triangle_ind].addFacetIndex(edge_index);
        }
        simplices.push_back(edge);
    } 
    
    //store pointer to list of simplices
    simplices_ptr = &simplices;
    return true;
    
}

//compute Alpha filtration values and intervals for current Delaunay triangulation
void Alpha_Complex::computeAlphaComplex()
{
    //initialize lists storing informations about simplices
    in_complex = std::vector<bool>(simplices.size(),false);
    filtration_values = std::vector<exact>(simplices.size(),-1);
    critical = std::vector<bool>(simplices.size(),true);
    interval_faces = std::vector<std::vector<int> >(simplices.size(),std::vector<int>(0));
    included = std::vector<bool>(simplices.size(),false);
    excluded = std::vector<bool>(simplices.size(),false);
    
    //compute Delaunay radius function values for all simplices, higher-dimensional ones first
    //----------------------------------------------------------------------------------------
    if(!dim3 || on_standard_simplex){
        //compute Alpha for every triangle
        for(std::vector<Simplex>::iterator it = simplices.begin(); it<simplices.end(); ++it){
            if((*it).getDim()==2){
                computeDelaunayRadius(&(*it),false,false);
            }
        }
    }else{
    //3-dim
        //compute Alpha for every tetrahedron
        for(std::vector<Simplex>::iterator it = simplices.begin(); it<simplices.end(); ++it){
            if((*it).getDim()==3){
                computeDelaunayRadius(&(*it),false,false);
            }
        }
        
        //compute Alpha for every triangle
        for(std::vector<Simplex>::iterator it = simplices.begin(); it<simplices.end(); ++it){
            if((*it).getDim()==2){
                computeDelaunayRadius(&(*it),false,false);
            }
        }       
        
        //if tetrahedron has two triangles in same interval, also common edge has to belong to interval
        //if it has three triangles in same interval, also  common point and 3 edges belong to (0,3)-interval (size 8)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
        for(std::vector<Simplex>::iterator it = simplices.begin(); it<simplices.end(); ++it){
            if((*it).getDim()==3){
                Simplex tetrahedron = *it;
                if(!(tetrahedron.isDeleted())){
                    std::vector<int> tetra_interval_faces = interval_faces[tetrahedron.getIndex()];
                    if(tetra_interval_faces.size()==2){
                        Simplex triangle1 = simplices[tetra_interval_faces[0]];
                        Simplex triangle2 = simplices[tetra_interval_faces[1]];
                        std::vector<int> triangle2_edges = triangle2.getFacetIndices();
                        int common_edge_index;
                        std::vector<int> triangle1_edges = triangle1.getFacetIndices();
                        for(std::vector<int>::iterator it2 = triangle1_edges.begin(); it2<triangle1_edges.end(); ++it2){
                            common_edge_index = *it2; 
                            if(common_edge_index == triangle2_edges[0] || common_edge_index == triangle2_edges[1] || common_edge_index == triangle2_edges[2]){
                                break;
                            }               
                        }
                        //set common edge of two interval triangles to be in interval as well
                        critical[common_edge_index]=false;
                        exact edgeradius2 = filtration_values[tetrahedron.getIndex()];
                        filtration_values[common_edge_index]=edgeradius2;
                        filtration_current.appendElement(edgeradius2,common_edge_index); 
                        //add edge to interval faces of tetrahedron
                        interval_faces[it->getIndex()].push_back(common_edge_index);
                        critical[it->getIndex()]=false;
                    }else if(tetra_interval_faces.size()==3){
                        Simplex triangle1 = simplices[tetra_interval_faces[0]];
                        Simplex triangle2 = simplices[tetra_interval_faces[1]];
                        Simplex triangle3 = simplices[tetra_interval_faces[2]];
                        int common_point_index = triangle1.getVertex(0);
                        if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                            common_point_index = triangle1.getVertex(1);
                            if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                common_point_index = triangle1.getVertex(2);
                                if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                    std::cerr << "Error(Alpha_Complex::setPoints): no common point found in (0,3)-interval" << std::endl;
                                    continue;
                                }
                            }
                        }
                        //triangle edges that have common point as vertex are in interval
                        std::set<int> interval_edges_ind;
                        std::vector<int> triangle_edges = triangle1.getFacetIndices();
                        for(int i=0; i<triangle_edges.size(); i++){
                            if(simplices[triangle_edges[i]].hasVertex(common_point_index)){
                                interval_edges_ind.insert(triangle_edges[i]);
                            }
                        }
                        triangle_edges = triangle2.getFacetIndices();
                        for(int i=0; i<triangle_edges.size(); i++){
                            if(simplices[triangle_edges[i]].hasVertex(common_point_index)){
                                interval_edges_ind.insert(triangle_edges[i]);
                            }
                        }
                        //we do not need to check triangle3, shares relevant edges with other two triangles
                        //mark edges as noncritical, add to interval faces of tetrahedron
                        for(std::set<int>::iterator it2 = interval_edges_ind.begin(); it2 != interval_edges_ind.end(); ++it2){
                            int edge_index = (*it2);
                            critical[edge_index]=false;
                            exact edgeradius2 = filtration_values[tetrahedron.getIndex()];
                            filtration_values[edge_index]=edgeradius2;
                            filtration_current.appendElement(edgeradius2,edge_index); 
                            //add edge to interval faces of tetrahedron
                            interval_faces[it->getIndex()].push_back(edge_index);
                            critical[it->getIndex()]=false;
                        }
                        //mark common point as noncritical, add as interval face of tetrahedron
                        critical[data_points_simplex_indices[common_point_index]]=false;
                        exact pointradius2 = filtration_values[tetrahedron.getIndex()];
                        filtration_values[data_points_simplex_indices[common_point_index]]=pointradius2;
                        
                        filtration_current.appendElement(pointradius2,data_points_simplex_indices[common_point_index]);
                        //add point to interval faces of triangle
                        interval_faces[it->getIndex()].push_back(data_points_simplex_indices[common_point_index]);
                        critical[it->getIndex()]=false;
                    }
                }
            }
        }
    }
        
    //compute Alpha for every edge
    for(std::vector<Simplex>::iterator it = simplices.begin(); it<simplices.end(); ++it){
        Simplex edge = *it;
        if(edge.getDim()==1){
            if(critical[edge.getIndex()]){ //edge was not already put in interval of size 4
                computeDelaunayRadius(&(*it),false,false);
            }
        }
    } 

    //if triangle has two edges in same interval, also common point has to belong to interval
    for(std::vector<Simplex>::iterator it = simplices.begin(); it != simplices.end(); ++it){
        Simplex triangle = *it;
        if(triangle.getDim()==2){
            if(!(triangle.isDeleted())){
                std::vector<int> interval_faces_triangle = interval_faces[triangle.getIndex()];
                if(interval_faces_triangle.size()==2){
                    Simplex edge1 = simplices[interval_faces_triangle[0]];
                    Simplex edge2 = simplices[interval_faces_triangle[1]];
                    int common_point_index = edge1.getVertex(0);
                    if(common_point_index!=edge2.getVertex(0) && common_point_index!=edge2.getVertex(1)){
                        common_point_index = edge1.getVertex(1);
                        if(common_point_index!=edge2.getVertex(0) && common_point_index!=edge2.getVertex(1)){
                            std::cerr << "Error(Alpha_Complex::setPoints): no common point found in (0,2)-interval" << std::endl;
                            continue;
                        }
                    }
                    //set common point of two interval edges to be noncritical, add as interval face of triangle
                    critical[data_points_simplex_indices[common_point_index]]=false;
                    exact pointradius2 = filtration_values[triangle.getIndex()];
                    filtration_values[data_points_simplex_indices[common_point_index]]=pointradius2;
                    filtration_current.appendElement(pointradius2,data_points_simplex_indices[common_point_index]);
                    //add point to interval faces of triangle
                    interval_faces[triangle.getIndex()].push_back(data_points_simplex_indices[common_point_index]);
                    critical[triangle.getIndex()]=false;
                }
            }
        }
    }

    //compute Alpha for every point
    for(int i=0; i<data_points->size(); i++){
        if(!(data_points->at(i)).isHidden()){
            if(critical[data_points_simplex_indices[i]]){
                computeDelaunayRadius(&simplices[data_points_simplex_indices[i]],false,false);
            }
        }
    }
    
    //store pointers to lists
    critical_ptr = &critical;
    
    //sort filtration simplices
    filtration_current.sort(&simplices,&critical,&included,&inclusion_values_and_counter);
    
    
    
    //CGAL ----------------------- 
    //compute Alpha complex with CGAL to compare
    /*if(!dim3 && weighted){
        std::cout << "CGAL ALPHA COMPLEX" <<std::endl;
        
        //build list of weighted points
        std::vector<std::pair<weighted_pointi,int>> cpoints_weighted;
        for(int i=0; i<data_points->size(); i++){
            DataPoint p = data_points->at(i);
            if(!p.isDeleted()){
                cpoints_weighted.push_back( std::make_pair(weighted_pointi(CPoint2i(CGAL::to_double(p.getX()),CGAL::to_double(p.getY())),CGAL::to_double(p.getWeight())),i));
            }
        }
        //construct alpha complex with CGAL
        weighted_Alpha_shape weighted_alpha_shape_cgal(cpoints_weighted.begin(),cpoints_weighted.end(),1,weighted_Alpha_shape::GENERAL);
        //print list of alphas
        std::cout << "Alpha values" << std::endl;
        for(weighted_Alpha_shape::Alpha_iterator ait = weighted_alpha_shape_cgal.alpha_begin(); ait != weighted_alpha_shape_cgal.alpha_end(); ++ait)
        {
            std::cout << (CGAL::to_double(*ait)) << std::endl;
        }
        std::cout << "Alpha faces and edges" << std::endl;
        for(weighted_Alpha_shape::Finite_faces_iterator fit = weighted_alpha_shape_cgal.finite_faces_begin();
            fit != weighted_alpha_shape_cgal.finite_faces_end(); ++fit) {
            weighted_Alpha_shape::Face_handle face = fit; //CGAL face
            //get vertex indices
            std::vector<int> vertices;
            for(int i=0; i<3; i++){ 
                vertices.push_back(face->vertex(i)->info()); 
            }
            std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices
            std::cout << " face";
            for(int i=0; i<3; i++){
                std::cout << " " << vertices[i];
            }
            std::cout << " alpha " << face->get_alpha() << std::endl;
            for(int i=0; i<3; i++){
               std::cout << "  edge " << face->vertex((i+1)%3)->info() << " " << face->vertex((i+2)%3)->info() << " [" << (face->get_ranges(i)).first << "," << (face->get_ranges(i)).second << "," << (face->get_ranges(i)).third << "]" << std::endl; 
            }
        }
    }*/
    
    /*if(dim3 && weighted){
        std::cout << "ALPHA COMPLEX" <<std::endl;
        filtration.print(&simplices); 
        
        std::cout << "CGAL ALPHA COMPLEX 3D" <<std::endl;
        
        //build list of weighted points
        std::vector<std::pair<weighted_point3i,int>> cpoints_weighted;
        for(int i=0; i<data_points->size(); i++){
            DataPoint p = data_points->at(i);
            if(!p.isDeleted()){
                cpoints_weighted.push_back( std::make_pair(weighted_point3i(CPoint3i(CGAL::to_double(p.getX()),CGAL::to_double(p.getY()),CGAL::to_double(p.getZ())),CGAL::to_double(p.getWeight())),i));
            }
        }
        //construct alpha complex with CGAL
        weighted_Alpha_shape3 weighted_alpha_shape_cgal(cpoints_weighted.begin(),cpoints_weighted.end(),1,weighted_Alpha_shape3::GENERAL);
        //print list of alphas
        std::cout << "Alpha values" << std::endl;
        for(weighted_Alpha_shape3::Alpha_iterator ait = weighted_alpha_shape_cgal.alpha_begin(); ait != weighted_alpha_shape_cgal.alpha_end(); ++ait)
        {
            std::cout << (CGAL::to_double(*ait)) << std::endl;
        }
        std::cout << "Alpha faces" << std::endl;
        for(weighted_Alpha_shape3::Finite_cells_iterator fit = weighted_alpha_shape_cgal.finite_cells_begin();
            fit != weighted_alpha_shape_cgal.finite_cells_end(); ++fit) {
            weighted_Alpha_shape3::Cell_handle face = fit; //CGAL tetrahedron
            //get vertex indices
            std::vector<int> vertices;
            for(int i=0; i<4; i++){ 
                vertices.push_back(face->vertex(i)->info()); 
            }
            std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices
            std::cout << " tetrahedron";
            for(int i=0; i<3; i++){
                std::cout << " " << vertices[i];
            }
            std::cout << " alpha " << face->get_alpha() << std::endl;
            //for(int i=0; i<4; i++){
               //std::cout << "  triangle " << face->vertex((i+1)%4)->info() << " " << face->vertex((i+2)%4)->info() << " [" << (face->get_ranges(i)).first << "," << (face->get_ranges(i)).second << "," << (face->get_ranges(i)).third << "]" << std::endl; 
            //}
        }
        std::vector<CGAL::Object> cgal_simplices;
        std::vector<weighted_Alpha_shape3::FT> filtration_values;

        typedef CGAL::Dispatch_output_iterator<
        CGAL::cpp11::tuple<CGAL::Object, weighted_Alpha_shape3::FT>,
        CGAL::cpp11::tuple<std::back_insert_iterator< std::vector<CGAL::Object> >,
                           std::back_insert_iterator< std::vector<weighted_Alpha_shape3::FT> >
                           > > Dispatch;

        Dispatch disp = CGAL::dispatch_output<CGAL::Object, weighted_Alpha_shape3::FT>(
          std::back_inserter(cgal_simplices),
          std::back_inserter(filtration_values));

        weighted_alpha_shape_cgal.filtration_with_alpha_values(disp); 

        for(int i=0; i<cgal_simplices.size(); i++){
            std::cout << i << " alpha2 " << filtration_values[i] << std::endl;
            std::vector<int> vertices;
            if(cgal_simplices[i].is<weighted_Alpha_shape3::Cell_handle>()){
                weighted_Alpha_shape3::Cell_handle face = CGAL::object_cast<weighted_Alpha_shape3::Cell_handle>(cgal_simplices[i]);
                for(int i=0; i<4; i++){ 
                    vertices.push_back(face->vertex(i)->info()); 
                }
                std::cout << " tetrahedron";
            }else if(cgal_simplices[i].is<weighted_Alpha_shape3::Facet>()){
                weighted_Alpha_shape3::Facet face = CGAL::object_cast<weighted_Alpha_shape3::Facet>(cgal_simplices[i]);
                weighted_Alpha_shape3::Cell_handle tetra1 = face.first;
                int tetra_index = face.second;
                for(int j=0; j<4; j++){
                    if(j!=tetra_index){
                        vertices.push_back(tetra1->vertex(j)->info());
                    }
                }
                std::cout << " triangle ";
            }else if(cgal_simplices[i].is<weighted_Alpha_shape3::Edge>()){
                weighted_Alpha_shape3::Edge edge = CGAL::object_cast<weighted_Alpha_shape3::Edge>(cgal_simplices[i]);
                weighted_Alpha_shape3::Cell_handle tetra = edge.first;
                for(int j=0; j<4; j++){
                    if(j==edge.second || j==edge.third){
                        vertices.push_back(tetra->vertex(j)->info());
                    }
                }
                std::cout << " edge ";
            }else if(cgal_simplices[i].is<weighted_Alpha_shape3::Vertex_handle>()){
                weighted_Alpha_shape3::Vertex_handle vertex = CGAL::object_cast<weighted_Alpha_shape3::Vertex_handle>(cgal_simplices[i]);
                vertices.push_back(vertex->info());
                std::cout << " point";
            }
            
            std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices
            for(int i=0; i<vertices.size(); i++){
                std::cout << " " << vertices[i];
            }
            std::cout << std::endl;
        }
    }*/
    
    /*if(const Cell_handle *cell = CGAL::object_cast<Cell_handle>(&object_iterator)) {
      vertex_list = from_cell<Vertex_list, Cell_handle>(*cell);
      count_cells++;
    } else if (const Facet *facet = CGAL::object_cast<Facet>(&object_iterator)) {
      vertex_list = from_facet<Vertex_list, Facet>(*facet);
      count_facets++;
    } else if (const Edge_3 *edge = CGAL::object_cast<Edge_3>(&object_iterator)) {
      vertex_list = from_edge<Vertex_list, Edge_3>(*edge);
      count_edges++;
    } else if (const Vertex_handle *vertex = CGAL::object_cast<Vertex_handle>(&object_iterator)) {
      count_vertices++;
      vertex_list = from_vertex<Vertex_list, Vertex_handle>(*vertex);
    }*/
    
    
    //CGAL ----------------------- 
    //compute Alpha complex
    /*if(dim3){
        std::vector<std::pair<CPoint3,int> > cpoints;    
        for (int i=0; i<data_points->size(); i++)
        {
            DataPoint p=data_points->at(i);
            cpoints.push_back( std::make_pair(CPoint3(p.getX(),p.getY(),p.getZ()),i) );
        };        
        //compute alpha complex (only regularized, otherwise error)
        alpha_complex3_cgal.set_mode(Alpha_shape3::REGULARIZED);
        alpha_complex3_cgal.set_alpha(0);
        alpha_complex3_cgal.make_alpha_shape(cpoints.begin(),cpoints.end()); //construct alpha shape        
        //get list of alphas
        for(Alpha_shape3::Alpha_iterator ait = alpha_complex3_cgal.alpha_begin(); ait != alpha_complex3_cgal.alpha_end(); ++ait)
        {
            std::cout << (CGAL::to_double(*ait)) << std::endl;
        }
        alpha_complex3_cgal.set_alpha(current_alpha2);
       //get list of simplices
        std::list<Alpha_shape3::Cell_handle> tetrahedra_cgal;
        std::list<Alpha_shape3::Facet>       triangles_cgal;
        std::list<Alpha_shape3::Edge>        edges_cgal;
        alpha_complex3_cgal.get_alpha_shape_cells(std::back_inserter(tetrahedra_cgal),
                      Alpha_shape3::INTERIOR);
        alpha_complex3_cgal.get_alpha_shape_facets(std::back_inserter(triangles_cgal),
                      Alpha_shape3::REGULAR);
        alpha_complex3_cgal.get_alpha_shape_facets(std::back_inserter(triangles_cgal),
                      Alpha_shape3::SINGULAR);
        alpha_complex3_cgal.get_alpha_shape_facets(std::back_inserter(triangles_cgal),
                      Alpha_shape3::INTERIOR);
        alpha_complex3_cgal.get_alpha_shape_edges(std::back_inserter(edges_cgal),
                     Alpha_shape3::REGULAR);
        alpha_complex3_cgal.get_alpha_shape_edges(std::back_inserter(edges_cgal),
                     Alpha_shape3::SINGULAR);
        alpha_complex3_cgal.get_alpha_shape_edges(std::back_inserter(edges_cgal),
                     Alpha_shape3::INTERIOR);
        std::cout << "CGAL: ";
        std::cout << edges_cgal.size() << " " << triangles_cgal.size() << " " << tetrahedra_cgal.size() << std::endl;
        std::cout << "Mine: " << getNumEdges() <<  " " << getNumTriangles() << " " << getNumTetrahedra() << std::endl;
        std::cout << std::endl;
    } */
    //---------------------------------------------------------------------------
}

void Alpha_Complex::computeWrapAndPersistence(bool compute_persistence, bool printstats)
{
    wrap = Wrap(&filtration_values,&critical,&interval_faces,dim3,weighted,periodic_size,getNumDataPoints(),data_points,&simplices,printstats);
    
    //store original filtrations for hole operations
    filtration_original = filtration_current;
    wrap.storeUnadapted(); 
    
    wrap.setExhaustivePersistence(exhaustive_persistence);
    
    //compute persistence for Alpha and Wrap complex
    if(compute_persistence){
        computePersistence(true,printstats); 
        wrap.computePersistence(true,printstats);
    }
    //set alpha^2
    setAlpha2(current_alpha2); //set alpha^2 value
    wrap.setAlpha2(current_alpha2);
}

//compute Delaunay radius function values for simplex, add to filtration (sorted or just at the end)
//needs to be called first for all simplices of higher dimensions (edges depend on triangles, triangles depend on tetrahedra, ...)
//only check intervals of size 4 or 8 (edge is in interval with tetrahedron, point is in an interval with triangle) if corresponding parameter is set
void Alpha_Complex::computeDelaunayRadius(Simplex* simplex, bool sorted_filtration, bool checkIntervalsOfSize4and8)
{
    if(simplex->isDeleted()){
        return;
    }
    
    bool periodic = periodic_size>0;
    if(!dim3){
        //triangle in 2D
        if(simplex->getDim()==2){
            Simplex triangle = simplices[simplex->getIndex()];
            exact triradius;
            
            if(!weighted){
                //compute squared circumradius of triangle with CGAL
                //get triangle vertices
                std::vector<CPoint2> tri_points;
                DataPoint p=data_points->at(triangle.getVertex(0));
                tri_points.push_back(CPoint2(p.getX(),p.getY()));
                p=data_points->at(triangle.getVertex(1));
                tri_points.push_back(CPoint2(p.getX(),p.getY()));
                p=data_points->at(triangle.getVertex(2));
                tri_points.push_back(CPoint2(p.getX(),p.getY()));
                //add offset if periodic triangulation
                if(periodic_size>0){
                    for(int i=0; i<3; i++){
                        if(triangle.isOffset(i)){
                            CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*triangle.getOffset(i).getX(),periodic_size*triangle.getOffset(i).getY());
                            tri_points[i]=tri_points[i]+off; 
                        }
                    }
                }
                triradius = CGAL::squared_radius(tri_points[0],tri_points[1],tri_points[2]);
            }else{
                //compute weighted squared circumradius of triangle with CGAL
                //get (weighted) triangle vertices
                std::vector<weighted_point> tri_points;
                DataPoint p = data_points->at(triangle.getVertex(0));
                tri_points.push_back(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()));
                p = data_points->at(triangle.getVertex(1));
                tri_points.push_back(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()));
                p = data_points->at(triangle.getVertex(2));
                tri_points.push_back(weighted_point(CPoint2(p.getX(),p.getY()),p.getWeight()));
                //add offset if periodic triangulation
                if(periodic_size>0){
                    for(int i=0; i<3; i++){
                        if(triangle.isOffset(i)){
                            tri_points[i] = weighted_point(CPoint2(tri_points[i].x()+periodic_size*triangle.getOffset(i).getX(),tri_points[i].y()+periodic_size*triangle.getOffset(i).getY()),tri_points[i].weight()); 
                        }
                    }
                }
                Kernel::Compute_squared_radius_smallest_orthogonal_circle_2 compute_radius_object = kernel.compute_squared_radius_smallest_orthogonal_circle_2_object();
                triradius = compute_radius_object(tri_points[0],tri_points[1],tri_points[2]);
            }
            //save as squared radius value for triangle
            filtration_values[triangle.getIndex()]=triradius; 
            if(sorted_filtration){
                filtration_current.insertElement(triradius,triangle.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter);
            }else{
                filtration_current.appendElement(triradius,triangle.getIndex());
            }
            
        //edge in 2D
        }else if(simplex->getDim()==1){
            
            Simplex edge = simplices[simplex->getIndex()];
            exact edgeradius2;
            
            if(!weighted){
                //compute squared radius and midpoint of diameter circle = smallest enclosing circle (with CGAL)
                //get edge vertices
                CPoint2 a = CPoint2(data_points->at(edge.getVertex(0)).getX(),data_points->at(edge.getVertex(0)).getY());
                CPoint2 b = CPoint2(data_points->at(edge.getVertex(1)).getX(),data_points->at(edge.getVertex(1)).getY());
                //compute midpoint and radius
                CPoint2 midpoint;
                if(!periodic){
                    midpoint = CGAL::midpoint(a,b);
                    edgeradius2 = CGAL::squared_distance(a,midpoint);
                }else{
                    //add offsets to vertices if periodic triangulation
                    CPoint2 a_off = a;
                    CPoint2 b_off = b;
                    if(edge.isOffset(0)){
                        CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*edge.getOffset(0).getX(),periodic_size*edge.getOffset(0).getY());
                        a_off=a_off+off;                
                    }
                    if(edge.isOffset(1)){
                        CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*edge.getOffset(1).getX(),periodic_size*edge.getOffset(1).getY());
                        b_off=b_off+off;                
                    }
                    midpoint = CGAL::midpoint(a_off,b_off);
                    edgeradius2 = CGAL::squared_distance(a_off,midpoint);
                } 

                //check if any neighbor (lies on common triangle) inside circle
                std::vector<int> inci_triangles = edge.getCofacetIndices();
                for(std::vector<int>::iterator it3 = inci_triangles.begin(); it3 < inci_triangles.end(); ++it3){
                    Simplex inci_triangle = simplices[(*it3)];
                    //get index of neighboring vertex (different to vertices of current edge)
                    int neighbor_ind = inci_triangle.getVertex(0);
                    int neighbor_ind_tri = 0;
                    if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                        neighbor_ind = inci_triangle.getVertex(1);
                        neighbor_ind_tri = 1;
                        if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                            neighbor_ind = inci_triangle.getVertex(2);
                            neighbor_ind_tri = 2;
                        }
                    }
                    //check if neighbor is inside (triangle is obtuse)
                    //REMARK degenerate cases (point is exaclty on circumpshere) are classified as critical
                    //compute distance from edge midpoint to neighboring vertex
                    CPoint2 neighbor = CPoint2(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY());
                    exact dist2;
                    if(!periodic){
                        dist2 = CGAL::squared_distance(neighbor,midpoint);
                    }else{
                    //periodic
                    //possibly add offsets to points if periodic triangulation
                        //offset for neighboring vertex
                        if(inci_triangle.isOffset(neighbor_ind_tri)){
                            CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getX(),periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getY());
                            neighbor=neighbor+off;                
                        }
                        //edge might have a different offset for this triangle than what was stored in edge
                        //rather get other two triangle vertices (= edge vertices) and compute with them (beware: order might be different than in edge! possibly a_edge == b_triangle)
                        int a_ind_tri = (neighbor_ind_tri+1)%3;
                        int b_ind_tri = (neighbor_ind_tri+2)%3;
                        CPoint2 a_triangle = CPoint2(data_points->at(inci_triangle.getVertex(a_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(a_ind_tri)).getY());
                        CPoint2 b_triangle = CPoint2(data_points->at(inci_triangle.getVertex(b_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(b_ind_tri)).getY());                 
                        if(inci_triangle.isOffset(a_ind_tri)){
                            CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*inci_triangle.getOffset(a_ind_tri).getX(),periodic_size*inci_triangle.getOffset(a_ind_tri).getY());
                            a_triangle=a_triangle+off;                
                        }
                        if(inci_triangle.isOffset(b_ind_tri)){
                            CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*inci_triangle.getOffset(b_ind_tri).getX(),periodic_size*inci_triangle.getOffset(b_ind_tri).getY());
                            b_triangle=b_triangle+off;                
                        }
                        CPoint2 midpoint_triangle = CGAL::midpoint(a_triangle,b_triangle);                    
                        dist2 = CGAL::squared_distance(neighbor,midpoint_triangle);
                    }

                    if(dist2<edgeradius2){ //obtuse triangle (right-angled triangles are considered acute/critical!) 
                        //if neighbor inside: circumcircle defined by edge vertices and neighbor is smallest empty circle
                        edgeradius2 = filtration_values[inci_triangle.getIndex()]; 
                        interval_faces[(*it3)].push_back(edge.getIndex()); //triangle and this edge are in the same interval, triangle is obtuse
                        critical[*it3]=false;
                        critical[edge.getIndex()]=false;   
                        
                        break; //only one neighbor can lie inside (otherwise triangles are not Delaunay)
                    }
                }
                
            }else{
                //compute squared radius of smallest orthogonal circle (with CGAL)
                //get edge vertices
                weighted_point a = weighted_point(CPoint2(data_points->at(edge.getVertex(0)).getX(),data_points->at(edge.getVertex(0)).getY()),data_points->at(edge.getVertex(0)).getWeight());
                weighted_point b = weighted_point(CPoint2(data_points->at(edge.getVertex(1)).getX(),data_points->at(edge.getVertex(1)).getY()),data_points->at(edge.getVertex(1)).getWeight());
                
                Kernel::Compute_squared_radius_smallest_orthogonal_circle_2 compute_radius_object = kernel.compute_squared_radius_smallest_orthogonal_circle_2_object();
                if(!periodic){
                    edgeradius2 = compute_radius_object(a,b);
                }
                if(periodic){
                    weighted_point a_off = a;
                    weighted_point b_off = b;
                    if(edge.isOffset(0)){
                        a_off = weighted_point(CPoint2(a_off.x()+periodic_size*edge.getOffset(0).getX(),a_off.y()+periodic_size*edge.getOffset(0).getY()),a_off.weight());               
                    }
                    if(edge.isOffset(1)){
                        b_off = weighted_point(CPoint2(b_off.x()+periodic_size*edge.getOffset(1).getX(),b_off.y()+periodic_size*edge.getOffset(1).getY()),b_off.weight());                
                    }
                    edgeradius2 = compute_radius_object(a_off,b_off);
                }
                
                //check if any neighbor (lies on common triangle) inside circle
                std::vector<int> inci_triangles = edge.getCofacetIndices();
                for(std::vector<int>::iterator it3 = inci_triangles.begin(); it3 < inci_triangles.end(); ++it3){
                    Simplex inci_triangle = simplices[(*it3)];
                    //get index of neighboring vertex (different to vertices of current edge)
                    int neighbor_ind = inci_triangle.getVertex(0);
                    int neighbor_ind_tri = 0;
                    if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                        neighbor_ind = inci_triangle.getVertex(1);
                        neighbor_ind_tri = 1;
                        if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                            neighbor_ind = inci_triangle.getVertex(2);
                            neighbor_ind_tri = 2;
                        }
                    }
                    
                    //check if neighbor is inside
                    //REMARK degenerate cases (point is exaclty on circumpshere) are classified as critical
                    //compute power distance from smallest orthogonal circle
                    weighted_point neighbor = weighted_point(CPoint2(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY()),data_points->at(neighbor_ind).getWeight());
                    CGAL::Bounded_side side;
                    Kernel::Power_side_of_bounded_power_circle_2 compute_powerside_object = kernel.power_side_of_bounded_power_circle_2_object();
                    if(!periodic){
                        side = compute_powerside_object(a,b,neighbor);
                    }
                    if(periodic){
                        //possibly add offsets to points if periodic triangulation
                        //offset for neighboring vertex
                        if(inci_triangle.isOffset(neighbor_ind_tri)){
                            neighbor = weighted_point(CPoint2(neighbor.x()+periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getX(),neighbor.y()+periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getY()),neighbor.weight());
                        }
                        //edge might have a different offset for this triangle than what was stored in edge
                        //rather get other two triangle vertices (= edge vertices) and compute with them (beware: order might be different than in edge! possibly a_edge == b_triangle)
                        int a_ind_tri = (neighbor_ind_tri+1)%3;
                        int b_ind_tri = (neighbor_ind_tri+2)%3;
                        weighted_point a_triangle = weighted_point(CPoint2(data_points->at(inci_triangle.getVertex(a_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(a_ind_tri)).getY()),data_points->at(inci_triangle.getVertex(a_ind_tri)).getWeight());
                        weighted_point b_triangle = weighted_point(CPoint2(data_points->at(inci_triangle.getVertex(b_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(b_ind_tri)).getY()),data_points->at(inci_triangle.getVertex(b_ind_tri)).getWeight());                 
                        if(inci_triangle.isOffset(a_ind_tri)){
                            a_triangle = weighted_point(CPoint2(a_triangle.x()+periodic_size*inci_triangle.getOffset(a_ind_tri).getX(),a_triangle.y()+periodic_size*inci_triangle.getOffset(a_ind_tri).getY()),a_triangle.weight());                
                        }
                        if(inci_triangle.isOffset(b_ind_tri)){
                            b_triangle = weighted_point(CPoint2(b_triangle.x()+periodic_size*inci_triangle.getOffset(b_ind_tri).getX(),b_triangle.y()+periodic_size*inci_triangle.getOffset(b_ind_tri).getY()),b_triangle.weight());                
                        }
                        side = compute_powerside_object(a_triangle,b_triangle,neighbor);
                    }
                    if(side==CGAL::ON_BOUNDED_SIDE){
                        //if neighbor inside: circumcircle defined by edge vertices and neighbor is smallest empty circle
                        edgeradius2 = filtration_values[inci_triangle.getIndex()]; 
                        interval_faces[(*it3)].push_back(edge.getIndex()); //triangle and this edge are in the same interval
                        critical[*it3]=false;
                        critical[edge.getIndex()]=false;                
                        break; //only one neighbor can lie inside (otherwise triangles are not Delaunay)
                    }
                }
            }
            
            filtration_values[edge.getIndex()]=edgeradius2;
            if(sorted_filtration){
                filtration_current.insertElement(edgeradius2,edge.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter);  
            }else{
                filtration_current.appendElement(edgeradius2,edge.getIndex());  
            }
            
        }else if(simplex->getDim()==0){
            Simplex point = simplices[simplex->getIndex()];
            DataPoint data_point = data_points->at(point.getVertex(0));
            int index = point.getIndex();
            exact pointradius2 = 0;
            if(weighted){
                std::vector<int> inci_edges = point.getCofacetIndices();
                if(inci_edges.size()==0){
                    return;
                }else{
                    //check if point is in interval of size 4 (two incident edge are in interval with same triangle)
                    bool inIntervalWithTriangle = false;
                    if(checkIntervalsOfSize4and8){
                        //get all incident triangles
                        std::set<int> inci_triangles;
                        if(inci_edges.size()>=2){
                            for(int i=0; i<inci_edges.size();i++){
                                std::vector<int> edge_triangles = simplices[inci_edges[i]].getCofacetIndices();
                                inci_triangles.insert(edge_triangles.begin(),edge_triangles.end());
                            }
                        }
                        //if triangle has two edges in same interval, also common point has to belong to interval
                        for(std::set<int>::iterator it = inci_triangles.begin(); it != inci_triangles.end(); ++it){
                            Simplex triangle = simplices[(*it)];
                            std::vector<int> interval_faces_triangle = interval_faces[triangle.getIndex()];
                            if(interval_faces_triangle.size()==2){
                                Simplex edge1 = simplices[interval_faces_triangle[0]];
                                Simplex edge2 = simplices[interval_faces_triangle[1]];
                                int common_point_index = edge1.getVertex(0);
                                if(common_point_index!=edge2.getVertex(0) && common_point_index!=edge2.getVertex(1)){
                                    common_point_index = edge1.getVertex(1);
                                    if(common_point_index!=edge2.getVertex(0) && common_point_index!=edge2.getVertex(1)){
                                        std::cerr << "Error(Alpha_Complex::ComputeDelaunayRadius): problem with point in (0,2)-interval" << std::endl;
                                    }
                                }
                                //common point is the one we currently consider (could be other vertex of triangle)
                                if(data_points_simplex_indices[common_point_index] == index){
                                    //set common point of two interval edges to be in interval as well
                                    critical[index]=false;
                                    pointradius2 = filtration_values[triangle.getIndex()];
                                    
                                    //add point to interval faces of triangle
                                    interval_faces[triangle.getIndex()].push_back(index);
                                    critical[triangle.getIndex()]=false;
                                    
                                    inIntervalWithTriangle = true;
                                    break;
                                }
                            }
                        }
                    }
                    
                    //point is not in (0,2)-interval, check if (0,1) interval
                    if(!inIntervalWithTriangle){
                        pointradius2 = -data_point.getWeight();
                        CPoint2 cpoint = CPoint2(data_point.getX(),data_point.getY());
                        //check if point is in interval with incident edge, not inside own Voronoi cell, not critical
                        std::vector<int> possible_interval_edges;
                        for(int i=0; i<inci_edges.size();i++){
                            Simplex inci_edge = simplices[inci_edges[i]];
                            //get index of neighboring vertex
                            int neighbor_ind = inci_edge.getVertex(0);
                            int neighbor_ind_edge = 0;
                            int point_ind_edge = 1; 
                            if(neighbor_ind==point.getVertex(0)){
                                neighbor_ind = inci_edge.getVertex(1);
                                int neighbor_ind_edge = 1;
                                int point_ind_edge = 0; 
                            }
                            //check if point is inside Voronoi cell of neighboring point (outside own) -> point is not critical
                            //in interval with such edge of smallest radius
                            CPoint2 neighbor = CPoint2(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY());
                            CPoint2 cpoint1 = cpoint;
                            if(periodic){
                                //possibly add offsets to points if periodic triangulation
                                if(inci_edge.isOffset(point_ind_edge)){
                                    CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*inci_edge.getOffset(point_ind_edge).getX(),periodic_size*inci_edge.getOffset(point_ind_edge).getY());
                                    cpoint1 = cpoint1 + off;
                                }
                                if(inci_edge.isOffset(neighbor_ind_edge)){
                                    CGAL::Vector_2<Kernel> off = CGAL::Vector_2<Kernel>(periodic_size*inci_edge.getOffset(neighbor_ind_edge).getX(),periodic_size*inci_edge.getOffset(neighbor_ind_edge).getY());
                                    neighbor = neighbor + off;
                                }
                            }
                            //compute power distance of smallest orthogonal sphere (centered at point, -weight) to neighbor
                            //if negative -> conflict, same interval
                            exact dist2 = CGAL::squared_distance(cpoint,neighbor)-(-data_point.getWeight())-((data_points->at(neighbor_ind)).getWeight());
                            
                            if(dist2<0){ //non-critical point, neighboring point has negative power distance
                                possible_interval_edges.push_back(inci_edge.getIndex());
                            }
                        }
                        if(possible_interval_edges.size()>0){
                            int interval_edge_ind = -1;
                            //point is in interval with incident edge of smallest radius
                            //this should still be critical (not in interval with triangle or other point)
                            for(int i=0; i<possible_interval_edges.size();i++){
                                Simplex inci_edge = simplices[possible_interval_edges[i]];
                                if(critical[inci_edge.getIndex()]){
                                    exact edgeradius2 = filtration_values[inci_edge.getIndex()];
                                    if(interval_edge_ind == -1 || edgeradius2<pointradius2){
                                        //edge with smaller radius (or first one) found
                                        interval_edge_ind = inci_edge.getIndex();
                                        pointradius2 = edgeradius2;
                                    }
                                }
                            }
                            if(interval_edge_ind < 0 ){
                                std::cerr << "ERROR(Alpha_Complex::computeDelaunayRadius): edge for (0,1)-pair was not found" << std::endl;
                                std::cerr << " point " << index << std::endl;
                            }else{
                                interval_faces[interval_edge_ind].push_back(index);
                                critical[interval_edge_ind]=false;
                                critical[index]=false;
                            }
                        }
                    }
                }
            }
            filtration_values[point.getIndex()]=pointradius2;
            if(sorted_filtration){
                filtration_current.insertElement(pointradius2,point.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter);  
            }else{
                filtration_current.appendElement(pointradius2,point.getIndex());  
            }
        }
        
    //3D    
    }else{
        //tetrahedron in 3D
        if(simplex->getDim()==3){
            Simplex tetrahedron = simplices[simplex->getIndex()];
            exact tetraradius2;
            
            if(!weighted){
                //compute squared circumradius of tetrahedron with CGAL
                //get tetrahedron vertices
                std::vector<CPoint3> tetra_points;
                DataPoint p=data_points->at(tetrahedron.getVertex(0));
                tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                p=data_points->at(tetrahedron.getVertex(1));
                tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                p=data_points->at(tetrahedron.getVertex(2));
                tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                p=data_points->at(tetrahedron.getVertex(3));
                tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                //add offset if periodic triangulation
                if(periodic){
                    for(int i=0; i<4; i++){
                        if(tetrahedron.isOffset(i)){
                            CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*tetrahedron.getOffset(i).getX(),periodic_size*tetrahedron.getOffset(i).getY(),periodic_size*tetrahedron.getOffset(i).getZ());
                            tetra_points[i]=tetra_points[i]+off; 
                        }
                    }
                }
                tetraradius2 = CGAL::squared_radius(tetra_points[0],tetra_points[1],tetra_points[2],tetra_points[3]);
            }else{
                //compute weighted squared circumradius of tetrahedron with CGAL
                //get (weighted) tetrahedron vertices
                std::vector<weighted_point3> tetra_points;
                DataPoint p=data_points->at(tetrahedron.getVertex(0));
                tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                p=data_points->at(tetrahedron.getVertex(1));
                tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                p=data_points->at(tetrahedron.getVertex(2));
                tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                p=data_points->at(tetrahedron.getVertex(3));
                tetra_points.push_back(weighted_point3(CPoint3(p.getX(),p.getY(),p.getZ()),p.getWeight()));
                //add offset if periodic triangulation
                if(periodic){
                    for(int i=0; i<4; i++){
                        if(tetrahedron.isOffset(i)){
                            tetra_points[i]=weighted_point3(CPoint3(tetra_points[i].x()+periodic_size*tetrahedron.getOffset(i).getX(),tetra_points[i].y()+periodic_size*tetrahedron.getOffset(i).getY(),tetra_points[i].z()+periodic_size*tetrahedron.getOffset(i).getZ()),tetra_points[i].weight());
                        }
                    }
                }
                Kernel::Compute_squared_radius_smallest_orthogonal_sphere_3 compute_radius_object = kernel.compute_squared_radius_smallest_orthogonal_sphere_3_object();
                tetraradius2 = compute_radius_object(tetra_points[0],tetra_points[1],tetra_points[2],tetra_points[3]);
            }
            
            //save as squared radius value for tetrahedron
            filtration_values[tetrahedron.getIndex()]=tetraradius2;    
            if(sorted_filtration){
                filtration_current.insertElement(tetraradius2,tetrahedron.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter);
            }else{
                filtration_current.appendElement(tetraradius2,tetrahedron.getIndex());
            }
            
        //triangle in 3D
        }else if(simplex->getDim()==2){
            Simplex triangle = simplices[simplex->getIndex()];
            exact triradius2;
            
            if(!weighted){
                //for every triangle: compute radius of smallest circumsphere, check if it is empty 
                //get triangle vertices
                std::vector<CPoint3> tri_points;
                tri_points.push_back(CPoint3(data_points->at(triangle.getVertex(0)).getX(),data_points->at(triangle.getVertex(0)).getY(),data_points->at(triangle.getVertex(0)).getZ()));
                tri_points.push_back(CPoint3(data_points->at(triangle.getVertex(1)).getX(),data_points->at(triangle.getVertex(1)).getY(),data_points->at(triangle.getVertex(1)).getZ()));
                tri_points.push_back(CPoint3(data_points->at(triangle.getVertex(2)).getX(),data_points->at(triangle.getVertex(2)).getY(),data_points->at(triangle.getVertex(2)).getZ()));
                //compute squared radius and midpoint of equator sphere (smallest circumsphere) with CGAL
                CPoint3 tricenter;
                if(!periodic){
                    tricenter = CGAL::circumcenter(tri_points[0],tri_points[1],tri_points[2]);
                    triradius2 = CGAL::squared_distance(tri_points[0],tricenter);
                }else{
                //add offset if periodic triangulation
                    std::vector<CPoint3> tri_points_off = tri_points;
                    for(int i=0; i<3; i++){
                        if(triangle.isOffset(i)){
                            CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*triangle.getOffset(i).getX(),periodic_size*triangle.getOffset(i).getY(),periodic_size*triangle.getOffset(i).getZ());
                            tri_points_off[i]=tri_points_off[i]+off; 
                        }
                    }
                    tricenter = CGAL::circumcenter(tri_points_off[0],tri_points_off[1],tri_points_off[2]);
                    triradius2 = CGAL::squared_distance(tri_points_off[0],tricenter);
                }      

                //check if any neighbor (lies on common tetrahedron) inside sphere
                std::vector<int> inci_tetrahedra = triangle.getCofacetIndices();
                for(std::vector<int>::iterator it3 = inci_tetrahedra.begin(); it3<inci_tetrahedra.end(); ++it3){
                    Simplex inci_tetrahedron = simplices[*it3];
                    //get index of neighboring vertex (different to vertices of current triangle)   
                    int neighbor_ind_tetra;
                    int neighbor_ind;
                    for(int i=0; i<4; i++){
                        neighbor_ind_tetra=i;
                        neighbor_ind = inci_tetrahedron.getVertex(i);
                        if(neighbor_ind!=triangle.getVertex(0)&&neighbor_ind!=triangle.getVertex(1)&&neighbor_ind!=triangle.getVertex(2)){
                            break;
                        }
                    }
                    //check if neighbor is inside
                    //REMARK degenerate cases (point is exaclty on circumpshere) are classified as critical
                    //compute distance from triangle center to neighboring vertex
                    CPoint3 neighbor = CPoint3(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY(),data_points->at(neighbor_ind).getZ());
                    exact dist2;
                    if(!periodic){
                        dist2 = CGAL::squared_distance(neighbor,tricenter);
                    }else{
                    //periodic
                    //possibly add offsets to points if periodic triangulation
                        //offset for neighboring vertex
                        if(inci_tetrahedron.isOffset(neighbor_ind_tetra)){ 
                            CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_tetrahedron.getOffset(neighbor_ind_tetra).getX(),periodic_size*inci_tetrahedron.getOffset(neighbor_ind_tetra).getY(),periodic_size*inci_tetrahedron.getOffset(neighbor_ind_tetra).getZ());
                            neighbor=neighbor+off;                
                        }
                        //triangle might have a different offset for this tetrahedron than what was stored in triangle
                        //rather get other three tetrahedron vertices (= triangle vertices) and compute with them
                        std::vector<int> tri_points_ind_tetra;
                        for(int i=1; i<4; i++){ 
                            tri_points_ind_tetra.push_back((neighbor_ind_tetra+i)%4);
                        }
                        //get the vertices and their offsets
                        std::vector<CPoint3> tri_points_tetra;
                        for(int i=0; i<3; i++){
                            int tri_point_ind_tetra = (neighbor_ind_tetra+i+1)%4; //get indices of triangle vertices as tetrahedron vertices (0,1,2 or 3)
                            CPoint3 tri_point_tetra = CPoint3(data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getX(),data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getY(),data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getZ());
                            if(inci_tetrahedron.isOffset(tri_point_ind_tetra)){ //get offset
                                CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_tetrahedron.getOffset(tri_point_ind_tetra).getX(),periodic_size*inci_tetrahedron.getOffset(tri_point_ind_tetra).getY(),periodic_size*inci_tetrahedron.getOffset(tri_point_ind_tetra).getZ());
                                tri_point_tetra=tri_point_tetra+off;     
                            }
                            tri_points_tetra.push_back(tri_point_tetra);
                        }
                        CPoint3 tricenter_tetra = CGAL::circumcenter(tri_points_tetra[0],tri_points_tetra[1],tri_points_tetra[2]);
                        dist2 = CGAL::squared_distance(neighbor,tricenter_tetra);    
                    }

                    if(dist2<triradius2){ //noncritical tetrahedron (in same interval as triangle)
                        triradius2 = filtration_values[inci_tetrahedron.getIndex()];
                        interval_faces[(*it3)].push_back(triangle.getIndex()); //tetrahedron and this triangular face are in the same interval, noncritical
                        critical[*it3]=false;
                        critical[triangle.getIndex()]=false;
                        break; //only one neighbor can lie inside (otherwise tetrahedra are not Delaunay)                                               
                    }
                }
            }else{
                //weighted
                
                //compute squared radius of smallest orthogonal circle
                
                //get triangle vertices
                std::vector<weighted_point3> tri_points;
                tri_points.push_back(weighted_point3(CPoint3(data_points->at(triangle.getVertex(0)).getX(),data_points->at(triangle.getVertex(0)).getY(),data_points->at(triangle.getVertex(0)).getZ()),data_points->at(triangle.getVertex(0)).getWeight()));
                tri_points.push_back(weighted_point3(CPoint3(data_points->at(triangle.getVertex(1)).getX(),data_points->at(triangle.getVertex(1)).getY(),data_points->at(triangle.getVertex(1)).getZ()),data_points->at(triangle.getVertex(1)).getWeight()));
                tri_points.push_back(weighted_point3(CPoint3(data_points->at(triangle.getVertex(2)).getX(),data_points->at(triangle.getVertex(2)).getY(),data_points->at(triangle.getVertex(2)).getZ()),data_points->at(triangle.getVertex(2)).getWeight()));
                //compute radius
                Kernel::Compute_squared_radius_smallest_orthogonal_sphere_3 compute_radius_object = kernel.compute_squared_radius_smallest_orthogonal_sphere_3_object();
                if(!periodic){
                    triradius2 = compute_radius_object(tri_points[0],tri_points[1],tri_points[2]);
                }else{
                //add offset if periodic triangulation
                    std::vector<weighted_point3> tri_points_off = tri_points;
                    for(int i=0; i<3; i++){
                        if(triangle.isOffset(i)){
                            tri_points_off[i]=weighted_point3(CPoint3(tri_points[i].x()+periodic_size*triangle.getOffset(i).getX(),tri_points[i].y()+periodic_size*triangle.getOffset(i).getY(),tri_points[i].z()+periodic_size*triangle.getOffset(i).getZ()),tri_points[i].weight());
                        }
                    }
                    triradius2 = compute_radius_object(tri_points_off[0],tri_points_off[1],tri_points_off[2]);
                }  
                
                //check if any neighbor (lies on common tetrahedron) inside sphere
                std::vector<int> inci_tetrahedra = triangle.getCofacetIndices();
                for(std::vector<int>::iterator it3 = inci_tetrahedra.begin(); it3<inci_tetrahedra.end(); ++it3){
                    Simplex inci_tetrahedron = simplices[*it3];
                    //get index of neighboring vertex (different to vertices of current triangle)   
                    int neighbor_ind_tetra;
                    int neighbor_ind;
                    for(int i=0; i<4; i++){
                        neighbor_ind_tetra=i;
                        neighbor_ind = inci_tetrahedron.getVertex(i);
                        if(neighbor_ind!=triangle.getVertex(0)&&neighbor_ind!=triangle.getVertex(1)&&neighbor_ind!=triangle.getVertex(2)){
                            break;
                        }
                    }
                    //check if neighbor is inside
                    //REMARK degenerate cases (point is exaclty on circumpshere) are classified as critical
                    //compute power distance from smallest orthogonal sphere
                    weighted_point3 neighbor = weighted_point3(CPoint3(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY(),data_points->at(neighbor_ind).getZ()),data_points->at(neighbor_ind).getWeight());
                    CGAL::Bounded_side side;
                    Kernel::Power_side_of_bounded_power_sphere_3 compute_powerside_object = kernel.power_side_of_bounded_power_sphere_3_object();
                    if(!periodic){
                        side = compute_powerside_object(tri_points[0],tri_points[1],tri_points[2],neighbor);
                    }else{
                    //periodic
                    //possibly add offsets to points if periodic triangulation
                        //offset for neighboring vertex
                        if(inci_tetrahedron.isOffset(neighbor_ind_tetra)){ 
                            neighbor = weighted_point3(CPoint3(neighbor.x()+periodic_size*inci_tetrahedron.getOffset(neighbor_ind_tetra).getX(),neighbor.y()+periodic_size*inci_tetrahedron.getOffset(neighbor_ind_tetra).getY(),neighbor.z()+periodic_size*inci_tetrahedron.getOffset(neighbor_ind_tetra).getZ()),neighbor.weight());             
                        }
                        //triangle might have a different offset for this tetrahedron than what was stored in triangle
                        //rather get other three tetrahedron vertices (= triangle vertices) and compute with them
                        std::vector<int> tri_points_ind_tetra;
                        for(int i=1; i<4; i++){ 
                            tri_points_ind_tetra.push_back((neighbor_ind_tetra+i)%4);
                        }
                        //get the vertices and their offsets
                        std::vector<weighted_point3> tri_points_tetra;
                        for(int i=0; i<3; i++){
                            int tri_point_ind_tetra = (neighbor_ind_tetra+i+1)%4; //get indices of triangle vertices as tetrahedron vertices (0,1,2 or 3)
                            weighted_point3 tri_point_tetra = weighted_point3(CPoint3(data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getX(),data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getY(),data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getZ()),data_points->at(inci_tetrahedron.getVertex(tri_point_ind_tetra)).getWeight());
                            if(inci_tetrahedron.isOffset(tri_point_ind_tetra)){ //get offset
                                tri_point_tetra = weighted_point3(CPoint3(tri_point_tetra.x()+periodic_size*inci_tetrahedron.getOffset(tri_point_ind_tetra).getX(),tri_point_tetra.y()+periodic_size*inci_tetrahedron.getOffset(tri_point_ind_tetra).getY(),tri_point_tetra.z()+periodic_size*inci_tetrahedron.getOffset(tri_point_ind_tetra).getZ()),tri_point_tetra.weight()); 
                            }
                            tri_points_tetra.push_back(tri_point_tetra);
                        }
                        side = compute_powerside_object(tri_points_tetra[0],tri_points_tetra[1],tri_points_tetra[2],neighbor);
                    }

                    if(side==CGAL::ON_BOUNDED_SIDE){ //noncritical tetrahedron (in same interval as triangle)
                        triradius2 = filtration_values[inci_tetrahedron.getIndex()];
                        interval_faces[(*it3)].push_back(triangle.getIndex()); //tetrahedron and this triangular face are in the same interval, noncritical
                        critical[*it3]=false;
                        critical[triangle.getIndex()]=false;
                        break; //only one neighbor can lie inside (otherwise tetrahedra are not Delaunay)                                               
                    }
                }
            }
            filtration_values[triangle.getIndex()]=triradius2;
            if(sorted_filtration){
                filtration_current.insertElement(triradius2,triangle.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter);
            }else{
                filtration_current.appendElement(triradius2,triangle.getIndex());
            }
            
        //edge in 3D
        }else if(simplex->getDim()==1){
            Simplex edge = simplices[simplex->getIndex()];
            exact edgeradius2;
            //check if edge is in interval with tetrahedron ((1,3)- or (0,3)-interval)
            bool inIntervalWithTetrahedron = false;
            if(checkIntervalsOfSize4and8){
                std::vector<int> inci_triangles = edge.getCofacetIndices();
                //get all incident tetrahedra
                std::set<int> inci_tetrahedra;
                if(inci_triangles.size()>=2){
                    for(int i=0; i<inci_triangles.size(); i++){
                        std::vector<int> tri_tetra = simplices[inci_triangles[i]].getCofacetIndices();
                        inci_tetrahedra.insert(tri_tetra.begin(),tri_tetra.end());
                    }
                }
                for(std::set<int>::iterator it = inci_tetrahedra.begin(); it != inci_tetrahedra.end(); ++it){
                    Simplex tetrahedron = simplices[(*it)];
                    std::vector<int> interval_faces_tetrahedron = interval_faces[tetrahedron.getIndex()];
                    std::vector<int> interval_triangles;
                    for(int i=0; i<interval_faces_tetrahedron.size(); i++){
                        if(simplices[interval_faces_tetrahedron[i]].getDim()==2){
                            interval_triangles.push_back(interval_faces_tetrahedron[i]);
                        }
                    }
                    if(interval_faces_tetrahedron.size()==2){
                        //check for (1,3)-interval
                        //if tetrahedron has two triangles in same interval, also common edge has to belong to interval, no other edge was added to interval yet
                        Simplex triangle1 = simplices[interval_triangles[0]];
                        Simplex triangle2 = simplices[interval_triangles[1]];
                        std::vector<int> triangle2_edges = triangle2.getFacetIndices();
                        int common_edge_index;
                        std::vector<int> triangle1_edges = triangle1.getFacetIndices();
                        for(std::vector<int>::iterator it2 = triangle1_edges.begin(); it2<triangle1_edges.end(); ++it2){
                            common_edge_index = *it2; 
                            if(common_edge_index == triangle2_edges[0] || common_edge_index == triangle2_edges[1] || common_edge_index == triangle2_edges[2]){
                                break;
                            }               
                        }
                        //check if common edge of two interval triangles is the edge we currently consider
                        if(common_edge_index == edge.getIndex()){
                            //set common edge of two interval triangles to be in interval as well, noncritical
                            critical[edge.getIndex()]=false;

                            edgeradius2 = filtration_values[tetrahedron.getIndex()];
                            //add edge to interval faces of tetrahedron
                            interval_faces[tetrahedron.getIndex()].push_back(edge.getIndex());
                            critical[tetrahedron.getIndex()]=false;

                            inIntervalWithTetrahedron = true;
                            break;
                        }
                    }else if(interval_triangles.size()==3){
                        //check for (0,3)-interval
                        //if tetrahedron has three triangles in same interval, also common point and three edges belong to interval, other edges were possibly already added
                        Simplex triangle1 = simplices[interval_triangles[0]];
                        Simplex triangle2 = simplices[interval_triangles[1]];
                        Simplex triangle3 = simplices[interval_triangles[2]];
                        int common_point_index = triangle1.getVertex(0);
                        if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                            common_point_index = triangle1.getVertex(1);
                            if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                common_point_index = triangle1.getVertex(2);
                                if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                    std::cerr << "Error(Alpha_Complex::computeDelaunayRadius): no common point found in (0,3)-interval" << std::endl;
                                    continue;
                                }
                            }
                        }
                        //check if edge also contains common point index, whether also in interval
                        if(edge.hasVertex(common_point_index)){
                            //set common edge of two interval triangles to be in interval as well, noncritical
                            critical[edge.getIndex()]=false;
                            edgeradius2 = filtration_values[tetrahedron.getIndex()];
                            //add edge to interval faces of tetrahedron
                            interval_faces[tetrahedron.getIndex()].push_back(edge.getIndex());
                            critical[tetrahedron.getIndex()]=false;
                            inIntervalWithTetrahedron = true;
                            break;
                        }
                        
                    }
                }
            }
            
            //edge is not in interval with tetrahedron
            if(!inIntervalWithTetrahedron){
                
                if(!weighted){
                    //compute squared radius and midpoint of diameter sphere (with CGAL)
                    CPoint3 a = CPoint3(data_points->at(edge.getVertex(0)).getX(),data_points->at(edge.getVertex(0)).getY(),data_points->at(edge.getVertex(0)).getZ());
                    CPoint3 b = CPoint3(data_points->at(edge.getVertex(1)).getX(),data_points->at(edge.getVertex(1)).getY(),data_points->at(edge.getVertex(1)).getZ());
                    //compute midpoint and radius
                    CPoint3 midpoint;
                    if(!periodic){
                        midpoint = CGAL::midpoint(a,b);
                        edgeradius2 = CGAL::squared_distance(a,midpoint);
                    }else{
                    //add offset to vertices if periodic triangulation
                        CPoint3 a_off = a;
                        CPoint3 b_off = b;

                        if(edge.isOffset(0)){
                           CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*edge.getOffset(0).getX(),periodic_size*edge.getOffset(0).getY(),periodic_size*edge.getOffset(0).getZ());
                           a_off=a_off+off; 
                        }
                        if(edge.isOffset(1)){
                            CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*edge.getOffset(1).getX(),periodic_size*edge.getOffset(1).getY(),periodic_size*edge.getOffset(1).getZ());
                            b_off=b_off+off;          
                        }
                        midpoint = CGAL::midpoint(a_off,b_off);
                        edgeradius2 = CGAL::squared_distance(a_off,midpoint);
                    }

                    //edge can be longest edge of multiple obtuse triangles, is in same interval as the one with smallest empty circumsphere!
                    //all other obtuse triangles are in interval with tetrahedron which contains the closest vertex (otherwise they do not have empty circumsphere)
                    //these triangles were already processed, just take the one obtuse triangle that is still marked as critical
                    //std::vector<int> obtuse_triangles;
                    //find obtuse triangles where this edge is the longest, neighboring vertex inside diameter circle
                    std::vector<int> inci_triangles = edge.getCofacetIndices();
                    for(std::vector<int>::iterator it3 = inci_triangles.begin(); it3 < inci_triangles.end(); ++it3){
                        Simplex inci_triangle = simplices[(*it3)];
                        //edge is in interval with obtuse triangle with smallest empty circumsphere
                        //other obtuse triangles have the third vertex of this triangle inside triangle sphere -> in interval with tetrahedron (these triangles were already processed)
                        if(critical[inci_triangle.getIndex()]){
                            //get index of neighboring vertex (different to vertices of current edge)
                            int neighbor_ind = inci_triangle.getVertex(0);
                            int neighbor_ind_tri = 0;
                            if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                                neighbor_ind = inci_triangle.getVertex(1);
                                neighbor_ind_tri = 1;
                                if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                                    neighbor_ind = inci_triangle.getVertex(2);
                                    neighbor_ind_tri = 2;
                                }
                            }
                            //check if neighbor is inside (triangle is obtuse)
                            //REMARK degenerate cases (point is exaclty on circumpshere) are classified as critical
                            //compute distance from edge midpoint to neighboring vertex
                            CPoint3 neighbor = CPoint3(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY(),data_points->at(neighbor_ind).getZ());
                            exact dist2;
                            if(!periodic){
                                dist2 = CGAL::squared_distance(neighbor,midpoint);
                            }else{
                            //periodic
                            //possibly add offsets to points if periodic triangulation
                                //offset for neighboring vertex
                                if(inci_triangle.isOffset(neighbor_ind_tri)){
                                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getX(),periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getY(),periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getZ());
                                    neighbor=neighbor+off;  
                                }
                                //edge might have a different offset for this triangle than what was stored in edge
                                //rather get other two triangle vertices (= edge vertices) and compute with them (beware: order might be different than in edge! possibly a_edge == b_triangle)
                                int a_ind_tri = (neighbor_ind_tri+1)%3;
                                int b_ind_tri = (neighbor_ind_tri+2)%3;
                                CPoint3 a_triangle = CPoint3(data_points->at(inci_triangle.getVertex(a_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(a_ind_tri)).getY(),data_points->at(inci_triangle.getVertex(a_ind_tri)).getZ());
                                CPoint3 b_triangle = CPoint3(data_points->at(inci_triangle.getVertex(b_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(b_ind_tri)).getY(),data_points->at(inci_triangle.getVertex(b_ind_tri)).getZ());
                                if(inci_triangle.isOffset(a_ind_tri)){
                                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_triangle.getOffset(a_ind_tri).getX(),periodic_size*inci_triangle.getOffset(a_ind_tri).getY(),periodic_size*inci_triangle.getOffset(a_ind_tri).getZ());
                                    a_triangle=a_triangle+off;                
                                }
                                if(inci_triangle.isOffset(b_ind_tri)){
                                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_triangle.getOffset(b_ind_tri).getX(),periodic_size*inci_triangle.getOffset(b_ind_tri).getY(),periodic_size*inci_triangle.getOffset(b_ind_tri).getZ());
                                    b_triangle=b_triangle+off;                
                                }
                                CPoint3 midpoint_triangle = CGAL::midpoint(a_triangle,b_triangle);

                                dist2 = CGAL::squared_distance(neighbor,midpoint_triangle);
                            }

                            if(dist2<edgeradius2){ //triangle is obtuse (90 deg is considered acute) with this edge as the longest, in same interval 

                                edgeradius2 = filtration_values[inci_triangle.getIndex()]; 
                                interval_faces[inci_triangle.getIndex()].push_back(edge.getIndex());
                                critical[inci_triangle.getIndex()]=false;
                                critical[edge.getIndex()]=false;
                                break;
                            }
                        }
                    }
                }else{
                    //compute squared radius of smallest orthogonal circle
                    
                    weighted_point3 a = weighted_point3(CPoint3(data_points->at(edge.getVertex(0)).getX(),data_points->at(edge.getVertex(0)).getY(),data_points->at(edge.getVertex(0)).getZ()),data_points->at(edge.getVertex(0)).getWeight());
                    weighted_point3 b = weighted_point3(CPoint3(data_points->at(edge.getVertex(1)).getX(),data_points->at(edge.getVertex(1)).getY(),data_points->at(edge.getVertex(1)).getZ()),data_points->at(edge.getVertex(1)).getWeight());
                    
                    Kernel::Compute_squared_radius_smallest_orthogonal_sphere_3 compute_radius_object = kernel.compute_squared_radius_smallest_orthogonal_sphere_3_object();
                    if(!periodic){
                        edgeradius2 = compute_radius_object(a,b);
                    }else{
                    //add offset to vertices if periodic triangulation
                        weighted_point3 a_off = a;
                        weighted_point3 b_off = b;

                        if(edge.isOffset(0)){
                            a_off = weighted_point3(CPoint3(a.x()+periodic_size*edge.getOffset(0).getX(),a.y()+periodic_size*edge.getOffset(0).getY(),a.z()+periodic_size*edge.getOffset(0).getZ()),a.weight());
                        }
                        if(edge.isOffset(1)){
                            a_off = weighted_point3(CPoint3(b.x()+periodic_size*edge.getOffset(1).getX(),b.y()+periodic_size*edge.getOffset(1).getY(),b.z()+periodic_size*edge.getOffset(1).getZ()),b.weight());        
                        }
                        edgeradius2 = compute_radius_object(a_off,b_off);
                    }
                    
                    //edge is in interval with conflicting triangle of smallest radius
                    //this triangle is possibly not marked critical anymore (in interval with multiple edges), so we need to check all triangles
                    bool noncritical = false;
                    std::vector<int> possible_interval_triangles;
                    std::vector<int> inci_triangles = edge.getCofacetIndices();
                    for(std::vector<int>::iterator it3 = inci_triangles.begin(); it3 < inci_triangles.end(); ++it3){
                        Simplex inci_triangle = simplices[(*it3)];
                        //get index of neighboring vertex (different to vertices of current edge)
                        int neighbor_ind = inci_triangle.getVertex(0);
                        int neighbor_ind_tri = 0;
                        if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                            neighbor_ind = inci_triangle.getVertex(1);
                            neighbor_ind_tri = 1;
                            if(neighbor_ind==edge.getVertex(0) || neighbor_ind==edge.getVertex(1)){
                                neighbor_ind = inci_triangle.getVertex(2);
                                neighbor_ind_tri = 2;
                            }
                        }
                        //check if neighbor is inside orthogonal sphere 
                        weighted_point3 neighbor = weighted_point3(CPoint3(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY(),data_points->at(neighbor_ind).getZ()),data_points->at(neighbor_ind).getWeight());
                        CGAL::Bounded_side side;
                        Kernel::Power_side_of_bounded_power_sphere_3 compute_powerside_object = kernel.power_side_of_bounded_power_sphere_3_object();
                        if(!periodic){
                            side = compute_powerside_object(a,b,neighbor);
                        }else{
                        //periodic
                        //possibly add offsets to points if periodic triangulation
                            //offset for neighboring vertex
                            if(inci_triangle.isOffset(neighbor_ind_tri)){
                                neighbor = weighted_point3(CPoint3(neighbor.x()+periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getX(),neighbor.y()+periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getY(),neighbor.z()+periodic_size*inci_triangle.getOffset(neighbor_ind_tri).getZ()));
                            }
                            //edge might have a different offset for this triangle than what was stored  in edge
                            //rather get other two triangle vertices (= edge vertices) and compute with them (beware: order might be different than in edge, possibly a_edge == b_triangle)
                            int a_ind_tri = (neighbor_ind_tri+1)%3;
                            int b_ind_tri = (neighbor_ind_tri+2)%3;
                            weighted_point3 a_triangle = weighted_point3(CPoint3(data_points->at(inci_triangle.getVertex(a_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(a_ind_tri)).getY(),data_points->at(inci_triangle.getVertex(a_ind_tri)).getZ()),data_points->at(inci_triangle.getVertex(a_ind_tri)).getWeight());
                            weighted_point3 b_triangle = weighted_point3(CPoint3(data_points->at(inci_triangle.getVertex(b_ind_tri)).getX(),data_points->at(inci_triangle.getVertex(b_ind_tri)).getY(),data_points->at(inci_triangle.getVertex(b_ind_tri)).getZ()),data_points->at(inci_triangle.getVertex(b_ind_tri)).getWeight());
                            if(inci_triangle.isOffset(a_ind_tri)){
                                a_triangle = weighted_point3(CPoint3(a_triangle.x()+periodic_size*inci_triangle.getOffset(a_ind_tri).getX(),a_triangle.y()+periodic_size*inci_triangle.getOffset(a_ind_tri).getY(),a_triangle.z()+periodic_size*inci_triangle.getOffset(a_ind_tri).getZ()),a_triangle.weight());                
                            }
                            if(inci_triangle.isOffset(b_ind_tri)){
                                b_triangle = weighted_point3(CPoint3(b_triangle.x()+periodic_size*inci_triangle.getOffset(b_ind_tri).getX(),b_triangle.y()+periodic_size*inci_triangle.getOffset(b_ind_tri).getY(),b_triangle.z()+periodic_size*inci_triangle.getOffset(b_ind_tri).getZ()),b_triangle.weight());              
                            }
                            side = compute_powerside_object(a_triangle,b_triangle,neighbor);
                        }
                        if(side==CGAL::ON_BOUNDED_SIDE){ 
                            noncritical = true;
                            //triangle and edge are conflicting (possibly in same interval)
                            possible_interval_triangles.push_back(inci_triangle.getIndex());
                        }
                    }
                    //point is in interval with conflicting triangle of smallest radius
                    int interval_triangle_ind = -1;
                    for(int i=0; i<possible_interval_triangles.size(); i++){
                        Simplex inci_triangle = simplices[possible_interval_triangles[i]];
                        exact triangleradius2 = filtration_values[inci_triangle.getIndex()];
                        if(interval_triangle_ind == -1 || triangleradius2<edgeradius2){
                            //triangle with smaller radius (or first one) found
                            interval_triangle_ind = inci_triangle.getIndex();
                            edgeradius2 = triangleradius2;
                        }
                    }
                    if(interval_triangle_ind>=0){
                        interval_faces[interval_triangle_ind].push_back(edge.getIndex());
                        critical[interval_triangle_ind]=false;
                        critical[edge.getIndex()]=false;
                    }
                }
            }
            filtration_values[edge.getIndex()]=edgeradius2;
            if(sorted_filtration){
                filtration_current.insertElement(edgeradius2,edge.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter); 
            }else{
                filtration_current.appendElement(edgeradius2,edge.getIndex()); 
            }
            
        //point in 3D
        }else if(simplex->getDim()==0){
            Simplex point = simplices[simplex->getIndex()];
            DataPoint data_point = data_points->at(simplex->getVertex(0));
            int index = point.getIndex();
            exact pointradius2 = 0;
            if(weighted){
                std::set<int> cofaces = getCofacesIndices(point.getIndex(),&simplices);
                if(cofaces.size()==0){
                    return;
                }else{
                    std::vector<int> incident_edges;
                    std::vector<int> incident_triangles;
                    std::vector<int> incident_tetrahedra;
                    for(std::set<int>::iterator it = cofaces.begin(); it!=cofaces.end(); ++it){
                        int coface_dim = simplices[(*it)].getDim();
                        if(coface_dim==1){
                            incident_edges.push_back(*it);
                        }else if(coface_dim==2){
                            incident_triangles.push_back(*it);
                        }else if(coface_dim==3){
                            incident_tetrahedra.push_back(*it);
                        }
                    }
                    //check if point in (0,2) or (0,3) interval
                    bool inBiggerInterval = false;
                    if(checkIntervalsOfSize4and8){
                        //check all incident triangles for (0,2)-interval
                        for(int i=0; i<incident_triangles.size(); i++){
                            Simplex triangle = simplices[incident_triangles[i]];
                            std::vector<int> interval_faces_triangle = interval_faces[triangle.getIndex()];
                            if(interval_faces_triangle.size()==2){
                                //if triangle has two edges in same interval, also common point has to belong to interval
                                Simplex edge1 = simplices[interval_faces_triangle[0]];
                                Simplex edge2 = simplices[interval_faces_triangle[1]];
                                int common_point_index = edge1.getVertex(0);
                                if(common_point_index!=edge2.getVertex(0) && common_point_index!=edge2.getVertex(1)){
                                    common_point_index = edge1.getVertex(1);
                                    if(common_point_index!=edge2.getVertex(0) && common_point_index!=edge2.getVertex(1)){
                                        std::cerr << "Error(Alpha_Complex::setPoints): no common point found in (0,2)-interval" << std::endl;
                                        continue;
                                    }
                                }
                                //check if common point is the one we currently consider
                                if(data_points_simplex_indices[common_point_index] == index){
                                    //set common point of two interval edges to be in interval as well, add as interval face of triangle
                                    critical[index]=false;
                                    pointradius2 = filtration_values[triangle.getIndex()];
                                    interval_faces[triangle.getIndex()].push_back(index);
                                    critical[triangle.getIndex()]=false;
                                    inBiggerInterval = true;
                                    break;
                                }
                            }
                        }
                        if(!inBiggerInterval){
                            //check all incident tetrahedra for (0,3)-interval
                            for(int i=0; i<incident_tetrahedra.size(); i++){
                                Simplex tetrahedron = simplices[incident_tetrahedra[i]];
                                std::vector<int> interval_faces_tetrahedron = interval_faces[tetrahedron.getIndex()];
                                if(interval_faces_tetrahedron.size()==6){
                                    //check for (0,3)-interval
                                    //if tetrahedron has three triangles and three edges in same interval, also common point belongs to interval
                                    Simplex triangle1 = simplices[interval_faces_tetrahedron[0]];
                                    Simplex triangle2 = simplices[interval_faces_tetrahedron[1]];
                                    Simplex triangle3 = simplices[interval_faces_tetrahedron[2]];
                                    int common_point_index = triangle1.getVertex(0);
                                    if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                        common_point_index = triangle1.getVertex(1);
                                        if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                            common_point_index = triangle1.getVertex(2);
                                            if(!triangle2.hasVertex(common_point_index)||!triangle3.hasVertex(common_point_index)){
                                                std::cerr << "Error(Alpha_Complex::computeDelaunayRadius): no common point found in (0,3)-interval" << std::endl;
                                                continue;
                                            }
                                        }
                                    }
                                    //check if common point is current one
                                    if(data_points_simplex_indices[common_point_index]==index){
                                        //mark common point as noncritical, add as interval face of tetrahedron
                                        critical[index]=false;
                                        exact pointradius2 = filtration_values[tetrahedron.getIndex()];
                                        //add point to interval faces of triangle
                                        interval_faces[tetrahedron.getIndex()].push_back(index);
                                        critical[tetrahedron.getIndex()]=false;

                                        inBiggerInterval = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    if(!inBiggerInterval){
                        pointradius2 = -data_point.getWeight();
                        CPoint3 cpoint = CPoint3(data_point.getX(),data_point.getY(),data_point.getZ());
                        //check if point is in interval with incident edge, not inside own Voronoi cell, not critical
                        std::vector<int> possible_interval_edges;
                        bool noncritical = false;
                        for(int i=0; i<incident_edges.size();i++){
                            Simplex inci_edge = simplices[incident_edges[i]];
                            //get index of neighboring vertex
                            int neighbor_ind = inci_edge.getVertex(0);
                            int neighbor_ind_edge = 0;
                            int point_ind_edge = 1; 
                            if(neighbor_ind==simplex->getVertex(0)){
                                neighbor_ind = inci_edge.getVertex(1);
                                int neighbor_ind_edge = 1;
                                int point_ind_edge = 0; 
                            }
                            //check if point is inside Voronoi cell of neighboring point (outside own) -> point is not critical
                            //in interval with such edge of smallest radius
                            CPoint3 neighbor = CPoint3(data_points->at(neighbor_ind).getX(),data_points->at(neighbor_ind).getY(),data_points->at(neighbor_ind).getZ());
                            CPoint3 cpoint1 = cpoint;
                            if(periodic){
                                //possibly add offsets to points if periodic triangulation
                                if(inci_edge.isOffset(point_ind_edge)){
                                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_edge.getOffset(point_ind_edge).getX(),periodic_size*inci_edge.getOffset(point_ind_edge).getY(),periodic_size*inci_edge.getOffset(point_ind_edge).getZ());
                                    cpoint1 = cpoint1 + off;
                                }
                                if(inci_edge.isOffset(neighbor_ind_edge)){
                                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*inci_edge.getOffset(neighbor_ind_edge).getX(),periodic_size*inci_edge.getOffset(neighbor_ind_edge).getY(),periodic_size*inci_edge.getOffset(neighbor_ind_edge).getZ());
                                    neighbor = neighbor + off;
                                }
                            }
                            //compute power distance of smallest orthogonal sphere of point (has negative weight) to adjacent point
                            //if negative the point is not critical
                            exact dist2 = CGAL::squared_distance(cpoint,neighbor)-(-data_point.getWeight())-((data_points->at(neighbor_ind)).getWeight());
                           
                            if(dist2<0){ //non-critical point, neighboring point has negative power distance
                                noncritical=true;
                                possible_interval_edges.push_back(inci_edge.getIndex());
                            }
                        }
                        if(possible_interval_edges.size()>0){
                            int interval_edge_ind = -1;
                            //point is in interval with incident edge of smallest radius
                            //this should still be critical (not in interval with triangle or other point)
                            for(int i=0; i<possible_interval_edges.size();i++){
                                Simplex inci_edge = simplices[possible_interval_edges[i]];
                                if(critical[inci_edge.getIndex()]){
                                    exact edgeradius2 = filtration_values[inci_edge.getIndex()];
                                    if(interval_edge_ind == -1 || edgeradius2<pointradius2){
                                        //edge with smaller radius (or first one) found
                                        interval_edge_ind = inci_edge.getIndex();
                                        pointradius2 = edgeradius2;
                                    }
                                }
                            }
                            if(interval_edge_ind < 0 ){
                                std::cerr << "ERROR(Alpha_Complex::computeDelaunayRadius): edge for (0,1)-pair was not found" << std::endl;
                                std::cerr << " point " << index << std::endl;
                            }else{
                                critical[index]=false;
                                interval_faces[interval_edge_ind].push_back(index);
                                critical[interval_edge_ind]=false;
                            }
                        }
                    }
                }
            }
            filtration_values[index]=pointradius2;
            if(sorted_filtration){
                filtration_current.insertElement(pointradius2,point.getIndex(),&simplices,&critical,&included,&inclusion_values_and_counter);  
            }else{
                filtration_current.appendElement(pointradius2,point.getIndex());  
            }
        } 
    }
}

//import filtered complex with given points, simplices and radii from file and use as alpha complexes
std::vector<DataPoint> Alpha_Complex::importFilteredComplexFromFile(std::string filename, bool compute_wrap, bool compute_persistence) 
{
    std::cout << "import filtered complex from file: " << filename << std::endl;
    clear();
    //clear delaunay triangulation in CGAL, not used for imported complex
    delaunay_tri.clear(); //data structure for Delaunay triangulation (computed with CGAL)
    periodic_delaunay_tri.clear(); //data structure for periodic Delaunay triangulation (computed with CGAL)
    delaunay_tri3.clear(); //3-dim Delaunay triangulation  (computed with CGAL)
    periodic_delaunay_tri3.clear(); //3-dim periodic Delaunay triangulation  (computed with CGAL)
    weighted_delaunay_tri.clear(); //weighted Delaunay triangulation
    weighted_delaunay_tri3.clear(); //weighted 3-dim Delaunay triangulation
    vertex_handles.clear();
    vertex_handles_periodic.clear();
    vertex_handles3.clear();
    vertex_handles_periodic3.clear();
    vertex_handles_weighted.clear();
    vertex_handles_weighted3.clear();
    
    std::vector<std::vector<std::string> > parsed_file = readCSVFile(filename);

    //read general parameters
    int current_line_index=1;
    std::vector<std::string> parameter_line = parsed_file[current_line_index];
    
    int dim=std::stoi(parameter_line[0]);
    dim3=false;
    if(dim==3){
        dim3=true;
    }
    weighted=false;
    int element = 0;
    element=std::stoi(parameter_line[1]);
    if(element==1){
        weighted=true;
    }
    periodic_size=-1;
    element=std::stoi(parameter_line[2]);
    if(element>=0){
        periodic_size=element;
    }
    on_standard_simplex=false;
    element=std::stoi(parameter_line[3]);
    if(element==1){
        on_standard_simplex=true;
    }
    
    //read points
    std::vector<DataPoint> input_data_points;
    DataPoint point;
    current_line_index += 3;
    std::vector<std::string> current_line=parsed_file[current_line_index];
    if(current_line.size()<3){ //no points
        return input_data_points;
    }
    
    if(!dim3){
        while(current_line.size()>1){ //until we reach new line
            point = DataPoint(std::stod(current_line[1]),std::stod(current_line[2]));
            if(weighted){
                point.setWeight(std::stod(current_line[3]));
            }
            point.setHidden(true); //set to false if in filtration
            input_data_points.push_back(point);
            current_line_index++;
            current_line=parsed_file[current_line_index];
        }
    }else{
        while(current_line.size()>1){ //until we reach new line
            point = DataPoint(false,std::stod(current_line[1]),std::stod(current_line[2]),std::stod(current_line[3]));
            if(weighted){
                point.setWeight(std::stod(current_line[4]));
            }
            point.setHidden(true); //set to false if in filtration
            input_data_points.push_back(point);
            current_line_index++;
            current_line=parsed_file[current_line_index];
        }
    }
    num_data_points = input_data_points.size();
    data_points = &input_data_points;
    
    //initialize arrays
    data_points_simplex_indices = std::vector<int>(data_points->size(),-1);
    std::vector<std::vector<int> > edges_with_min_vertex = std::vector<std::vector<int> >(data_points->size(),std::vector<int>(0));
    std::vector<std::vector<int> > triangles_with_min_vertex = std::vector<std::vector<int> >(data_points->size(),std::vector<int>(0));
    
    //read filtered complex
    
    //empty simplex
    simplices.push_back(Simplex(0));
    in_complex.push_back(true);filtration_values.push_back(-1);
    critical.push_back(true);interval_faces.push_back(std::vector<int>(0));
    included.push_back(false);excluded.push_back(false);
    filtration_current.appendElement(-1,0);
    current_line_index+=3;
    if(current_line_index>=parsed_file.size()){
        return input_data_points;
    }
    int filtration_index = 1;
    while(current_line_index<parsed_file.size()){
        current_line=parsed_file[current_line_index];
        if(current_line.size()<1){ //empty line
            break;
        }
        int simplex_dim=std::stoi(current_line[1]);
        exact radius2=std::stod(current_line[2]);
        std::vector<int> vertices;
        std::istringstream ss(current_line[3]);
        std::string vertex_string;
        while(std::getline(ss,vertex_string,' ')){
            vertices.push_back(std::stoi(vertex_string));
        }
        if(vertices.size()!=(simplex_dim+1)){
            std::cerr << "ERROR(Alpha_Complex::importFilteredComplexFromFile): problem with dimension of simplex " << vertices.size() << " " << simplex_dim+1 << std::endl;
        }
        if(simplex_dim==0){
            Simplex simplex(filtration_index,vertices[0]);
            simplex.setIndex(filtration_index);
            data_points_simplex_indices[vertices[0]]=filtration_index;
            input_data_points[vertices[0]].setHidden(false);
            simplices.push_back(simplex);
        }else if(simplex_dim==1){
            Simplex simplex(filtration_index,vertices[0],vertices[1]);
            simplex.setIndex(filtration_index);
            for(int i=0; i<vertices.size(); i++){ //store relation with facets
                simplices[data_points_simplex_indices[vertices[i]]].addCofacetIndex(filtration_index);
                simplex.addFacetIndex(data_points_simplex_indices[vertices[i]]);
            }
            simplices.push_back(simplex);
            edges_with_min_vertex[simplex.getVertex(0)].push_back(filtration_index);
        }else if(simplex_dim==2){
            Simplex simplex(filtration_index,vertices[0],vertices[1],vertices[2]);
            simplex.setIndex(filtration_index);
            //find facets and store relations
            std::vector<int> possible_edges = edges_with_min_vertex[vertices[0]];
            for(int i=0; i<possible_edges.size(); i++){
                if(simplices[possible_edges[i]].getVertex(1)==vertices[1] || simplices[possible_edges[i]].getVertex(1)==vertices[2]){
                    simplices[possible_edges[i]].addCofacetIndex(filtration_index);
                    simplex.addFacetIndex(possible_edges[i]);
                }
            }
            possible_edges = edges_with_min_vertex[vertices[1]];
            for(int i=0; i<possible_edges.size(); i++){
                if(simplices[possible_edges[i]].getVertex(1)==vertices[2]){
                    simplices[possible_edges[i]].addCofacetIndex(filtration_index);
                    simplex.addFacetIndex(possible_edges[i]);
                }
            }
            simplices.push_back(simplex);
            triangles_with_min_vertex[simplex.getVertex(0)].push_back(filtration_index);
        }else if(simplex_dim==3){
            Simplex simplex(filtration_index,vertices[0],vertices[1],vertices[2],vertices[3]);
            simplex.setIndex(filtration_index);
            //find facets and store relations
            std::vector<int> possible_triangles = triangles_with_min_vertex[vertices[0]];
            for(int i=0; i<possible_triangles.size(); i++){
                if((simplices[possible_triangles[i]].getVertex(1)==vertices[1] && (simplices[possible_triangles[i]].getVertex(2)==vertices[2] || simplices[possible_triangles[i]].getVertex(2)==vertices[3])) ||
                    (simplices[possible_triangles[i]].getVertex(1)==vertices[2] && simplices[possible_triangles[i]].getVertex(2)==vertices[3])){
                    simplices[possible_triangles[i]].addCofacetIndex(filtration_index);
                    simplex.addFacetIndex(possible_triangles[i]);
                }
            }
            possible_triangles = triangles_with_min_vertex[vertices[1]];
            for(int i=0; i<possible_triangles.size(); i++){
                if(simplices[possible_triangles[i]].getVertex(1)==vertices[2] && simplices[possible_triangles[i]].getVertex(2)==vertices[3]){
                    simplices[possible_triangles[i]].addCofacetIndex(filtration_index);
                    simplex.addFacetIndex(possible_triangles[i]);
                }
            }
            simplices.push_back(simplex);
        }
        in_complex.push_back(true);
        filtration_values.push_back(radius2);
        critical.push_back(true);
        interval_faces.push_back(std::vector<int>(0));
        included.push_back(false);
        excluded.push_back(false);
        filtration_current.appendElement(radius2,filtration_index);
        
        filtration_index++;
        current_line_index++;
    }
    
    critical_ptr = &critical;
    simplices_ptr = &simplices;
    filtration_original = filtration_current;
    
    if(compute_wrap){
        //reconstruct intervals
        std::vector<exact> numeric_filtration_values = filtration_values;
        exact threshold = computeIntervalsFromNoisyFiltration(&numeric_filtration_values,-1,weighted,true,false,true,compute_persistence,false,false);
        //compute wrap
        computeWrapAndPersistence(compute_persistence,false);
    }
    
    return input_data_points;
    
}

//lock (fill) cycle for full complex by including canonical cycle (chain) of the birth (death) simplex
void Alpha_Complex::lockOrFillCycleFullComplex(bool lock, PersistencePair persistence_pair, exact inclusion_alpha2, bool printinfo, bool printstats)
{
    //get index of main simplex (birth or death)
    int filtration_index_of_main_simplex;
    if(lock){
        filtration_index_of_main_simplex = persistence_pair.getBirthIndexFiltration();
    }else{
        filtration_index_of_main_simplex = persistence_pair.getDeathIndexFiltration();
    }
    if(printinfo){
        if(lock){
            std::cout << " birth simplex " << filtration_index_of_main_simplex << std::endl;
        }else{
            std::cout << " death simplex " << filtration_index_of_main_simplex << std::endl;
        }
        Simplex* main_simplex = filtration_original.getSimplex(filtration_index_of_main_simplex,&simplices);
        main_simplex->print();
    }
    
    //get canonical cycle (chain) that should be included
    std::vector<int> chain = persistence.getChain(filtration_index_of_main_simplex);
    std::string chain_name = "chain";
    if(lock){
        chain_name = "cycle";
    }
    if(printstats || printinfo){
        std::cout << " # " << chain_name << " simplices included, " << chain.size() << std::endl;
    }
    if(printinfo){
        std::cout << chain_name;
    }
    //include simplices with new value in filtration
    for(int i=0; i<chain.size(); i++){
        if(printinfo){
            std::cout << chain[i] << " ";
        }
        int filtration_index = chain[i];
        bool included = setSimplexIncludedWithAlpha2(filtration_original.getElementIndex(filtration_index),inclusion_alpha2,true);
        if(included){
            //also include faces
            std::vector<int> faces = persistence.getDependencesBoundaryRecursively(filtration_index);
            for(int i=0; i<faces.size(); i++){
                if(faces[i]>0){ //empty simplex not in filtration
                    setSimplexIncludedWithAlpha2(filtration_original.getElementIndex(faces[i]),inclusion_alpha2,true);
                }
            }
        }
    }
    if(printinfo){
        std::cout << std::endl;
    }
    holes_modified=true;
}

//unlock (unfill) cycle for full complex by excluding canonical cochain (cocycle) of birth simplex (death)
void Alpha_Complex::unlockOrUnfillCycleFullComplex(bool lock, PersistencePair persistence_pair, bool printinfo, bool printstats)
{
    //get index of main simplex (birth or death)
    int filtration_index_of_main_simplex;
    if(lock){
        filtration_index_of_main_simplex = persistence_pair.getBirthIndexFiltration();
    }else{
        filtration_index_of_main_simplex = persistence_pair.getDeathIndexFiltration();
    }
    if(printinfo){
        if(lock){
            std::cout << " birth simplex " << filtration_index_of_main_simplex << std::endl;
        }else{
            std::cout << " death simplex " << filtration_index_of_main_simplex << std::endl;
        }
        Simplex* main_simplex = filtration_original.getSimplex(filtration_index_of_main_simplex,simplices_ptr);
        main_simplex->print();
    }
    
    //get canonical cocycle (unfill) or cochain (unlock) which should be excluded
    std::vector<int> cochain = persistence.getCocycle(filtration_index_of_main_simplex);
    std::string chain_name = "cochain";
    if(!lock){
        chain_name = "cocycle";
    }
    if(printstats){
        std::cout << " # " << chain_name << " simplices excluded, " << cochain.size() << std::endl;
    }
    if(printinfo){
       std::cout << chain_name << ": "  << std::endl;
    }
    //exclude simplices in cochain (cocycle)
    for(int j=0; j<cochain.size(); j++){
        int excluded_filtration_index =  cochain[j];
        int excluded_simplex_index = filtration_original.getElementIndex(excluded_filtration_index);
        excluded[excluded_simplex_index]=true;
        if(printinfo){
            std::cout << cochain[j] << " "; 
        }
        //exclude cofaces
        std::set<int> cofaces = getCofacesIndices(excluded_simplex_index,&simplices);
        for(std::set<int>::iterator it = cofaces.begin(); it!=cofaces.end(); ++it){
            excluded[(*it)]=true;
        }
    }
    
    if(printinfo){
        std::cout << std::endl;
    }
    
    holes_modified=true;
}

//insert new point in Alpha and Wrap complex and update
bool Alpha_Complex::addPoint(std::vector<DataPoint> *updated_points, int data_point_index, bool printinfo, bool printstats, bool compute_persistence, bool compute_statistics)
{ 
    //assumption: no hole operation was performed to modify complexes
    if(holes_modified){
        undoHoleOperations(compute_persistence,compute_statistics);
    } 
    
    //assumption: critical descendants were computed
    if(!wrap.wereDescendantsComputed()){
        wrap.computeFiltrationValuesWrap(true);
    }
    
    double time1 = getCPUTime();
    
    //update information about data points
    num_data_points++;
    data_points = updated_points;
    simplices_ptr = &simplices;
    DataPoint new_data_point = data_points->at(data_point_index);

    // 1) detect the simplices that have to be removed or adapted (in conflict with the new point, point is inside their circumsphere)

    double time_del = getCPUTime();
    //locate all the simplices in conflict with the new point (for dim 1,2(,3): pair of lists of simplices, first: interior ..., second: exterior of conflict zone (some empty depending on case))  
    std::vector<std::pair<std::vector<int>, std::vector<int> > > conflict_simplices = getSimplicesInConflictZoneForPointAddition(new_data_point, printinfo);
    if(printstats){
        std::cout << " time for detecting conflicted simplices in Delaunay, " << getCPUTime()-time_del << std::endl;
    }
    std::vector<int> conflict_edges_interior = conflict_simplices[0].first;
    std::vector<int> conflict_edges_boundary = conflict_simplices[0].second;
    std::vector<int> conflict_triangles = conflict_simplices[1].first;
    std::vector<int> conflict_triangles_boundary;
    std::vector<int> conflict_tetrahedra;
    if(dim3){
        conflict_triangles_boundary = conflict_simplices[1].second;
        conflict_tetrahedra = conflict_simplices[2].first;
    }

    // 2) update CGAL Delaunay triangulation
    // 3) get new simplices, put in our data structure
    
    std::vector<std::vector<int>* > conflict_simplices_boundary;
    conflict_simplices_boundary.push_back(&conflict_edges_boundary);
    if(dim3){
        conflict_simplices_boundary.push_back(&conflict_triangles_boundary);
    }
    std::vector<std::vector<int>* > conflict_simplices_interior;
    conflict_simplices_interior.push_back(&conflict_edges_interior);
    conflict_simplices_interior.push_back(&conflict_triangles);
    conflict_simplices_interior.push_back(&conflict_tetrahedra);
    time_del = getCPUTime();
    
    //get all new simplices by updating CGAL delaunay triangulation, return lists of new simplices (dim 1,2(,3)) + lists of extra boundary simplices
    std::pair<std::vector<std::vector<int> >, std::vector<std::vector<int> > > out = getNewSimplicesForPointAddition(new_data_point,data_point_index,conflict_simplices_boundary,conflict_simplices_interior,printinfo);
    if(printstats){
        std::cout << " time for insertion in Delaunay, " << getCPUTime()-time_del << std::endl;
    }
    std::vector<int> new_edges = out.first[0];
    std::vector<int> new_triangles = out.first[1];
    std::vector<int> new_tetrahedra;
    if(dim3){
        new_tetrahedra = out.first[2];
    }
    //take care of extra boundary simplices that were only detected as face of new simplex (possible for convex hull simplices)
    std::vector<int> extra_boundary_edges = out.second[0];
    std::vector<int> extra_boundary_triangles;
    if(dim3){
        extra_boundary_triangles = out.second[1];
    }
    if(printinfo){
        std::cout << "extra boundary edges ";
        for(std::vector<int>::iterator it = extra_boundary_edges.begin(); it != extra_boundary_edges.end(); ++it){
            std::cout << " " << (*it); simplices[*it].print();
        }
        std::cout << std::endl;
        if(dim3){
            std::cout << "extra boundary triangles ";
            for(std::vector<int>::iterator it = extra_boundary_triangles.begin(); it != extra_boundary_triangles.end(); ++it){
                std::cout << " " << (*it); simplices[*it].print();
            } 
            std::cout << std::endl;
        }
    }
    conflict_edges_boundary.insert(conflict_edges_boundary.end(),extra_boundary_edges.begin(),extra_boundary_edges.end());
    conflict_simplices_boundary[0]=&conflict_edges_boundary;
    if(dim3){
        conflict_triangles_boundary.insert(conflict_triangles_boundary.end(),extra_boundary_triangles.begin(),extra_boundary_triangles.end());
        conflict_simplices_boundary[1]=&conflict_triangles_boundary;
    }
    
    //double time_after_del = getCPUTime(); 

    // 4) update alpha complex

    //update alpha complex deleting/updating/inserting given simplices, return deleted intervals
    std::vector<std::vector<int>* > old_simplices;
    std::vector<int> old_points; //no points deleted
    old_simplices.push_back(&old_points);
    old_simplices.push_back(&conflict_edges_interior); old_simplices.push_back(&conflict_triangles);
    if(dim3){
        old_simplices.push_back(&conflict_tetrahedra);
    }
    std::vector<std::vector<int>* > new_simplices_ptr;
    std::vector<int> new_points; new_points.push_back(data_points_simplex_indices[data_point_index]); //1 point added
    new_simplices_ptr.push_back(&new_points);
    new_simplices_ptr.push_back(&new_edges); new_simplices_ptr.push_back(&new_triangles);
    if(dim3){
        new_simplices_ptr.push_back(&new_tetrahedra);
    }
    std::set<int> deleted_intervals = updateAlphaComplexForPointManipulation(old_simplices,conflict_simplices_boundary,new_simplices_ptr);
    if(printinfo){
        if(dim3){
            std::cout << "deleted " << conflict_tetrahedra.size() << " tetrahedra, inserted " << new_tetrahedra.size() << " tetrahedra" << std::endl;
        }
        std::cout << "deleted " << conflict_triangles.size() << " triangles, " << conflict_edges_interior.size() << " edges" << std::endl;
        std::cout << "inserted " << new_triangles.size() << " triangles, " << new_edges.size() << " edges" << std::endl;
    }
    
    if(printstats){
        std::cout << " # old simplices in interior of conflict zone, " << conflict_tetrahedra.size() << ", " << conflict_triangles.size() << ", " << conflict_edges_interior.size() << std::endl;
        std::cout << " # simplices on boundary of conflict zone, " << conflict_triangles_boundary.size() << ", " << conflict_edges_boundary.size() << std::endl; 
        std::cout << " # new simplices in interior of conflict zone, " << new_tetrahedra.size() << ", " << new_triangles.size() << ", " << new_edges.size() << std::endl;  
    }

    // 5) update wrap complex
    
    //double time1_wrap = getCPUTime(); 

    //update wrap complex for point manipulation by updating/creating intervals, updating respective lower sets
    updateWrapComplexForPointManipulation(new_simplices_ptr, conflict_simplices_boundary, deleted_intervals, printinfo, printstats);
    
    //store simplex indices of simplices deleted during this operation
    for(int i=0; i<old_simplices.size(); i++){
        for(int j=0; j<old_simplices[i]->size(); j++){
            free_simplex_indices.push_back(old_simplices[i]->at(j));
        }
    }
    

    double time2 = getCPUTime();
    //std::cout << "TIME FOR UPDATING WRAP, " << time2-time1_wrap << std::endl; 
    //std::cout << "TIME FOR UPDATING ALPHA AND WRAP FOR POINT INSERTION (WITHOUT DELAUNAY), " << time2 - time_after_del << std::endl; 
    std::cout << "TIME FOR UPDATING COMPLEXES FOR POINT INSERTION, " << time2 - time1 << std::endl;

    //filtration.print(&simplices);   
    //wrap.printOutput();
    //wrap.printIntervals();

    //store unadapted filtration for hole operations
    filtration_original = filtration_current;
    wrap.storeUnadapted(); 
    
    //recompute persistence 
    if(compute_persistence){
        computePersistence(true,false); 
        wrap.computePersistence(true,false);
    }
    //update statistics 
    if(compute_statistics){
        computeFiltrationStatistics(false,false);
        wrap.computeFiltrationStatistics(false,false);
    }
    setAlpha2(current_alpha2);
    wrap.setAlpha2(current_alpha2);
 	
    return true;
}

//update complexes by deleting point of given index
bool Alpha_Complex::deletePoint(DataPoint old_data_point, int old_data_point_index, bool printinfo, bool printstats, bool compute_persistence, bool compute_statistics)
{
    //assumption: no hole operation was performed to modify complexes
    if(holes_modified){
        undoHoleOperations(compute_persistence,compute_statistics);
    }
    
    //assumption: critical descendants were computed
    if(!wrap.wereDescendantsComputed()){
        wrap.computeFiltrationValuesWrap(true);
    }
    
    double time1 = getCPUTime();
    
    //update number of data points
    num_data_points--;
    
    //store simplices incident to deleted point
    std::set<int> cofaces = getCofacesIndices(data_points_simplex_indices[old_data_point_index], &simplices);
    std::vector<int> old_edges_incident_to_point;
    std::vector<int> old_triangles_incident_to_point;
    std::vector<int> old_tetrahedra_incident_to_point;
    for(std::set<int>::iterator it = cofaces.begin(); it != cofaces.end(); ++it){
        int coface_dim = simplices[*it].getDim();
        if(coface_dim==1){
            old_edges_incident_to_point.push_back(*it);
        }else if(coface_dim==2){
            old_triangles_incident_to_point.push_back(*it);
        }else if(coface_dim==3){
            old_tetrahedra_incident_to_point.push_back(*it);
        }
    }
    
    //delete point from filtrations, delete its interval
    deleteSimplex(&simplices[data_points_simplex_indices[old_data_point_index]]);
    
    
    // 1) detect the simplices that have to be removed (incident to old point) and those on boundary of that region

    //interior of conflict zone is already stored in old_*_incident_to_point

    if(printinfo){
        if(dim3){
            std::cout << "OLD TETRAHEDRA" << std::endl;
            for(int i=0; i<old_tetrahedra_incident_to_point.size(); i++){
                simplices[old_tetrahedra_incident_to_point[i]].print();
            }
        }
        std::cout << "OLD TRIANGLES" << std::endl;
        for(int i=0; i<old_triangles_incident_to_point.size(); i++){
            simplices[old_triangles_incident_to_point[i]].print();
        }
        std::cout << "OLD EDGES" << std::endl;
        for(int i=0; i<old_edges_incident_to_point.size(); i++){
            simplices[old_edges_incident_to_point[i]].print();
        }
    }

    //get simplices on boundary of conflict zone
    std::vector<std::vector<int> > conflict_simplices_boundary;
    if(!dim3){
        conflict_simplices_boundary = getSimplicesOnConflictBoundaryForPointDeletion(old_data_point_index, &old_triangles_incident_to_point, printinfo);
    }else{
        conflict_simplices_boundary = getSimplicesOnConflictBoundaryForPointDeletion(old_data_point_index, &old_tetrahedra_incident_to_point, printinfo);
    }
    std::vector<int> conflict_edges_boundary = conflict_simplices_boundary[0];
    std::vector<int> conflict_triangles_boundary;
    if(dim3){
        conflict_triangles_boundary = conflict_simplices_boundary[1];
    }
    
    // 2) update CGAL Delaunay triangulation
    // 3) get new simplices (in conflict zone of old point, point is inside their circumsphere), put in our data structure
    
    std::vector<std::vector<int>* > conflict_simplices_boundary_ptr;
    conflict_simplices_boundary_ptr.push_back(&conflict_edges_boundary);
    if(dim3){
        conflict_simplices_boundary_ptr.push_back(&conflict_triangles_boundary);
    }
    //get all new simplices by updating CGAL Delaunay triangulation and getting simplices in conflict zone of old point, return lists of new simplices (dim 1,2(,3))
    std::vector<std::vector<int> > new_simplices = getNewSimplicesForPointDeletion(old_data_point, old_data_point_index, conflict_simplices_boundary_ptr, printinfo);
    std::vector<int> new_edges = new_simplices[0];
    std::vector<int> new_triangles = new_simplices[1];
    std::vector<int> new_tetrahedra;
    if(dim3){
        new_tetrahedra = new_simplices[2];
    }
    
    //double time_after_del = getCPUTime();

    // 4) update alpha complex

    std::vector<std::vector<int>* > old_simplices;
    std::vector<int> old_points; old_points.push_back(data_points_simplex_indices[old_data_point_index]); //1 point deleted
    old_simplices.push_back(&old_points);
    old_simplices.push_back(&old_edges_incident_to_point); old_simplices.push_back(&old_triangles_incident_to_point);
    if(dim3){
        old_simplices.push_back(&old_tetrahedra_incident_to_point);
    }
    std::vector<std::vector<int>* > new_simplices_ptr;
    std::vector<int> new_points; //no point added
    new_simplices_ptr.push_back(&new_points);
    new_simplices_ptr.push_back(&new_edges); new_simplices_ptr.push_back(&new_triangles);
    if(dim3){
        new_simplices_ptr.push_back(&new_tetrahedra);
    }
    //update alpha complex deleting/updating/inserting given simplices, return deleted intervals
    std::set<int> deleted_intervals = updateAlphaComplexForPointManipulation(old_simplices,conflict_simplices_boundary_ptr,new_simplices_ptr);   
    
    //store freed simplex indices
    std::vector<int> free_simplex_indices_now; //simplex indices that just now became free, put later into general list (avoid interferences)
    for(int i=0; i<old_simplices.size(); i++){
        for(int j=0; j<old_simplices[i]->size(); j++){
            free_simplex_indices_now.push_back(old_simplices[i]->at(j));
        }
    }
    
    if(printinfo){
        if(dim3){
            std::cout << "deleted " << old_tetrahedra_incident_to_point.size() << " tetrahedra, inserted " << new_tetrahedra.size() << " tetrahedra" << std::endl;
        }
        std::cout << "deleted " << old_triangles_incident_to_point.size() << " triangles, " << old_edges_incident_to_point.size() << " edges" << std::endl;
        std::cout << "inserted " << new_triangles.size() << " triangles, " << new_edges.size() << " edges" << std::endl;
    }
    
    if(printstats){
        std::cout << " # old simplices in interior of conflict zone, " << old_tetrahedra_incident_to_point.size() << ", " << old_triangles_incident_to_point.size() << ", " << old_edges_incident_to_point.size() << std::endl;
        std::cout << " # simplices on boundary of conflict zone, " << conflict_triangles_boundary.size() << ", " << conflict_edges_boundary.size() << std::endl; 
        std::cout << " # new simplices in interior of conflict zone, " << new_tetrahedra.size() << ", " << new_triangles.size() << ", " << new_edges.size() << std::endl;  
    }

    // 5) update wrap complex

    //update wrap complex for point manipulation by updating/creating intervals, updating respective lower sets
    updateWrapComplexForPointManipulation(new_simplices_ptr, conflict_simplices_boundary_ptr, deleted_intervals, printinfo, printstats);
    
    //store simplex indices of simplices deleted during this operation
    for(int i=0; i<old_simplices.size(); i++){
        for(int j=0; j<old_simplices[i]->size(); j++){
            free_simplex_indices.push_back(old_simplices[i]->at(j));
        }
    }
    
    double time2 = getCPUTime();
    //std::cout << "TIME FOR UPDATING ALPHA AND WRAP FOR POINT INSERTION (WITHOUT DELAUNAY), " << time2 - time_after_del << std::endl;
    std::cout << "TIME FOR UPDATING COMPLEXES FOR POINT DELETION, " << time2 - time1 << std::endl;
    
    //store unadapted filtration for hole operations
    filtration_original = filtration_current;
    wrap.storeUnadapted(); 
    
    //recompute persistence 
    if(compute_persistence){
        computePersistence(true,false); 
        wrap.computePersistence(true,false);
    }
    //update statistics 
    if(compute_statistics){
        computeFiltrationStatistics(false,false);
        wrap.computeFiltrationStatistics(false,false);
    }
    setAlpha2(current_alpha2);
    wrap.setAlpha2(current_alpha2);
    
    return true;
}

//delete simplex from Delaunay triangulation, Alpha and Wrap complex 
void Alpha_Complex::deleteSimplex(Simplex* simplex)
{
    if(simplex->isDeleted()) {
        return;
    }
    
    int dim = simplex->getDim();
    int index = simplex->getIndex();
    exact alpha2 = getAlpha2Simplex(index);
    
    //delete from filtrations
    if(dim>=0){
        //- delete from Alpha filtration
        filtration_current.deleteSimplex(index,alpha2,&simplices,&critical,&included,&inclusion_values_and_counter);
        //- delete from Wrap filtration, delete interval, other simplices in same interval also deleted from filtration, temporarily in no interval
        std::vector<int> deleted_interval_simplices = wrap.deleteSimplex(simplex);
        //reset criticality
        for(int i=0; i<deleted_interval_simplices.size(); i++){
            interval_faces[deleted_interval_simplices[i]].clear();
            critical[deleted_interval_simplices[i]]=true;
        }
        wrap.updateIntervalFaces(&interval_faces);
        wrap.updateCritical(&critical);
    }
    
    //delete all references
    //tetrahedron
    if(dim==3){
        //- delete reference in incident triangles
        std::vector<int> tetra_triangles = simplices[index].getFacetIndices();
        for(int j=0; j<tetra_triangles.size(); j++){
            simplices[tetra_triangles[j]].deleteCofacetIndex(index);
        }
    //triangle
    }else if(dim==2){
        //- incident tetrahedra must also be deleted, no need to take care of references
        //- delete reference in incident edges
        std::vector<int> tri_edges = simplices[index].getFacetIndices();
        for(int j=0; j<tri_edges.size(); j++){
            simplices[tri_edges[j]].deleteCofacetIndex(index);
        }
    //edge
    }else if(dim==1){
        //delete all references
        //- incident cofaces must also be deleted, no need to take care of references
        //- delete reference in incident points
        std::vector<int> edge_points = simplices[index].getFacetIndices();
        for(int j=0; j<edge_points.size(); j++){
            simplices[edge_points[j]].deleteCofacetIndex(index);
        }
    }
            
    //delete in list of simplices
    if(dim>=0){
        simplices[index]=Simplex(); //set deleted simplex
    }
}

//for point addition locate all the simplices in conflict with new point 
// for dim 1,2,(3): pair of lists of simplices, first: interior ..., second: exterior of conflict zone (some empty depending on case)
std::vector<std::pair<std::vector<int>, std::vector<int> > > Alpha_Complex::getSimplicesInConflictZoneForPointAddition(DataPoint new_data_point, bool printinfo)
{
    //get conflicted faces from CGAL Delaunay triangulation
    
    //just for 3d
    std::vector<int> conflict_tetrahedra;
    std::set<int> conflict_triangles_interior;
    std::set<int> conflict_triangles_boundary;
    
    //just for 2d
    std::vector<int> conflict_triangles;
    
    //both
    std::set<int> conflict_edges_interior;
    std::set<int> conflict_edges_boundary;
    
    if(!dim3){
        
        if(!(periodic_size>0)){
            
            //locate point in triangulation
            CPoint2 new_cpoint = CPoint2(new_data_point.getX(),new_data_point.getY());
            Delaunay_triangulation::Face_handle new_point_face = delaunay_tri.locate(new_cpoint);
            
            //get conflicted faces in CGAL Delaunay triangulation
            std::vector<Delaunay_triangulation::Face_handle> conflict_faces;
            delaunay_tri.get_conflicts(new_cpoint,std::back_inserter(conflict_faces),new_point_face);
            if(printinfo){
                std::cout << "CONFLICTED FACES " << conflict_faces.size() << " of " << delaunay_tri.number_of_faces() << std::endl;
            }
            
            //find corresponding triangles in our data structure
            for(std::vector<Delaunay_triangulation::Face_handle>::iterator fit = conflict_faces.begin(); fit!=conflict_faces.end(); ++fit){
                Delaunay_triangulation::Face_handle face = *fit; //CGAL face
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<3; i++){ 
                    //only add finite vertices (=>  infinite simplex has less vertices than it should)
                    if(!delaunay_tri.is_infinite(face->vertex(i))){
                        vertices.push_back(face->vertex(i)->info()); 
                    }
                }
                std::sort(vertices.begin(),vertices.end());
                if(printinfo){
                    std::cout << " triangle ";
                    for(int i=0; i<vertices.size(); i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                std::set<int> cofaces_vertex0 = getCofacesIndices(data_points_simplex_indices[vertices[0]],&simplices);
                std::vector<int> edges_incident_to_vertex0;
                std::vector<int> triangles_incident_to_vertex0;
                for(std::set<int>::iterator it = cofaces_vertex0.begin(); it!=cofaces_vertex0.end(); ++it){
                    int coface_dim = simplices[(*it)].getDim();
                    if(coface_dim==1){
                        edges_incident_to_vertex0.push_back(*it);
                    }else if(coface_dim==2){
                        triangles_incident_to_vertex0.push_back(*it);
                    }
                }
                if(vertices.size()<3){
                    //infinite triangle
                    if(printinfo){
                        std::cout << "infinite";
                    }
                    if(vertices.size()==2){
                        //find its only finite edge in our data structure, is in conflict zone
                        int edge_index=-1;
                        for(int j=0; j<edges_incident_to_vertex0.size(); j++){ 
                            int index = edges_incident_to_vertex0[j];
                            if(!simplices[index].isDeleted() && simplices[index].sameSimplex(vertices[0],vertices[1])){
                                edge_index=index;
                                break;
                            }
                        }
                        if(edge_index==-1){
                            std::cerr << " not found" << std::endl;
                        }
                        conflict_edges_interior.insert(edge_index);
                        if(printinfo){
                            std::cout << " edge index " << edge_index;
                        }
                    }
                }else{
                    //find in our data structure
                    int triangle_index=-1;
                    for(int j=0; j<triangles_incident_to_vertex0.size(); j++){ 
                        int index = triangles_incident_to_vertex0[j];
                        if(!simplices[index].isDeleted() && simplices[index].sameSimplex(vertices[0],vertices[1],vertices[2])){
                            triangle_index=index;
                            break;
                        }
                    }
                    if(triangle_index==-1){
                        std::cerr << "not found" << std::endl;
                    }
                    conflict_triangles.push_back(triangle_index);
                    if(printinfo){
                        std::cout << " index " << triangle_index;
                        simplices[triangle_index].print();
                    }
                }
            }
            
        }else{
        //periodic 2D
            
            //locate point in triangulation
            CPoint2 new_cpoint = CPoint2(new_data_point.getX(),new_data_point.getY());
            periodic_Delaunay_triangulation::Face_handle new_point_face = periodic_delaunay_tri.locate(new_cpoint);
            
            //get conflicted faces in CGAL Delaunay triangulation
            std::vector<periodic_Delaunay_triangulation::Face_handle> conflict_faces;
            periodic_delaunay_tri.get_conflicts(new_cpoint,std::back_inserter(conflict_faces),new_point_face);
            if(printinfo){
                std::cout << "CONFLICTED FACES " << conflict_faces.size() << " of " << periodic_delaunay_tri.number_of_faces() << std::endl;
            }
            
            //find corresponding triangles in our data structure
            for(std::vector<periodic_Delaunay_triangulation::Face_handle>::iterator fit = conflict_faces.begin(); fit!=conflict_faces.end(); ++fit){
                periodic_Delaunay_triangulation::Face_handle face = *fit; //CGAL face
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<3; i++){ 
                    //no infinite vertices in periodic case
                    vertices.push_back(face->vertex(i)->info()); 
                }
                std::sort(vertices.begin(),vertices.end());
                if(printinfo){
                    std::cout << " triangle ";
                    for(int i=0; i<vertices.size(); i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                //find in our data structure
                std::set<int> cofaces_vertex0 = getCofacesIndices(data_points_simplex_indices[vertices[0]],&simplices);
                std::vector<int> triangles_incident_to_vertex0;
                for(std::set<int>::iterator it = cofaces_vertex0.begin(); it!=cofaces_vertex0.end(); ++it){
                    int coface_dim = simplices[(*it)].getDim();
                    if(coface_dim==2){
                        triangles_incident_to_vertex0.push_back(*it);
                    }
                }
                int triangle_index=-1;
                for(int j=0; j<triangles_incident_to_vertex0.size(); j++){ 
                    int index = triangles_incident_to_vertex0[j];
                    if(!simplices[index].isDeleted() && simplices[index].sameSimplex(vertices[0],vertices[1],vertices[2])){
                        triangle_index=index;
                        break;
                    }
                }
                if(triangle_index==-1){
                    std::cerr << "not found" << std::endl;
                }else{
                    conflict_triangles.push_back(triangle_index);
                    if(printinfo){
                        std::cout << " index " << triangle_index << std::endl;
                    }
                }
            }
        }
        
        //get edges in conflict zone (edges of conflict triangles) (above finite edges of infinite triangles are taken care of)
        for(std::vector<int>::iterator it = conflict_triangles.begin(); it != conflict_triangles.end(); ++it){
            std::vector<int> tri_edges = simplices[(*it)].getFacetIndices();
            for(int j=0; j<tri_edges.size(); j++){
                conflict_edges_interior.insert(tri_edges[j]);
            }
        }
        
        //if conflict edge only has incident triangles that are deleted, it must also be deleted ("interior", could also be former convex hull edge)
        //otherwise it is on boundary of conflict zone
        for(std::set<int>::iterator it = conflict_edges_interior.begin(); it != conflict_edges_interior.end(); ){
            Simplex edge = simplices[*it];
            std::vector<int> inci_triangles = edge.getCofacetIndices();
            bool has_nonconflict_triangle = false;
            for(int j=0; j<inci_triangles.size(); j++){
                if(std::find(conflict_triangles.begin(),conflict_triangles.end(),inci_triangles[j]) == conflict_triangles.end()){
                    //incident non-conflict triangle found => boundary edges
                    has_nonconflict_triangle = true;
                    break;
                }
            }
            if(has_nonconflict_triangle){
                conflict_edges_boundary.insert((*it));
                it = conflict_edges_interior.erase(it);
            }else {
                ++it;
            }
        }
        
    }else{
        
        if(!(periodic_size>0)){
            //3D
            
            //locate point in triangulation
            CPoint3 new_cpoint = CPoint3(new_data_point.getX(),new_data_point.getY(),new_data_point.getZ());
            Delaunay_triangulation3::Cell_handle new_point_face = delaunay_tri3.locate(new_cpoint);
            
            //get conflicted cells (tetrahedra) in CGAL Delaunay triangulation
            std::vector<Delaunay_triangulation3::Cell_handle> conflict_cells;
            std::vector<Delaunay_triangulation3::Facet> conflict_facets_boundary;
            delaunay_tri3.find_conflicts(new_cpoint,new_point_face,std::back_inserter(conflict_facets_boundary),std::back_inserter(conflict_cells));
            if(printinfo){
                std::cout << "CONFLICTED FACES " << conflict_cells.size() << " of " << delaunay_tri3.number_of_cells() << std::endl;
            }
            
            //find corresponding tetrahedra in our data structure
            for(std::vector<Delaunay_triangulation3::Cell_handle>::iterator cit = conflict_cells.begin(); cit!=conflict_cells.end(); ++cit){
                Delaunay_triangulation3::Cell_handle cell = *cit; //CGAL face
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<4; i++){ 
                    //only add finite vertices (=>  infinite simplex has less vertices than it should)
                    if(!delaunay_tri3.is_infinite(cell->vertex(i))){
                        vertices.push_back(cell->vertex(i)->info()); 
                    }
                }
                std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices
                if(printinfo){
                    std::cout << " tetrahedron ";
                    for(int i=0; i<vertices.size(); i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                //find in our data structure
                std::set<int> cofaces_vertex0 = getCofacesIndices(data_points_simplex_indices[vertices[0]],&simplices);
                std::vector<int> triangles_incident_to_vertex0;
                std::vector<int> tetrahedra_incident_to_vertex0;
                for(std::set<int>::iterator it = cofaces_vertex0.begin(); it!=cofaces_vertex0.end(); ++it){
                    int coface_dim = simplices[(*it)].getDim();
                    if(coface_dim==2){
                        triangles_incident_to_vertex0.push_back(*it);
                    }else if(coface_dim==3){
                        tetrahedra_incident_to_vertex0.push_back(*it);
                    }
                }
                if(vertices.size()<4){
                    //infinite tetrahedron
                    if(printinfo){
                        std::cout << "infinite";
                    }
                    if(vertices.size()==3){
                        //find its only finite triangle in our data structure, is in conflict zone
                        int triangle_index=-1;
                        for(int j=0; j<triangles_incident_to_vertex0.size(); j++){ 
                            int index = triangles_incident_to_vertex0[j];
                            if(!simplices[index].isDeleted() && simplices[index].sameSimplex(vertices[0],vertices[1],vertices[2])){
                                triangle_index=index;
                                break;
                            }
                        }
                        if(triangle_index==-1){
                            std::cerr << " not found" << std::endl;
                        }
                        conflict_triangles_interior.insert(triangle_index);
                        if(printinfo){
                            std::cout << " triangle index " << triangle_index << std::endl;
                        }
                    }
                }else{
                    //find in our data structure
                    int tetrahedron_index=-1;
                    for(int j=0; j<tetrahedra_incident_to_vertex0.size(); j++){ 
                        int index = tetrahedra_incident_to_vertex0[j];
                        if(!simplices[index].isDeleted() && simplices[index].sameSimplex(vertices[0],vertices[1],vertices[2],vertices[3])){
                            tetrahedron_index=index;
                            break;
                        }
                    }
                    if(tetrahedron_index==-1){
                        std::cerr << "not found" << std::endl;
                    }
                    conflict_tetrahedra.push_back(tetrahedron_index);
                    if(printinfo){
                        std::cout << " index " << tetrahedron_index << " ";
                        simplices[tetrahedron_index].print();
                    }
                }
            }
            
        }else{
            //3D periodic
            
            //locate point in triangulation
            CPoint3 new_cpoint = CPoint3(new_data_point.getX(),new_data_point.getY(),new_data_point.getZ());
            periodic_Delaunay_triangulation3::Cell_handle new_point_face = periodic_delaunay_tri3.locate(new_cpoint);
            
            //get conflicted cells (tetrahedra) in CGAL Delaunay triangulation
            std::vector<periodic_Delaunay_triangulation3::Cell_handle> conflict_cells;
            std::vector<periodic_Delaunay_triangulation3::Facet> conflict_facets_boundary;
            periodic_delaunay_tri3.find_conflicts(new_cpoint,new_point_face,std::back_inserter(conflict_facets_boundary),std::back_inserter(conflict_cells));
            if(printinfo){
                std::cout << "CONFLICTED FACES " << conflict_cells.size() << " of " << periodic_delaunay_tri3.number_of_cells() << std::endl;
            }
            
            //find corresponding tetrahedra in our data structure
            for(std::vector<periodic_Delaunay_triangulation3::Cell_handle>::iterator cit = conflict_cells.begin(); cit!=conflict_cells.end(); ++cit){
                periodic_Delaunay_triangulation3::Cell_handle cell = *cit; //CGAL face
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<4; i++){ 
                    //no infinite vertices in periodic case
                    vertices.push_back(cell->vertex(i)->info()); 
                }
                std::sort(vertices.begin(),vertices.end()); //vertex indices should be sorted for all simplices
                if(printinfo){
                    std::cout << " tetrahedron ";
                    for(int i=0; i<vertices.size(); i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                std::set<int> cofaces_vertex0 = getCofacesIndices(data_points_simplex_indices[vertices[0]],&simplices);
                std::vector<int> tetrahedra_incident_to_vertex0;
                for(std::set<int>::iterator it = cofaces_vertex0.begin(); it!=cofaces_vertex0.end(); ++it){
                    int coface_dim = simplices[(*it)].getDim();
                    if(coface_dim==3){
                        tetrahedra_incident_to_vertex0.push_back(*it);
                    }
                }
                //find in our data structure
                int tetrahedron_index=-1;
                for(int j=0; j<tetrahedra_incident_to_vertex0.size(); j++){ 
                    int index = tetrahedra_incident_to_vertex0[j];
                    if(!simplices[index].isDeleted() && simplices[index].sameSimplex(vertices[0],vertices[1],vertices[2],vertices[3])){
                        tetrahedron_index=index;
                        break;
                    }
                }
                if(tetrahedron_index==-1){
                    std::cerr << "not found" << std::endl;
                }
                conflict_tetrahedra.push_back(tetrahedron_index);
                if(printinfo){
                    std::cout << " index " << tetrahedron_index << " ";
                    simplices[tetrahedron_index].print();
                }
            }
        }
        
        //get triangles in conflict zone (triangles of conflict tetrahedra) (above finite triangles of infinite tetrahedra are taken care of)
        for(std::vector<int>::iterator it = conflict_tetrahedra.begin(); it != conflict_tetrahedra.end(); ++it){
            std::vector<int> tetra_triangles = simplices[(*it)].getFacetIndices();
            for(int j=0; j<tetra_triangles.size(); j++){
                conflict_triangles_interior.insert(tetra_triangles[j]);
            }
        }
        
        //if conflict triangle only has incident tetrahedra that are deleted, it must also be deleted ("interior", could also be former convex hull triangle)
        //(but it is possible that if it is former convex hull triangle that it gets reintroduced with new simplices)
        //otherwise it is on boundary of conflict zone
        for(std::set<int>::iterator it = conflict_triangles_interior.begin(); it != conflict_triangles_interior.end(); ){
            Simplex triangle = simplices[*it];
            std::vector<int> inci_tetrahedra = triangle.getCofacetIndices();
            bool has_nonconflict_tetrahedron = false;
            for(int j=0; j<inci_tetrahedra.size(); j++){
                if(std::find(conflict_tetrahedra.begin(),conflict_tetrahedra.end(),inci_tetrahedra[j]) == conflict_tetrahedra.end()){
                    //incident non-conflict triangle found => boundary edges
                    has_nonconflict_tetrahedron = true;
                    break;
                }
            }
            if(has_nonconflict_tetrahedron){
                conflict_triangles_boundary.insert((*it));
                it = conflict_triangles_interior.erase(it);
            }else {
                ++it;
            }
        }
        
        //get edges in conflict zone (edges of conflict triangles)
        //boundary triangles have exactly all the boundary edges
        for(std::set<int>::iterator it = conflict_triangles_boundary.begin(); it != conflict_triangles_boundary.end(); ++it){
            std::vector<int> tri_edges = simplices[(*it)].getFacetIndices();
            for(int j=0; j<tri_edges.size(); j++){
                conflict_edges_boundary.insert(tri_edges[j]);
            }
        }
        //interior triangles possibly have interior and boundary edges
        for(std::set<int>::iterator it = conflict_triangles_interior.begin(); it != conflict_triangles_interior.end(); ++it){
            std::vector<int> tri_edges = simplices[(*it)].getFacetIndices();
            for(int j=0; j<tri_edges.size(); j++){
                //if edge was not found on boundary yet, it is in interior
                if(conflict_edges_boundary.find(tri_edges[j])==conflict_edges_boundary.end()){
                    conflict_edges_interior.insert(tri_edges[j]);
                }
            }
        }
        
        if(printinfo){
            std::cout << "CONFLICT TRIANGLES (BOUNDARY): " << std::endl;
            for(std::set<int>::iterator it = conflict_triangles_boundary.begin(); it != conflict_triangles_boundary.end(); ++it){
                std::cout << " " << *it << " ";
                simplices[*it].print();
            }
            std::cout << "CONFLICT TRIANGLES (INTERIOR): " << std::endl;
            for(std::set<int>::iterator it = conflict_triangles_interior.begin(); it != conflict_triangles_interior.end(); ++it){
                std::cout << " " << *it << " ";
                simplices[*it].print();
            }
        }
    }
    
    if(printinfo){
        std::cout << "CONFLICT EDGES (BOUNDARY): " << std::endl;
        for(std::set<int>::iterator it = conflict_edges_boundary.begin(); it != conflict_edges_boundary.end(); ++it){
            std::cout << " " << *it << " ";
            simplices[*it].print();
        }
        std::cout << "CONFLICT EDGES (INTERIOR): " << std::endl;
        for(std::set<int>::iterator it = conflict_edges_interior.begin(); it != conflict_edges_interior.end(); ++it){
            std::cout << " " << *it << " ";
            simplices[*it].print();
        }
    }
    
    std::vector<std::pair<std::vector<int>, std::vector<int> > > conflict_simplices;
    
    conflict_simplices.push_back(std::make_pair(std::vector<int>(conflict_edges_interior.begin(),conflict_edges_interior.end()),std::vector<int>(conflict_edges_boundary.begin(),conflict_edges_boundary.end())));
    std::vector<int> empty_vector = std::vector<int>(0);
    if(!dim3){
        conflict_simplices.push_back(std::make_pair(conflict_triangles,empty_vector));
    }else{
        conflict_simplices.push_back(std::make_pair(std::vector<int>(conflict_triangles_interior.begin(),conflict_triangles_interior.end()),std::vector<int>(conflict_triangles_boundary.begin(),conflict_triangles_boundary.end())));
        conflict_simplices.push_back(std::make_pair(conflict_tetrahedra,empty_vector));
    }
    
    return conflict_simplices;
}

//for point addition get all new simplices by updating CGAL Delaunay triangulation, return lists of new simplices (dim 1,2(,3)) + lists of extra boundary simplices
std::pair<std::vector<std::vector<int> >, std::vector<std::vector<int> > > Alpha_Complex::getNewSimplicesForPointAddition(DataPoint new_data_point, int new_data_point_index, std::vector<std::vector<int>* > conflict_simplices_boundary, std::vector<std::vector<int>* > conflict_simplices_interior, bool printinfo)
{
    if(printinfo){
        std::cout << "NEW FACES" << std::endl;
    }
    std::vector<int> new_tetrahedra;
    std::vector<int> new_triangles;
    std::vector<int> new_edges;
    
    std::vector<int>* conflict_edges_boundary = conflict_simplices_boundary[0];
    
    std::set<int> extra_boundary_triangles_set;
    std::set<int> extra_boundary_edges_set;
    
    //add point to list of simplices
    int simplex_index = simplices.size();
    Simplex simplex;
    if(free_simplex_indices.size()==0){
        //append to end of list
        simplex = Simplex(simplex_index,new_data_point_index);
        simplices.push_back(simplex);
    }else{
        //fill empty space in list
        simplex_index = free_simplex_indices.back();
        free_simplex_indices.pop_back();
        simplex = Simplex(simplex_index,new_data_point_index);
        simplices[simplex_index] = simplex;
    }
    if(data_points_simplex_indices.size()>new_data_point_index){
        data_points_simplex_indices[new_data_point_index]=simplex_index;
    }else{
        data_points_simplex_indices.push_back(simplex_index);
    } 

    if(!dim3){
        
        if(!(periodic_size>0)){
            
            // 2) update CGAL Delaunay triangulation
            
            CPoint2 new_cpoint = CPoint2(new_data_point.getX(),new_data_point.getY());
            Delaunay_triangulation::Vertex_handle vertex = delaunay_tri.insert(new_cpoint);
            //store vertex handle
            vertex->info()=new_data_point_index;
            if(vertex_handles.size()==new_data_point_index){
                vertex_handles.push_back(vertex);
            }else{
                vertex_handles[new_data_point_index]=vertex; 
            } 
            
            // 3) get new simplices, put in our data structure
        
            //get new CGAL faces, are incident to new point
            Delaunay_triangulation::Face_circulator incident_faces_circ = delaunay_tri.incident_faces(vertex),done(incident_faces_circ);           
            if(incident_faces_circ != 0){
                do{
                    Delaunay_triangulation::Face_handle face = incident_faces_circ; //CGAL face
                    //get vertex indices
                    std::vector<int> vertices;
                    for(int i=0; i<3; i++){ 
                        vertices.push_back(face->vertex(i)->info()); 
                    }
                    std::sort(vertices.begin(),vertices.end());
                    if(printinfo){
                        std::cout << " triangle ";
                        for(int i=0; i<3; i++){ 
                            std::cout << vertices[i] << " ";
                        }
                    }
                    if(delaunay_tri.is_infinite(face)){
                        if(printinfo){
                            std::cout << "infinite";
                        }
                    }else{
                        //add to list of simplices
                        int triangle_index = simplices.size();
                        Simplex triangle;
                        if(free_simplex_indices.size()==0){
                            //append to end of list
                            triangle = Simplex(triangle_index,vertices[0],vertices[1],vertices[2]);
                            simplices.push_back(triangle);
                        }else{
                            //fill empty space in list
                            triangle_index = free_simplex_indices.back();
                            free_simplex_indices.pop_back();
                            triangle = Simplex(triangle_index,vertices[0],vertices[1],vertices[2]);
                            simplices[triangle_index] = triangle;
                        }
                        new_triangles.push_back(triangle_index);
                    }
                    if(printinfo){
                        std::cout << std::endl;
                    }
                }while(++incident_faces_circ != done);
            }
            
        }else{
            
            // 2) update CGAL Delaunay triangulation
            
            CPoint2 new_cpoint = CPoint2(new_data_point.getX(),new_data_point.getY());
            periodic_Delaunay_triangulation::Vertex_handle vertex = periodic_delaunay_tri.insert(new_cpoint);;
            //store vertex handle
            vertex->info()=new_data_point_index;
            if(vertex_handles_periodic.size()==new_data_point_index){
                vertex_handles_periodic.push_back(vertex);
            }else{
                vertex_handles_periodic[new_data_point_index]=vertex;
            }
            
            // 3) get new simplices, put in our data structure
        
            //get new CGAL faces, are incident to new point
            periodic_Delaunay_triangulation::Face_circulator incident_faces_circ = periodic_delaunay_tri.incident_faces(vertex),done(incident_faces_circ);
            if(incident_faces_circ != 0){
                do{
                    periodic_Delaunay_triangulation::Face_handle face = incident_faces_circ; //CGAL face
                    //get vertex indices and their offsets
                    std::vector<std::pair<int,periodic_point_offset> > vertices_with_offsets;
                    for(int i=0; i<3; i++){ 
                        vertices_with_offsets.push_back(std::make_pair(face->vertex(i)->info(),periodic_delaunay_tri.periodic_point(face,i)));
                    }
                    //vertex indices should be sorted for all simplices
                    //offsets need to be sorted together with respective indices!
                    std::sort(vertices_with_offsets.begin(),vertices_with_offsets.end());
                    if(printinfo){
                        std::cout << " triangle ";
                        for(int i=0; i<3; i++){ 
                            std::cout << vertices_with_offsets[i].first << " ";
                        }
                    }
                    //(no infinite faces in periodic case)
                    
                    int triangle_index = simplices.size();
                    if(free_simplex_indices.size()>0){
                        triangle_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                    }
                    //new triangle
                    Simplex triangle(triangle_index,vertices_with_offsets[0].first,vertices_with_offsets[1].first,vertices_with_offsets[2].first);
                    //store vertex offsets (if any vertex is not in original domain but in neighboring copy)
                    for(int i=0; i<3; i++){ 
                        periodic_point_offset ppoint = vertices_with_offsets[i].second;
                        if(ppoint.second.x()>0 || ppoint.second.y()>0){ //if vertex has any offset
                            Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0);
                            triangle.setOffset(i,off);
                        }   
                    }
                    //add to list of triangles
                    if(triangle_index==simplices.size()){
                        //append to end of list
                        simplices.push_back(triangle);
                    }else{
                        //fill empty space in list
                        simplices[triangle_index] = triangle;
                    }
                    
                    new_triangles.push_back(triangle_index);
                    if(printinfo){
                        std::cout << std::endl;
                    }
                }while(++incident_faces_circ != done);
            }
        }
    }else{
        //3D
        
        std::vector<int>* conflict_triangles_boundary = conflict_simplices_boundary[1];
        
        if(!(periodic_size>0)){
            
            // 2) update CGAL Delaunay triangulation
            
            CPoint3 new_cpoint = CPoint3(new_data_point.getX(),new_data_point.getY(),new_data_point.getZ());
            Delaunay_triangulation3::Vertex_handle vertex = delaunay_tri3.insert(new_cpoint);
            //store vertex handle
            vertex->info()=new_data_point_index;
            if(vertex_handles3.size()==new_data_point_index){
                vertex_handles3.push_back(vertex);
            }else{
                vertex_handles3[new_data_point_index]=vertex;
            }
            
            // 3) get new simplices, put in our data structure
            
            //get new CGAL cells, are incident to new point
            std::vector<Delaunay_triangulation3::Cell_handle> incident_cells; 
            delaunay_tri3.incident_cells(vertex,std::back_inserter(incident_cells));
            for(int i=0; i<incident_cells.size(); i++){
                Delaunay_triangulation3::Cell_handle cell = incident_cells[i];
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<4; i++){ 
                    vertices.push_back(cell->vertex(i)->info()); 
                }
                std::sort(vertices.begin(),vertices.end());
                if(printinfo){
                    std::cout << " tetrahedron ";
                    for(int i=0; i<4; i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                if(delaunay_tri3.is_infinite(cell)){
                    if(printinfo){
                        std::cout << "infinite";
                    }
                }else{
                    //add to list of simplices
                    int tetrahedron_index = simplices.size();
                    Simplex tetrahedron;
                    if(free_simplex_indices.size()==0){
                        //append to end of list
                        tetrahedron = Simplex(tetrahedron_index,vertices[0],vertices[1],vertices[2],vertices[3]);
                        simplices.push_back(tetrahedron);
                    }else{
                        //fill empty space in list
                        tetrahedron_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                        tetrahedron = Simplex(tetrahedron_index,vertices[0],vertices[1],vertices[2],vertices[3]);
                        simplices[tetrahedron_index] = tetrahedron;
                    }
                    new_tetrahedra.push_back(tetrahedron_index);
                }
                if(printinfo){
                    std::cout << std::endl;
                }
            }
            
            //create triangles and mutual references, add to list of simplices
            for(int i=0; i<new_tetrahedra.size(); i++){
                int tetrahedron_index = new_tetrahedra[i];
                //create or identify tetrahedron triangles
                for(int j=0; j<4; j++){
                    //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                    std::vector<int> triangle_vertices;
                    for(int k=0; k<4; k++){
                        if(k!=3-j){
                            triangle_vertices.push_back(simplices[tetrahedron_index].getVertex(k));
                        }
                    }  
                    //check if triangle was already created (boundary or for other new tetrahedron)
                    int triangle_index=-1;
                    for(std::vector<int>::iterator it=conflict_triangles_boundary->begin(); it!=conflict_triangles_boundary->end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    for(std::vector<int>::iterator it=new_triangles.begin(); it!=new_triangles.end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    //we need to check more than the boundary and new triangles, triangle could be on boundary of convex hull, not necessarily found with find_conflicts
                    if(triangle_index<0){
                        std::set<int> cofaces_vertex0 = getCofacesIndices(data_points_simplex_indices[triangle_vertices[0]],&simplices);
                        std::vector<int> triangles_incident_to_vertex0;
                        for(std::set<int>::iterator it = cofaces_vertex0.begin(); it!=cofaces_vertex0.end(); ++it){
                            int coface_dim = simplices[(*it)].getDim();
                            if(coface_dim==2){
                                triangles_incident_to_vertex0.push_back(*it);
                            }
                        }
                        for(int j=0; j<triangles_incident_to_vertex0.size(); j++){ 
                            int index = triangles_incident_to_vertex0[j];
                            if(!simplices[index].isDeleted() && simplices[index].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                                triangle_index=index;
                                break;
                            }
                        }
                        //check if the triangle we found is in list of triangles to be deleted -> create new one
                        if(triangle_index>=0){
                            for(int j=0; j<conflict_simplices_interior[1]->size(); j++){
                                if(std::find(conflict_simplices_interior[1]->begin(),conflict_simplices_interior[1]->end(),triangle_index) != conflict_simplices_interior[1]->end()){
                                    triangle_index = -1;
                                    break;
                                }
                            }
                        }
                        if(triangle_index>=0){
                           extra_boundary_triangles_set.insert(triangle_index);
                        }
                    }
                    if(triangle_index<0){
                        //create new triangle if it was not created before
                        triangle_index = simplices.size();
                        Simplex triangle;
                        if(free_simplex_indices.size()==0){
                            //append to end of list
                            triangle = Simplex(triangle_index,triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                            simplices.push_back(triangle);
                        }else{
                            //fill empty space in triangles list
                            triangle_index = free_simplex_indices.back();
                            free_simplex_indices.pop_back();
                            triangle = Simplex(triangle_index,triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                            simplices[triangle_index] = triangle;
                        }
                        new_triangles.push_back(triangle_index);
                    }
                    //add references
                    simplices[triangle_index].addCofacetIndex(tetrahedron_index);
                    simplices[tetrahedron_index].addFacetIndex(triangle_index);
                }
            }
            
        }else{
            //3D periodic
            
            // 2) update CGAL Delaunay triangulation
            
            CPoint3 new_cpoint = CPoint3(new_data_point.getX(),new_data_point.getY(),new_data_point.getZ());
            periodic_Delaunay_triangulation3::Vertex_handle vertex = periodic_delaunay_tri3.insert(new_cpoint);
            //store vertex handle
            vertex->info()=new_data_point_index;
            if(vertex_handles_periodic3.size()==new_data_point_index){
                vertex_handles_periodic3.push_back(vertex);
            }else{
                vertex_handles_periodic3[new_data_point_index]=vertex;  
            }
            
            // 3) get new simplices, put in our data structure
            
            //get new CGAL cells, are incident to new point
            std::vector<periodic_Delaunay_triangulation3::Cell_handle> incident_cells; 
            periodic_delaunay_tri3.incident_cells(vertex,std::back_inserter(incident_cells));
            for(int i=0; i<incident_cells.size(); i++){
                periodic_Delaunay_triangulation3::Cell_handle cell = incident_cells[i];
                //get vertex indices and their offsets
                std::vector<std::pair<int,periodic_point_offset3> > vertices_with_offsets;
                for(int i=0; i<4; i++){ 
                    vertices_with_offsets.push_back(std::make_pair(cell->vertex(i)->info(),periodic_delaunay_tri3.periodic_point(cell,i)));
                }
                //vertex indices should be sorted for all simplices
                //offsets need to be sorted together with respective indices!
                std::sort(vertices_with_offsets.begin(),vertices_with_offsets.end());
                if(printinfo){
                    std::cout << " tetrahedron ";
                    for(int i=0; i<4; i++){ 
                        std::cout << vertices_with_offsets[i].first << " ";
                    }
                }
                //(no infinite faces in periodic case)
                //add to list of simplices
                int tetrahedron_index = simplices.size();
                if(free_simplex_indices.size()>0){
                    tetrahedron_index = free_simplex_indices.back();
                    free_simplex_indices.pop_back();
                }
                //new tetrahedron
                Simplex tetrahedron(tetrahedron_index,vertices_with_offsets[0].first,vertices_with_offsets[1].first,vertices_with_offsets[2].first,vertices_with_offsets[3].first);
                //store vertex offsets (if any vertex is not in original domain but in neighboring copy)
                for(int i=0; i<4; i++){ 
                    periodic_point_offset3 ppoint = vertices_with_offsets[i].second;
                    if(ppoint.second.x()>0 || ppoint.second.y()>0 || ppoint.second.z()>0){ //if vertex has any offset
                        Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0, ppoint.second.z()>0);
                        tetrahedron.setOffset(i,off);
                    }   
                }
                //add to list of simplices
                if(tetrahedron_index==simplices.size()){
                    //append to end of list
                    simplices.push_back(tetrahedron);
                }else{
                    //fill empty space in list
                    simplices[tetrahedron_index] = tetrahedron;
                }
                
                new_tetrahedra.push_back(tetrahedron_index);
                if(printinfo){
                    std::cout << std::endl;
                }
            }
            
            //create triangles and mutual references, add to list of simplices
            for(int i=0; i<new_tetrahedra.size(); i++){
                int tetrahedron_index = new_tetrahedra[i];
                //create or identify tetrahedron triangles
                for(int j=0; j<4; j++){
                    //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                    std::vector<int> triangle_vertices;
                    std::vector<Offset> offsets;
                    for(int k=0; k<4; k++){
                        if(k!=3-j){
                            triangle_vertices.push_back(simplices[tetrahedron_index].getVertex(k));
                            offsets.push_back(simplices[tetrahedron_index].getOffset(k));
                        }
                    }  
                    //check if triangle was already created (boundary or for other new tetrahedron)
                    int triangle_index=-1;
                    for(std::vector<int>::iterator it=conflict_triangles_boundary->begin(); it!=conflict_triangles_boundary->end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    for(std::vector<int>::iterator it=new_triangles.begin(); it!=new_triangles.end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    //we need to check more than the boundary and new triangles, triangle could be on boundary of convex hull, not necessarily found with find_conflicts
                    if(triangle_index<0){
                        std::set<int> cofaces_vertex0 = getCofacesIndices(data_points_simplex_indices[triangle_vertices[0]],&simplices);
                        std::vector<int> triangles_incident_to_vertex0;
                        for(std::set<int>::iterator it = cofaces_vertex0.begin(); it!=cofaces_vertex0.end(); ++it){
                            int coface_dim = simplices[(*it)].getDim();
                            if(coface_dim==2){
                                triangles_incident_to_vertex0.push_back(*it);
                            }
                        }
                        for(int j=0; j<triangles_incident_to_vertex0.size(); j++){ 
                            int index = triangles_incident_to_vertex0[j];
                            if(!simplices[index].isDeleted() && simplices[index].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                                triangle_index=index;
                                break;
                            }
                        }
                        //check if the triangle we found is in list of triangles to be deleted -> create new one
                        if(triangle_index>=0){
                            for(int j=0; j<conflict_simplices_interior[1]->size(); j++){
                                if(std::find(conflict_simplices_interior[1]->begin(),conflict_simplices_interior[1]->end(),triangle_index) != conflict_simplices_interior[1]->end()){
                                    triangle_index = -1;
                                    break;
                                }
                            }
                        }
                        if(triangle_index>=0){
                           extra_boundary_triangles_set.insert(triangle_index);
                        }
                    }
                    if(triangle_index<0){
                        //create new triangle if it was not created before
                        triangle_index = simplices.size();
                        if(free_simplex_indices.size()>0){
                            triangle_index = free_simplex_indices.back();
                            free_simplex_indices.pop_back();
                        }
                        Simplex triangle(triangle_index,triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                        //set vertex offsets
                        triangle.setOffset(0,offsets[0]);
                        triangle.setOffset(1,offsets[1]);
                        triangle.setOffset(2,offsets[2]);
                        //add to list of simplices
                        if(triangle_index==simplices.size()){
                            //append to end of list
                            simplices.push_back(triangle);
                        }else{
                            //fill empty space in list
                            simplices[triangle_index] = triangle;
                        }
                        new_triangles.push_back(triangle_index);
                    }
                    //add references
                    simplices[triangle_index].addCofacetIndex(tetrahedron_index);
                    simplices[tetrahedron_index].addFacetIndex(triangle_index);
                }
            }
        }
    }
    
    //create edges and mutual references, add to list of simplices
    if(!(periodic_size>0)){
        for(int i=0; i<new_triangles.size(); i++){
            int triangle_index = new_triangles[i];
            //create or identify triangle edges
            for(int j = 0; j<3; j++){
                int a = simplices[triangle_index].getVertex(j%3); //first edge vertex
                int b = simplices[triangle_index].getVertex((j+1)%3); //second edge vertex
                if(a>b){ //vertices should be ordered
                    int tmp=a;
                    a=b; b=tmp;
                }
                //check if edge was already created (boundary or for other new triangle)
                int edge_index=-1;
                for(std::vector<int>::iterator it=conflict_edges_boundary->begin(); it!=conflict_edges_boundary->end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                for(std::vector<int>::iterator it=new_edges.begin(); it!=new_edges.end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                //we need to check more than the boundary and new edges, edge could be on boundary of convex hull, not necessarily found with find_conflicts
                if(edge_index<0){
                    std::vector<int> incident_edges = simplices[data_points_simplex_indices[a]].getCofacetIndices();
                    for(int j=0; j<incident_edges.size(); j++){ 
                        int index = incident_edges[j];
                        if(!simplices[index].isDeleted() && simplices[index].sameSimplex(a,b)){
                            edge_index=index;
                            break;
                        }
                    }
                    if(edge_index>=0){
                        //if edge is to be deleted -> create new one
                        for(int j=0; j<conflict_simplices_interior[0]->size(); j++){
                            if(std::find(conflict_simplices_interior[0]->begin(),conflict_simplices_interior[0]->end(),edge_index) != conflict_simplices_interior[0]->end()){
                                edge_index = -1;
                                break;
                            }
                        }
                    }
                    if(edge_index>=0){
                        extra_boundary_edges_set.insert(edge_index);
                    }
                }
                Simplex edge;
                if(edge_index<0){
                    //create new edge if it was not created before
                    edge_index = simplices.size();
                    if(free_simplex_indices.size()==0){
                        //append to end of list
                        edge = Simplex(edge_index,a,b);
                        simplices.push_back(edge);
                    }else{
                        //fill empty space in list
                        edge_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                        edge = Simplex(edge_index,a,b);
                        simplices[edge_index] = edge;
                    }
                    new_edges.push_back(edge_index);
                    
                    //add references to points
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(0)]);
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(1)]);
                    simplices[data_points_simplex_indices[edge.getVertex(0)]].addCofacetIndex(edge_index);
                    simplices[data_points_simplex_indices[edge.getVertex(1)]].addCofacetIndex(edge_index);
                }else{
                    edge = simplices[edge_index];
                }
                //add references to triangle
                simplices[edge_index].addCofacetIndex(triangle_index);
                simplices[triangle_index].addFacetIndex(edge_index);
            }
        }
    }else{
        //periodic case 
        
        for(int i=0; i<new_triangles.size(); i++){
            int triangle_index = new_triangles[i];
            //create or identify triangle edges
            for(int j = 0; j<3; j++){
                int a = simplices[triangle_index].getVertex(j%3); //first edge vertex
                Offset a_offset = simplices[triangle_index].getOffset(j%3);
                int b = simplices[triangle_index].getVertex((j+1)%3); //second edge vertex
                Offset b_offset = simplices[triangle_index].getOffset((j+1)%3);
                if(a>b){ //vertices should be ordered
                    int tmp=a;
                    Offset tmp_offset = a_offset;
                    a=b; b=tmp;
                    a_offset=b_offset; b_offset=tmp_offset;
                }
                //check if edge was already created (boundary or for other new triangle)
                int edge_index=-1;
                for(std::vector<int>::iterator it=conflict_edges_boundary->begin(); it!=conflict_edges_boundary->end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                for(std::vector<int>::iterator it=new_edges.begin(); it!=new_edges.end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                //we need to check more than the boundary and new edges, edge could be on boundary of convex hull, not necessarily found with find_conflicts
                if(edge_index<0){
                    std::vector<int> incident_edges = simplices[data_points_simplex_indices[a]].getCofacetIndices();
                    for(int j=0; j<incident_edges.size(); j++){ 
                        int index = incident_edges[j];
                        if(!simplices[index].isDeleted() && simplices[index].sameSimplex(a,b)){
                            edge_index=index;
                            break;
                        }
                    }
                    //if edge is to be deleted -> create new one
                    if(edge_index>=0){
                        for(int j=0; j<conflict_simplices_interior[0]->size(); j++){
                            if(std::find(conflict_simplices_interior[0]->begin(),conflict_simplices_interior[0]->end(),edge_index) != conflict_simplices_interior[0]->end()){
                                edge_index = -1;
                                break;
                            }
                        }
                    }
                    if(edge_index>=0){
                        extra_boundary_edges_set.insert(edge_index);
                    }
                }
                Simplex edge;
                if(edge_index<0){
                    //create new edge if it was not created before
                    edge_index = simplices.size();
                    if(free_simplex_indices.size()>0){
                        edge_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                    }
                    edge = Simplex(edge_index,a,b);
                    //set vertex offsets
                    edge.setOffset(0,a_offset);
                    edge.setOffset(1,b_offset);
                    //add to list of simplices
                    if(edge_index==simplices.size()){
                        //append to end of list
                        simplices.push_back(edge);
                    }else{
                        //fill empty space in list
                        simplices[edge_index] = edge;
                    }
                    new_edges.push_back(edge_index);
                    
                    //add references to points
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(0)]);
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(1)]);
                    simplices[data_points_simplex_indices[edge.getVertex(0)]].addCofacetIndex(edge_index);
                    simplices[data_points_simplex_indices[edge.getVertex(1)]].addCofacetIndex(edge_index);
                }else{
                    edge = simplices[edge_index];
                }
                //add references to triangle
                simplices[edge_index].addCofacetIndex(triangle_index);
                simplices[triangle_index].addFacetIndex(edge_index);
            }
        }
    }
    
    std::vector<std::vector<int> > new_simplices;
    new_simplices.push_back(new_edges);
    new_simplices.push_back(new_triangles);
    if(dim3){
        new_simplices.push_back(new_tetrahedra);
    }
    std::vector<std::vector<int> > extra_boundary_simplices;
    extra_boundary_simplices.push_back(std::vector<int>(extra_boundary_edges_set.begin(),extra_boundary_edges_set.end()));
    if(dim3){
        extra_boundary_simplices.push_back(std::vector<int>(extra_boundary_triangles_set.begin(),extra_boundary_triangles_set.end()));
    }
    
    //increase size of lists that store information about simplices if maximum index of simplices increased
    int increase_simplices_size = simplices.size()-filtration_values.size();
    if(increase_simplices_size){
        std::vector<bool> extra_false(increase_simplices_size,false);
        in_complex.insert(in_complex.end(),extra_false.begin(),extra_false.end());
        included.insert(included.end(),extra_false.begin(),extra_false.end());
        excluded.insert(excluded.end(),extra_false.begin(),extra_false.end());
        std::vector<int> extra_filtration_values(increase_simplices_size,-1);
        filtration_values.insert(filtration_values.end(),extra_filtration_values.begin(),extra_filtration_values.end());
        std::vector<bool> extra_critical(increase_simplices_size,true);
        critical.insert(critical.end(),extra_critical.begin(),extra_critical.end());
        std::vector<std::vector<int> > extra_interval_faces(increase_simplices_size,std::vector<int>(0));
        interval_faces.insert(interval_faces.end(),extra_interval_faces.begin(),extra_interval_faces.end());
    }
    
    return std::make_pair(new_simplices,extra_boundary_simplices);
}

//for point deletion get all simplices on boundary of conflict zone, those inside are already known, returns lists of simplices for dim 1(,2)
std::vector<std::vector<int> >  Alpha_Complex::getSimplicesOnConflictBoundaryForPointDeletion(int old_data_point_index, std::vector<int>* conflict_maxsimplices, bool printinfo)
{
    //get the simplices on boundary of conflict zone
    std::set<int> conflict_boundary_triangles_set;
    std::set<int> conflict_boundary_edges_set;
    
    
    //2D: edges of incident (with old point) triangles which are not incident themselves
    if(!dim3){
        
        for(int i=0; i<conflict_maxsimplices->size(); i++){
            std::vector<int> tri_edges = simplices[conflict_maxsimplices->at(i)].getFacetIndices();
            for(int j=0; j<tri_edges.size(); j++){
                Simplex edge = simplices[tri_edges[j]];
                if(edge.getVertex(0)!=old_data_point_index && edge.getVertex(1)!=old_data_point_index){
                    //found edge that is not incident to deleted point
                    conflict_boundary_edges_set.insert(tri_edges[j]);
                    break;
                }
            }
        }
        
    //3D: triangular faces of incident tetrahedra, which are not incident themselves + their edges
    }else{
        for(int i=0; i<conflict_maxsimplices->size(); i++){
            std::vector<int> tetra_triangles = simplices[conflict_maxsimplices->at(i)].getFacetIndices();
            for(int j=0; j<tetra_triangles.size(); j++){
                Simplex triangle = simplices[tetra_triangles[j]];
                if(triangle.getVertex(0)!=old_data_point_index && triangle.getVertex(1)!=old_data_point_index && triangle.getVertex(2)!=old_data_point_index){
                    //found triangle that is not incident to deleted point
                    conflict_boundary_triangles_set.insert(tetra_triangles[j]);
                    break;
                }
            }
        }
        for(std::set<int>::iterator it = conflict_boundary_triangles_set.begin(); it != conflict_boundary_triangles_set.end(); ++it){
            std::vector<int> tri_edges = simplices[(*it)].getFacetIndices();
            for(int j=0; j<tri_edges.size(); j++){
                conflict_boundary_edges_set.insert(tri_edges[j]);
            }
        }
    }
    
    //convert from set to vector
    std::vector<int> conflict_boundary_edges(conflict_boundary_edges_set.begin(),conflict_boundary_edges_set.end());
    if(printinfo){
        if(dim3){
            std::cout << "CONFLICT BOUNDARY TRIANGLES" << std::endl;
            for(std::set<int>::iterator it = conflict_boundary_triangles_set.begin(); it != conflict_boundary_triangles_set.end(); ++it){
                simplices[(*it)].print();
            }
        }
        std::cout << "CONFLICT BOUNDARY EDGES" << std::endl;
        for(std::vector<int>::iterator it = conflict_boundary_edges.begin(); it != conflict_boundary_edges.end(); ++it){
            simplices[(*it)].print();
        }
    }
    std::vector<std::vector<int> > conflict_boundary_simplices;
    conflict_boundary_simplices.push_back(conflict_boundary_edges);
    if(dim3){
        std::vector<int> conflict_boundary_triangles(conflict_boundary_triangles_set.begin(),conflict_boundary_triangles_set.end());
        conflict_boundary_simplices.push_back(conflict_boundary_triangles);
    }
    return conflict_boundary_simplices;
}        

//for point deletion get all new simplices by updating CGAL Delaunay triangulation and getting simplices in conflict zone of old point, return lists of new simplices (dim 1,2(,3))
std::vector<std::vector<int> > Alpha_Complex::getNewSimplicesForPointDeletion(DataPoint old_point, int old_data_point_index, std::vector<std::vector<int>* > conflict_simplices_boundary, bool printinfo)
{
    if(printinfo){
        std::cout << "NEW FACES" << std::endl;
    }
    
    std::vector<int> new_tetrahedra;
    std::vector<int> new_triangles;
    std::vector<int> new_edges;
    
    std::vector<int>* conflict_boundary_edges = conflict_simplices_boundary[0];
    
    if(!dim3){
        
        if(!(periodic_size>0)){
            
            // 2) update CGAL Delaunay triangulation

            //get vertex handle
            Delaunay_triangulation::Vertex_handle vertex_handle = vertex_handles[old_data_point_index];
            //remove vertex from triangulation
            delaunay_tri.remove(vertex_handle);

            // 3) get new simplices (in conflict zone of old point, point is inside their circumsphere), put in our data structure

            //locate old point in triangulation
            CPoint2 old_cpoint = CPoint2(old_point.getX(),old_point.getY());
            Delaunay_triangulation::Face_handle old_point_face = delaunay_tri.locate(old_cpoint);
            //get faces in conflict with old point in current CGAL Delaunay triangulation
            std::vector<Delaunay_triangulation::Face_handle> conflict_faces;
            delaunay_tri.get_conflicts(old_cpoint,std::back_inserter(conflict_faces),old_point_face);
            
            //add new triangles to our data structure
            for(std::vector<Delaunay_triangulation::Face_handle>::iterator fit = conflict_faces.begin(); fit!=conflict_faces.end(); ++fit){
                Delaunay_triangulation::Face_handle face = *fit; //CGAL face
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<3; i++){ 
                    //only add finite vertices (=>  infinite simplex has less vertices than it should)
                    if(!delaunay_tri.is_infinite(face->vertex(i))){
                        vertices.push_back(face->vertex(i)->info()); 
                    }
                }
                std::sort(vertices.begin(),vertices.end());
                if(printinfo){
                    std::cout << " triangle ";
                    for(int i=0; i<vertices.size(); i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                if(vertices.size()<3){
                    //infinite triangle
                    if(printinfo){
                        std::cout << "infinite" << std::endl;
                    }
                    //nothing to do (we already have the boundary edges)
                }else{
                    //new (finite) triangle

                    //add to list of simplices
                    int triangle_index = simplices.size();
                    Simplex triangle;
                    if(free_simplex_indices.size()==0){
                        //append to end of list
                        triangle = Simplex(triangle_index,vertices[0],vertices[1],vertices[2]);
                        simplices.push_back(triangle);
                    }else{
                        //fill empty space in list
                        triangle_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                        triangle = Simplex(triangle_index,vertices[0],vertices[1],vertices[2]);
                        simplices[triangle_index] = triangle;
                    }
                    new_triangles.push_back(triangle_index);
                    if(printinfo){
                        triangle.print();
                    } 
                }
            }
            
        }else{
            //2D periodic
            
            // 2) update CGAL Delaunay triangulation

            //get vertex handle
            periodic_Delaunay_triangulation::Vertex_handle vertex_handle = vertex_handles_periodic[old_data_point_index];
            //remove vertex from triangulation
            periodic_delaunay_tri.remove(vertex_handle);

            // 3) get new simplices (in conflict zone of old point, point is inside their circumsphere), put in our data structure
            
            //locate old point in triangulation
            CPoint2 old_cpoint = CPoint2(old_point.getX(),old_point.getY());
            periodic_Delaunay_triangulation::Face_handle old_point_face = periodic_delaunay_tri.locate(old_cpoint);
            //get faces in conflict with old point in current CGAL Delaunay triangulation
            std::vector<periodic_Delaunay_triangulation::Face_handle> conflict_faces;
            periodic_delaunay_tri.get_conflicts(old_cpoint,std::back_inserter(conflict_faces),old_point_face);
            
            //add new triangles to our data structure
            for(std::vector<periodic_Delaunay_triangulation::Face_handle>::iterator fit = conflict_faces.begin(); fit!=conflict_faces.end(); ++fit){
                periodic_Delaunay_triangulation::Face_handle face = *fit; //CGAL face
                //get vertex indices and their offsets
                std::vector<std::pair<int,periodic_point_offset> > vertices_with_offsets;
                for(int i=0; i<3; i++){ 
                    vertices_with_offsets.push_back(std::make_pair(face->vertex(i)->info(),periodic_delaunay_tri.periodic_point(face,i)));
                }
                //vertex indices should be sorted for all simplices
                //offsets need to be sorted together with respective indices
                std::sort(vertices_with_offsets.begin(),vertices_with_offsets.end());
                if(printinfo){
                    std::cout << " triangle ";
                    for(int i=0; i<3; i++){ 
                        std::cout << vertices_with_offsets[i].first << " ";
                    }
                }
                //(no infinite faces in periodic case)

                //new (finite) triangle 
                //create and add to list of triangles
                int triangle_index = simplices.size();
                if(free_simplex_indices.size()>0){
                    triangle_index = free_simplex_indices.back();
                    free_simplex_indices.pop_back();
                }
                Simplex triangle(triangle_index,vertices_with_offsets[0].first,vertices_with_offsets[1].first,vertices_with_offsets[2].first);
                //store vertex offsets (if any vertex is not in original domain but in neighboring copy)
                for(int i=0; i<3; i++){ 
                    periodic_point_offset ppoint = vertices_with_offsets[i].second;
                    if(ppoint.second.x()>0 || ppoint.second.y()>0){ //if vertex has any offset
                        Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0);
                        triangle.setOffset(i,off);
                    }   
                }
                //add to list of simplices
                if(triangle_index==simplices.size()){
                    //append to end of list
                    simplices.push_back(triangle);
                }else{
                    //fill empty space in list
                    simplices[triangle_index] = triangle;
                }
                new_triangles.push_back(triangle_index);
                if(printinfo){
                    triangle.print();
                } 
            }
            
        }
    }else{
        //3D
        
        std::vector<int>* conflict_boundary_triangles = conflict_simplices_boundary[1];
        
        if(!(periodic_size>0)){
            
            // 2) update CGAL Delaunay triangulation

            //get vertex handle
            Delaunay_triangulation3::Vertex_handle vertex_handle = vertex_handles3[old_data_point_index];
            //remove vertex from triangulation
            delaunay_tri3.remove(vertex_handle);
            
            // 3) get new simplices (in conflict zone of old point, point is inside their circumsphere), put in our data structure

            //locate old point in triangulation
            CPoint3 old_cpoint = CPoint3(old_point.getX(),old_point.getY(), old_point.getZ());
            Delaunay_triangulation3::Cell_handle old_point_face = delaunay_tri3.locate(old_cpoint);
            //get cells (tetrahedra) in conflict with old point in current CGAL delaunay triangulation
            std::vector<Delaunay_triangulation3::Cell_handle> conflict_cells;
            std::vector<Delaunay_triangulation3::Facet> conflict_facets_boundary;
            delaunay_tri3.find_conflicts(old_cpoint,old_point_face,std::back_inserter(conflict_facets_boundary),std::back_inserter(conflict_cells));
            
            //add new tetrahedra to our data structure
            for(std::vector<Delaunay_triangulation3::Cell_handle>::iterator cit = conflict_cells.begin(); cit!=conflict_cells.end(); ++cit){
                Delaunay_triangulation3::Cell_handle cell = *cit; //CGAL cell
                //get vertex indices
                std::vector<int> vertices;
                for(int i=0; i<4; i++){ 
                    //only add finite vertices (=>  infinite simplex has less vertices than it should)
                    if(!delaunay_tri3.is_infinite(cell->vertex(i))){
                        vertices.push_back(cell->vertex(i)->info()); 
                    }
                }
                std::sort(vertices.begin(),vertices.end());
                if(printinfo){
                    std::cout << " tetrahedron ";
                    for(int i=0; i<vertices.size(); i++){ 
                        std::cout << vertices[i] << " ";
                    }
                }
                if(vertices.size()<4){
                    //infinite tetrahedron
                    if(printinfo){
                        std::cout << "infinite" << std::endl;
                    }
                    //nothing to do (we already have the boundary triangles)
                }else{
                    //new (finite) tetrahedron

                    //add to list of simplices
                    int tetrahedron_index = simplices.size();
                    Simplex tetrahedron;
                    if(free_simplex_indices.size()==0){
                        //append to end of list
                        tetrahedron = Simplex(tetrahedron_index,vertices[0],vertices[1],vertices[2],vertices[3]);
                        simplices.push_back(tetrahedron);
                    }else{
                        //fill empty space in list
                        tetrahedron_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                        tetrahedron = Simplex(tetrahedron_index,vertices[0],vertices[1],vertices[2],vertices[3]);
                        simplices[tetrahedron_index] = tetrahedron;
                    }
                    new_tetrahedra.push_back(tetrahedron_index);
                    if(printinfo){
                        tetrahedron.print();
                    } 
                }
            }
            
            //add new triangles
            for(int i=0; i<new_tetrahedra.size(); i++){
                int tetrahedron_index = new_tetrahedra[i];
                //create or identify boundary triangles of new tetrahedra
                for(int j = 0; j<4; j++){
                    //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                    std::vector<int> triangle_vertices;
                    for(int k=0; k<4; k++){
                        if(k!=3-j){
                            triangle_vertices.push_back(simplices[tetrahedron_index].getVertex(k));
                        }
                    }
                    //check if triangle was already created (boundary or for other new tetrahedron)
                    int triangle_index=-1;
                    
                    for(std::vector<int>::iterator it=conflict_boundary_triangles->begin(); it!=conflict_boundary_triangles->end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    for(std::vector<int>::iterator it=new_triangles.begin(); it!=new_triangles.end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    if(triangle_index<0){
                        //create new triangle if it was not created before
                        triangle_index = simplices.size();
                        Simplex triangle;
                        if(free_simplex_indices.size()==0){
                            //append to end of list
                            triangle = Simplex(triangle_index,triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                            simplices.push_back(triangle);
                        }else{
                            //fill empty space in list
                            triangle_index = free_simplex_indices.back();
                            free_simplex_indices.pop_back();
                            triangle = Simplex(triangle_index,triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                            simplices[triangle_index] = triangle;
                        }
                        new_triangles.push_back(triangle_index);
                    }
                    //add references between triangle and tetrahedron
                    simplices[triangle_index].addCofacetIndex(tetrahedron_index);
                    simplices[tetrahedron_index].addFacetIndex(triangle_index);
                }
            }
        }else{
            //3D periodic
            
            // 2) update CGAL Delaunay triangulation

            //get vertex handle
            periodic_Delaunay_triangulation3::Vertex_handle vertex_handle = vertex_handles_periodic3[old_data_point_index];
            //remove vertex from triangulation
            periodic_delaunay_tri3.remove(vertex_handle);
            
            // 3) get new simplices (in conflict zone of old point, point is inside their circumsphere), put in our data structure

            //locate old point in triangulation
            CPoint3 old_cpoint = CPoint3(old_point.getX(),old_point.getY(), old_point.getZ());
            periodic_Delaunay_triangulation3::Cell_handle old_point_face = periodic_delaunay_tri3.locate(old_cpoint);
            //get cells (tetrahedra) in conflict with old point in current CGAL Delaunay triangulation
            std::vector<periodic_Delaunay_triangulation3::Cell_handle> conflict_cells;
            std::vector<periodic_Delaunay_triangulation3::Facet> conflict_facets_boundary;
            periodic_delaunay_tri3.find_conflicts(old_cpoint,old_point_face,std::back_inserter(conflict_facets_boundary),std::back_inserter(conflict_cells));
            
            //add new tetrahedra to our data structure
            for(std::vector<periodic_Delaunay_triangulation3::Cell_handle>::iterator cit = conflict_cells.begin(); cit!=conflict_cells.end(); ++cit){
                periodic_Delaunay_triangulation3::Cell_handle cell = *cit; //CGAL cell
                //get vertex indices and their offsets
                std::vector<std::pair<int,periodic_point_offset3> > vertices_with_offsets;
                for(int i=0; i<4; i++){ 
                    vertices_with_offsets.push_back(std::make_pair(cell->vertex(i)->info(),periodic_delaunay_tri3.periodic_point(cell,i)));
                }
                //vertex indices should be sorted for all simplices
                //offsets need to be sorted together with respective indices
                std::sort(vertices_with_offsets.begin(),vertices_with_offsets.end());
                if(printinfo){
                    std::cout << " tetrahedron ";
                    for(int i=0; i<4; i++){ 
                        std::cout << vertices_with_offsets[i].first << " ";
                    }
                }
                //(no infinite faces in periodic case)
                
                //new (finite) tetrahedron
                //add to list of simplices
                int tetrahedron_index = simplices.size();
                if(free_simplex_indices.size()>0){
                    tetrahedron_index = free_simplex_indices.back();
                    free_simplex_indices.pop_back();
                }
                Simplex tetrahedron(tetrahedron_index,vertices_with_offsets[0].first,vertices_with_offsets[1].first,vertices_with_offsets[2].first,vertices_with_offsets[3].first);
                //store vertex offsets (if any vertex is not in original domain but in neighboring copy)
                for(int i=0; i<4; i++){ 
                    periodic_point_offset3 ppoint = vertices_with_offsets[i].second;
                    if(ppoint.second.x()>0 || ppoint.second.y()>0 || ppoint.second.z()>0){ //if vertex has any offset
                        Offset off = Offset(ppoint.second.x()>0, ppoint.second.y()>0, ppoint.second.z()>0);
                        tetrahedron.setOffset(i,off);
                    }   
                }
                if(tetrahedron_index==simplices.size()){
                    //append to end of list
                    simplices.push_back(tetrahedron);
                }else{
                    //fill empty space in list
                    simplices[tetrahedron_index] = tetrahedron;
                }
                new_tetrahedra.push_back(tetrahedron_index);
                if(printinfo){
                    tetrahedron.print();
                } 
            }
            
            //add new triangles
            for(int i=0; i<new_tetrahedra.size(); i++){
                int tetrahedron_index = new_tetrahedra[i];
                //create or identify boundary triangles of new tetrahedra
                for(int j = 0; j<4; j++){
                    //pick 3 vertices for each triangle, should stay sorted, achieved by always leaving one out
                    std::vector<int> triangle_vertices;
                    std::vector<Offset> offsets;
                    for(int k=0; k<4; k++){
                        if(k!=3-j){
                            triangle_vertices.push_back(simplices[tetrahedron_index].getVertex(k));
                            offsets.push_back(simplices[tetrahedron_index].getOffset(k));
                        }
                    }
                    //check if triangle was already created (boundary or for other new tetrahedron)
                    int triangle_index=-1;
                    
                    for(std::vector<int>::iterator it=conflict_boundary_triangles->begin(); it!=conflict_boundary_triangles->end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    for(std::vector<int>::iterator it=new_triangles.begin(); it!=new_triangles.end(); ++it){
                        if(simplices[(*it)].sameSimplex(triangle_vertices[0],triangle_vertices[1],triangle_vertices[2])){
                            triangle_index=(*it);
                            break;
                        }
                    }
                    if(triangle_index<0){
                        //create new triangle if it was not created before
                        triangle_index = simplices.size();
                        if(free_simplex_indices.size()>0){                            
                            triangle_index = free_simplex_indices.back();
                            free_simplex_indices.pop_back();
                        }
                        Simplex triangle(triangle_index,triangle_vertices[0],triangle_vertices[1],triangle_vertices[2]);
                        //set vertex offsets
                        triangle.setOffset(0,offsets[0]);
                        triangle.setOffset(1,offsets[1]);
                        triangle.setOffset(2,offsets[2]);
                        //add to list of simplices
                        if(triangle_index==simplices.size()){
                            //append to end of list
                            simplices.push_back(triangle);
                        }else{
                            //fill empty space in list
                            simplices[triangle_index] = triangle;
                        }
                        new_triangles.push_back(triangle_index);
                    }
                    //add references between triangle and tetrahedron
                    simplices[triangle_index].addCofacetIndex(tetrahedron_index);
                    simplices[tetrahedron_index].addFacetIndex(triangle_index);
                }
            }
        }
    }
    
    //add new edges (same for 2D and 3D)
    
    if(!(periodic_size>0)){
        
        for(int i=0; i<new_triangles.size(); i++){
            int triangle_index = new_triangles[i];
            //create or identify triangle edges
            for(int j = 0; j<3; j++){
                int a = simplices[triangle_index].getVertex(j%3); //first edge vertex
                int b = simplices[triangle_index].getVertex((j+1)%3); //second edge vertex
                if(a>b){ //vertices should be ordered
                    int tmp=a;
                    a=b; b=tmp;
                }
                //check if edge was already created (boundary or for other new triangle)
                int edge_index=-1;
                for(std::vector<int>::iterator it=conflict_boundary_edges->begin(); it!=conflict_boundary_edges->end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                for(std::vector<int>::iterator it=new_edges.begin(); it!=new_edges.end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                if(edge_index<0){
                    //create new edge if it was not created before
                    edge_index = simplices.size();
                    Simplex edge;
                    if(free_simplex_indices.size()==0){
                        //append to end of list
                        edge = Simplex(edge_index,a,b);
                        simplices.push_back(edge);
                    }else{
                        //fill empty space in list
                        edge_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                        edge = Simplex(edge_index,a,b);
                        simplices[edge_index] = edge;
                    }
                    //add references with points
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(0)]);
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(1)]);
                    simplices[data_points_simplex_indices[edge.getVertex(0)]].addCofacetIndex(edge_index);
                    simplices[data_points_simplex_indices[edge.getVertex(1)]].addCofacetIndex(edge_index);
                    new_edges.push_back(edge_index);
                }
                //add references between triangle and edge
                simplices[edge_index].addCofacetIndex(triangle_index);
                simplices[triangle_index].addFacetIndex(edge_index);
                
            }
        }
        
    }else{
        
        for(int i=0; i<new_triangles.size(); i++){
            int triangle_index = new_triangles[i];
            //create or identify triangle edges
            for(int j = 0; j<3; j++){
                int a = simplices[triangle_index].getVertex(j%3); //first edge vertex
                Offset a_offset = simplices[triangle_index].getOffset(j%3);
                int b = simplices[triangle_index].getVertex((j+1)%3); //second edge vertex
                Offset b_offset = simplices[triangle_index].getOffset((j+1)%3);
                if(a>b){ //vertices should be ordered
                    int tmp=a;
                    Offset tmp_offset = a_offset;
                    a=b; b=tmp;
                    a_offset=b_offset; b_offset=tmp_offset;
                }
                //check if edge was already created (boundary or for other new triangle)
                int edge_index=-1;
                for(std::vector<int>::iterator it=conflict_boundary_edges->begin(); it!=conflict_boundary_edges->end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                for(std::vector<int>::iterator it=new_edges.begin(); it!=new_edges.end(); ++it){
                    if(simplices[(*it)].sameSimplex(a,b)){
                        edge_index=(*it);
                        break;
                    }
                }
                if(edge_index<0){
                    //create new edge if it was not created before
                    edge_index = simplices.size();
                    if(free_simplex_indices.size()>0){
                        edge_index = free_simplex_indices.back();
                        free_simplex_indices.pop_back();
                    }
                    Simplex edge(edge_index,a,b);
                    //set vertex offsets
                    edge.setOffset(0,a_offset);
                    edge.setOffset(1,b_offset);
                    //add to edge list
                    if(edge_index==simplices.size()){
                        //append to end of list
                        simplices.push_back(edge);
                    }else{
                        //fill empty space in edges list
                        simplices[edge_index] = edge;
                    }
                    //add references with points
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(0)]);
                    simplices[edge_index].addFacetIndex(data_points_simplex_indices[edge.getVertex(1)]);
                    simplices[data_points_simplex_indices[edge.getVertex(0)]].addCofacetIndex(edge_index);
                    simplices[data_points_simplex_indices[edge.getVertex(1)]].addCofacetIndex(edge_index);
                    new_edges.push_back(edge_index);
                }
                //add references between edge and triangle
                simplices[edge_index].addCofacetIndex(triangle_index);
                simplices[triangle_index].addFacetIndex(edge_index);
            }
        }
        
    }
    
    //increase size of lists that store information about simplices if maximum index of simplices increased
    int increase_simplices_size = simplices.size()-filtration_values.size();
    if(increase_simplices_size){
        std::vector<bool> extra_false(increase_simplices_size,false);
        in_complex.insert(in_complex.end(),extra_false.begin(),extra_false.end());
        included.insert(included.end(),extra_false.begin(),extra_false.end());
        excluded.insert(excluded.end(),extra_false.begin(),extra_false.end());
        std::vector<int> extra_filtration_values(increase_simplices_size,-1);
        filtration_values.insert(filtration_values.end(),extra_filtration_values.begin(),extra_filtration_values.end());
        std::vector<bool> extra_critical(increase_simplices_size,true);
        critical.insert(critical.end(),extra_critical.begin(),extra_critical.end());
        std::vector<std::vector<int> > extra_interval_faces(increase_simplices_size,std::vector<int>(0));
        interval_faces.insert(interval_faces.end(),extra_interval_faces.begin(),extra_interval_faces.end());
    }
    
    std::vector<std::vector<int> > new_simplices;
    new_simplices.push_back(new_edges);
    new_simplices.push_back(new_triangles);
    if(dim3){
        new_simplices.push_back(new_tetrahedra);
    }
    
    return new_simplices;
}

//update alpha complex for point manipulation by deleting/updating/inserting given simplices, return deleted intervals
std::set<int> Alpha_Complex::updateAlphaComplexForPointManipulation(std::vector<std::vector<int>* > old_simplices, std::vector<std::vector<int>* > conflict_boundary_simplices, std::vector<std::vector<int>* > new_simplices)
{ 
    //delete old simplices (simplices in interior of conflict zone), start with higher-dimensional ones (cofaces need to be deleted before a simplex)
    std::set<int> deleted_intervals;
    int maxdim = 2;
    if(dim3){
        maxdim = 3;
    }
    for(int d=maxdim; d>=0; d--){
        std::vector<int>* old_d_simplices = old_simplices[d];
        for(std::vector<int>::iterator it = old_d_simplices->begin(); it != old_d_simplices->end(); ++it){
            int interval_index = wrap.getIntervalIndex(*it);
            if(interval_index>=0){
                deleted_intervals.insert(interval_index);
            }
            deleteSimplex(&(simplices[(*it)]));
        }
    }
    
    //temporarily delete boundary simplices (in 2D: that are not in interval with old simplex from filtration), their radius will be recomputed, newly inserted in filtration
    // in 3D: we also need to update (in Wrap complex) old non-boundary simplices (outside of conflict zone) that are in interval with boundary simplex
    std::vector<int>* conflict_boundary_triangles;
    if(dim3){
        conflict_boundary_triangles = conflict_boundary_simplices[1];
        for(std::vector<int>::iterator it = conflict_boundary_triangles->begin(); it != conflict_boundary_triangles->end(); ++it){
            Simplex* triangle = &simplices[*it];
            if(critical[*it]){ 
                filtration_current.deleteSimplex(triangle->getIndex(),getAlpha2Simplex(triangle->getIndex()),&simplices,&critical,&included,&inclusion_values_and_counter);
            }else{
                //take care of intervals inside boundary 
                if(interval_faces[*it].size()>0){
                    //every face of boundary triangle is also on boundary -> whole interval inside boundary if triangle is maximum simplex in interval
                    //need to reset criticality and recompute, interval might merge to bigger interval with new simplices
                    //delete interval for now
                    filtration_current.deleteSimplex(triangle->getIndex(),getAlpha2Simplex(triangle->getIndex()),&simplices,&critical,&included,&inclusion_values_and_counter);
                    deleted_intervals.insert(wrap.getIntervalIndex(triangle->getIndex()));
                    std::vector<int> deleted_interval_simplices = wrap.deleteSimplex(triangle); //this will remove all interval simplices from Wrap filtration
                    //reset criticality
                    for(int i=0; i<deleted_interval_simplices.size(); i++){
                        interval_faces[deleted_interval_simplices[i]].clear();
                        critical[deleted_interval_simplices[i]]=true;
                    }
                    wrap.updateIntervalFaces(&interval_faces);
                    wrap.updateCritical(&critical);
                }
                
            }
        }
    }
    std::vector<int>* conflict_boundary_edges = conflict_boundary_simplices[0];
    for(std::vector<int>::iterator it = conflict_boundary_edges->begin(); it != conflict_boundary_edges->end(); ++it){
        if(critical[(*it)]){ 
            Simplex* edge = &simplices[*it];
            filtration_current.deleteSimplex((*it),getAlpha2Simplex(*it),&simplices,&critical,&included,&inclusion_values_and_counter);
        }
    }
    
    //compute radius for new simplices, starting with higher-dimensional ones, insert them in alpha filtration
    //also recompute radius (and criticality) for boundary simplices (2D: if they are not in interval with old simplex)
    if(dim3){
        std::vector<int>* new_tetrahedra = new_simplices[3];
        for(int i=0; i<new_tetrahedra->size(); i++){
            computeDelaunayRadius(&simplices[new_tetrahedra->at(i)],true,true);
        }
    }
    std::vector<int>* new_triangles = new_simplices[2];
    for(int i=0; i<new_triangles->size(); i++){
        computeDelaunayRadius(&simplices[new_triangles->at(i)],true,true);
    }
    if(dim3){
        for(std::vector<int>::iterator it = conflict_boundary_triangles->begin(); it != conflict_boundary_triangles->end(); ++it){
            if(critical[(*it)]){ 
            computeDelaunayRadius(&simplices[(*it)],true,true);
            }
        }
    }
    std::vector<int>* new_edges = new_simplices[1];
    for(int i=0; i<new_edges->size(); i++){
        computeDelaunayRadius(&simplices[new_edges->at(i)],true,true);
    }
    
    for(std::vector<int>::iterator it = conflict_boundary_edges->begin(); it != conflict_boundary_edges->end(); ++it){
        if(critical[(*it)]){ 
            computeDelaunayRadius(&simplices[(*it)],true,true);
        }
    }
    
    std::vector<int>* new_points = new_simplices[0];
    for(int i=0; i<new_points->size(); i++){
        computeDelaunayRadius(&simplices[new_points->at(i)],true,true);
    }
    
    //update pointer to list of simplices (size might have changed)
    simplices_ptr = &simplices;
    
    return deleted_intervals;
}

//update Wrap complex for point manipulation by updating/creating intervals, updating respective lower sets
void Alpha_Complex::updateWrapComplexForPointManipulation(std::vector<std::vector<int>* > new_simplices, std::vector<std::vector<int>* > conflict_boundary_simplices, std::set<int> deleted_intervals, bool printinfo, bool printstats)
{
    //update pointers to lists in Wrap complex
    wrap.updateDataPointsPtr(data_points);
    wrap.updateNumDataPoints(num_data_points);
    wrap.updateSimplicesPtr(&simplices);
    wrap.updateFiltrationValuesAlpha(&filtration_values);
    wrap.updateCritical(&critical);
    wrap.updateIntervalFaces(&interval_faces);
    wrap.updateListSizes();

    //get list of boundary intervals: intervals of boundary simplices and their cofaces
    std::set<int> old_boundary_intervals;
    std::vector<int>* conflict_boundary_edges = conflict_boundary_simplices[0];
    for(std::vector<int>::iterator it = conflict_boundary_edges->begin(); it != conflict_boundary_edges->end(); ++it){
        Simplex edge = simplices[*it];
        //add edge interval (if index>=0 i.e. not deleted)
        if(wrap.getIntervalIndex(*it)>=0){
            old_boundary_intervals.insert(wrap.getIntervalIndex(*it));
        }
        //add intervals of incident triangles (if index>=0 i.e. old triangle)
        std::vector<int> inci_triangles = edge.getCofacetIndices();
        for(int j=0; j<inci_triangles.size(); j++){
            if(simplices[inci_triangles[j]].isDeleted()){
                std::cerr << "ERROR(Alpha_Complex::addPoint): deleted triangle reference" << std::endl;
            }else{
                int triangle_interval_index = wrap.getIntervalIndex(inci_triangles[j]);
                if(triangle_interval_index>=0){
                    old_boundary_intervals.insert(triangle_interval_index);
                }
            }
        }
    }
    std::vector<int>* conflict_boundary_triangles;
    if(dim3){
        conflict_boundary_triangles = conflict_boundary_simplices[1];
        for(std::vector<int>::iterator it = conflict_boundary_triangles->begin(); it != conflict_boundary_triangles->end(); ++it){
            Simplex triangle = simplices[*it];
            //add triangle interval (if index>=0 i.e. not deleted)
            if( wrap.getIntervalIndex(*it)>=0){
                old_boundary_intervals.insert(wrap.getIntervalIndex(*it));
            }
            //add intervals of incident tetrahedra (if index>=0 i.e. old tetrahedron)
            std::vector<int> inci_tetrahedra = triangle.getCofacetIndices();
            for(int j=0; j<inci_tetrahedra.size(); j++){
                if(simplices[inci_tetrahedra[j]].isDeleted()){
                    std::cerr << "ERROR(Alpha_Complex::addPoint): deleted tetrahedron reference" << std::endl;
                }else{
                    int tetrahedron_interval_index = wrap.getIntervalIndex(inci_tetrahedra[j]);
                    if(tetrahedron_interval_index>=0){
                        old_boundary_intervals.insert(tetrahedron_interval_index);
                    }
                }
            }
        }
    }
    
    //create new intervals, start with higher-dimensional simplices

    //create new intervals for new simplices and those that were in interval with deleted simplex, some old intervals might be deleted
    std::vector<int> new_intervals;
    if(dim3){
        std::vector<int>* new_tetrahedra = new_simplices[3];
        for(int i=0; i<new_tetrahedra->size(); i++){
            std::pair<int,std::vector<int> > out = wrap.createInterval(&simplices[new_tetrahedra->at(i)]);
            int new_interval_index = out.first;
            std::vector<int> extra_deleted_intervals = out.second;
            if(new_interval_index>=0){
                new_intervals.push_back(new_interval_index);
            }
            if(extra_deleted_intervals.size()>0){
                deleted_intervals.insert(extra_deleted_intervals.begin(),extra_deleted_intervals.end());
                for(int j=0; j<extra_deleted_intervals.size(); j++){
                    std::set<int>::iterator it = old_boundary_intervals.find(extra_deleted_intervals[j]);
                    if(it != old_boundary_intervals.end()){
                        old_boundary_intervals.erase(it);
                    }
                }
            }
        }
    }
    std::vector<int>* new_triangles = new_simplices[2];
    for(int i=0; i<new_triangles->size(); i++){
        std::pair<int,std::vector<int> > out = wrap.createInterval(&simplices[new_triangles->at(i)]);
        int new_interval_index = out.first;
        std::vector<int> extra_deleted_intervals = out.second;
        if(new_interval_index>=0){
            new_intervals.push_back(new_interval_index);
        }
        if(extra_deleted_intervals.size()>0){
            deleted_intervals.insert(extra_deleted_intervals.begin(),extra_deleted_intervals.end());
            for(int j=0; j<extra_deleted_intervals.size(); j++){
                std::set<int>::iterator it = old_boundary_intervals.find(extra_deleted_intervals[j]);
                if(it != old_boundary_intervals.end()){
                    old_boundary_intervals.erase(it);
                }
            }
        }
    }
    
    std::set<int> boundary_intervals;
    
    //simplices on boundary need new interval if their nonsingular interval was deleted
    if(dim3){
        if(printinfo){
            std::cout << "BOUNDARY TRIANGLES" << std::endl;
        }
        for(std::vector<int>::iterator it = conflict_boundary_triangles->begin(); it != conflict_boundary_triangles->end(); ++it){
            int interval_index = wrap.getIntervalIndex(*it);
            if(printinfo){
                std::cout << " interval " << interval_index << " ";
                simplices[*it].print();
            }
            if(interval_index<0){
                std::pair<int,std::vector<int> > out = wrap.createInterval(&simplices[(*it)]);
                int new_interval_index = out.first;
                std::vector<int> extra_deleted_intervals = out.second; 
                if(new_interval_index>=0){
                    new_intervals.push_back(new_interval_index);
                }
                if(extra_deleted_intervals.size()>0){
                    deleted_intervals.insert(extra_deleted_intervals.begin(),extra_deleted_intervals.end());
                    for(int j=0; j<extra_deleted_intervals.size(); j++){
                        std::set<int>::iterator it = old_boundary_intervals.find(extra_deleted_intervals[j]);
                        if(it != old_boundary_intervals.end()){
                            old_boundary_intervals.erase(it);
                        }
                    }
                }
            }
            boundary_intervals.insert(wrap.getIntervalIndex(*it));
        }
    }
    
    std::vector<int>* new_edges = new_simplices[1];
    for(int i=0; i<new_edges->size(); i++){
        std::pair<int,std::vector<int> > out = wrap.createInterval(&simplices[new_edges->at(i)]);
        int new_interval_index = out.first;
        std::vector<int> extra_deleted_intervals = out.second; //no deleted intervals in this case
        if(new_interval_index>=0){
            new_intervals.push_back(new_interval_index);
        }
        if(extra_deleted_intervals.size()>0){
            deleted_intervals.insert(extra_deleted_intervals.begin(),extra_deleted_intervals.end());
            for(int j=0; j<extra_deleted_intervals.size(); j++){
                std::set<int>::iterator it = old_boundary_intervals.find(extra_deleted_intervals[j]);
                if(it != old_boundary_intervals.end()){
                    old_boundary_intervals.erase(it);
                }
            }
        }
    }
    
    if(printinfo){
        std::cout << "BOUNDARY EDGES" << std::endl;
    }
    for(std::vector<int>::iterator it = conflict_boundary_edges->begin(); it != conflict_boundary_edges->end(); ++it){
        //edges on boundary need new interval if their nonsingular interval was deleted
        int interval_index = wrap.getIntervalIndex(*it);
        if(printinfo){
            std::cout << " interval " << interval_index << " ";
            simplices[*it].print();
        }
        if(interval_index<0){
            std::pair<int,std::vector<int> > out = wrap.createInterval(&simplices[(*it)]);
            int new_interval_index = out.first;
            std::vector<int> extra_deleted_intervals = out.second; 
            if(new_interval_index>=0){
                new_intervals.push_back(new_interval_index);
                
            }
            if(extra_deleted_intervals.size()>0){
                deleted_intervals.insert(extra_deleted_intervals.begin(),extra_deleted_intervals.end());
                for(int j=0; j<extra_deleted_intervals.size(); j++){
                    std::set<int>::iterator it = old_boundary_intervals.find(extra_deleted_intervals[j]);
                    if(it != old_boundary_intervals.end()){
                        old_boundary_intervals.erase(it);
                    }
                }
            }
        }
        boundary_intervals.insert(wrap.getIntervalIndex(*it));
    }
    
    std::vector<int>* new_points = new_simplices[0];
    for(int i=0; i<new_points->size(); i++){
        std::pair<int,std::vector<int> > out = wrap.createInterval(&simplices[(new_points->at(i))]);
        int new_interval_index = out.first;
       // std::vector<int> extra_deleted_intervals = out.second; //no deleted intervals in this case
        if(new_interval_index>=0){
            new_intervals.push_back(new_interval_index);
        }
    }
    
    //compute predecessors for new intervals
    for(int i=0; i<new_intervals.size(); i++){
        wrap.computeIntervalPredecessors(new_intervals[i]);
    }
    if(printinfo){
        std::cout << "old boundary intervals ";
        for(std::set<int>::iterator it = old_boundary_intervals.begin(); it != old_boundary_intervals.end(); ++it){
            std::cout << (*it) << " ";
        }
        std::cout << std::endl;
        std::cout << "boundary intervals ";
        for(std::set<int>::iterator it = boundary_intervals.begin(); it != boundary_intervals.end(); ++it){
            std::cout << (*it) << " ";
        }
        std::cout << std::endl;
        std::cout << "deleted intervals ";
        for(std::set<int>::iterator it = deleted_intervals.begin(); it != deleted_intervals.end(); ++it){
            std::cout << (*it) << " ";
        }
        std::cout << std::endl;
        std::cout << "new intervals ";
        for(int i=0; i<new_intervals.size(); i++){
            std::cout << new_intervals[i] << " ";
        }
        std::cout << std::endl;
    }
    
    if(printstats){
        std::cout << " # deleted intervals, " << deleted_intervals.size() << std::endl;
        std::cout << " # new intervals, " << new_intervals.size() << std::endl;  
    }

    //delete predecessor references to deleted intervals from old intervals next to boundary
    wrap.clearDeletedPredecessors(&old_boundary_intervals,&deleted_intervals); 

    //add predecessor reference to new intervals for intervals next to boundary
    for(int i=0; i<new_intervals.size(); i++){
        wrap.addIntervalAsPredecessor(new_intervals[i]);
    }

    //update alpha2 in conflict zone (new intervals) and in lower set of noncritical boundary simplices
    
    //(1) delete references to deleted critical descendants (and update first_critical_descendant and alpha2 if necessary),
    //for boundary intervals, delete references to deleted critical descendants (and update first_critical_descendant and alpha2 if necessary)
    // go recursively to predecessors, stop recursion at critical simplices or if nothing was deleted (this predecessor was already found)
    int count_delete_old_references = 0;
    if(dim3){
       for(std::vector<int>::iterator it = conflict_boundary_triangles->begin(); it != conflict_boundary_triangles->end(); ++it){
            int interval_index = wrap.getIntervalIndex(*it);
            count_delete_old_references += wrap.deleteOldReferencesFromLowerSet(interval_index,&deleted_intervals);
        } 
    }
    for(std::vector<int>::iterator it = conflict_boundary_edges->begin(); it != conflict_boundary_edges->end(); ++it){
        int interval_index = wrap.getIntervalIndex(*it);
        count_delete_old_references += wrap.deleteOldReferencesFromLowerSet(interval_index,&deleted_intervals);
    }
    int count_update_alpha_lowerset_noncritical_boundary = 0;
    
    //(2) update lower sets of (now) noncritical boundary intervals (with old critical descendants)
    //propagate old critical descendants through noncritical boundary into conflict zone (and maybe beyond), their lower set might contain additional intervals now
    for(std::set<int>::iterator it = boundary_intervals.begin(); it != boundary_intervals.end(); ++it){
        //for nonsingular intervals only
        if(wrap.getIntervalSize(*it)>1){
            count_update_alpha_lowerset_noncritical_boundary += wrap.updateFiltrationValuesInLowerSetOfNonCriticalBoundary(*it);
        }
    }
    
    //(3) update lower sets for new critical intervals
    int count_update_alpha_lowerset_new = wrap.updateFiltrationValuesInLowerSetsOf(&new_intervals);
    
    if(printstats){
        std::cout << " # calls of delete old references, " << count_delete_old_references << std::endl;
        std::cout << " # calls of updates in lower set of noncritical boundary, " << count_update_alpha_lowerset_noncritical_boundary << std::endl;
        std::cout << " # calls of updates in lower set of new intervals, " << count_update_alpha_lowerset_new << std::endl;
    }
    
    //put deleted interval indices in list of free indices (can be only used for next operation to avoid confusion)
    wrap.addFreeIndicesIntervals(&deleted_intervals);
    
}

//compute valid interval structure (discrete Morse function) from given noisy filtration values (assume simplices were already computed), positive imprecision threshold computed from conflicts or prescribed
exact Alpha_Complex::computeIntervalsFromNoisyFiltration(std::vector<exact>* noisy_filtration_values, exact prescribed_threshold, bool weighted_, bool use_value_of_min_simplex, bool relative_threshold, bool reconsider_blocked_intervals, bool compute_wrap_and_persistence, bool printstats, bool printinfo)
{
    weighted = weighted_;
    
    //TEST
    /*bool test_threshold_persistence=false;
    if(test_threshold_persistence){
        printinfo=false;
        std::cout << "THRESHOLD, NUM_ACCEPTED_INTERVALS" << std::endl;
    }*/
    
    //determine maximum negative difference (absolute and relative) between filtration value of a simplex and its face
    exact max_negative_difference = 0;
    exact max_relative_negative_difference = 0;
    exact min_noisy_value = 0;
    for(int i=0; i<simplices.size(); i++){
        if(simplices[i].getDim()>=0 && noisy_filtration_values->at(i)<min_noisy_value){
            min_noisy_value = noisy_filtration_values->at(i);
        }
    }
    for(int i=0; i<simplices.size(); i++){
        std::set<int> faces = getFacesIndices(i,&simplices);
        for(std::set<int>::iterator it = faces.begin(); it != faces.end(); ++it){
            int face_index = (*it);
            exact difference = noisy_filtration_values->at(i) - noisy_filtration_values->at(face_index);
            if(-difference>max_negative_difference){
                max_negative_difference = -difference;
            }
            if(relative_threshold && (noisy_filtration_values->at(i)-min_noisy_value)>0){
                exact relative_difference = (noisy_filtration_values->at(i) - noisy_filtration_values->at(face_index))/(noisy_filtration_values->at(i)-min_noisy_value);
                if(-relative_difference > max_relative_negative_difference){
                    max_relative_negative_difference = -relative_difference;
                }
            }
        }
    }
    
    if(printinfo){
        std::cout << "max negative difference, " << -max_negative_difference << std::endl;
        if(relative_threshold){
            std::cout << "max relative negative difference, " << -max_relative_negative_difference << std::endl;
        }
    }
    
    exact epsilon_repair_filtration = 0.;
    std::vector<exact> reconstructed_filtration_values = repairFiltration(noisy_filtration_values, epsilon_repair_filtration,!use_value_of_min_simplex,printinfo);
    
    //compute list of possible intervals, sort by total error (assuming maximum value is assigned)  
    std::priority_queue<std::pair<exact,std::vector<int> > > possible_intervals;
    //for each simplex compute possible intervals, where this simplex is maximum, all faces can be minimum
    for(int i=0; i<simplices.size(); i++){
        std::set<int> faces = getFacesIndices(i,&simplices);
        for(std::set<int>::iterator it = faces.begin(); it != faces.end(); ++it){
            if(weighted || (simplices_ptr->at(*it).getDim())>0){ //points are not in bigger intervals in unweighted case
                std::vector<int> interval_simplices;
                interval_simplices.push_back(i);
                int min_simplex = (*it);
                interval_simplices.push_back(min_simplex);
                std::vector<int> min_simplex_vertices = (simplices_ptr->at(min_simplex)).getVertices();
                for(std::set<int>::iterator it2 = faces.begin(); it2 != faces.end(); ++it2){
                    int face_index = (*it2);
                    if(face_index!=min_simplex){
                        bool coface_of_min = true;
                        for(int j=0; j<min_simplex_vertices.size(); j++){
                            if(!(simplices_ptr->at(face_index)).hasVertex(min_simplex_vertices[j])){
                                coface_of_min = false; 
                                break;
                            }
                        }
                        if(coface_of_min){
                            interval_simplices.push_back(face_index);
                        }
                    }
                }
                exact max_error = 0;
                for(int j=0; j<interval_simplices.size(); j++){
                    if(!use_value_of_min_simplex && j>0){
                        if(reconstructed_filtration_values[i]-reconstructed_filtration_values[interval_simplices[j]]>max_error){
                            max_error = reconstructed_filtration_values[i]-reconstructed_filtration_values[interval_simplices[j]];
                        }
                    }else if(j!=min_simplex){
                        if(reconstructed_filtration_values[interval_simplices[j]]-reconstructed_filtration_values[min_simplex]>max_error){
                            max_error = reconstructed_filtration_values[interval_simplices[j]]-reconstructed_filtration_values[min_simplex];
                        }
                    }
                }
                possible_intervals.push(std::make_pair(-max_error,interval_simplices));
            }
        }
    }
    
    //initialize: all simplices critical
    std::vector<std::vector<int> > reconstructed_interval_faces(simplices.size(),std::vector<int>(0));
    std::vector<bool> reconstructed_critical(simplices.size(),true);
    std::vector<int> max_simplex_of_simplex_interval(simplices.size());
    for(int i=0; i<simplices.size(); i++){ 
        max_simplex_of_simplex_interval[i] = i;
    }
    
    //set threshold for maximal error, either max_negative_difference+epsilon_repair_filtration (necessary for repairing the filtration) or prescribed value
    exact threshold = max_negative_difference+epsilon_repair_filtration; 
    if(prescribed_threshold>=0){
        threshold = prescribed_threshold;
    }
    
    //store intervals whose insertion was blocked by a specific simplex, reconsider if the value of that simplex changes
    std::unordered_map<int,std::vector<std::pair<exact,std::vector<int> > > > blocked_intervals_by_simplex;
    
    int count_accepted_intervals = 0;
    
    //iterate through intervals, starting with smallest error (biggest negative)
    if(printinfo){
        std::cout << "COMPUTE INTERVALS FROM NOISY FILTRATION VALUES" << std::endl; 
        std::cout << "prescribed threshold, " << prescribed_threshold << std::endl;
        std::cout << "relative threshold, " << relative_threshold << std::endl;
        std::cout << "use value of min simplex, " << use_value_of_min_simplex << std::endl;
        std::cout << "reconsider blocked intervals, " << reconsider_blocked_intervals << std::endl;  
    }
    exact last_queue_value = -1;
    while(!possible_intervals.empty()){
        std::pair<exact,std::vector<int> > queue_element = possible_intervals.top();
        exact interval_error = -queue_element.first; 
        std::vector<int> interval_simplices = queue_element.second;
        possible_intervals.pop();
        
        //stop if error exceeds threshold
        if(!relative_threshold){
            //if(test_threshold_persistence){
            //    if(interval_error>last_queue_value){
            //      std::cout << interval_error << ", " << count_accepted_intervals << std::endl;
            //  }
            //}else{
                if(interval_error>threshold){
                    break;
                }
            //}
        }else{
            //alternatively we use threshold relative to filtration values, if interval exceeds it we skip it, later ones can have smaller relative errors
            if((reconstructed_filtration_values[interval_simplices[0]]-min_noisy_value)>0){
                exact relative_error = interval_error/(reconstructed_filtration_values[interval_simplices[0]]-min_noisy_value);
                if(!(relative_error<=max_relative_negative_difference)){
                    if(printinfo){
                        std::cout << " SKIP" << std::endl;
                    }
                    continue;
                }
            }
        }
        
        if(printinfo){
            std::cout << "interval error " << interval_error << " simplices ";
            for(int i=0; i<interval_simplices.size(); i++){
                std::cout << interval_simplices[i] << " ";
            }
            if(relative_threshold){
                exact relative_error = interval_error/(reconstructed_filtration_values.at(interval_simplices[0])-min_noisy_value);
                std::cout << " relative_error " << relative_error;
            }
            std::cout << std::endl;
        }
        
        //check compatibility with already added intervals
        //1. other intervals need to be contained or disjoint
        
        //check whether current intervals of future interval simplices are completely contained
        std::set<int> old_intervals_max_simplices;
        bool old_intervals_contained = true;
        //current interval of max simplex has same size -> skip (either disjoint or the same, can happen if this interval was prescribed)
        if(reconstructed_interval_faces[interval_simplices[0]].size()+1==interval_simplices.size()){
            old_intervals_contained = false;
        }
        //get current intervals (maximum simplices)
        for(int i=0; i<interval_simplices.size(); i++){
            old_intervals_max_simplices.insert(max_simplex_of_simplex_interval[interval_simplices[i]]);
        }
        for(std::set<int>::iterator it=old_intervals_max_simplices.begin(); it!=old_intervals_max_simplices.end(); ++it){
            if(!old_intervals_contained){
                break;
            }
            std::vector<int> old_interval_simplices = reconstructed_interval_faces[*it];
            //check if all simplices of old interval are contained in new one
            if(std::find(interval_simplices.begin(),interval_simplices.end(),(*it))==interval_simplices.end()){
                old_intervals_contained = false;
                if(printinfo){
                    std::cout << " incompatible with interval of  " << (*it) << std::endl;
                }
                break;
            }
            for(int i=0; i<old_interval_simplices.size(); i++){
                if(std::find(interval_simplices.begin(),interval_simplices.end(),old_interval_simplices[i])==interval_simplices.end()){
                    old_intervals_contained = false;
                    if(printinfo){
                        std::cout << " incompatible with interval of  " << (*it) << std::endl;
                    }
                    break;
                }
            }
        }
        
        //2. values of all cofacets (facets) of interval simplex cannot be smaller (bigger) than value of maximum (minimum) simplex (new value for interval simplex)
        bool cofacets_ok = true;
        if(old_intervals_contained){
            for(int i=0; i<interval_simplices.size(); i++){
                std::vector<int> co_facets = (simplices_ptr->at(interval_simplices[i])).getCofacetIndices();
                if(use_value_of_min_simplex){
                    co_facets = (simplices_ptr->at(interval_simplices[i])).getFacetIndices();
                }
                for(int j=0; j<co_facets.size(); j++){
                    if(std::find(interval_simplices.begin(),interval_simplices.end(),co_facets[j])==interval_simplices.end()){ //only outside connections
                        if(!use_value_of_min_simplex && reconstructed_filtration_values[co_facets[j]]-reconstructed_filtration_values[interval_simplices[0]]<0){
                            blocked_intervals_by_simplex[co_facets[j]].push_back(queue_element);
                            cofacets_ok = false;
                            if(printinfo){
                                std::cout << " blocked by connection with " << co_facets[j] << std::endl;
                            }
                            break;
                        }
                        if(use_value_of_min_simplex && reconstructed_filtration_values[co_facets[j]]-reconstructed_filtration_values[interval_simplices[1]]>0){
                            blocked_intervals_by_simplex[co_facets[j]].push_back(queue_element);
                            cofacets_ok = false;
                            if(printinfo){
                                std::cout << " blocked by connection with " << co_facets[j] << std::endl;
                            }
                            break;
                        }
                    }
                }
                if(!cofacets_ok){
                    break;
                }
            }
        }
        
        //put simplices in new interval, adapt values, other related data
        if(old_intervals_contained && cofacets_ok){
            count_accepted_intervals++;
            exact interval_value = reconstructed_filtration_values[interval_simplices[0]];
            if(use_value_of_min_simplex){
                interval_value = reconstructed_filtration_values[interval_simplices[1]];
            }
            if(printinfo){
                std::cout << " accepted value " << CGAL::to_double(interval_value) << std::endl;
            }
            for(int i=0; i<interval_simplices.size(); i++){
                reconstructed_critical[interval_simplices[i]]=false;
                if(reconstructed_filtration_values[interval_simplices[i]]!=interval_value){
                    reconstructed_filtration_values[interval_simplices[i]]=interval_value;
                    //when the value of a simplex changes, we reinsert the blocked intervals into the queue for reconsideration 
                    if(reconsider_blocked_intervals){
                        if(blocked_intervals_by_simplex.find(interval_simplices[i])!=blocked_intervals_by_simplex.end()){
                            for(int j=0; j<blocked_intervals_by_simplex[interval_simplices[i]].size(); j++){
                                possible_intervals.push(blocked_intervals_by_simplex[interval_simplices[i]][j]);
                            }
                            blocked_intervals_by_simplex.erase(interval_simplices[i]);
                        }
                    }
                }
                if(max_simplex_of_simplex_interval[interval_simplices[i]]!=interval_simplices[0]){
                    max_simplex_of_simplex_interval[interval_simplices[i]]=interval_simplices[0];
                    reconstructed_interval_faces[interval_simplices[0]].push_back(interval_simplices[i]);
                    reconstructed_interval_faces[interval_simplices[i]].clear();
                }
            }
        }
        last_queue_value=interval_error;
    }   
    
    
    //in unweighted case: set all negative filtration values 0
    if(!weighted){
        for(int i=0; i<simplices.size(); i++){
            if(reconstructed_filtration_values[i]<0){
                if(simplices[i].getDim()>=0){
                    reconstructed_filtration_values[i]=0;
                }else{
                    reconstructed_filtration_values[i]=-1;
                }
            }
        }
    }
    
    if(printinfo){
        std::cout << "threshold: " << threshold << std::endl;
    }
    
    
    //TEST
    /*int count_changes=0;
    for(int i=0; i<simplices.size(); i++){
        std::vector<int> reconstructed_interval_faces_i = reconstructed_interval_faces[i];
        bool same_interval_faces = (reconstructed_interval_faces_i.size() == interval_faces[i].size());
        std::sort(reconstructed_interval_faces_i.begin(),reconstructed_interval_faces_i.end());
        std::sort(interval_faces[i].begin(),interval_faces[i].end());
        if(same_interval_faces){
            for(int j=0; j<interval_faces[i].size(); j++){
                if(interval_faces[i][j]!=reconstructed_interval_faces_i[j]){
                    same_interval_faces = false;
                    break;
                }
            }
        }
        if(critical[i]!=reconstructed_critical[i] || !same_interval_faces){
            count_changes++;
            (simplices[i]).printShort();
            std::cout << "value " << filtration_values[i] << " -> " << reconstructed_filtration_values[i] << ", critical " << critical[i] << " -> " << reconstructed_critical[i];
            std::cout << ", interval_faces ";
            for(int j=0; j<interval_faces[i].size(); j++){
                std::cout << interval_faces[i][j] << " ";
            }
            std::cout << "-> ";
            for(int j=0; j<reconstructed_interval_faces_i.size(); j++){
                std::cout << reconstructed_interval_faces_i[j] << " ";
            };
            std::cout << std::endl;
        }
    }
    std::cout << "# changes " << count_changes << std::endl;*/
    
    
    //store computed filtration values, criticality, interval faces
    filtration_values = reconstructed_filtration_values;
    critical = reconstructed_critical; critical_ptr = &critical;
    interval_faces = reconstructed_interval_faces;
    
    //reset other lists storing information about simplices
    included = std::vector<bool>(simplices.size(),false);
    excluded = std::vector<bool>(simplices.size(),false);
    
    //compute filtration
    filtration_current.clear();
    for(int i=0; i<simplices.size(); i++){
        if(!simplices[i].isDeleted()){
            filtration_current.appendElement(filtration_values[i],i);
        }
    }
    filtration_current.sort(&simplices,&critical,&included,&inclusion_values_and_counter);
    filtration_original = filtration_current;
    
    
    if(compute_wrap_and_persistence){
        //compute Wrap complex
        wrap = Wrap(&filtration_values,&critical,&interval_faces,dim3,weighted,periodic_size,getNumDataPoints(),data_points,&simplices,false);
        wrap.storeUnadapted(); 
        wrap.setExhaustivePersistence(exhaustive_persistence);

        //compute persistence for Alpha and Wrap complex
        computePersistence(true,false); 
        wrap.computePersistence(true,false);
        
        wrap.setAlpha2(current_alpha2);
    }
    
    //TEST
    if(printinfo){
        std::cout << "FILTRATION RECONSTRUCTED" << std::endl;
        for(int i=0; i<filtration_current.size(); i++){
            std::cout << i << " " << filtration_current.getElementValue(i) << " (" << noisy_filtration_values->at(filtration_current.getElementIndex(i)) << ") critical " << critical[filtration_current.getElementIndex(i)] << " ";
            if(filtration_current.getElementValue(i)!=noisy_filtration_values->at(filtration_current.getElementIndex(i))){
                std::cout << "CHANGE ";
                double change = std::abs(CGAL::to_double(filtration_current.getElementValue(i)-noisy_filtration_values->at(filtration_current.getElementIndex(i))));
            }
            simplices[filtration_current.getElementIndex(i)].print();
        }
    }
    if(printstats){
        int count_nonsingular_intervals = 0;
        int count_singular_intervals = 0;
        int sum_interval_sizes = 0;
        for(int i=0; i<simplices_ptr->size(); i++){
            if(max_simplex_of_simplex_interval[i]==i){
                if(reconstructed_interval_faces[i].size()>0){
                    //nonsingular interval with this simplex as maximum
                    count_nonsingular_intervals++;
                    sum_interval_sizes += (1+reconstructed_interval_faces[i].size());
                }else{
                    //singular interval
                    count_singular_intervals++;
                    sum_interval_sizes++;
                }
            }
        }
        
        std::vector<int> count_value_changed;
        std::vector<int> count_simplices;
        std::vector<double> sum_values_changed;
        std::vector<double> max_values_changed;
        for(int d=0; d<3+dim3; d++){
            count_value_changed.push_back(0);
            count_simplices.push_back(0);
            sum_values_changed.push_back(0);
            max_values_changed.push_back(0);
        }
       
        double max_change = 0;
        for(int i=0; i<filtration_current.size(); i++){
            if(filtration_current.getElementDim(i,simplices_ptr)>=0){
                count_simplices[filtration_current.getElementDim(i,simplices_ptr)]++;
            }
            if(filtration_current.getElementValue(i)!=noisy_filtration_values->at(filtration_current.getElementIndex(i))){
                int dim = filtration_current.getElementDim(i,simplices_ptr);
                if(dim>=0){
                    count_value_changed[dim]++;
                    double change = std::abs(CGAL::to_double(filtration_current.getElementValue(i)-noisy_filtration_values->at(filtration_current.getElementIndex(i))));
                    sum_values_changed[dim]+=change;
                    if(change>max_values_changed[dim]){
                        max_values_changed[dim]=change;
                    }
                }
            }
        }
     
        if(printinfo){
            std::cout << "INTERVAL RECONSTRUCTION STATISTICS" << std::endl;
            std::cout << "threshold, " << threshold << std::endl;
            std::cout << "prescribed threshold, " << prescribed_threshold << std::endl;
            std::cout << "relative threshold, " << relative_threshold << std::endl;
            std::cout << "use value of min simplex, " << use_value_of_min_simplex << std::endl;
            std::cout << "reconsider blocked intervals, " << reconsider_blocked_intervals << std::endl;  
            std::cout << " ------" << std::endl;
            std::cout << "# simplices, " << simplices_ptr->size() << std::endl;
            std::cout << "# intervals, " << count_nonsingular_intervals+count_singular_intervals << std::endl;
            std::cout << "# accepted intervals, " << count_accepted_intervals << std::endl;
            std::cout << "# nonsingular intervals, " << count_nonsingular_intervals <<  ", " << (count_nonsingular_intervals*100.)/((count_nonsingular_intervals+count_singular_intervals)*1.) << "%"<< std::endl;
            std::cout << "avg interval size, " << (sum_interval_sizes*1.)/((count_nonsingular_intervals+count_singular_intervals)*1.) << std::endl;
            std::cout << " ------ num, %, max, avg" << std::endl;
            for(int d=0; d<3+dim3; d++){
                std::cout << "values changed for " << d << "-simplices, " << count_value_changed[d] << ", "  << (count_value_changed[d]*100.)/(count_simplices[d]*1.) << "%";
                std::cout << ", " << max_values_changed[d] << ", " << (sum_values_changed[d]*1.)/(count_value_changed[d]*1.) << std::endl;
            }
        }
    }
    //set alpha^2
    setAlpha2(current_alpha2); //set alpha^2 value
    
    if(compute_wrap_and_persistence){
        computeFiltrationStatistics(false,false);
        wrap.computeFiltrationStatistics(false,false);
    }
    return threshold;
}

//compute circumcenters of maximum simplices in Bregman-Delaunay triangulation, either restrict to domain 
//(delete all simplices with circumcenter outside of domain (positive quadrant/orthant or standard simplex)) or print as output
void Alpha_Complex::computeCircumcentersMaxSimplices(bool restrict_to_domain, bool print)
{
    if(print){
        std::cout << "simplex_index, vertices, circumcenter" << std::endl;
    }
    //iterate over all maximum simplices, check if circumcenter outside domain, if so delete with interval simplices
    std::vector<int> simplices_to_delete;
    for(int i=0; i<simplices_ptr->size(); i++){
        Simplex simplex = simplices_ptr->at(i);
        if(!simplex.isDeleted() && simplex.getDim()==(2+(dim3&&!on_standard_simplex))){
            std::vector<exact> circumcenter;
            if(!dim3){
                //circumcenter of triangle in 2d
                std::vector<CPoint2> tri_points;
                DataPoint p=data_points->at(simplex.getVertex(0));
                tri_points.push_back(CPoint2(p.getX(),p.getY()));
                p=data_points->at(simplex.getVertex(1));
                tri_points.push_back(CPoint2(p.getX(),p.getY()));
                p=data_points->at(simplex.getVertex(2));
                tri_points.push_back(CPoint2(p.getX(),p.getY()));
                CPoint2 center = CGAL::circumcenter(tri_points[0],tri_points[1],tri_points[2]);
                circumcenter.push_back(center.x()); 
                circumcenter.push_back(center.y());
            }else{
                if(!on_standard_simplex){
                    //circumcenter of tetrahedron in 3d
                    std::vector<CPoint3> tetra_points;
                    DataPoint p=data_points->at(simplex.getVertex(0));
                    tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    p=data_points->at(simplex.getVertex(1));
                    tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    p=data_points->at(simplex.getVertex(2));
                    tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    p=data_points->at(simplex.getVertex(3));
                    tetra_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    CPoint3 center = CGAL::circumcenter(tetra_points[0],tetra_points[1],tetra_points[2],tetra_points[3]);
                    circumcenter.push_back(center.x()); 
                    circumcenter.push_back(center.y());
                    circumcenter.push_back(center.z());
                }else{
                    //circumcenter of triangle in 3d
                    std::vector<CPoint3> tri_points;
                    DataPoint p=data_points->at(simplex.getVertex(0));
                    tri_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    p=data_points->at(simplex.getVertex(1));
                    tri_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    p=data_points->at(simplex.getVertex(2));
                    tri_points.push_back(CPoint3(p.getX(),p.getY(),p.getZ()));
                    CPoint3 center = CGAL::circumcenter(tri_points[0],tri_points[1],tri_points[2]);
                    circumcenter.push_back(center.x()); 
                    circumcenter.push_back(center.y());
                    circumcenter.push_back(center.z());

                }
            }
            if(print){
                std::cout << i << ",";
                std::vector<int> vertices = simplex.getVertices();
                for(int j=0; j<vertices.size(); j++){
                    std::cout  << " " << vertices[j];
                }
                std::cout << ",";
                for(int j=0; j<circumcenter.size(); j++){
                    std::cout  << " " << circumcenter[j];
                }
                std::cout << std::endl;
            }
            if(restrict_to_domain){
                bool outside = false;
                for(int j=0; j<circumcenter.size(); j++){
                    if(circumcenter[j]<=0){
                        outside = true;
                        break;
                    }
                }
                if(outside){
                    simplices_to_delete.push_back(simplex.getIndex());
                }
            }
        }
    }
    
    if(restrict_to_domain){
        deleteSimplicesWithIntervals(&simplices_to_delete);
    }
}

//delete given set of simplices together with their intervals
void Alpha_Complex::deleteSimplicesWithIntervals(std::vector<int>* simplices_to_delete){
    
    if(simplices_to_delete->size()>0){
        for(int i=0; i<simplices_to_delete->size(); i++){
            Simplex simplex = simplices_ptr->at(simplices_to_delete->at(i));
            
            //get all interval simplices
            std::vector<int> interval_simplices = ((wrap.getIntervals())->at(wrap.getIntervalIndex(simplex.getIndex()))).getSimplices();

            for(int j=0; j<interval_simplices.size(); j++){
                int index = interval_simplices[j];
                //delete simplex from Alpha filtration
                bool deleted = filtration_current.deleteSimplex(index,getAlpha2Simplex(index),&simplices,&critical,&included,&inclusion_values_and_counter);
                //reset criticality
                interval_faces[index].clear();
                critical[index]=true;
                //delete references from facets
                std::vector<int> facets = simplices[index].getFacetIndices();
                for(int j=0; j<facets.size(); j++){
                    simplices[facets[j]].deleteCofacetIndex(index);
                }
                //delete in list of simplices
                simplices[index]=Simplex(); //set deleted simplex
            }
        }
        
        filtration_original = filtration_current;
        computeWrapAndPersistence(true,false);
    }
} 


//transform noisy filtration into correct filtration by either moving simplices before their cofaces or after their faces
std::vector<exact> Alpha_Complex::repairFiltration(std::vector<exact>* noisy_filtration_values, exact step_size, bool move_before, bool printinfo)
{
    //initialize new filtration values with input
    std::vector<exact> repaired_filtration_values(noisy_filtration_values->begin(),noisy_filtration_values->end());
    std::vector<int> count_simplices_moved_next_to_simplex(simplices.size(),0);
    
    //iterate through all simplices
    //move before cofaces: start with higher-dim, move after faces: start with lower dim
    int maxdim = 2+dim3;
    for(int d=1; d<=maxdim; d++){
        for(int i=0; i<simplices_ptr->size(); i++){
            if((move_before && (simplices_ptr->at(i)).getDim()==(maxdim-d))||(!move_before && (simplices_ptr->at(i)).getDim()==d)){
                //either compare filtration value with faces (move after) or cofaces (move before)
                std::set<int> co_faces;
                if(move_before){
                    co_faces = getCofacesIndices(i,simplices_ptr);
                }else{
                    co_faces = getFacesIndices(i,simplices_ptr);
                }
                //check if there is any conflict with cofaces (faces), if yes find coface (face) with minimum (maximum) value, corresponds to maximal negative difference
                bool conflict=false;
                int extreme_co_face = -1;
                exact extreme_value = 0;
                for(std::set<int>::iterator it = co_faces.begin(); it!=co_faces.end(); ++it){
                    if((move_before && (repaired_filtration_values[*it]-repaired_filtration_values[i])<0) 
                     ||(!move_before && (repaired_filtration_values[i]-repaired_filtration_values[*it])<0)){
                        if(!conflict){
                            conflict = true; extreme_value = repaired_filtration_values[*it]; extreme_co_face = (*it);
                        }else if((move_before && repaired_filtration_values[*it]<extreme_value)||(!move_before && repaired_filtration_values[*it]>extreme_value)){
                            extreme_value = repaired_filtration_values[*it]; extreme_co_face = (*it);
                        }
                    }
                }
                //in case of conflict move before coface of minimum value (move after face of maximum value)
                //move by maximum_negative_difference/(maxdim*(counter)) before (after) this simplex,)
                //like this every simplex gets a distinct value, simplex does not get moved further than 2*max_negative_difference (some have to get moved by at least this amount, 
                //   if we move face before coface before coface total movement should still be less than 2*max_negative_difference because of maxdim-factor)
                if(conflict){
                    count_simplices_moved_next_to_simplex[extreme_co_face]=count_simplices_moved_next_to_simplex[extreme_co_face]+1;
                    double sign = 1.;
                    if(move_before){
                        sign = -1.;
                    }
                    exact old_value = repaired_filtration_values[i];
                    repaired_filtration_values[i]=repaired_filtration_values[extreme_co_face]+sign*step_size/(maxdim*count_simplices_moved_next_to_simplex[extreme_co_face]);
                    if(printinfo){
                        if(move_before){
                            std::cout << " move " << i << " before " << extreme_co_face << " value " << old_value << "->"  << repaired_filtration_values[i] << std::endl;
                        }else{ 
                            std::cout << " move " << i << " after " << extreme_co_face << " value " << old_value << "->"  << repaired_filtration_values[i] << std::endl;
                        }
                    }
                }
            }
        }
    }
    return repaired_filtration_values;
}

//check whether there are no objections to merging interval of simplex with interval of its facet with given set of common vertices
std::pair<bool,std::vector<int> > Alpha_Complex::checkMergeIntervals(int simplex_index, int facet_index, std::vector<int> common_vertices, bool use_value_of_min_simplex, std::vector<bool>* current_critical, std::vector<std::vector<int> >* current_interval_faces, std::vector<int>* max_simplex_of_simplex_interval, std::vector<exact>* current_filtration_values)
{
    std::vector<int> additional_interval_simplices; //list of simplices in merged interval additional to those in current interval of simplex and in current interval of facet
    std::vector<int> simplices_with_new_values; //list of simplices that will change their filtration value when merging the intervals
    bool check_ok = true; //all checks were passed so far
    bool new_common_vertices = false; //merging with current set of common vertices does not work, but we found a new possible set of common vertices (minimum simplex)
    
    //a. get list of additional interval simplices (are faces of of max simplex which contain all common vertices, not already contained in either interval)
    std::set<int> simplex_faces = getFacesIndices(simplex_index,simplices_ptr);
    for(std::set<int>::iterator it = simplex_faces.begin(); it != simplex_faces.end(); ++it){
        bool additional_ok = true;
        if(facet_index == (*it)){
            additional_ok = false;
            continue; //this is the facet we currently consider
        }
        for(int i=0; i<current_interval_faces->at(simplex_index).size(); i++){
            if(current_interval_faces->at(simplex_index)[i] == (*it)){
                additional_ok = false; //simplex already contained in simplex interval
                break; 
            }
        }
        if(additional_ok){
            for(int i=0; i<current_interval_faces->at(facet_index).size(); i++){
                if(current_interval_faces->at(facet_index)[i] == (*it)){
                    additional_ok = false; //simplex already contained in facet interval
                    break;
                }
            }
            if(additional_ok){
                for(int i=0; i<common_vertices.size(); i++){
                    if(!simplices[*it].hasVertex(common_vertices[i])){
                        additional_ok = false; //simplex does not contain all common vertices
                        break;
                    }
                }
            }
        }
        //all checks ok -> additional interval simplex
        if(additional_ok){
            additional_interval_simplices.push_back(*it);
        }
    }
    
    //store all simplices of merged interval in list
    //store all simplices with new values in list
    std::vector<int> merged_interval_simplices;
    merged_interval_simplices.push_back(simplex_index);
    merged_interval_simplices.push_back(facet_index);
    //simplices_with_new_values.push_back(facet_index);
    for(int i=0; i<current_interval_faces->at(facet_index).size(); i++){
        merged_interval_simplices.push_back(current_interval_faces->at(facet_index)[i]);
        //simplices_with_new_values.push_back(current_interval_faces->at(facet_index)[i]);
    }
    for(int i=0; i<additional_interval_simplices.size(); i++){
        merged_interval_simplices.push_back(additional_interval_simplices[i]);
        //simplices_with_new_values.push_back(additional_interval_simplices[i]);
    }
    for(int i=0; i<current_interval_faces->at(simplex_index).size(); i++){
        merged_interval_simplices.push_back(current_interval_faces->at(simplex_index)[i]);
    }
    
    int use_value_of_this_simplex = simplex_index;
    if(use_value_of_min_simplex){
        for(int i=0; i<merged_interval_simplices.size(); i++){
            if(simplices[merged_interval_simplices[i]].getDim()<simplices[use_value_of_this_simplex].getDim()){
                use_value_of_this_simplex = merged_interval_simplices[i];
            }
        }
    }
    for(int i=0; i<merged_interval_simplices.size(); i++){
        if((max_simplex_of_simplex_interval->at(use_value_of_this_simplex)) != (max_simplex_of_simplex_interval->at(merged_interval_simplices[i]))){
            simplices_with_new_values.push_back(merged_interval_simplices[i]);
        }
    }
    
    //b. check if additional interval simplices are available (are critical or in interval with other additional simplices)
    for(int i=0; i<additional_interval_simplices.size(); i++){
        int additional_index = additional_interval_simplices[i];
        
        if(!current_critical->at(additional_index)){
            if(max_simplex_of_simplex_interval->at(additional_index)!=additional_index){
                //in interval of coface => coface must also be in interval, face of main simplex
                bool max_simplex_in_new_interval = false;
                for(std::set<int>::iterator it = simplex_faces.begin(); it != simplex_faces.end(); ++it){
                    if(max_simplex_of_simplex_interval->at(additional_index)==(*it)){
                        max_simplex_in_new_interval = true;
                    }
                }
                if(!max_simplex_in_new_interval){
                    check_ok = false;
                    break;
                }
                
            }
            //all faces in interval of additional simplex must also be additional simplices, contain common vertices
            //if there is a simplex with lower dimension than current minimum simplex, we try to update set of common vertices
            std::vector<int> interval_faces_of_additional_simplex = current_interval_faces->at(max_simplex_of_simplex_interval->at(additional_index));
            for(int j=0; j<interval_faces_of_additional_simplex.size(); j++){
                for(int k=0; k<common_vertices.size(); k++){
                    if(!simplices[interval_faces_of_additional_simplex[j]].hasVertex(common_vertices[k])){
                        check_ok = false; //simplex does not contain all common vertices
                        new_common_vertices = true;
                        break;
                    }
                }
                if(!check_ok){
                    break;
                }
            }
            if(!check_ok && new_common_vertices){
                //find minimum simplex of this interval
                int min_simplex = interval_faces_of_additional_simplex[0];
                for(int j=0; j<interval_faces_of_additional_simplex.size(); j++){
                    if(simplices[interval_faces_of_additional_simplex[j]].getDim()<simplices[min_simplex].getDim()){
                        min_simplex = interval_faces_of_additional_simplex[j];
                    }
                }
                //minimum simplex is lower dimensional than current candidate for merged interval
                if(simplices[min_simplex].getDim()<common_vertices.size()-1){
                    //check if it is contained in set of common vertices
                    std::vector<int> min_simplex_vertices = simplices[min_simplex].getVertices();
                    for(int j=0; j<min_simplex_vertices.size(); j++){
                        if(std::find(common_vertices.begin(),common_vertices.end(),min_simplex_vertices[j])==common_vertices.end()){
                            //new minimum simplex is not a face of previous minimum simplex -> cannot merge
                            new_common_vertices = false;
                            break;
                        }
                    }
                    //update set of common vertices = minimum simplex, check function will be recalled with new set of common vertices
                    if(new_common_vertices){
                        common_vertices = min_simplex_vertices;
                    }
                }else{
                    new_common_vertices = false;
                    break;
                }
            }
            if(!check_ok){
                if(new_common_vertices){
                    //recall check function with new set of common vertices
                    return std::make_pair(false,common_vertices);
                }else{
                    //merging not possible
                    return std::make_pair(false,std::vector<int>(0));
                }
            }
        }
    }
    
    //c. check if for any of the simplices which will get a new filtration value assigned, a conflict (negative difference) would be created with one of its facets or cofacets
    for(int i=0; i<simplices_with_new_values.size(); i++){
        std::vector<int> facets = simplices[simplices_with_new_values[i]].getFacetIndices();
        for(std::vector<int>::iterator it = facets.begin(); it != facets.end(); ++it){
            //check facets that are not in merged interval
            if(std::find(merged_interval_simplices.begin(),merged_interval_simplices.end(),(*it)) == merged_interval_simplices.end()){
                exact new_difference = current_filtration_values->at(use_value_of_this_simplex) - current_filtration_values->at(*it);
                exact old_difference = current_filtration_values->at(simplices_with_new_values[i]) - current_filtration_values->at(*it);
                if(new_difference<0 && new_difference<old_difference){
                    check_ok = false;
                    //merging the intervals like this would create a new conflict, maybe we can also add the corresponding face to the interval
                    //check which of the common vertices is not contained in this simplex -> delete
                    for(std::vector<int>::iterator it2 = common_vertices.begin(); it2 != common_vertices.end();){ 
                        if(simplices[*it].hasVertex(*it2)){
                           ++it2;
                        }else{
                           it2 = common_vertices.erase(it2); 
                           new_common_vertices = true;
                           //update set of common vertices
                        }
                    }
                    if(common_vertices.size()<1 || (weighted || common_vertices.size()<2)){
                        new_common_vertices = false;
                    }
                }
            }
            if(!check_ok){
                break;
            }
        }
        if(check_ok){
           //also check cofacets
            std::vector<int> cofacets = simplices[simplices_with_new_values[i]].getCofacetIndices();
            for(std::vector<int>::iterator it = cofacets.begin(); it != cofacets.end(); ++it){
                //check cofacets that are not in merged interval
                if(std::find(merged_interval_simplices.begin(),merged_interval_simplices.end(),(*it)) == merged_interval_simplices.end()){
                    exact new_difference = current_filtration_values->at(*it) - current_filtration_values->at(use_value_of_this_simplex);
                    exact old_difference = current_filtration_values->at(*it) - current_filtration_values->at(simplices_with_new_values[i]);
                    if(new_difference<0 && new_difference<old_difference){
                        //merging the intervals like this would create a new conflict => do not merge
                        check_ok = false;
                        break;
                    }
                }
            }
        }
        if(!check_ok){
            if(new_common_vertices){
                //recall check function with new set of common vertices
                return std::make_pair(false,common_vertices);
            }else{
                //merging not possible
                return std::make_pair(false,std::vector<int>(0));
            }
        }
    }
    //intervals can be merged, return set of simplices in merged interval
    return std::make_pair(true,merged_interval_simplices);
}

//compute statistics (# simplices, ...) for whole filtration (for every filtration value)
void Alpha_Complex::computeFiltrationStatistics(bool printstats, bool after_hole_operation)
{
    //basic filtered complex statistics
    computeFiltrationStatistics0(printstats);

    filtration_statistics_computed = true;
}

//compute and print all results for the computational experiments in the holes paper, for a single run
void Alpha_Complex::computeResultsPaperHoles(std::ostream& output)
{
    output << ">>Delaunay_Triangulation<<" << std::endl;
    output << "num_vertices, " << getNumDataPoints() << std::endl;
    output << "num_edges, " << getNumEdgesFull() << std::endl;
    output << "num_triangles, " << getNumTrianglesFull() << std::endl;
    output << "num_tetrahedra, " << getNumTetrahedraFull() << std::endl;
    output << "num_simplices, " << filtration_original.size() << std::endl;
    output << std::endl;
    
    output << ">>Persistence_Matrices_Densities<<" << std::endl;
    setExhaustivePersistence(false);
    wrap.setExhaustivePersistence(false);
    computePersistence(true,false);
    wrap.computePersistence(true,false);
    persistence.computeResultsPaperHolesDensities("standard_",output);
    setExhaustivePersistence(true);
    wrap.setExhaustivePersistence(true);
    computePersistence(true,false);
    wrap.computePersistence(true,false);
    persistence.computeResultsPaperHolesDensities("exhaustive_",output);
    output << std::endl;
    
    output << ">>Canonical_Chains_Size<<" << std::endl;
    persistence.computeResultsPaperHolesCanonicalChainsSize(output);
    output << std::endl;
    
    output << ">>Dependence_Structure_Size<<" << std::endl;
    setHoleOperationsCohomology(false);
    persistence.computeResultsPaperHolesDependencesSize(output);
    output << std::endl;
    setHoleOperationsCohomology(true);
    persistence.computeResultsPaperHolesDependencesSize(output);
    output << std::endl;
}

//print full information about alpha and wrap complexes including points, simplices, radii, persistence pairs
void Alpha_Complex::printFullInformation(std::ostream &output)
{
    //general parameters
    output << "dim, weighted, periodic_size, on_standard_simplex" << std::endl;
    int dim=2; 
    if(dim3){
        dim=3;
    }
    output << dim << ", " << weighted << ", " << periodic_size << ", " << on_standard_simplex << std::endl;
    output << std::endl;
    
    //points
    output << "POINTS: index, x, y";
    if(dim3){
        output << ", z";
    }
    if(weighted){
        output << ", weight";
    }
    output << std::endl;
    for(int i=0; i<data_points->size(); i++){
        DataPoint point = data_points->at(i);
        if(!(point.isDeleted())){
            output << i << ", " << point.getX_double() << ", " << point.getY_double();
            if(dim3){
                output << ", " << point.getZ_double();
            }
            if(weighted){
                output << ", " << point.getWeight_double();
            }
            output << std::endl;
        }
    }
    output << std::endl;
    
    output << "SIMPLICES: alpha_filtration_index, dim, alpha_radius^2, wrap_radius^2, birth?, critical?, point1 point2 ..." << std::endl;
    std::cout << "0, -1, -1, -1, 1, 1, " << std::endl;
    for(int i=1; i<filtration_current.size();i++){ 
        exact value = filtration_current.getElement(i).first;
        Simplex* simplex = filtration_current.getSimplex(i,simplices_ptr);
        output << i << ", " << simplex->getDim() << ", ";
        output << (CGAL::to_double(value)) << ", ";
        if(wrap.isInComplex(simplex->getIndex())){
            output << CGAL::to_double(wrap.getAlpha2Simplex(simplex->getIndex())) << ", ";
        }else{
            output << "-1" << ", ";
        }
        output << persistence.isPositiveSimplexCurrent(i) << ", ";
        output << critical[simplex->getIndex()] << ", ";
        std::vector<int> vertices = simplex->getVertices();
        for(int j=0; j<vertices.size()-1; j++){
            output << vertices[j] << " ";
        }
        if(vertices.size()>0){
            output << vertices[vertices.size()-1];
        }
        output << std::endl;
    }
    output << std::endl;
    output << "BOUNDARY MATRIX" << std::endl;
    persistence.printBoundaryMatrix(output);
    
    output << std::endl;
    persistence.printPersistencePairs(output,&filtration_current);
}

//print statistics in two lines: 
//# points, # Delaunay edges, # Delaunay triangles, (# Delaunay tetrahedra), # intervals 
//Alpha, # Alpha edges, # Alpha triangles, (# Alpha tetrahedra), # Wrap edges, # Wrap triangles, (# Wrap tetrahedra), # neg crit edges, # pos crit edges, # neg crit triangles, (# pos crit triangles), (# critical tetrahedra), Betti0, Betti1, (Betti2)
void Alpha_Complex::print(){
   if(filtration_statistics_computed){
    //print values that do not depend on alpha: # points, # Delaunay edges, # Delaunay triangles, (# Delaunay tetrahedra), # intervals
    std::cout << std::setw(10) << std::right << "# points" << ", " << std::setw(16) << std::right << "# Delaunay edges" << ", " << std::setw(20) << std::right << "# Delaunay triangles" << ", ";
    if(dim3){
        std::cout << std::setw(16) << std::right << "# Delaunay tetra" << ", ";
    }
    std::cout << std::setw(11) << std::right << "# intervals" << std::endl;
    
    std::cout << std::setw(10) << std::right << getNumDataPoints() << ", " << std::setw(16) << std::right << getNumEdgesFull() << ", " << std::setw(20) << std::right << getNumTrianglesFull() << ", ";
    if(dim3){
        std::cout << std::setw(16) << std::right << getNumTetrahedraFull() << ", ";
    }
    std::cout << std::setw(11) << std::right << wrap.getNumIntervals() << ", " << std::endl;
    
    //for current alpha print in one line: Alpha, # Alpha edges, # Alpha triangles, (# Alpha tetrahedra), # Wrap edges, # Wrap triangles, (# Wrap tetrahedra)
    //   # neg crit edges, # pos crit edges, # neg crit triangles, (# pos crit triangles), (# critical tetrahedra), Betti0, Betti1, (Betti2)
    if(!weighted){
        std::cout << std::setw(10) << std::right << "alpha" << ", ";
    }else{
        std::cout << std::setw(10) << std::right << "alpha^2" << ", ";
    }
    std::cout << std::setw(10) << std::right << "# Alpha ed" << ", " << std::setw(11) << std::right << "# Alpha tri" << ", ";
    if(dim3){
        std::cout << std::setw(13) << std::right << "# Alpha tetra" << ", ";
    }
    std::cout << std::setw(9) << std::right << "# Wrap ed" << ", " << std::setw(10) << std::right << "# Wrap tri" << ", " ;
    if(dim3){
        std::cout << std::setw(12) << std::right << "# Wrap tetra" << ", ";
    }
    std::cout << std::setw(11) << std::right << "# crit ed -" << ", " << std::setw(11) << std::right << "# crit ed +" << ", ";
    if(dim3){
        std::cout << std::setw(12) << std::right << "# crit tri -" << ", " << std::setw(12) << std::right << "# crit tri +" << ", ";
        std::cout << std::setw(12) << std::right << "# crit tetra" << ", ";
    }else{
        std::cout << std::setw(10) << std::right << "# crit tri" << ", ";
    }
    std::cout << std::setw(6) << std::right << "Betti0" << ", " << std::setw(6) << std::right << "Betti1";
    if(dim3){
        std::cout << ", " << std::setw(6) << std::right << "Betti2";
    }
    std::cout << std::endl;
    
    if(!weighted){
        std::cout << std::setw(10) << std::right << std::sqrt(CGAL::to_double(getAlpha2_double())) << ", ";
    }else{
        std::cout << std::setw(10) << std::right << CGAL::to_double(getAlpha2_double()) << ", ";
    }
    std::cout << std::setw(10) << std::right << getNumEdges() << ", " << std::setw(11) << std::right << getNumTriangles() << ", ";
    if(dim3){
        std::cout << std::setw(13) << std::right << getNumTetrahedra() << ", ";
    }
    std::cout << std::setw(9) << std::right << wrap.getNumEdges() << ", " << std::setw(10) << std::right << wrap.getNumTriangles() << ", "; 
    if(dim3){
        std::cout << std::setw(12) << std::right << wrap.getNumTetrahedra() << ", ";
    }  
    std::cout << std::setw(11) << std::right << wrap.getNumCriticalEdgesNeg() << ", " << std::setw(11) << std::right << wrap.getNumCriticalEdgesPos() << ", ";
    if(dim3){
        std::cout << std::setw(12) << std::right << wrap.getNumCriticalTrianglesNeg() << ", " << std::setw(12) << std::right << wrap.getNumCriticalTrianglesPos() << ", ";
        std::cout << std::setw(12) << std::right << wrap.getNumCriticalTetrahedra() << ", ";
    }else{
        std::cout << std::setw(10) << std::right << wrap.getNumCriticalTrianglesNeg() << ", ";
    }  
    std::cout << std::setw(6) << std::right << wrap.getBetti0() << ", " << std::setw(6) << std::right << wrap.getBetti1();
    if(dim3){
        std::cout << ", " << std::setw(6) << std::right << wrap.getBetti2();
    }
    std::cout << std::endl;
   }
}

//print all statistics for all critical values of alpha^2
void Alpha_Complex::printAlphaStatistics(bool readable){
   if(filtration_statistics_computed){
    //print values that do not depend on alpha: # points, # Delaunay edges, # Delaunay triangles, (# Delaunay tetrahedra), # intervals
    std::cout << std::setw(10) << std::right << "# points" << ", " << std::setw(16) << std::right << "# Delaunay edges" << ", " << std::setw(20) << std::right << "# Delaunay triangles" << ", ";
    if(dim3){
        std::cout << std::setw(16) << std::right << "# Delaunay tetra" << ", ";
    }
    std::cout << std::setw(11) << std::right << "# intervals" << std::endl;
   
    std::cout << std::setw(10) << std::right << getNumDataPoints() << ", " << std::setw(16) << std::right << getNumEdgesFull() << ", " << std::setw(20) << std::right << getNumTrianglesFull() << ", " ;
    if(dim3){
        std::cout << std::setw(16) << std::right << getNumTetrahedraFull() << ", ";
    }
    std::cout << std::setw(11) << std::right << wrap.getNumIntervals() << std::endl;
    
    //for every critical alpha print in one line: Alpha, # Alpha edges, # Alpha triangles, (# Alpha tetrahedra), # Wrap edges, # Wrap triangles, (# Wrap tetrahedra)
    //   # neg crit edges, # pos crit edges, # neg crit triangles, (# pos crit triangles), (# critical tetrahedra), Betti0, Betti1, (Betti2)
    if(!weighted){
        std::cout << std::setw(10) << std::right << "alpha" << ", ";
    }else{
        std::cout << std::setw(10) << std::right << "alpha^2" << ", ";
    }
    std::cout << std::setw(10) << std::right << "# Alpha ed" << ", " << std::setw(11) << std::right << "# Alpha tri" << ", ";
    if(dim3){
        std::cout << std::setw(13) << std::right << "# Alpha tetra" << ", ";
    }
    std::cout << std::setw(9) << std::right << "# Wrap ed" << ", " << std::setw(10) << std::right << "# Wrap tri" << ", " ;
    if(dim3){
        std::cout << std::setw(12) << std::right << "# Wrap tetra" << ", ";
    }
    std::cout << std::setw(11) << std::right << "# crit ed -" << ", " << std::setw(11) << std::right << "# crit ed +" << ", ";
    if(dim3){
        std::cout << std::setw(12) << std::right << "# crit tri -" << ", " << std::setw(12) << std::right << "# crit tri +" << ", ";
        std::cout << std::setw(12) << std::right << "# crit tetra" << ", ";
    }else{
        std::cout << std::setw(12) << std::right << "# crit tri -" << ", ";
    }
    std::cout << std::setw(6) << std::right << "Betti0" << ", " << std::setw(6) << std::right << "Betti1";
    if(dim3){
        std::cout << ", " << std::setw(6) << std::right << "Betti2";
    }
    std::cout << std::endl;
    
    std::vector<int>* num_edges_wrap_filtration = wrap.getNumEdgesFiltration();
    std::vector<int>* num_triangles_wrap_filtration = wrap.getNumTrianglesFiltration();
    std::vector<int>* num_tetrahedra_wrap_filtration = wrap.getNumTetrahedraFiltration();  
    std::vector<int>* num_crit_edges_neg_filtration = wrap.getNumCriticalEdgesNegFiltration();
    std::vector<int>* num_crit_edges_pos_filtration = wrap.getNumCriticalEdgesPosFiltration();
    std::vector<int>* num_crit_triangles_neg_filtration = wrap.getNumCriticalTrianglesNegFiltration();
    std::vector<int>* num_crit_triangles_pos_filtration = wrap.getNumCriticalTrianglesPosFiltration();
    std::vector<int>* num_crit_tetrahedra_filtration = wrap.getNumCriticalTetrahedraFiltration();
    std::vector<int>* betti0_filtration = wrap.getBetti0Filtration();
    std::vector<int>* betti1_filtration = wrap.getBetti1Filtration();
    std::vector<int>* betti2_filtration = wrap.getBetti2Filtration();
    Filtration* filtration_wrap = wrap.getFiltration();
    
    //alpha=0
    if(readable){
        std::cout << std::setw(10) << std::right << 0 << ", ";
        std::cout << std::setw(10) << std::right << 0 << ", " << std::setw(11) << std::right << 0 << ", ";
        if(dim3){
            std::cout << std::setw(13) << std::right << 0 << ", ";
        }
        std::cout << std::setw(9) << std::right << 0 << ", " << std::setw(10) << std::right << 0 << ", ";
        if(dim3){
            std::cout << std::setw(12) << std::right << 0 << ", ";
        }
        std::cout << std::setw(11) << std::right << 0 << ", " << std::setw(11) << std::right << 0 << ", ";
        if(dim3){
            std::cout << std::setw(12) << std::right << 0 << ", " << std::setw(12) << std::right << 0 << ", ";
            std::cout << std::setw(12) << std::right << 0 << ", ";
        }else{
            std::cout << std::setw(12) << std::right << 0 << ", ";
        }
        std::cout << std::setw(6) << std::right << num_data_points << ", " << std::setw(6) << std::right << 0;
        if(dim3){
            std::cout << ", " << std::setw(6) << std::right << 0 << ", ";
        }
        std::cout << std::endl;
    }else{
        std::cout << 0 << ",";
        std::cout << 0 << "," << 0 << ",";
        if(dim3){
            std::cout << 0 << ",";
        }
        std::cout << 0 << "," << 0 << ",";
        if(dim3){
            std::cout << 0 << ",";
        }
        std::cout << 0 << "," << 0 << ",";
        if(dim3){
            std::cout << 0 << "," << 0 << ",";
            std::cout << 0 << ",";
        }else{
            std::cout << 0 << ",";
        }
        std::cout << num_data_points << "," << 0;
        if(dim3){
            std::cout << "," << 0 << ",";
        }
        std::cout << std::endl;
    }
    
    exact alpha2=0; int i_wrap=0;
    for(int i=0; i<filtration_current.size(); i++){        
        exact element_alpha = filtration_current.getElement(i).first;       
        if(i==(filtration_current.size()-1)||filtration_current.getElement(i+1).first > element_alpha){
            //last element with this alpha value reached -> print information about Alpha complex for this index of Alpha filtration
            alpha2=element_alpha;
            if(readable){
                if(!weighted){
                    std::cout << std::setw(10) << std::right << std::sqrt(CGAL::to_double(alpha2)) << ", ";
                }else{
                    std::cout << std::setw(10) << std::right << CGAL::to_double(alpha2) << ", ";
                }
                std::cout << std::setw(10) << std::right << num_simplices_filtration[1][i] << ", " << std::setw(11) << std::right << num_simplices_filtration[2][i] << ", ";
                if(dim3){
                    std::cout << std::setw(13) << std::right << num_simplices_filtration[3][i] << ", ";
                }    
            }else{ //no whitespace
                if(!weighted){
                    std::cout << std::sqrt(CGAL::to_double(alpha2)) << ",";
                }else{
                    std::cout << CGAL::to_double(alpha2) << ",";
                }
                std::cout << num_simplices_filtration[1][i] << "," << num_simplices_filtration[2][i] << ",";
                if(dim3){
                    std::cout << num_simplices_filtration[3][i] << ",";
                }   
            }
            
            //we also need corresponding index of Wrap filtration
            //alpha value critical for Alpha complex might not be critical for Wrap complex
            if(i_wrap<filtration_wrap->size()){ //we have not reached end of Wrap filtration yet
                exact value_wrap = ( filtration_wrap->getElement(i_wrap)).first;   
                if(value_wrap==alpha2){ //this alpha^2 value is also critical for Wrap complex 
                    for(int j=0;j<(filtration_wrap->size()-i_wrap);j++){
                        if(i_wrap+j==(filtration_wrap->size()-1)||filtration_wrap->getElement(i_wrap+j+1).first > alpha2){
                            //last element of Wrap filtration with this alpha value reached
                            i_wrap=i_wrap+j;
                            break;
                        }
                    }
                    //print information about Wrap complex for this index of Wrap filtration
                    if(readable){
                        std::cout << std::setw(9) << std::right << num_edges_wrap_filtration->at(i_wrap) << ", " << std::setw(10) << std::right << num_triangles_wrap_filtration->at(i_wrap)<< ", ";
                        if(dim3){
                            std::cout << std::setw(12) << std::right << num_tetrahedra_wrap_filtration->at(i_wrap) << ", ";
                        } 
                        std::cout << std::setw(11) << std::right << num_crit_edges_neg_filtration->at(i_wrap) << ", " << std::setw(11) << std::right << num_crit_edges_pos_filtration->at(i_wrap) << ", ";
                        if(dim3){
                            std::cout << std::setw(12) << std::right << num_crit_triangles_neg_filtration->at(i_wrap) << ", " << std::setw(12) << std::right << num_crit_triangles_pos_filtration->at(i_wrap) << ", ";                       
                            std::cout << std::setw(12) << std::right << num_crit_tetrahedra_filtration->at(i_wrap) << ", ";
                        }else{
                            std::cout << std::setw(12) << std::right << num_crit_triangles_neg_filtration->at(i_wrap) << ", ";
                        }                   
                        std::cout << std::setw(6) << std::right << betti0_filtration->at(i_wrap) << ", " << std::setw(6) << std::right << betti1_filtration->at(i_wrap);                   
                        if(dim3){
                            std::cout << ", " << std::setw(6) << std::right << betti2_filtration->at(i_wrap) << ", ";
                        }
                    }else{ //no whitespace
                        std::cout << num_edges_wrap_filtration->at(i_wrap) << "," << num_triangles_wrap_filtration->at(i_wrap)<< ",";
                        if(dim3){
                            std::cout << num_tetrahedra_wrap_filtration->at(i_wrap) << ",";
                        } 
                        std::cout << num_crit_edges_neg_filtration->at(i_wrap) << "," << num_crit_edges_pos_filtration->at(i_wrap) << ",";
                        if(dim3){
                            std::cout << num_crit_triangles_neg_filtration->at(i_wrap) << "," << num_crit_triangles_pos_filtration->at(i_wrap) << ",";                       
                            std::cout << num_crit_tetrahedra_filtration->at(i_wrap) << ",";
                        }else{
                            std::cout << num_crit_triangles_neg_filtration->at(i_wrap) << ",";
                        }                   
                        std::cout << betti0_filtration->at(i_wrap) << "," << betti1_filtration->at(i_wrap);                   
                        if(dim3){
                            std::cout << "," << betti2_filtration->at(i_wrap) << ",";
                        }
                    }
                    i_wrap++;
                }           
            }
            std::cout << std::endl;
        }       
    } 
   }
}

//print list of points, simplices, with information about birth and death simplices
void Alpha_Complex::printAlphaSimplicesPartition()
{
    //points
    std::cout << "POINTS: index, x, y";
    if(dim3){
        std::cout << ", z";
    }
    if(weighted){
        std::cout << ", weight";
    }
    std::cout << std::endl;
    for(int i=0; i<data_points->size(); i++){
        DataPoint point = data_points->at(i);
        if(!(point.isDeleted())){
            std::cout << i << ", " << point.getX_double() << ", " << point.getY_double();
            if(dim3){
                std::cout << ", " << point.getZ_double();
            }
            if(weighted){
                std::cout << ", " << point.getWeight_double();
            }
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
    std::cout << "SIMPLICES: index, dim, radius^2, point1 point2 ... , birth_simplex?" << std::endl;
    std::cout << "0, -1, -1, , 1" << std::endl;
    for(int i=1; i<filtration_current.size();i++){ 
        exact value = filtration_current.getElement(i).first;
        Simplex* simplex = filtration_current.getSimplex(i,simplices_ptr);
        std::cout << i << ", " << simplex->getDim() << ", ";
        std::cout << (CGAL::to_double(value)) << ", ";
        std::vector<int> vertices = simplex->getVertices();
        for(int j=0; j<vertices.size()-1; j++){
            std::cout << vertices[j] << " ";
        }
        if(vertices.size()>0){
            std::cout << vertices[vertices.size()-1];
        }
        std::cout << ", " << persistence.isPositiveSimplexCurrent(i) << std::endl;
    }
    std::cout << std::endl;
    std::cout << "BOUNDARY MATRIX" << std::endl;
    persistence.printBoundaryMatrix(std::cout); 
}
    
//CGAL -----------------------
//get triangles and edges of Alpha complex
    /*
    for(Alpha_shape::Finite_faces_iterator fit = this->alpha_complex_cgal.finite_faces_begin(); //iterate over all triangles
            fit != this->alpha_complex_cgal.finite_faces_end(); ++fit) {
        Alpha_shape::Face_handle face = fit; //CGAL face
        if(this->alpha_complex_cgal.classify(face) == Alpha_shape::INTERIOR){ //check if triangle inside alpha shape
            Simplex tri = Simplex(simplices.size(),face->vertex(0)->info(), face->vertex(1)->info(), face->vertex(2)->info());
            simplices.push_back(tri);
        }
    }

    for(Alpha_shape::Alpha_shape_edges_iterator eit = this->alpha_complex_cgal.alpha_shape_edges_begin(); //iterate over all edges
            eit != this->alpha_complex_cgal.alpha_shape_edges_end(); ++eit){
        Alpha_shape::Edge ed = *eit; 
        //if(this->alpha_complex_cgal.classify(*eit) == Alpha_shape::SINGULAR){ //check if edge is singular
            //CGAL edge is pair (face,index)
            Alpha_shape::Face& face = *(ed.first);
            int id = ed.second;
            Alpha_shape::Vertex_handle v1 = face.vertex(face.cw(id)); //retrieve vertices from triangulation data structure
            Alpha_shape::Vertex_handle v2 = face.vertex(face.ccw(id));
            Simplex edge = Simplex(simplices.size(),v1->info(), v2->info());
            simplices.push_back(edge);
        //}
    } 
    */
