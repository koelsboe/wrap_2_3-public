//alpha_complex.h
//author: koelsboe

#ifndef ALPHA_COMPLEX_H
#define ALPHA_COMPLEX_H

#include "wrap.h"

//class representing the Alpha complex (for every alpha) and Delaunay triangulation
class Alpha_Complex : public Filtered_Complex
{      
    private:
        std::vector<Simplex> simplices; //list of Delaunay simplices
        std::vector<int> data_points_simplex_indices; //indices of data points in list of simplices
        
        std::vector<bool> critical; //whether simplex is critical
        std::vector<std::vector<int> > interval_faces; //list of faces that are in same interval, for each simplex that is maximal in its interval
        
        //manipulating points and thereby deleting simplices might result in unused indices in the list of simplices
        std::vector<int> free_simplex_indices;
        
        Kernel kernel; //necessary for some CGAL computations
        //Alpha_shape3 alpha_complex3_cgal; //CGALweighted_Alpha 3d alpha complex (for all alphas)
        
        Wrap wrap; //corresponding Wrap complex
        
        //CGAL data structures for Delaunay triangulation (keep them for dynamic computations)
        Delaunay_triangulation delaunay_tri; //data structure for Delaunay triangulation (computed with CGAL)
        periodic_Delaunay_triangulation periodic_delaunay_tri; //data structure for periodic Delaunay triangulation (computed with CGAL)
        Delaunay_triangulation3 delaunay_tri3; //3-dim Delaunay triangulation  (computed with CGAL)
        periodic_Delaunay_triangulation3 periodic_delaunay_tri3; //3-dim periodic Delaunay triangulation  (computed with CGAL)
        weighted_Delaunay_triangulation weighted_delaunay_tri; //weighted 2-dim Delaunay triangulation (computed with CGAL)weighted
        weighted_Delaunay_triangulation3 weighted_delaunay_tri3; //weighted 3-dim Delaunay triangulation (computed with CGAL)
        //store vertex handles, enable to access points in CGAL triangulation
        std::vector<Delaunay_triangulation::Vertex_handle> vertex_handles;
        std::vector<periodic_Delaunay_triangulation::Vertex_handle> vertex_handles_periodic;
        std::vector<Delaunay_triangulation3::Vertex_handle> vertex_handles3;
        std::vector<periodic_Delaunay_triangulation3::Vertex_handle> vertex_handles_periodic3;
        std::vector<weighted_Delaunay_triangulation::Vertex_handle> vertex_handles_weighted;
        std::vector<weighted_Delaunay_triangulation3::Vertex_handle> vertex_handles_weighted3;
        
        
    public:    
        Alpha_Complex(double alpha2){current_alpha2 = alpha2; dim3=false;exhaustive_persistence=true;holes_modified=false;name="Alpha";name_uppercase="ALPHA";} //constructor
        void clear(); //clear complex
        
        std::vector<std::vector<int> >* getIntervalFaces(){return &interval_faces;} //for maximum simplices in intervals get list of other interval simplices
        Wrap* getWrapComplex(){return &wrap;}; //get Wrap complex 
        int getDataPointSimplexIndex(int data_point_index){return data_points_simplex_indices[data_point_index];} //return simplex index corresponding to given data point
        
        bool setPoints(std::vector<DataPoint> *new_points, int num_data_points_, int periodic_size, bool dim3, bool weighted, bool compute_persistence, bool printstats); //build Alpha complex from given points
        bool computeDelaunayTriangulation(std::vector<DataPoint> *new_points, int num_data_points_, int periodic_size, bool dim3, bool weighted, bool printstats); //compute Delaunay triangulation for given point set
        std::vector<DataPoint> importFilteredComplexFromFile(std::string filename, bool compute_wrap, bool compute_persistence); //import filtered complex with given points, simplices and radii from file and use as alpha complexes
        
        //point manipulation
        bool addPoint(std::vector<DataPoint> *updated_points, int data_point_index, bool printinfo, bool printstats, bool compute_persistence, bool compute_statistics); //insert new point in Alpha and Wrap complex and update
        bool deletePoint(DataPoint old_data_point, int old_data_point_index, bool printinfo, bool printstats, bool compute_persistence, bool compute_statistics); //update complexes by deleting point of given index
        
        //noisy input
        exact computeIntervalsFromNoisyFiltration(std::vector<exact>* noisy_filtration_values, exact prescribed_threshold, bool weighted_, bool use_value_of_min_simplex, bool relative_threshold, bool reconsider_blocked_intervals, bool compute_wrap_and_persistence, bool printstats, bool printinfo); //compute valid interval structure (discrete Morse function) from given noisy filtration values (assume simplices were already computed), positive imprecision threshold computed from conflicts or prescribed
        
        void computeCircumcentersMaxSimplices(bool restrict_to_domain, bool print); //compute circumcenters of maximum simplices in Bregman-Delaunay triangulation, either restrict to domain (delete all simplices with circumcenter outside of domain (positive quadrant/orthant or standard simplex)) or print as output
        void deleteSimplicesWithIntervals(std::vector<int>* simplices_to_delete); //delete given set of simplices together with their intervals
        
        //statistics
        void computeFiltrationStatistics(bool printstats, bool after_hole_operation); ///compute statistics (# simplices, ...) for whole filtration (for every filtration value)      
        void computeResultsPaperHoles(std::ostream& output); //compute and print all results for the computational experiments in the holes paper, for a single run
        
        //print
        void printFullInformation(std::ostream& output); //print full information about alpha and wrap complexes including points, simplices, radii, persistence pairs
        void print(); //print all statistics for current alpha
        void printAlphaStatistics(bool readable); //print all statistics for all critical values of alpha^2
        void printAlphaSimplicesPartition(); //print list of points, simplices, with information about birth and death simplices
        void printAlphaComplexes(std::ostream& output){printFilteredComplex(output);} //export alpha complexes
        void printWrapComplexes(std::ostream& output){wrap.printFilteredComplex(output);} //export wrap complexes
        
    protected:
        bool computeDelaunayTriangulation(bool printstats); //compute Delaunay triangulation for current point set, with CGAL, store simplices
        bool computeDelaunayTriangulationOnStandardSimplex(bool fisher_metric, bool printstats); //compute Delaunay triangulation on standard simplex transformed to 3d weighted points
        void computeAlphaComplex(); //compute Alpha filtration values and intervals for current Delaunay triangulation
        void computeWrapAndPersistence(bool compute_persistence, bool printstats); //compute Wrap complex and persistence for current Alpha complex     
        
    private:
        
        void computeDelaunayRadius(Simplex* simplex, bool sorted_filtration, bool checkIntervalsOfSize4and8); //compute Delaunay radius function values for given simplex, add to filtration (sorted or just at the end)
        
        void lockOrFillCycleFullComplex(bool lock, PersistencePair persistence_pair, exact inclusion_alpha2, bool printinfo, bool printstats); //lock (fill) cycle for full complex by including canonical cycle (chain) of the birth (death) simplex
        void unlockOrUnfillCycleFullComplex(bool lock, PersistencePair persistence_pair, bool printinfo, bool printstats); //unlock (unfill) cycle for full complex by excluding canonical cochain (cocycle) of birth simplex (death)
       
        void deleteSimplex(Simplex* simplex); //delete simplex from Delaunay triangulation, Alpha and Wrap complex 
        std::vector<std::pair<std::vector<int>, std::vector<int> > >  getSimplicesInConflictZoneForPointAddition(DataPoint new_data_point, bool printinfo); //for point addition locate all the simplices in conflict with new point (for dim 1,2(,3): pair of lists of simplices, first: interior ..., second: exterior of conflict zone (some empty depending on case))       
        std::pair<std::vector<std::vector<int> >, std::vector<std::vector<int> > > getNewSimplicesForPointAddition(DataPoint new_data_point, int new_data_point_index, std::vector<std::vector<int>* > conflict_simplices_boundary, std::vector<std::vector<int>* > conflict_simplices_interior, bool printinfo); //for point addition get all new simplices by updating CGAL Delaunay triangulation, return lists of new simplices (dim 1,2(,3)) + lists of extra boundary simplices
        std::vector<std::vector<int> >  getSimplicesOnConflictBoundaryForPointDeletion(int old_point_index, std::vector<int>* conflict_maxsimplices, bool printinfo); //for point deletion get all simplices on boundary of conflict zone, those inside are already known, returns lists of simplices for dim 1(,2)
        std::vector<std::vector<int> > getNewSimplicesForPointDeletion(DataPoint old_point, int old_point_index, std::vector<std::vector<int>* > conflict_simplices_boundary, bool printinfo); //for point deletion get all new simplices by updating CGAL Delaunay triangulation and getting simplices in conflict zone of old point, return lists of new simplices (dim 1,2(,3))
        std::set<int> updateAlphaComplexForPointManipulation(std::vector<std::vector<int>* > old_simplices, std::vector<std::vector<int>* > conflict_boundary_simplices, std::vector<std::vector<int>* > new_simplices); //update Alpha complex for point manipulation by deleting/updating/inserting given simplices 
        void updateWrapComplexForPointManipulation(std::vector<std::vector<int>* > new_simplices, std::vector<std::vector<int>* > conflict_boundary_simplices, std::set<int> deleted_intervals, bool printinfo, bool printstats); //update Wrap complex for point manipulation by updating/creating intervals, updating respective lower sets 

        std::vector<exact> repairFiltration(std::vector<exact>* noisy_filtration_values, exact max_negative_difference, bool move_before, bool printinfo); //transform noisy filtration into correct filtration by either moving simplices before their cofaces or after their faces
        std::pair<bool,std::vector<int> > checkMergeIntervals(int simplex_index, int facet_index, std::vector<int> common_vertices, bool use_value_of_min_simplex, std::vector<bool>* current_critical, std::vector<std::vector<int> >* current_interval_faces, std::vector<int>* max_simplex_of_simplex_interval, std::vector<exact>* current_filtration_values); //check whether there are no objections to merging interval of simplex with interval of its facet with given set of common vertices
};

#endif // ALPHA_COMPLEX_H