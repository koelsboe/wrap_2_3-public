//filtered_complex.h
//author: koelsboe

#ifndef FILTERED_COMPLEX_H
#define FILTERED_COMPLEX_H

#include "basics.h"
#include "persistence.h"

//basic class for any filtered complex (e.g. Alpha complex, Wrap complex)
class Filtered_Complex 
{      
    protected:
        std::string name; //name of the complex (e.g. "Alpha", "Wrap")
        std::string name_uppercase; //name of the complex in uppercase (e.g. "ALPHA", "WRAP")
        
        bool dim3; //3-dim instead of 2-dim space (we only work with 2- and 3-dim complexes)
        bool on_standard_simplex; //point set is on standard simplex
        int periodic_size; //whether domain is periodic, if yes store period length (same in x,y(,z)-direction), else -1
        bool weighted; //whether we are given a weighted point set
        int num_data_points; //number of data points (might be less than data_points->size() since list might contain deleted points)
        
        std::vector<DataPoint> *data_points; //pointer to list of data points
        std::vector<Simplex> *simplices_ptr; //pointer to list of simplices
        std::vector<bool> *critical_ptr; //whether simplex is critical (pointer since shared among multiple complexes)
        std::vector<bool> in_complex; //whether simplex of corresponding index is contained in (unadapted) filtration
        std::vector<exact> filtration_values; //(original) filtration values of simplices, -1 if not in filtration
        
        std::vector<bool> excluded; //whether simplex was excluded from current filtration
        std::vector<bool> included; //whether simplex was included with different value in current filtration
        std::unordered_map<int,std::pair<exact,int> > inclusion_values_and_counter; //dictionary that stores new filtration values and inclusion counter (necessary to get correct filtration order) for simplices after hole manipulation
        int inclusion_counter; //global inclusion counter (how many simplices were already included at different place in filtration)
        
        exact current_alpha2; //current alpha^2 (internally: squared radius, for output/input converted to radius) defining current subcomplex
        Filtration filtration_current; //list of simplices sorted by current filtration values (differ from original ones by inclusions or exclusions)
        Filtration filtration_original; //original filtration disregarding any hole operations         
        int filtration_index_greater_than_alpha2; // index of first element in current filtration with value above the current alpha squared
        
        Persistence persistence; //persistent homology, hole dependences structure
        bool exhaustive_persistence; //compute persistence with exhaustive reduction
        bool persistence_computed; //whether persistence was computed
        
        bool holes_modified; //a hole operation was performed on the complex
        exact current_exclusion_alpha2; //insert simplices with this alpha2 that should exactly be outside current subcomplex
        int last_hole_operation_simplex; //remember main simplex of last hole operation
        
        bool filtration_statistics_computed; //whether filtration statistics were computed (only for interactive mode) 
        //statistics for all filtration values
        std::vector<std::vector<int> > num_simplices_filtration; //number of simplices of different dimension (0,1,2,..), filtered
        std::vector<std::vector<int> > betti_filtration; //betti numbers (0,1,2), filtered
        
    public:  
        Filtered_Complex(){dim3=false;on_standard_simplex=false;periodic_size=-1;weighted=false;num_data_points=0;
        inclusion_counter=0;current_alpha2=-1;filtration_index_greater_than_alpha2=-1;
        exhaustive_persistence=true;persistence_computed=false;
        holes_modified=false;current_exclusion_alpha2=-1;last_hole_operation_simplex=-1;
        filtration_statistics_computed=false;name="";name_uppercase="";} //empty constructor
        
        void clear0(); //clear complex
        
        bool isDim3(){return dim3;}
        bool isPeriodic(){return periodic_size>0;}
        int getPeriodicSize(){return periodic_size;}
        bool isWeighted(){return weighted;}
        int getNumDataPoints(){return num_data_points;}
        bool isOnStandardSimplex(){return on_standard_simplex;}
        void setOnStandardSimplex(bool on_standard_simplex_){on_standard_simplex=on_standard_simplex_; if(on_standard_simplex){dim3=true;}}
        
        std::vector<Simplex>* getSimplices(){return simplices_ptr;} //get list of simplices
        std::vector<bool>* getCritical(){return critical_ptr;} //get list of bool values whether simplex is critical
        std::vector<exact>* getFiltrationValues(){return &filtration_values;} //get list of filtration values for each simplex
        std::vector<bool>* getInComplex(){return &in_complex;} //get list of bools whether simplex is in full complex
        
        bool isInComplex(int index); //check if simplex of given index is in complex
        exact getAlpha2(){return current_alpha2;} //get alpha2 determining current subcomplex
        double getAlpha2_double(){return CGAL::to_double(current_alpha2);}  //get current alpha
        exact getAlpha2Simplex(int index); //return filtration value of simplex of given index (respecting inclusions, exclusions)
        void setAlpha2(exact alpha2); //set new alpha squared
        
        Filtration* getFiltration(){return &filtration_current;} //get current filtration
        Filtration* getFiltrationFull(){return &filtration_original;} //get original filtration (before hole manipulations)
        int getFiltrationIndexGreaterThanAlpha2(){return filtration_index_greater_than_alpha2;} //get first index not in subcomplex = size of subcomplex
        void scaleFiltrationValues(exact factor); //scale transformation values by given factor
        
        exact getMinAlpha2(){if(filtration_original.size()>0){return filtration_original.getMinValue();}else{return 0;}} //get minimum alpha value in original filtration
        exact getMaxAlpha2(){if(filtration_original.size()>0){return filtration_original.getMaxValue();}else{return 0;}} //get maximum alpha value in original filtration
        double getAlpha2RangeLength_double(){return CGAL::to_double(getMaxAlpha2()-getMinAlpha2());} //get size of alpha2 range
        double getAlphaRangeLength_double(){return std::sqrt(CGAL::to_double(getMaxAlpha2()))-std::sqrt(CGAL::to_double(getMinAlpha2()));} //get size of alpha range
       
        void computePersistence(bool first_computation, bool printstats); //compute persistence for current filtration
        bool wasPersistenceComputed(){return persistence_computed;} //was persistence already computed
        bool wasPersistenceDiagramAdapted(){return persistence.wasPersistenceDiagramAdapted();} //has the filtration changed and new persistence was computed
        void setExhaustivePersistence(bool exhaustive_persistence_){exhaustive_persistence=exhaustive_persistence_;} //decide whether to use exhaustive or standard matrix reduction
        double getMaxPersistence(int dim){return persistence.getMaxPersistenceCurrent(dim,&filtration_current);} //get maximum persistence value in current filtration for given dimension
        double getMaxPersistence0(){return getMaxPersistence(0);}
        double getMaxPersistence1(){return getMaxPersistence(1);}
        double getMaxPersistence2(){return getMaxPersistence(2);}
        std::vector<PersistencePair>* getPersistencePairsCurrent(){return persistence.getPersistencePairsCurrent();} //get persistence pairs for current filtration
        std::vector<PersistencePair>* getPersistencePairsOriginal(){return persistence.getPersistencePairsOriginal();} //get persistence pairs for original filtration
        
        bool isExcluded(int index){return excluded[index];} //is simplex excluded
        bool isIncluded(int index){return included[index];} //is simplex included
        std::vector<bool>* getIncluded(){return &included;} //get list of bools indicating whether simplex was included
        std::unordered_map<int,std::pair<exact,int> >* getInclusionValuesAndCounter(){return &inclusion_values_and_counter;} //get inclusion counter and values of included simplices
        bool setSimplexIncludedWithAlpha2(int simplex_index, exact inclusion_alpha2, bool earlier); //set inclusion alpha2 (earlier or later than original alpha2) for simplex, dependent simplices already handled
        exact getIncludedWithAlpha2(int index){if(inclusion_values_and_counter.find(index) == inclusion_values_and_counter.end()){std::cerr << "ERROR(Filtered_Complex::getIncludedWithAlpha2): this simplex was not included, index " << index << " inclusion_values_size " << inclusion_values_and_counter.size()  << std::endl; return -1;}else{return inclusion_values_and_counter[index].first;}}
        
        //hole operations
        void holeOperation(bool lock, bool un, int dim, int pers_pair_num, bool subcomplex, double persistence_bound, bool lowerbound, bool printinfo, bool printstats, bool recompute_persistence, bool compute_statistics); //perform hole operation on given persistence pair or above/below given bound
        void undoHoleOperations(bool recompute_persistence, bool compute_statistics); //undo all hole operations
        void lockCycleRecursively(PersistencePair persistence_pair, exact current_alpha2, bool printinfo, bool printstats); //recursive lock-operation on current subcomplex
        void fillHoleRecursively(PersistencePair persistence_pair, exact current_exclusion_alpha2, bool printinfo, bool printstats); //recursive fill-operation on current subcomplex
        void unlockCycleRecursively(PersistencePair persistence_pair, exact current_exclusion_alpha2, bool printinfo, bool printstats); //recursive unlock-operation on current subcomplex
        void unfillHoleRecursively(PersistencePair persistence_pair, exact current_alpha2, bool printinfo, bool printstats); //recursive unfill-operation on current subcomplex
        
        void setHoleOperationsCohomology(bool hole_operations_cohomology){bool changed = persistence.setHoleOperationsCohomology(hole_operations_cohomology); if(changed){undoHoleOperations(true,true);}} //change hole operations mode to (co)homology (use dependences computed with row/column reduction)
        bool holesWereModified(){return holes_modified;} //if any hole manipulations were applied
        PersistencePair getPersistencePairOfDim(int dim, int ranking){return persistence.getPersistencePairOfDim(dim,ranking);} //get persistence pair of given dimension with ranking-th highest persistence
        PersistencePair getPersistencePairOfDimStatus(int dim, int ranking, exact alpha2, bool exclude_notbornyet, bool exclude_active, bool exclude_dead){return persistence.getPersistencePairOfDimStatus(dim,ranking,alpha2,exclude_notbornyet,exclude_active,exclude_dead,&filtration_original);} //get persistence pair of given dimension and status with ranking-th highest persistence       
        PersistencePair getPersistencePairForSimplex(int filtration_index, bool birth){return persistence.getPersistencePairForSimplexOriginal(filtration_index,birth);} //get persistence pair corresponding to simplex with given filtration index
        std::vector<PersistencePair*> getPersistencePairsWithBoundStatus(int dim, double persistence_bound, bool lowerbound, exact alpha2, bool exclude_notbornyet, bool exclude_active, bool exclude_dead){return persistence.getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,alpha2,exclude_notbornyet,exclude_active,exclude_dead,&filtration_original);} //get persistence pairs of given dimension and status with persistence above/below given bound
        std::vector<int> getCanonicalCycleChain(int filtration_index, bool column){if(column){return persistence.getChain(filtration_index);}else{return persistence.getCocycle(filtration_index);}}
        std::vector<PersistencePair> getDependences(int filtration_index, bool birth_i, bool birth_j, bool transposed) {return persistence.getDependentPairs(filtration_index,birth_i,birth_j,transposed);} //get dependences of given type for simplex with given filtration index
        std::vector<PersistencePair> getDependencesBoundary(int filtration_index,bool transposed) {return persistence.getDependentPairsBoundary(filtration_index,transposed);} //get boundary dependences of given simplex
        std::vector<PersistencePair> getDependentPairsOperation(int filtration_index, bool lock, bool un, bool recursively, bool original){return persistence.getDependentPairsOperation(filtration_index,lock,un,recursively,original);} //get all dependent pairs relevant for given operation
        int getStatusOfCorrespondingPersistencePair(int simplex_index){return persistence.getStatusOfCorrespondingPersistencePair(simplex_index);}
        bool isSimplexPositive(int filtration_index){return persistence.isPositiveSimplexOriginal(filtration_index);} //is simplex with given filtration index positive
        int getLastHoleOperationSimplex(){return last_hole_operation_simplex;} //get filtration index of main simplex in last hole operation, -1 if none was applied
        std::vector<std::pair<PersistencePair,std::pair<exact,exact> > >* getManipulatedPersistencePoints(){return persistence.getManipulatedPersistencePoints();} //get list of persistence points that have (actively) been manipulated by hole operations and the resulting new location in persistence diagram       
        void comparePersistenceDiagramsOldNew(); //compare persistence diagram of original filtration and after hole operations, check if changes correspond exactly to recursive hole operations
              
        //update pointers after change in complex (point manipulation)
        void updateDataPointsPtr(std::vector<DataPoint>* updated_points){data_points=updated_points;}
        void updateNumDataPoints(int num_data_points_){num_data_points=num_data_points_;}
        void updateSimplicesPtr(std::vector<Simplex>* simplices_){simplices_ptr=simplices_;}
        void updateCritical(std::vector<bool>* critical_){critical_ptr=critical_;}
        
        //statistics
        void computeFiltrationStatistics0(bool printstats); //filtration statistics for any filtered complex, called by computeFiltrationStatistics
        virtual void computeFiltrationStatistics(bool printstats, bool after_hole_operation) = 0; //compute statistics (# simplices, ...) for whole filtration (for every filtration value)
        //get filtration statistics (recorded for every filtration value)
        std::vector<int>* getNumPointsFiltration(){return &num_simplices_filtration[0];}
        std::vector<int>* getNumEdgesFiltration(){return &num_simplices_filtration[1];}
        std::vector<int>* getNumTrianglesFiltration(){return &num_simplices_filtration[2];}
        std::vector<int>* getNumTetrahedraFiltration(){return &num_simplices_filtration[3];}   
        std::vector<int>* getBetti0Filtration(){return &betti_filtration[0];}
        std::vector<int>* getBetti1Filtration(){return &betti_filtration[1];}       
        std::vector<int>* getBetti2Filtration(){return &betti_filtration[2];}
        //get statistics for current subcomplex
        int getNumPoints(){if(filtration_index_greater_than_alpha2==0||num_simplices_filtration.size()<=0){return 0;}else{return num_simplices_filtration[0][filtration_index_greater_than_alpha2-1];}}
        int getNumEdges(){if(filtration_index_greater_than_alpha2==0||num_simplices_filtration.size()<=1){return 0;}else{return num_simplices_filtration[1][filtration_index_greater_than_alpha2-1];}}
        int getNumTriangles(){if(filtration_index_greater_than_alpha2==0||num_simplices_filtration.size()<=2){return 0;}else{return num_simplices_filtration[2][filtration_index_greater_than_alpha2-1];}}
        int getNumTetrahedra(){if(filtration_index_greater_than_alpha2==0 ||num_simplices_filtration.size()<=3){return 0;}else{return num_simplices_filtration[3][filtration_index_greater_than_alpha2-1];}}
        int getBetti0(){if(filtration_index_greater_than_alpha2==0 || num_simplices_filtration.size()<=0 || betti_filtration[0].size()==0){return 0;}else{return betti_filtration[0][filtration_index_greater_than_alpha2-1];}}
        int getBetti1(){if(filtration_index_greater_than_alpha2==0 || num_simplices_filtration.size()<=1 || betti_filtration[1].size()==0){return 0;}else{return betti_filtration[1][filtration_index_greater_than_alpha2-1];}}
        int getBetti2(){if(filtration_index_greater_than_alpha2==0 || num_simplices_filtration.size()<=2 || betti_filtration[2].size()==0){return 0;}else{return betti_filtration[2][filtration_index_greater_than_alpha2-1];}}
        //get statistics for full complex
        int getNumPointsFull(){if(num_simplices_filtration.size()>0 && num_simplices_filtration[0].size()>0){return num_simplices_filtration[0].back();}else{return -1;}}
        int getNumEdgesFull(){if(num_simplices_filtration.size()>1 && num_simplices_filtration[1].size()>0){return num_simplices_filtration[1].back();}else{return -1;}}
        int getNumTrianglesFull(){if(num_simplices_filtration.size()>2 && num_simplices_filtration[2].size()>0){return num_simplices_filtration[2].back();}else{return -1;}}
        int getNumTetrahedraFull(){if(num_simplices_filtration.size()>3 && num_simplices_filtration[3].size()>0){return num_simplices_filtration[3].back();}else{return -1;}}
        int getBetti0Full(){if(betti_filtration.size()<=0 || betti_filtration[0].size()==0){return 0;}else{return betti_filtration[0].back();}}
        int getBetti1Full(){if(betti_filtration.size()<=1 || betti_filtration[1].size()==0){return 0;}else{return betti_filtration[1].back();}}
        int getBetti2Full(){if(betti_filtration.size()<=2 || betti_filtration[2].size()==0){return 0;}else{return betti_filtration[2].back();}}
        
        //print
        void printFilteredComplex(std::ostream& output); //print simplices of filtered complex together with filtration values, for exporting and importing       
        void printBoundaryMatrix(std::ostream& output){persistence.printBoundaryMatrix(output);} //print boundary matrix to given output stream
        void printPersistenceMatrices(std::ostream& output){persistence.printRUQV(output);}; //print persistence matrices to given output stream
        void printPersistencePairs(std::ostream& output){persistence.printPersistencePairs(output, &filtration_current);}; //print persistence pairs to given output stream      
        void printPersistenceBarcode(); //print persistence barcode ((dim,birth,death)-triples sorted by dimension and persistence)
        
    protected:
        void computeManipulatedPersistencePointsFromStatus(){persistence.computeManipulatedPersistencePointsFromStatus(&filtration_original,current_alpha2,current_exclusion_alpha2);} //for all persistence pairs with new status, store manipulated point in diagram (connection)       
        void resetPersistencePairsStatus(bool recompute_original){persistence.resetPersistencePairsStatus(&filtration_original,current_alpha2,recompute_original);} //compute status (0 past, 1 presence, 2 future) of persistence pairs from original filtration, undo hole operations
        
        virtual void lockOrFillCycleFullComplex(bool lock, PersistencePair persistence_pair, exact inclusion_alpha2, bool printinfo, bool printstats) = 0; //lock (fill) cycle for full complex by including canonical cycle (chain) of the birth (death) simplex
        virtual void unlockOrUnfillCycleFullComplex(bool lock, PersistencePair persistence_pair, bool printinfo, bool printstats) = 0; //unlock (unfill) cycle for full complex by excluding canonical cochain (cocycle) of birth simplex (death)
};

#endif /* FILTERED_COMPLEX_H */