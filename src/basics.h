//basics.h
//author: koelsboe

#ifndef BASICS_H
#define BASICS_H

//#define NOQT5 //disable interactive version, if qt5 not available

//headers and classes needed for multiple files
//CGAL, QT headers, basic geometry classes

//general libraries
#include <iostream>
#include <fstream>
#include <algorithm>
#include <random>
#include <time.h> 
#include <iomanip>
#include <math.h>
#include <unordered_map>
#include <queue>

#ifndef NOQT5
    //Qt libraries
    #include <QtWidgets>
    #include <QtGui>
    #include <QString>
    #include <QSvgGenerator>
    #include <QPixmap>
    #include <QFileDialog>
#endif

//CGAL libraries
#include <CGAL/Exact_predicates_exact_constructions_kernel.h> //exact computation
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h> //exact computation, inexact constructions
#include <CGAL/Delaunay_triangulation_2.h> //Delaunay
#include <CGAL/Triangulation_vertex_base_with_info_2.h> //store vertex index together with coordinates
#include <CGAL/Periodic_2_triangulation_traits_2.h> //periodic points
#include <CGAL/Periodic_2_Delaunay_triangulation_traits_2.h> //periodic points
#include <CGAL/Periodic_2_Delaunay_triangulation_2.h>
#include <CGAL/Regular_triangulation_2.h> //weighted points
#include <CGAL/Regular_triangulation_3.h> //weighted points
#include <CGAL/Delaunay_triangulation_3.h> //3D
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Periodic_3_triangulation_traits_3.h> //periodic points
#include <CGAL/Periodic_3_Delaunay_triangulation_traits_3.h> 
#include <CGAL/Periodic_3_Delaunay_triangulation_3.h>
//#include <CGAL/Alpha_shape_2.h>
#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Alpha_shape_cell_base_3.h>
#include <CGAL/Alpha_shape_vertex_base_3.h>

//define CGAL types (shortcuts)
//kernel for exact computations
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel; 
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kerneli; 
//vertex class for triangulations: 2D-coordinates, index as additional info
typedef CGAL::Triangulation_vertex_base_with_info_2<int, Kernel> vertex_base; 
//delaunay triangulation classes
typedef CGAL::Triangulation_data_structure_2<vertex_base>  Tds;
typedef CGAL::Delaunay_triangulation_2<Kernel,Tds>  Delaunay_triangulation; //Delaunay triangulation
typedef Kernel::Point_2 CPoint2; //2D point
typedef CGAL::Lazy_exact_nt<CGAL::Gmpq> exact; //arbitrary precision rational number based on the Gmp library
//periodic Delaunay triangulation
typedef CGAL::Periodic_2_Delaunay_triangulation_traits_2<Kernel> periodic_traits;
typedef CGAL::Periodic_2_triangulation_vertex_base_2<periodic_traits>  periodic_vertex_base0;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, Kernel, periodic_vertex_base0> periodic_vertex_base;
typedef CGAL::Periodic_2_triangulation_face_base_2<periodic_traits> periodic_face_base;
typedef CGAL::Triangulation_data_structure_2<periodic_vertex_base, periodic_face_base>    periodic_Tds;
typedef CGAL::Periodic_2_Delaunay_triangulation_2<periodic_traits, periodic_Tds>  periodic_Delaunay_triangulation;
typedef periodic_Delaunay_triangulation::Point periodic_point;
typedef periodic_Delaunay_triangulation::Periodic_point periodic_point_offset;
//weighted Delaunay triangulation (regular triangulation)
typedef CGAL::Regular_triangulation_vertex_base_2<Kernel> weighted_vertex_base0;
typedef CGAL::Triangulation_vertex_base_with_info_2<int,Kernel,weighted_vertex_base0> weighted_vertex_base;
typedef CGAL::Regular_triangulation_face_base_2<Kernel> weighted_face_base;
typedef CGAL::Triangulation_data_structure_2<weighted_vertex_base,weighted_face_base> weighted_Tds;
typedef CGAL::Triangulation_data_structure_2<weighted_vertex_base0,weighted_face_base> weighted_Tds_noinfo;
typedef CGAL::Regular_triangulation_2<Kernel,weighted_Tds> weighted_Delaunay_triangulation;
typedef CGAL::Regular_triangulation_2<Kernel,weighted_Tds_noinfo> weighted_Delaunay_triangulation_noinfo;
typedef Kernel::Weighted_point_2 weighted_point;
//weighted Alpha shape (check if cgal is same as my implementation)
//if we want to use it with info we need to use inexact constructions Kernel, otherwise segmentation fault
//either no info or inexact construction -> works
/*typedef Kerneli::Point_2 CPoint2i; //2D point
typedef Kerneli::Weighted_point_2 weighted_pointi;
typedef CGAL::Regular_triangulation_vertex_base_2<Kerneli> weighted_vertex_base0i;
typedef CGAL::Regular_triangulation_face_base_2<Kerneli> weighted_face_basei;
typedef CGAL::Alpha_shape_vertex_base_2<Kerneli,weighted_vertex_base0i> weighted_vertex_base_alpha0;
typedef CGAL::Alpha_shape_face_base_2<Kerneli,weighted_face_basei> weighted_face_base_alpha;
typedef CGAL::Triangulation_vertex_base_with_info_2<int,Kerneli,weighted_vertex_base_alpha0> weighted_vertex_base_alpha;
typedef CGAL::Triangulation_data_structure_2<weighted_vertex_base_alpha,weighted_face_base_alpha> weighted_Tds_alpha;
typedef CGAL::Regular_triangulation_2<Kerneli,weighted_Tds_alpha> weighted_Delaunay_triangulation_alpha;
typedef CGAL::Alpha_shape_2<weighted_Delaunay_triangulation_alpha> weighted_Alpha_shape;*/

//typedef CGAL::Triangulation_data_structure_2<weighted_vertex_base_alpha0,weighted_face_base_alpha> weighted_Tds_alpha_noinfo;
//typedef CGAL::Regular_triangulation_2<Kernel,weighted_Tds_alpha_noinfo> weighted_Delaunay_triangulation_alpha_noinfo;
//typedef CGAL::Alpha_shape_2<weighted_Delaunay_triangulation_alpha_noinfo> weighted_Alpha_shape_noinfo;


//3d
typedef CGAL::Triangulation_vertex_base_with_info_3<int,Kernel> vertex_base3;
typedef CGAL::Triangulation_data_structure_3<vertex_base3> Tds3;
typedef CGAL::Delaunay_triangulation_3<Kernel,Tds3> Delaunay_triangulation3;
typedef Kernel::Point_3 CPoint3; //3D point
//3d periodic
typedef CGAL::Periodic_3_Delaunay_triangulation_traits_3<Kernel> periodic_traits3;

typedef CGAL::Periodic_3_triangulation_ds_vertex_base_3<>  periodic_vertex_base03;
typedef CGAL::Triangulation_vertex_base_3<periodic_traits3,periodic_vertex_base03> periodic_vertex_base3;

typedef CGAL::Triangulation_vertex_base_with_info_3<int, periodic_traits3, periodic_vertex_base3> periodic_vertex_base3_info;

typedef CGAL::Periodic_3_triangulation_ds_cell_base_3<> periodic_cell_base03;
typedef CGAL::Triangulation_cell_base_3<periodic_traits3,periodic_cell_base03> periodic_cell_base3;

typedef CGAL::Triangulation_data_structure_3<periodic_vertex_base3_info, periodic_cell_base3>    periodic_Tds3;
typedef CGAL::Periodic_3_Delaunay_triangulation_3<periodic_traits3, periodic_Tds3>  periodic_Delaunay_triangulation3;

typedef periodic_Delaunay_triangulation3::Point periodic_point3;
typedef periodic_Delaunay_triangulation3::Periodic_point periodic_point_offset3;
//3d weighted Delaunay triangulation (regular triangulation)
typedef CGAL::Regular_triangulation_vertex_base_3<Kernel> weighted_vertex_base03; 
typedef CGAL::Triangulation_vertex_base_with_info_3<int, Kernel, weighted_vertex_base03> weighted_vertex_base3;
typedef CGAL::Regular_triangulation_cell_base_3<Kernel> weighted_cell_base3;
typedef CGAL::Triangulation_data_structure_3<weighted_vertex_base3,weighted_cell_base3> weighted_Tds3;
typedef CGAL::Regular_triangulation_3<Kernel, weighted_Tds3>  weighted_Delaunay_triangulation3;
typedef Kernel::Weighted_point_3 weighted_point3;
//alpha shape CGAL
/*typedef CGAL::Alpha_shape_vertex_base_3<Kernel, vertex_base3> vertex_base_a3;
typedef CGAL::Alpha_shape_cell_base_3<Kernel>  face_basea3;
typedef CGAL::Triangulation_data_structure_3<vertex_base_a3,face_basea3>  Tdsa3;
typedef CGAL::Delaunay_triangulation_3<Kernel,Tdsa3>  Delaunay_triangulationa3; //Delaunay triangulation
typedef CGAL::Alpha_shape_3<Delaunay_triangulationa3>  Alpha_shape3;*/
//weighted Alpha shape CGAL (check if cgal is same as my implementation)
//if we want to use it with info we need to use inexact constructions Kernel, otherwise segmentation fault
//either no info or inexact construction -> works
typedef Kerneli::Point_3 CPoint3i; //2D point
typedef Kerneli::Weighted_point_3 weighted_point3i;
typedef CGAL::Regular_triangulation_vertex_base_3<Kerneli> weighted_vertex_base03i;
typedef CGAL::Regular_triangulation_cell_base_3<Kerneli> weighted_cell_base3i;
typedef CGAL::Alpha_shape_vertex_base_3<Kerneli,weighted_vertex_base03i> weighted_vertex_base_alpha03;
typedef CGAL::Alpha_shape_cell_base_3<Kerneli,weighted_cell_base3i> weighted_cell_base_alpha3;
typedef CGAL::Triangulation_vertex_base_with_info_3<int,Kerneli,weighted_vertex_base_alpha03> weighted_vertex_base_alpha3;
typedef CGAL::Triangulation_data_structure_3<weighted_vertex_base_alpha3,weighted_cell_base_alpha3> weighted_Tds_alpha3;
typedef CGAL::Regular_triangulation_3<Kerneli,weighted_Tds_alpha3> weighted_Delaunay_triangulation_alpha3;
typedef CGAL::Alpha_shape_3<weighted_Delaunay_triangulation_alpha3> weighted_Alpha_shape3;

#include "bregman_tools.h"

//given data points (input)
class DataPoint
{
    private:
        std::vector<exact> coordinates; //list of coordinates (dimension = size of list =  2 or 3)
        exact weight; //if no weight, weight = 0
        bool hidden; //whether point appears in weighted Delaunay triangulation or not
        bool deleted; //if data point was deleted by point manipulation, should not be considered for anything, will be overwritten when new data point added
        
    public:
        //constructors
        DataPoint(){weight=0;deleted=true;hidden=false;} //deleted point
        DataPoint(exact x, exact y){coordinates.push_back(x); coordinates.push_back(y); weight=0; deleted=false; hidden=false;} //2d unweighted
        DataPoint(bool weighted, exact x, exact y, exact z){ //2d weighted or 3d unweighted
            deleted=false; hidden=false;
            if(weighted){
                coordinates.push_back(x); coordinates.push_back(y); weight=z;
            }else{
                coordinates.push_back(x); coordinates.push_back(y); coordinates.push_back(z); weight=0;
            }
        }
        DataPoint(exact x, exact y, exact z, exact weight_):weight(weight_){deleted=false; hidden=false; coordinates.push_back(x); coordinates.push_back(y); coordinates.push_back(z);} //3d weighted
        DataPoint(std::vector<exact> coordinates_):coordinates(coordinates_){deleted=false; hidden=false; weight=0;} //general unweighted
        DataPoint(std::vector<exact> coordinates_, exact weight_):coordinates(coordinates_),weight(weight_){deleted=false;hidden=false;} //general weighted
        
        int getDim(){return coordinates.size();}
        
        std::vector<exact> getCoordinates(){return coordinates;}
        exact getCoordinate(int num){
            if(coordinates.size()>num){
                return coordinates[num];
            }else{
                //std::cerr << "ERROR(DataPoint::getCoordiante): dimension exceeded " << num << ">=" << coordinates.size() << std::endl; 
                return -1;
            }
        }
        double getCoordinate_double(int num){return CGAL::to_double(getCoordinate(num));}
        exact getX(){return getCoordinate(0);}
        double getX_double(){return getCoordinate_double(0);}
        exact getY(){return getCoordinate(1);}
        double getY_double(){return getCoordinate_double(1);}
        exact getZ(){if(coordinates.size()>2){return getCoordinate(2);}else{return 0;}}
        double getZ_double(){return CGAL::to_double(getZ());}
        void setX(exact new_x){if(coordinates.size()>0){coordinates[0]=new_x;}}
        void setY(exact new_y){if(coordinates.size()>1){coordinates[1]=new_y;}}
        void setZ(exact new_z){if(coordinates.size()>2){coordinates[2]=new_z;}}
        CPoint3 toCPoint3(){return CPoint3(getX(),getY(),getZ());}
        
        exact getWeight(){return weight;}
        double getWeight_double(){return CGAL::to_double(weight);}
        void setWeight(exact weight_){weight=weight_;}
        bool nonZeroWeight(){return CGAL::abs(getWeight())>0;}
        
        bool isHidden(){return hidden;}
        void setHidden(bool hidden_){hidden=hidden_;}
        
        bool isDeleted(){return deleted;}
        void setDeleted(){deleted=true;}
        
        //compare to other point
        bool isSame(double x, double y, double z){
            double eps = 1e-9;
            if(std::abs(getX_double()-x)<eps && std::abs(getY_double()-y)<eps){
                if(getDim()==2){
                    return true;
                }else if(std::abs(getZ_double()-x)<eps){
                    return true;
                }
                return false;
            }else{
                return false;
            }
        }
        //change coordinates
        void moveTo(exact x_, exact y_, exact z_){
            if(getDim()==2){
                coordinates[0]=x_;
                coordinates[1]=y_;
            }else if(getDim()==3){
                coordinates[0]=x_;
                coordinates[1]=y_;
                coordinates[2]=z_;
            }else{
                coordinates.clear();
                coordinates.push_back(x_); coordinates.push_back(y_); coordinates.push_back(z_);
            }
            hidden=false;
        }
        
        void print(){
            std::cout << "data_point";
            if(deleted){
                std::cout << " deleted" << std::endl;
            }else{ 
                std::cout << ":";
                for(int i=0; i<coordinates.size(); i++){
                    std::cout << " " << CGAL::to_double(coordinates[i]);
                }
                if(nonZeroWeight()){
                    std::cout << " weight " << getWeight_double();
                    if(isHidden()){
                        std::cout << " hidden";
                    }
                }
                std::cout << std::endl;
            }
        }
};

//offset for periodic points (2-dim or 3-dim)
class Offset
{
    private: 
        bool x;
        bool y;
        bool z;

    public:
        Offset(){x=false; y=false; z=false;} //default constructor
        Offset(bool x_, bool y_):x(x_),y(y_){z=false;} //2-dim constructor
        Offset(bool x_, bool y_, bool z_):x(x_),y(y_),z(z_){} //3-dim constructor
        Offset(int index){ //construct from integer (converted to binary number zyx)
            x = index%2==1;
            y = (index-x)/2%2==1;
            z = (index-x-y*2)/4%2==1;
        }
    
        bool getX() const {return x;}
        bool getY() const {return y;}
        bool getZ() const {return z;}
        bool isOffset()const{return x || y || z;} //check if there is offset in any direction

        int toInt(){return x*1+y*2+z*4;} //convert to integer (interpret as binary number zyx)
        // 0 ... 000
        // 1 ... 001
        // 2 ... 010
        // 3 ... 011
        // 4 ... 100
        // 5 ... 101
        // 6 ... 110
        // 7 ... 111
};

//simplex of any dimension
class Simplex
{
    private:
        std::vector<int> vertices; //indices of vertices (in list of data points), sorted 
        std::vector<Offset> offsets; //possible offset for every vertex (for periodic points)
        int index; //index in list of all simplices
        bool deleted; //if simplex was deleted by point manipulation, should not be considered for anything, will be overwritten when new simplices added
        
        std::vector<int> facet_indices; //indices of facets (face simplices of 1 dim lower), sorted
        std::vector<int> cofacet_indices; //indices of cofacets (coface simplices of 1 dim higher), sorted
        
    public:
        
        //general constructors
        Simplex(){index=-1;deleted=true;} //empty constructor, deleted simplex
        Simplex(int index_,std::vector<int> vertices_):index(index_),vertices(vertices_){ //without offsets
            std::sort(vertices.begin(),vertices.end());
            offsets = std::vector<Offset>(vertices.size(),Offset());
            deleted=false;
        }
        Simplex(int index_,std::vector<int> vertices_, std::vector<Offset> offsets_):index(index_),vertices(vertices_), offsets(offsets_){ //with offsets, we assume that vertices are already sorted
            deleted=false;
        }
        //constructors for each dimension: empty, point, edge, triangle, tetrahedron
        Simplex(int index_):index(index_){deleted=false;} //empty
        Simplex(int index_, int vertex0):index(index_){ //point
            vertices.push_back(vertex0);
            offsets = std::vector<Offset>(vertices.size(),Offset());
            deleted=false;
        }
        Simplex(int index_, int vertex0, int vertex1):index(index_){ //edge
            vertices.push_back(vertex0);vertices.push_back(vertex1);
            std::sort(vertices.begin(),vertices.end()); offsets = std::vector<Offset>(vertices.size(),Offset());
            deleted=false;
        }
        Simplex(int index_, int vertex0, int vertex1, int vertex2):index(index_){ //triangle
            vertices.push_back(vertex0);vertices.push_back(vertex1);vertices.push_back(vertex2);
            std::sort(vertices.begin(),vertices.end()); offsets = std::vector<Offset>(vertices.size(),Offset());
            deleted=false;
        }
        Simplex(int index_, int vertex0, int vertex1, int vertex2, int vertex3):index(index_){ //tetrahedron
            vertices.push_back(vertex0);vertices.push_back(vertex1);vertices.push_back(vertex2);vertices.push_back(vertex3);
            std::sort(vertices.begin(),vertices.end()); offsets = std::vector<Offset>(vertices.size(),Offset());
            deleted=false;
        }
        
        int getDim() const {return vertices.size()-1;} //dimension of simplex (also works for empty simplex of dim -1)
        
        int getVertex(int num) const {if(num<vertices.size()){return vertices[num];}else{std::cerr << "ERROR (Simplex::getVertex(num)) size exceeded " << num << ">" << vertices.size()-1 << std::endl;return -1;}} 
        bool hasVertex(int point_index){if(std::find(vertices.begin(), vertices.end(), point_index)!=vertices.end()){return true;}else{return false;}}
        std::vector<int> getVertices() const {return vertices;}
        
        Offset getOffset(int num) const {if(num<vertices.size()){return offsets[num];}else{std::cerr << "ERROR (Simplex::getOffset(num)) size exceeded " << num << ">" << vertices.size()-1 << std::endl;return -1;}} 
        void setOffset(int num, Offset offset){if(num<vertices.size()){offsets[num]=offset;}else{std::cerr << "ERROR (Simplex::setOffset(num)) size exceeded " << num << ">" << vertices.size()-1 << std::endl;}} 
        bool isOffset(int num) const {if(num<vertices.size()){return offsets[num].isOffset();}else{std::cerr << "ERROR (Simplex::isOffset(num)) size exceeded " << num << ">" << vertices.size()-1 << std::endl; return false;}} 
        
        int getIndex(){return index;}
        void setIndex(int index_){index=index_;}
        bool isDeleted() const {return deleted;}
        
        std::vector<int>getFacetIndices(){return facet_indices;}
        std::vector<int>getCofacetIndices() const {return cofacet_indices;}
        void addFacetIndex(int facet_index){facet_indices.push_back(facet_index);std::sort(facet_indices.begin(),facet_indices.end());}
        void addCofacetIndex(int cofacet_index){
            cofacet_indices.push_back(cofacet_index);std::sort(cofacet_indices.begin(),cofacet_indices.end());}
        bool deleteCofacetIndex(int cofacet_index){
            std::vector<int>::iterator it = std::find(cofacet_indices.begin(),cofacet_indices.end(),cofacet_index);
            if(it!=cofacet_indices.end()){
                cofacet_indices.erase(it); 
                return true;
            }else{
                return false;
            }
        }
        void deleteCofacetIndices(){cofacet_indices = std::vector<int>(0);}
        int getNumCofacets(){return cofacet_indices.size();}
        
        bool sameSimplex(std::vector<int> other_vertices){ //check if given vertices are the same
            if(vertices.size()==other_vertices.size()){
                for(int i=0; i<vertices.size(); i++){
                    if(vertices[i]!=other_vertices[i]){
                        return false;
                    }
                }
                return true;
            }else{
                return false;
            }
        }
        bool sameSimplex(const Simplex& other){ //check if other simplex has same vertices
            return sameSimplex(other.getVertices());
        }
        bool sameSimplex(int vert0, int vert1){std::vector<int> other_vertices; other_vertices.push_back(vert0);other_vertices.push_back(vert1); return sameSimplex(other_vertices);}
        bool sameSimplex(int vert0, int vert1, int vert2){std::vector<int> other_vertices; other_vertices.push_back(vert0);other_vertices.push_back(vert1);other_vertices.push_back(vert2); return sameSimplex(other_vertices);}
        bool sameSimplex(int vert0, int vert1, int vert2, int vert3){std::vector<int> other_vertices; other_vertices.push_back(vert0);other_vertices.push_back(vert1);other_vertices.push_back(vert2);other_vertices.push_back(vert3); return sameSimplex(other_vertices);}
        
        //compute diameter (length of longest edge)
        double getDiameter(std::vector<DataPoint>* data_points, int periodic_size){
            if(getDim()<=0){
                return 0;
            }
            std::vector<DataPoint> simplex_points;
            for(int i=0; i<vertices.size(); i++){
                simplex_points.push_back(data_points->at(vertices[i]));
            }
            exact max_dist2 = 0;
            bool dim3 = (((data_points->at(0)).getDim())==3);
            
            for(int i=0; i<vertices.size()-1; i++){
                DataPoint vert0 = simplex_points[i];
                for(int j=i+1;j<vertices.size();j++){
                    DataPoint vert1 = simplex_points[j];
                    CPoint3 cpoint0 = CPoint3(vert0.getX(),vert0.getY(),vert0.getZ());
                    CPoint3 cpoint1 = CPoint3(vert1.getX(),vert1.getY(),vert1.getZ());
                    exact dist2;
                    //compute distance
                    if(periodic_size >= 0){
                        //periodic case: for each pair of points compute minimum distance over all possible offsets
                        //we can compute the minimum distance independently for each coordinate (we look at ALL possible offsets): 
                        //      (point0_coord - point1_coord (+- periodic_size))^2
                        exact min_x2 = CGAL::min(CGAL::min(CGAL::square(cpoint0.x()-cpoint1.x()), CGAL::square(cpoint0.x()-cpoint1.x()+periodic_size)), CGAL::square(cpoint0.x()-cpoint1.x()-periodic_size));
                        exact min_y2 = CGAL::min(CGAL::min(CGAL::square(cpoint0.y()-cpoint1.y()), CGAL::square(cpoint0.y()-cpoint1.y()+periodic_size)), CGAL::square(cpoint0.y()-cpoint1.y()-periodic_size));
                        if(dim3){
                            exact min_z2 = CGAL::min(CGAL::min(CGAL::square(cpoint0.z()-cpoint1.z()), CGAL::square(cpoint0.z()-cpoint1.z()+periodic_size)), CGAL::square(cpoint0.z()-cpoint1.z()-periodic_size));
                            dist2 = min_x2+min_y2+min_z2;
                        }else{
                            dist2 = min_x2+min_y2;
                        }
                    }else{
                        //non-periodic case: compute distance directly
                        dist2 = CGAL::squared_distance(cpoint0,cpoint1);
                    }
                    if(dist2>max_dist2){
                        max_dist2=dist2;
                    }
                }
            }
            return std::sqrt(CGAL::to_double(max_dist2));
        }
        
        void print() const{
            if(isDeleted()){
                std::cout << "deleted " << index << std::endl;
                return;
            }
            if(getDim()==-1){
                std::cout << "empty_simplex ";
            }else if(getDim()==0){
                std::cout << "point ";
            }else if(getDim()==1){
                std::cout << "edge ";
            }else if(getDim()==2){
                std::cout << "triangle ";
            }else if(getDim()==3){
                std::cout << "tetrahedron ";
            }else{
                std::cout << "simplex_dim " << getDim() << " ";
            }
            std::cout << index << ": ";
            for(int i=0; i<vertices.size(); i++){
                std::cout << vertices[i] << " ";
            }
            if(facet_indices.size()>0){
                std::cout << "facets ";
                for(int i=0; i<facet_indices.size(); i++){
                    std::cout << facet_indices[i] << " ";
                }
            }
            if(cofacet_indices.size()>0){
                std::cout << "cofacets ";
                for(int i=0; i<cofacet_indices.size(); i++){
                    std::cout << cofacet_indices[i] << " ";
                }
            }
            std::cout << std::endl;
        }
        
        void printShort()
        {
            if(isDeleted()){
                std::cout << "deleted " << index << std::endl;
                return;
            }
            if(getDim()==-1){
                std::cout << "empty_simplex ";
            }else if(getDim()==0){
                std::cout << "point ";
            }else if(getDim()==1){
                std::cout << "edge ";
            }else if(getDim()==2){
                std::cout << "triangle ";
            }else if(getDim()==3){
                std::cout << "tetrahedron ";
            }else{
                std::cout << "simplex_dim " << getDim() << " ";
            }
            std::cout << index << ": ";
            for(int i=0; i<vertices.size(); i++){
                std::cout << vertices[i] << " ";
            }
            std::cout << std::endl;
        }
        
        bool operator <(const Simplex& other){ //sort by dim, lexicographically by vertices
            std::vector<int> other_vertices = other.getVertices();
            if(vertices.size()==other_vertices.size()){
                //same dimension, check vertices indices, we assume that they are sorted
                for(int i=0; i<vertices.size(); i++){
                    if(vertices[i]!=other_vertices[i]){
                        return vertices[i]<other_vertices[i]; //first vertex that is different
                    }
                }
                //all vertices same, check offsets
                for(int i=0; i<vertices.size(); i++){
                    if((isOffset(i)&&!other.isOffset(i))||(!isOffset(i)&&other.isOffset(i))){
                        return isOffset(i)<other.isOffset(i); //first offset that is different
                    }
                }
            }else{
                //less vertices = lower dimension
                return vertices.size()<other_vertices.size();
            }
            return false;
        }
};

//sort simplices of same dim lexicographically by indices of cofacets (none < every index)
struct sortPSimplicesByCofacets
{ 
    inline bool operator() (const Simplex& left, const Simplex& right)
    {
        std::vector<int> cofacets_left =  left.getCofacetIndices();
        std::vector<int> cofacets_right =  right.getCofacetIndices();
        int min_size = cofacets_left.size();
        if(cofacets_right.size()<min_size){
            min_size=cofacets_right.size();
        }
        for(int i=0; i<min_size; i++){
            if(cofacets_left[i]!=cofacets_left[i]){
                //cofacet i is different
                return cofacets_left[i]<cofacets_right[i];
            }
        }
        //until min_size cofacets were the same
        //if one simplex has less cofacets it comes first in this case
        return cofacets_left.size()<cofacets_right.size();
    }
};

//sort filtration of (filtration_value,simplex_index)-pairs, need additional information about criticality and inclusions
struct filtrationSort{
    std::vector<Simplex>* simplices;
    std::vector<bool>* critical; //whether simplex is critical
    std::vector<bool>* included; //whether simplex was included with different value in current filtration
    std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter; //dictionary that stores new filtration values and inclusion counter (necessary to get correct filtration order) for simplices after point manipulation
    
    filtrationSort(std::vector<Simplex>* simplices_,std::vector<bool>* critical_, std::vector<bool>* included_, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter_){
        simplices = simplices_; critical = critical_; included = included_; inclusion_values_and_counter = inclusion_values_and_counter_;
    }
    bool operator()(const std::pair<exact,int>& left, const std::pair<exact,int>& right){
        int leftindex = left.second;
        int rightindex = right.second;
        int leftdim, rightdim;
        if(leftindex>=0 && rightindex>=0){
            leftdim = (simplices->at(leftindex)).getDim();
            rightdim = (simplices->at(rightindex)).getDim();
            //empty simplex (dim=-1) always comes first
            if(leftdim<0){
                return true;
            }
            if(rightdim<0){
                return false;
            }
        }
        if(left.first == right.first){ //same alpha value -> order by dimension
            //if we got negative index (dummy simplex), this comes last
            if(leftindex<0){
                return false;
            }
            if(rightindex<0){
                return true;
            }
            if(leftdim == rightdim){ 
                //same dim, no inclusion -> critical last (important for persistence pairs)
                //if included, respect inclusion order
                bool leftcritical = critical->at(leftindex);
                bool leftincluded = included->at(leftindex);
                bool rightcritical = critical->at(rightindex);
                bool rightincluded = included->at(rightindex);
                if(leftincluded || rightincluded){
                    //at least one of the simplices was included by hole operation
                    //included simplices come later, respect inclusion order
                    if(!leftincluded){
                        return true;
                    }
                    if(!rightincluded){
                        return false;
                    }
                    int left_inclusion_number = (inclusion_values_and_counter->at(leftindex)).second;
                    int right_inclusion_number = (inclusion_values_and_counter->at(rightindex)).second;
                    return left_inclusion_number < right_inclusion_number;
                }else{
                    //no inclusions
                    if(leftcritical == rightcritical){ //same alpha value, same dim, same criticality -> order by index                    
                        return leftindex < rightindex; 
                    }else{
                        return leftcritical < rightcritical; //critical simplices last, important for persistence pairing!
                    }
                }
            }else{
                return leftdim < rightdim;
            }
        }else{
            return left.first < right.first;
        }
    }
};

//filtration of simplices, for filtered complexes
class Filtration
{
    private: 
        std::vector<std::pair<exact,int> > list; //list of (filtration_value, simplex_index)-pairs 

    public:
        Filtration(){}
        Filtration(std::vector<std::pair<exact,int> > list_){list = list_;}
        void clear(){list.clear();}

        std::vector<std::pair<exact,int> > getList(){return list;}
        std::pair<exact,int> getElement(int i){return list[i];} //get element at position i
        exact getElementValue(int i){return list[i].first;}
        int getElementIndex(int i){return list[i].second;}
        int getElementDim(int i, std::vector<Simplex>* simplices){return (simplices->at(getElementIndex(i))).getDim();}
        Simplex* getSimplex(int i, std::vector<Simplex>* simplices){return &(simplices->at(getElementIndex(i)));}
        
        int size(){return list.size();} //get number of elements
        exact getMinValue(){if(list.size()>1){return list[1].first;}else{return 0;}} //smallest filtration value (except empty simplex)
        exact getMaxValue(){if(list.size()>0){return list.back().first;}else{return 0;}} //greatest filtration value
        
        //append an element to the filtration list
        void appendElement(exact value, int index){list.push_back(std::make_pair(value,index));}
        //insert an element in sorted filtration
        void insertElement(exact value, int index, std::vector<Simplex>* simplices, std::vector<bool>* critical, std::vector<bool>* included, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter){
            int dim = (simplices->at(index)).getDim();
            if(list.size()==0){
                appendElement(value,index);
            }
            //find correct position, start at first simplex with greater alpha, go backwards
            int next_index =  getIndexGreaterThanAlpha2(value,simplices,critical,included,inclusion_values_and_counter);
            for(int i=next_index-1; i>=0; i--){
                std::pair<exact,int> element = list[i];
                int index_element = element.second;
                exact value_element = element.first;
                int dim_element = (simplices->at(index_element)).getDim();
                if(value_element<value || dim_element<dim){
                    //we should insert after this simplex
                    list.insert(list.begin()+i+1,std::make_pair(value,index));
                    return;
                }
                if(value_element==value && dim_element==dim){
                    if(included->at(index) || included->at(index_element)){
                        //at least one of the simplices was included by hole operation
                        //included simplices come later, respect inclusion order
                        if(!included->at(index_element)){
                            //we should insert after this simplex
                            list.insert(list.begin()+i+1,std::make_pair(value,index));
                            return;
                        }else{
                            if((inclusion_values_and_counter->at(index_element)).second < (inclusion_values_and_counter->at(index)).second){
                                list.insert(list.begin()+i+1,std::make_pair(value,index));
                                return;
                            }
                        }
                    }else{
                        //same value and dim, no inclusion -> critical last (important for persistence pairs)
                        bool thiscritical = critical->at(index);
                        bool othercritical = critical->at(index_element);
                        if(thiscritical || !othercritical){
                            //we should insert after this simplex
                            list.insert(list.begin()+i+1,std::make_pair(value,index));
                            return;
                        }
                    }
                }
            }
            //reached beginning of list -> insert there
            list.insert(list.begin(),std::make_pair(value,index));
        }
        void scaleValue(int index, exact factor){
            list[index].first=list[index].first*factor;
        }

        //sort list by filtration value
        void sort(std::vector<Simplex>* simplices, std::vector<bool>* critical, std::vector<bool>* included, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter){ 
            std::sort(list.begin(),list.end(),filtrationSort(simplices,critical,included,inclusion_values_and_counter));
        }
        
        //find index of first element with filtration value greater than the given alpha^2
        int getIndexGreaterThanAlpha2(exact alpha2,std::vector<Simplex>* simplices, std::vector<bool>* critical, std::vector<bool>* included, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter){
            std::vector<std::pair<exact,int> >::iterator up = std::upper_bound(list.begin(),list.end(),std::make_pair(alpha2,-1),filtrationSort(simplices,critical,included,inclusion_values_and_counter));
            return up-list.begin(); //convert to index
        }
        
        //return filtration of subcomplex of given size
        Filtration getSubfiltration(int size){
            return Filtration(std::vector<std::pair<exact,int> >(list.begin(),list.begin()+size));
        }
        
        //delete all excluded simplices from filtration
        void deleteExcluded(std::vector<Simplex>* simplices, std::vector<bool>* excluded){
            for(std::vector<std::pair<exact,int> >::iterator it = list.begin(); it != list.end();){ 
                int index = (*it).second;
                bool simplex_excluded = excluded->at(index);
                if(simplex_excluded){
                   it = list.erase(it); 
                }else{
                   ++it;
                }
            }
        }
        
        //delete simplex from filtration
        bool deleteSimplex(int index, exact alpha2, std::vector<Simplex>* simplices, std::vector<bool>* critical, std::vector<bool>* included, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter){
            //find simplex, start at first simplex with greater alpha, go backwards
            int next_index =  getIndexGreaterThanAlpha2(alpha2,simplices,critical,included,inclusion_values_and_counter);
            for(int i=next_index-1; i>=0; i--){
                std::pair<exact,int> element = list[i];
                if(element.first<alpha2){
                    return false; //simplex was not found
                }
                if(element.second==index){
                    list.erase(list.begin()+i);
                    return true;
                }
            }
            return false;
        }
        
        //recompute filtration after hole operations for which simplices were included with different value or excluded
        void recomputeWithIncluded(Filtration* full_filtration, std::vector<exact>* filtration_values, std::vector<Simplex>* simplices, std::vector<bool>* critical, std::vector<bool>* excluded, std::vector<bool>* included, std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter){
            clear();
            for(int i=0;i<full_filtration->size();i++){
                int simplex_index = full_filtration->getElementIndex(i);
                if(!excluded->at(simplex_index)){
                    exact alpha2;
                    if(included->at(simplex_index)){
                        if(inclusion_values_and_counter->find(simplex_index)==inclusion_values_and_counter->end()){
                            std::cerr << "ERROR(Filtration::recomputWithIncluded): this simplex was not included" << std::endl;
                            alpha2 = -1;
                        }else{
                            alpha2 = (inclusion_values_and_counter->at(simplex_index)).first;
                        }
                    }else{
                        alpha2=filtration_values->at(simplex_index);
                    }
                    appendElement(alpha2,simplex_index);
                }
            }
            sort(simplices,critical,included,inclusion_values_and_counter);
        }

        void print(std::vector<Simplex>* simplices){
            for(int i=0; i<list.size(); i++){
                exact alpha2 = list[i].first;
                Simplex* simplex = getSimplex(i,simplices);
                std::cout << i << " alpha^2 " << CGAL::to_double(alpha2) << " dim " << simplex->getDim() << " index " << simplex->getIndex();
                std::cout << std::endl;
                std::cout << " ";
                simplex->print(); 
            }
        } 
};

//Auxiliary Functions
//----------------------------------------------------

//time measuring
inline double getCPUTime(){
    return (double)clock() / CLOCKS_PER_SEC;
}

//get current date yyyy-mm-dd
inline const std::string getCurrentDate(){
    time_t timer;
    time(&timer);
    struct tm tstruct;
    char buffer[80];
    tstruct = *localtime(&timer);
    strftime(buffer,sizeof(buffer),"%Y-%m-%d", &tstruct );
    return buffer;
}

//get indices of all faces of given simplex        
inline std::set<int> getFacesIndices(int simplex_index, std::vector<Simplex>* simplices){
    std::set<int> faces; 
    std::vector<int> facets_ind =(simplices->at(simplex_index)).getFacetIndices();
    for(int i=0; i<facets_ind.size(); i++){
        faces.insert(facets_ind[i]);
        std::set<int> faces_recursive = getFacesIndices(facets_ind[i],simplices);
        faces.insert(faces_recursive.begin(),faces_recursive.end());
    }
    return faces;
}

//get indices of all cofaces of given simplex
inline std::set<int> getCofacesIndices(int simplex_index, std::vector<Simplex>* simplices){
    std::set<int> cofaces;
    std::vector<int> cofacets_ind = (simplices->at(simplex_index)).getCofacetIndices();
    for(int i=0; i<cofacets_ind.size(); i++){
        cofaces.insert(cofacets_ind[i]);
        std::set<int> cofaces_recursive = getCofacesIndices(cofacets_ind[i],simplices);
        cofaces.insert(cofaces_recursive.begin(),cofaces_recursive.end());
    }
    return cofaces;
}

inline std::vector<std::vector<std::string> > readCSVFile(std::string filename)
{
    std::ifstream file(filename);
    if(file.fail()){
        std::cout << "Error reading input file!" << std::endl;
        return std::vector<std::vector<std::string> >();
    }
    
    std::vector<std::vector<std::string> > result;
    std::string line = "";
    while(getline(file, line))
    {
        std::vector<std::string> vec;
        
        std::stringstream ss(line);
        while(ss.good()){
            std::string substring;
            getline(ss,substring,',');
            substring.erase(0, substring.find_first_not_of(" \t")); //trim whitespace at beginning of string
            substring.erase(substring.find_last_not_of(" \n\r\t")+1); //trim whitespace at end of string
            vec.push_back(substring);
        }
        result.push_back(vec);
    }
    file.close();
 
    return result;
}

#endif // BASICS_H