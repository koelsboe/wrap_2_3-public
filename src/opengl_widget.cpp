//opengl_widget.cpp
//author: koelsboe

#include <QPainter>
#include "opengl_widget.h"

//constructor
OpenGL_Widget::OpenGL_Widget(QWidget *parent) : QOpenGLWidget(parent)
{
    //initialize rotation, translation and scaling of scene
    rotate_x = 0; rotate_y = 0; rotate_z = 0;
    rotate_x_light = 0; rotate_y_light = 0; rotate_z_light = 0;
    translate_x = 0; translate_y = 0; 
    scale = 1.;
    
    num_data_points=0; 
    periodic=false;
    bregman=false;
    
    wrap_bool=true; alpha_bool=true; full_bool=false; 
    labels_intervals_bool=false; labels_points_bool=false;
    pale_triangles_bool = true;
    weighted_points_spheres_bool = false;
    draw_bregman_on_transformed_points_bool = false;
    color_by_radius_bool = false;
    max_value_color_gradient = -1;
    
    linewidth = 3.;
    pointsize = 5.;
    pointsize_input = 2.5;
    labelsize = 15; 
    
    show_oldpoint=false;
    show_newpoint=false;
    
    setDefaultColors();
    
    highlight_mode = 0;
    highlight_color1 = Qt::magenta;
    highlight_color2 = Qt::black;
    filter_highlighted = true;
    
    initializedGL=false;
    pointsWereSet=false;
    
    setFocusPolicy(Qt::StrongFocus); //keyboard focus 
}

//clean up before destroying openGL context
void OpenGL_Widget::cleanup()
{
    makeCurrent(); //openGL context has to be current
    vertexBuffer.destroy(); //destroy vertex buffer object
    VBOOldPoint.destroy();
    VBONewPoint.destroy();
    VBOWeightedPointsSpheres.destroy();
    indexBufferPointsWrap.destroy(); //destroy index buffer object
    indexBufferEdgesWrap.destroy();
    indexBufferTrianglesWrap.destroy();
    indexBufferTetrahedraWrap.destroy();
    indexBufferPointsAlpha.destroy();
    indexBufferEdgesAlpha.destroy();
    indexBufferTrianglesAlpha.destroy();
    indexBufferTetrahedraAlpha.destroy();
    indexBufferWeightedPointsSpheres.destroy();
    delete shader_program; //delete shader program
    shader_program = 0;
    doneCurrent();
}

//recommended minimum size for the widget
// QLayout will never resize a widget to a size smaller than the minimum size hint
QSize OpenGL_Widget::minimumSizeHint() const
{
    return QSize(50, 50);
}

//recommended size for the widget
QSize OpenGL_Widget::sizeHint() const
{
    return QSize(600, 600);
}

//vertex shader source code
static const char *vertexShaderSource =
    //input
    "attribute vec4 vertex;\n" //coordinates in vertex/object space (homogeneous)
    //output for fragment shader
    "varying vec3 vert;\n"      //xyz-coordinates in object space (needed to compute light vector)
    //global variables
    "uniform mat4 projMatrix;\n"      //projection matrix (projects from eye space to screen)
    "uniform mat4 modelViewMatrix;\n" //model view matrix (transforms vertex coordinates from object space to eye space)
    "uniform float pointSize;\n"       //point size in  # pixels
    "void main() {\n"
    "   vert = vertex.xyz;\n"
    "   gl_PointSize = pointSize;\n" //set point size 
    "   gl_Position = projMatrix * modelViewMatrix * vertex;\n" //compute transformed vertex coordinates
    "}\n";

//fragment shader source code
static const char *fragmentShaderSource =
    //input from vertex shader
    "varying highp vec3 vert;\n" //xyz-coordinates in object space
    //global variables
    "uniform highp vec3 lightPos;\n" //position of light source
    "uniform vec3 color;\n" //color
    "uniform bool normalShading;\n" //whether to use fragment normals for shading
    "void main() {\n"
    "   highp vec3 col;"
    "   if(normalShading){\n" //shade with color depending on normal vector of surface
    "       vec3 normal = normalize(cross(dFdx(vert), dFdy(vert)))\n;" //face normal
    "       highp vec3 L = normalize(lightPos - vert);\n" //compute light vector (vertex -> light source)
    "       highp float NL = dot(normalize(normal), L)/2.+0.5;\n" //dot product of light vector and normal vector
    //"       col = clamp(color * 0.2 + color * 0.8 * NL, 0.0, 1.0);\n" //change color depending on light and normal vector, values should be in [0,1]
    "       col = clamp(color * 0.1 + color * 0.9 * NL, 0.1, 1.0);\n" //change color depending on light and normal vector, values should be in [0,1]
    "   }else{\n"
    "       col = color;\n" //use color directly
    "   }\n"
    "   gl_FragColor = vec4(col, 1.0);\n"  //return fragment color
    "}\n";


//clear content
void OpenGL_Widget::clear()
{
    indicesPointsWrap.clear();
    indicesEdgesWrap.clear();
    indicesTrianglesWrap.clear();
    indicesTetrahedraWrap.clear();
    indicesPointsAlpha.clear();
    indicesEdgesAlpha.clear();
    indicesTrianglesAlpha.clear();
    indicesTetrahedraAlpha.clear();
    indicesWeightedPointsSpheres.clear();
    labels_intervals_coords.clear();
    labels_points_coords.clear();
    vertex_data.clear();
    vertex_data_weighted_points_spheres.clear();
    num_data_points = 0;
    vertex_data_bregman_transformed.clear();
    labels_intervals_coords_bregman_transformed.clear();
    labels_points_coords_bregman_transformed.clear();
    draw_bregman_on_transformed_points_bool = false;
}

//initialize required OpenGL resources
//called once before the first call to paintGL() or resizeGL()
void OpenGL_Widget::initializeGL()
{  
    if(pointsWereSet){
        //aboutToBeDestroyed is emitted before the underlying native OpenGL context is destroyed, such that users may clean up OpenGL resources 
        //can happen when top-level window changes, resources are recreated with initializeGL()
        connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &OpenGL_Widget::cleanup);

        //initialize openGL functions for current context
        initializeOpenGLFunctions();    

        //initialize shader program
        shader_program = new QOpenGLShaderProgram;
        shader_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource); //add vertex shader
        shader_program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource); //add fragment shader
        //bind attributes (input variables) to locations 0,1
        shader_program->bindAttributeLocation("vertex", 0); 
        shader_program->link(); //link the shaders

        //set camera (view matrix)
        viewMatrix.setToIdentity();
        viewMatrix.translate(0, 0, -1);

        shader_program->bind();

        //get location of uniform variables (used to communicate with shaders from 'outside')
        projMatrix_loc = shader_program->uniformLocation("projMatrix");
        modelViewMatrix_loc = shader_program->uniformLocation("modelViewMatrix");
        color_loc = shader_program->uniformLocation("color");
        shading_loc = shader_program->uniformLocation("normalShading");
        pointsize_loc = shader_program->uniformLocation("pointSize");

        //set fixed light position
        //shader_program->setUniformValue(shader_program->uniformLocation("lightPos"), QVector3D(0, 0, -200));
        shader_program->setUniformValue(shader_program->uniformLocation("lightPos"), QVector3D(-200, 0, 0));

        //setup a vertex buffer object containing the vertex data (coordinates) we want to draw
        vertexBuffer.create();

        //create a vertex array object for points
        // = openGL object that stores all of the state needed to supply data for drawing (vertex data, buffers)
        VAOPointsWrap.create();
        VAOPointsWrap.bind();   

        vertexBuffer.bind();    
        //setup vertex attributes (in our case just one at location 0: vertex coordinates)
        QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();  
        f->glEnableVertexAttribArray(0); //enable vertex attribute array (input data)  
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0); //specify location and data format of vertex attribute
        //f->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<void *>(3 * sizeof(GLfloat))); //vertex color vectors 
        //allocate vertex data
        if(!periodic){
            vertexBuffer.allocate(vertex_data.data(), data_points->size() * 3 * sizeof(GLfloat)); 
        }else {
            vertexBuffer.allocate(vertex_data.data(), data_points->size() * 3 * 8 * sizeof(GLfloat)); 
        }

        //create index buffer to store vertex indices for simplices (here: just points)
        indexBufferPointsWrap = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferPointsWrap.create();
        indexBufferPointsWrap.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferPointsWrap.bind();
        indexBufferPointsWrap.allocate(indicesPointsWrap.data(), (getNumPointsWrapFull()) * sizeof(unsigned int));

        //create a vertex array object and an index buffer for Wrap edges, bind vertex buffer
        VAOEdgesWrap.create(); VAOEdgesWrap.bind();   
        vertexBuffer.bind();       
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0); 
        indexBufferEdgesWrap = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferEdgesWrap.create(); indexBufferEdgesWrap.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferEdgesWrap.bind();
        indexBufferEdgesWrap.allocate(indicesEdgesWrap.data(), (2*getNumEdgesWrapFull()) * sizeof(unsigned int));

        //create a vertex array object and an index buffer for Wrap triangles, bind vertex buffer
        VAOTrianglesWrap.create();
        VAOTrianglesWrap.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);  
        indexBufferTrianglesWrap = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTrianglesWrap.create(); indexBufferTrianglesWrap.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTrianglesWrap.bind();
        indexBufferTrianglesWrap.allocate(indicesTrianglesWrap.data(), (3*getNumTrianglesWrapFull()) * sizeof(unsigned int));  

        //create a vertex array object and an index buffer for Wrap tetrahedra, bind vertex buffer
        VAOTetrahedraWrap.create();
        VAOTetrahedraWrap.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0); 
        indexBufferTetrahedraWrap = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTetrahedraWrap.create(); indexBufferTetrahedraWrap.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTetrahedraWrap.bind();
        indexBufferTetrahedraWrap.allocate(indicesTetrahedraWrap.data(), (12*getNumTetrahedraWrapFull()) * sizeof(unsigned int)); 
        VAOTetrahedraWrap.release(); //release before releasing anything else !!

        //create a vertex array object and an index buffer for Alpha points, bind vertex buffer
        VAOPointsAlpha.create(); VAOPointsAlpha.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferPointsAlpha = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferPointsAlpha.create(); indexBufferPointsAlpha.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferPointsAlpha.bind();
        indexBufferPointsAlpha.allocate(indicesPointsAlpha.data(), (getNumPointsAlphaFull()) * sizeof(unsigned int));
        
        //create a vertex array object and an index buffer for Alpha edges, bind vertex buffer
        VAOEdgesAlpha.create(); VAOEdgesAlpha.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferEdgesAlpha = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferEdgesAlpha.create(); indexBufferEdgesAlpha.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferEdgesAlpha.bind();
        indexBufferEdgesAlpha.allocate(indicesEdgesAlpha.data(), (2*getNumEdgesAlphaFull()) * sizeof(unsigned int));

        //create a vertex array object and an index buffer for Alpha triangles, bind vertex buffer
        VAOTrianglesAlpha.create();
        VAOTrianglesAlpha.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferTrianglesAlpha = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTrianglesAlpha.create(); indexBufferTrianglesAlpha.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTrianglesAlpha.bind();
        indexBufferTrianglesAlpha.allocate(indicesTrianglesAlpha.data(), (3*getNumTrianglesAlphaFull()) * sizeof(unsigned int));  

        //create a vertex array object and an index buffer for Alpha tetrahedra, bind vertex buffer
        VAOTetrahedraAlpha.create();
        VAOTetrahedraAlpha.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferTetrahedraAlpha = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTetrahedraAlpha.create(); indexBufferTetrahedraAlpha.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTetrahedraAlpha.bind();
        indexBufferTetrahedraAlpha.allocate(indicesTetrahedraAlpha.data(), (12*getNumTetrahedraAlphaFull()) * sizeof(unsigned int)); 
        VAOTetrahedraAlpha.release(); //release before releasing anything else !!
        
        //create a vertex array object and an index buffer for highlighted points, bind vertex buffer
        VAOPointsHighlighted1.create(); VAOPointsHighlighted1.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferPointsHighlighted1 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferPointsHighlighted1.create(); indexBufferPointsHighlighted1.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferPointsHighlighted1.bind();
        indexBufferPointsHighlighted1.allocate(indicesPointsHighlighted1.data(), indicesPointsHighlighted1.size() * sizeof(unsigned int));
        
        //create a vertex array object and an index buffer for highlighted edges, bind vertex buffer
        VAOEdgesHighlighted1.create(); VAOEdgesHighlighted1.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferEdgesHighlighted1 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferEdgesHighlighted1.create(); indexBufferEdgesHighlighted1.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferEdgesHighlighted1.bind();
        indexBufferEdgesHighlighted1.allocate(indicesEdgesHighlighted1.data(), indicesEdgesHighlighted1.size() * sizeof(unsigned int));

        //create a vertex array object and an index buffer for highlighted triangles, bind vertex buffer
        VAOTrianglesHighlighted1.create();
        VAOTrianglesHighlighted1.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferTrianglesHighlighted1 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTrianglesHighlighted1.create(); indexBufferTrianglesHighlighted1.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTrianglesHighlighted1.bind();
        indexBufferTrianglesHighlighted1.allocate(indicesTrianglesHighlighted1.data(), indicesTrianglesHighlighted1.size() * sizeof(unsigned int));  

        //create a vertex array object and an index buffer for highlighted tetrahedra, bind vertex buffer
        VAOTetrahedraHighlighted1.create();
        VAOTetrahedraHighlighted1.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferTetrahedraHighlighted1 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTetrahedraHighlighted1.create(); indexBufferTetrahedraHighlighted1.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTetrahedraHighlighted1.bind();
        indexBufferTetrahedraHighlighted1.allocate(indicesTetrahedraHighlighted1.data(), indicesTetrahedraHighlighted1.size() * sizeof(unsigned int)); 
        VAOTetrahedraHighlighted1.release(); //release before releasing anything else !!
        
        //create a vertex array object and an index buffer for highlighted points, bind vertex buffer
        VAOPointsHighlighted2.create(); VAOPointsHighlighted2.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferPointsHighlighted2 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferPointsHighlighted2.create(); indexBufferPointsHighlighted2.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferPointsHighlighted2.bind();
        indexBufferPointsHighlighted2.allocate(indicesPointsHighlighted2.data(), indicesPointsHighlighted2.size() * sizeof(unsigned int));
        
        //create a vertex array object and an index buffer for highlighted edges, bind vertex buffer
        VAOEdgesHighlighted2.create(); VAOEdgesHighlighted2.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferEdgesHighlighted2 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferEdgesHighlighted2.create(); indexBufferEdgesHighlighted2.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferEdgesHighlighted2.bind();
        indexBufferEdgesHighlighted2.allocate(indicesEdgesHighlighted2.data(), indicesEdgesHighlighted2.size() * sizeof(unsigned int));

        //create a vertex array object and an index buffer for highlighted triangles, bind vertex buffer
        VAOTrianglesHighlighted2.create();
        VAOTrianglesHighlighted2.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferTrianglesHighlighted2 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTrianglesHighlighted2.create(); indexBufferTrianglesHighlighted2.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTrianglesHighlighted2.bind();
        indexBufferTrianglesHighlighted2.allocate(indicesTrianglesHighlighted2.data(), indicesTrianglesHighlighted2.size() * sizeof(unsigned int));  

        //create a vertex array object and an index buffer for highlighted tetrahedra, bind vertex buffer
        VAOTetrahedraHighlighted2.create();
        VAOTetrahedraHighlighted2.bind();   
        vertexBuffer.bind();    
        f->glEnableVertexAttribArray(0); 
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        indexBufferTetrahedraHighlighted2 = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferTetrahedraHighlighted2.create(); indexBufferTetrahedraHighlighted2.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferTetrahedraHighlighted2.bind();
        indexBufferTetrahedraHighlighted2.allocate(indicesTetrahedraHighlighted2.data(), indicesTetrahedraHighlighted2.size() * sizeof(unsigned int)); 
        VAOTetrahedraHighlighted2.release(); //release before releasing anything else !!

        vertexBuffer.release();  
        
        //buffers for point manipulation
        VBOOldPoint.create(); VAOOldPoint.create(); 
        VAOOldPoint.bind(); VBOOldPoint.bind();
        f->glEnableVertexAttribArray(0); //enable vertex attribute array (input data)  
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0); //specify location and data format 
        VBONewPoint.create(); VAONewPoint.create(); 
        VAONewPoint.bind(); VBONewPoint.bind();
        f->glEnableVertexAttribArray(0); //enable vertex attribute array (input data)  
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0); //specify location and data format 
        VAONewPoint.release();
        VBONewPoint.release();
        
        //buffers for weighted points spheres
        VBOWeightedPointsSpheres.create(); VAOWeightedPointsSpheres.create();
        VAOWeightedPointsSpheres.bind(); VBOWeightedPointsSpheres.bind(); 
        f->glEnableVertexAttribArray(0); //enable vertex attribute array (input data)  
        f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0); //specify location and data format of vertex attribute
        VBOWeightedPointsSpheres.allocate(vertex_data_weighted_points_spheres.data(), vertex_data_weighted_points_spheres.size() * sizeof(GLfloat)); 
        indexBufferWeightedPointsSpheres = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
        indexBufferWeightedPointsSpheres.create(); indexBufferWeightedPointsSpheres.setUsagePattern(QOpenGLBuffer::StaticDraw);
        indexBufferWeightedPointsSpheres.bind();
        indexBufferWeightedPointsSpheres.allocate(indicesWeightedPointsSpheres.data(), indicesWeightedPointsSpheres.size() * sizeof(unsigned int)); 
        VAOWeightedPointsSpheres.release();
        VBOWeightedPointsSpheres.release();
        
        
        shader_program->release();

        initializedGL=true;
    }
}

//new filtered simplicial complexes (Wrap and Alpha) to be drawn
//store vertex coordinates, and vertex indices for all simplices of a filtration
void OpenGL_Widget::setPoints(int periodic_size, bool weighted_, bool bregman_, std::vector<DataPoint>* data_points_, int num_data_points_, std::vector<DataPoint>* data_points_bregman_transformed, std::vector<Simplex>* simplices_, std::vector<int>* interval_indices,
            Filtration* filtration_wrap_, int filtration_index_wrap_,           
            std::vector<int>* num_points_wrap_filtration_, std::vector<int>* num_edges_wrap_filtration_, std::vector<int>* num_triangles_wrap_filtration_, std::vector<int>* num_tetrahedra_wrap_filtration_,
            Filtration* filtration_alpha_, int filtration_index_alpha_,
            std::vector<int>* num_points_alpha_filtration_, std::vector<int>* num_edges_alpha_filtration_, std::vector<int>* num_triangles_alpha_filtration_, std::vector<int>* num_tetrahedra_alpha_filtration_,
            std::vector<exact>* alpha_filtration_values_, std::vector<exact>* wrap_filtration_values_
        )
{
    num_data_points = num_data_points_;
    data_points = data_points_;
    filtration_alpha = filtration_alpha_;
    filtration_wrap = filtration_wrap_;
    simplices = simplices_;
    alpha_filtration_values = alpha_filtration_values_;
    wrap_filtration_values = wrap_filtration_values_;
    
    periodic = false;
    if(periodic_size>0){
        periodic = true;
    }
    weighted = weighted_;
    bregman = bregman_;
    
    indicesPointsWrap.clear();
    indicesEdgesWrap.clear();
    indicesWeightedPointsSpheres.clear();
    indicesTrianglesWrap.clear();
    indicesTetrahedraWrap.clear();
    indicesPointsAlpha.clear();
    indicesEdgesAlpha.clear();
    indicesTrianglesAlpha.clear();
    indicesTetrahedraAlpha.clear();
    indicesPointsHighlighted1.clear();
    indicesEdgesHighlighted1.clear();
    indicesTrianglesHighlighted1.clear();
    indicesTetrahedraHighlighted1.clear();
    indicesPointsHighlighted2.clear();
    indicesEdgesHighlighted2.clear();
    indicesTrianglesHighlighted2.clear();
    indicesTetrahedraHighlighted2.clear();
    labels_intervals_coords.clear();
    labels_points_coords.clear();
    vertex_data.clear();
    vertex_data_weighted_points_spheres.clear();
    vertex_data_bregman_transformed.clear();
    labels_intervals_coords_bregman_transformed.clear();
    labels_points_coords_bregman_transformed.clear();
    draw_bregman_on_transformed_points_bool = false;
    
    center = QVector3D(0,0,0); //centroid of points   
     
    filtration_index_wrap = filtration_index_wrap_;
    filtration_index_alpha = filtration_index_alpha_;
    num_points_wrap_filtration=num_points_wrap_filtration_;
    num_edges_wrap_filtration=num_edges_wrap_filtration_;
    num_triangles_wrap_filtration=num_triangles_wrap_filtration_;
    num_tetrahedra_wrap_filtration=num_tetrahedra_wrap_filtration_;
    num_points_alpha_filtration=num_points_alpha_filtration_;
    num_edges_alpha_filtration=num_edges_alpha_filtration_;
    num_triangles_alpha_filtration=num_triangles_alpha_filtration_;
    num_tetrahedra_alpha_filtration=num_tetrahedra_alpha_filtration_;  
    
    
    //vertex data, compute center
    for(int i=0; i<data_points->size(); i++){
        if(!data_points->at(i).isDeleted()){
            vertex_data.push_back((GLfloat) data_points->at(i).getX_double());
            vertex_data.push_back((GLfloat) data_points->at(i).getY_double());
            vertex_data.push_back((GLfloat) data_points->at(i).getZ_double());
            center = center + QVector3D(data_points->at(i).getX_double(),data_points->at(i).getY_double(),data_points->at(i).getZ_double());
            //store points positions for labels
            labels_points_coords.push_back(QVector3D(data_points->at(i).getX_double(),data_points->at(i).getY_double(),data_points->at(i).getZ_double()));
        }else{ //dummy point, will not be used
            vertex_data.push_back((GLfloat) 0);
            vertex_data.push_back((GLfloat) 0);
            vertex_data.push_back((GLfloat) 0);
            labels_points_coords.push_back(QVector3D(0,0,0));
        }
        
        if(!data_points->at(i).isDeleted()){
            vertex_data_bregman_transformed.push_back((GLfloat) data_points_bregman_transformed->at(i).getX_double());
            vertex_data_bregman_transformed.push_back((GLfloat) data_points_bregman_transformed->at(i).getY_double());
            vertex_data_bregman_transformed.push_back((GLfloat) data_points_bregman_transformed->at(i).getZ_double());
            center_bregman_transformed = center_bregman_transformed + QVector3D(data_points_bregman_transformed->at(i).getX_double(),data_points_bregman_transformed->at(i).getY_double(),data_points_bregman_transformed->at(i).getZ_double());
            labels_points_coords_bregman_transformed.push_back(QVector3D(data_points_bregman_transformed->at(i).getX_double(),data_points_bregman_transformed->at(i).getY_double(),data_points_bregman_transformed->at(i).getZ_double()));
        }else{ //dummy point, will not be used
            vertex_data_bregman_transformed.push_back((GLfloat) 0); 
            vertex_data_bregman_transformed.push_back((GLfloat) 0);
            vertex_data_bregman_transformed.push_back((GLfloat) 0);
            labels_points_coords_bregman_transformed.push_back(QVector3D(0,0,0));
        }
    }      
    
   if(num_data_points>0){
    center = center/(double)num_data_points;
    center_bregman_transformed = center_bregman_transformed/(double)num_data_points;
    
    //in periodic case: also store all possible offset points
    if(periodic){
        //iterate through all 7 possible offsets, interpreted as binary number zyx
        for(int index=1; index<8; index++){
            //compute offsets in all directions from integer representation
            bool x = index%2==1;
            bool y = (index-x)/2%2==1;
            bool z = (index-x-y*2)/4%2==1;
            //store copy of each point with this offset
            for(int i=0; i<data_points->size(); i++){
                if(!data_points->at(i).isDeleted()){
                    vertex_data.push_back((GLfloat) data_points->at(i).getX_double()+periodic_size*x);
                    vertex_data.push_back((GLfloat) data_points->at(i).getY_double()+periodic_size*y);
                    vertex_data.push_back((GLfloat) data_points->at(i).getZ_double()+periodic_size*z);
                    
                    vertex_data_bregman_transformed.push_back((GLfloat) data_points_bregman_transformed->at(i).getX_double()+periodic_size*x);
                    vertex_data_bregman_transformed.push_back((GLfloat) data_points_bregman_transformed->at(i).getY_double()+periodic_size*y);
                    vertex_data_bregman_transformed.push_back((GLfloat) data_points_bregman_transformed->at(i).getZ_double()+periodic_size*z);
                }
            }
        }       
    }
    
   //get maximum absolute coordinate value of centered point cloud (radius of bounding sphere)
    max_coord = 0.;
    max_coord_bregman_transformed = 0.;
    for(int i=0; i<data_points->size(); i++){
        if(!data_points->at(i).isDeleted()){
            if(std::abs(data_points->at(i).getX_double()-center.x())>max_coord){
                max_coord = std::abs(data_points->at(i).getX_double()-center.x());
            }
            if(std::abs(data_points->at(i).getY_double()-center.y())>max_coord){
                max_coord = std::abs(data_points->at(i).getY_double()-center.y());
            }
            if(std::abs(data_points->at(i).getZ_double()-center.z())>max_coord){
                max_coord = std::abs(data_points->at(i).getZ_double()-center.z());
            }
            
            if(std::abs(data_points_bregman_transformed->at(i).getX_double()-center_bregman_transformed.x())>max_coord_bregman_transformed){
                max_coord_bregman_transformed = std::abs(data_points_bregman_transformed->at(i).getX_double()-center_bregman_transformed.x());
            }
            if(std::abs(data_points_bregman_transformed->at(i).getY_double()-center_bregman_transformed.y())>max_coord_bregman_transformed){
                max_coord_bregman_transformed = std::abs(data_points_bregman_transformed->at(i).getY_double()-center_bregman_transformed.y());
            }
            if(std::abs(data_points_bregman_transformed->at(i).getZ_double()-center_bregman_transformed.z())>max_coord_bregman_transformed){
                max_coord_bregman_transformed = std::abs(data_points_bregman_transformed->at(i).getZ_double()-center_bregman_transformed.z());
            }
        }
    }   
    
    //Wrap simplices
    for(int i=0; i<filtration_wrap->size(); i++){
        int element_dim = filtration_wrap->getElementDim(i,simplices);
        int element_index = filtration_wrap->getElementIndex(i);
        Simplex simplex = simplices->at(element_index);
        if(element_dim==0){ //point
            indicesPointsWrap.push_back(simplex.getVertex(0));
        }else if(element_dim==1){ //edge
            if(!periodic){
                indicesEdgesWrap.push_back(simplex.getVertex(0));
                indicesEdgesWrap.push_back(simplex.getVertex(1));
            }else{
                indicesEdgesWrap.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesEdgesWrap.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
            }
        }else if(element_dim==2){ //triangle
            if(!periodic){
                indicesTrianglesWrap.push_back(simplex.getVertex(0));
                indicesTrianglesWrap.push_back(simplex.getVertex(1));
                indicesTrianglesWrap.push_back(simplex.getVertex(2));
            }else{
                indicesTrianglesWrap.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesTrianglesWrap.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
                indicesTrianglesWrap.push_back(simplex.getVertex(2)+simplex.getOffset(2).toInt()*data_points->size());
            }
        }else if(element_dim==3){ //tetrahedron
            std::vector<int> triangles_ind = simplex.getFacetIndices();
            if(triangles_ind.size()!=4){
                std::cerr << "ERROR (OpenGL_Widget::setPoints)" << std::endl;
            }
            for(int i=0; i<4; i++){ 
                Simplex triangle = simplices->at(triangles_ind[i]);
                //add tetrahedron triangle vertices in the same order as for individual triangles to avoid z-fighting !!
                if(!periodic){
                    indicesTetrahedraWrap.push_back(triangle.getVertex(0));
                    indicesTetrahedraWrap.push_back(triangle.getVertex(1));
                    indicesTetrahedraWrap.push_back(triangle.getVertex(2));
                }else{
                    indicesTetrahedraWrap.push_back(triangle.getVertex(0)+triangle.getOffset(0).toInt()*data_points->size());
                    indicesTetrahedraWrap.push_back(triangle.getVertex(1)+triangle.getOffset(1).toInt()*data_points->size());
                    indicesTetrahedraWrap.push_back(triangle.getVertex(2)+triangle.getOffset(2).toInt()*data_points->size());
                }             
            }
        }
    } 
    
    //Alpha simplices
    for(int i=0; i<filtration_alpha->size(); i++){
        int element_dim = filtration_alpha->getElementDim(i,simplices);
        int element_index = filtration_alpha->getElementIndex(i);
        Simplex simplex = simplices->at(element_index);
        if(element_dim==0){ //point
            indicesPointsAlpha.push_back(simplex.getVertex(0));
        }else if(element_dim==1){ //edge
            if(!periodic){
                indicesEdgesAlpha.push_back(simplex.getVertex(0));
                indicesEdgesAlpha.push_back(simplex.getVertex(1));
            }else{
                indicesEdgesAlpha.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesEdgesAlpha.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
            }
        }else if(element_dim==2){ //triangle
            if(!periodic){
                indicesTrianglesAlpha.push_back(simplex.getVertex(0));
                indicesTrianglesAlpha.push_back(simplex.getVertex(1));
                indicesTrianglesAlpha.push_back(simplex.getVertex(2));
            }else{
                indicesTrianglesAlpha.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesTrianglesAlpha.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
                indicesTrianglesAlpha.push_back(simplex.getVertex(2)+simplex.getOffset(2).toInt()*data_points->size());
            }
        }else if(element_dim==3){ //tetrahedron
            std::vector<int> triangles_ind = simplex.getFacetIndices();
            if(triangles_ind.size()!=4){
                std::cerr << "ERROR (OpenGL_Widget::setPoints)" << std::endl;
            }
            for(int i=0; i<4; i++){
                Simplex triangle = simplices->at(triangles_ind[i]);
                if(!periodic){
                    indicesTetrahedraAlpha.push_back(triangle.getVertex(0));
                    indicesTetrahedraAlpha.push_back(triangle.getVertex(1));
                    indicesTetrahedraAlpha.push_back(triangle.getVertex(2));
                }else{
                    indicesTetrahedraAlpha.push_back(triangle.getVertex(0)+triangle.getOffset(0).toInt()*data_points->size());
                    indicesTetrahedraAlpha.push_back(triangle.getVertex(1)+triangle.getOffset(1).toInt()*data_points->size());
                    indicesTetrahedraAlpha.push_back(triangle.getVertex(2)+triangle.getOffset(2).toInt()*data_points->size());
                }
                //add tetrahedron triangle vertices in the same order as for individual triangles to avoid z-fighting
            }
        }
    } 
    
    //compute coordinates of tetrahedron labels
    for(int i = 0; i<simplices->size(); i++){
        Simplex tetrahedron = simplices->at(i);
        if(tetrahedron.getDim()==3 && !tetrahedron.isDeleted()){
            //compute centroid = coordinates for label
            std::vector<CPoint3> tetra_points; 
            //get tetrahedron vertices, possibly add offset if periodic 
            for(int j=0; j<4; j++){
                int point_ind = tetrahedron.getVertex(j);
                CPoint3 point = CPoint3(data_points->at(point_ind).getX(),data_points->at(point_ind).getY(),data_points->at(point_ind).getZ());
                if(periodic && tetrahedron.isOffset(j)){
                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*tetrahedron.getOffset(j).getX(),periodic_size*tetrahedron.getOffset(j).getY(),periodic_size*tetrahedron.getOffset(j).getZ());
                    point = point + off;
                }
                tetra_points.push_back(point);
            }
            CPoint3 centroid = CGAL::centroid(tetra_points[0],tetra_points[1],tetra_points[2],tetra_points[3]);
            labels_intervals_coords.push_back(std::make_pair(interval_indices->at(tetrahedron.getIndex()),QVector3D(CGAL::to_double(centroid.x()),CGAL::to_double(centroid.y()),CGAL::to_double(centroid.z()))));
            
            tetra_points.clear();
            for(int j=0; j<4; j++){
                int point_ind = tetrahedron.getVertex(j);
                CPoint3 point = CPoint3(data_points_bregman_transformed->at(point_ind).getX(),data_points_bregman_transformed->at(point_ind).getY(),data_points_bregman_transformed->at(point_ind).getZ());
                if(periodic && tetrahedron.isOffset(j)){
                    CGAL::Vector_3<Kernel> off = CGAL::Vector_3<Kernel>(periodic_size*tetrahedron.getOffset(j).getX(),periodic_size*tetrahedron.getOffset(j).getY(),periodic_size*tetrahedron.getOffset(j).getZ());
                    point = point + off;
                }
                tetra_points.push_back(point);
            }
            centroid = CGAL::centroid(tetra_points[0],tetra_points[1],tetra_points[2],tetra_points[3]);
            labels_intervals_coords_bregman_transformed.push_back(std::make_pair(interval_indices->at(tetrahedron.getIndex()),QVector3D(CGAL::to_double(centroid.x()),CGAL::to_double(centroid.y()),CGAL::to_double(centroid.z()))));
        }
    }
    
    //weighted points spheres
    //use polar coordinates
    if(weighted || bregman){ 
        int num_u = 30;
        int num_v = 30;
        int current_base_index = 0;
        double pi = std::atan(1)*4;
        for(int i=0; i<data_points_bregman_transformed->size(); i++){
            //for points with positive weights
            if(!data_points_bregman_transformed->at(i).isDeleted() && (data_points_bregman_transformed->at(i).getWeight())>0){
                
                double radius = std::sqrt(data_points_bregman_transformed->at(i).getWeight_double());
                double center_x = data_points_bregman_transformed->at(i).getX_double();
                double center_y = data_points_bregman_transformed->at(i).getY_double();
                double center_z = data_points_bregman_transformed->at(i).getZ_double();
                
                for(int j=0; j<=num_v; j++){
                    float v = j /(float)num_v;
                    float phi = v*pi;
                    for(int k=0; k<=num_u; k++){
                        float u = k / (float) num_u;
                        float theta = u*pi*2;
                        
                        float x = std::cos(theta)*std::sin(phi) * radius + center_x;
                        float y = std::cos(phi) * radius + center_y;
                        float z = std::sin(theta)*std::sin(phi) * radius + center_z;
                        
                        vertex_data_weighted_points_spheres.push_back((GLfloat) x);
                        vertex_data_weighted_points_spheres.push_back((GLfloat) y);
                        vertex_data_weighted_points_spheres.push_back((GLfloat) z);
                    }
                }
                
                for(int j=0; j<num_u*num_v+num_u; j++){
                    indicesWeightedPointsSpheres.push_back(current_base_index+j);
                    indicesWeightedPointsSpheres.push_back(current_base_index+j+num_u+1);
                    indicesWeightedPointsSpheres.push_back(current_base_index+j+num_u);
                    indicesWeightedPointsSpheres.push_back(current_base_index+j+num_u+1);
                    indicesWeightedPointsSpheres.push_back(current_base_index+j);
                    indicesWeightedPointsSpheres.push_back(current_base_index+j+1);
                }
                current_base_index = vertex_data_weighted_points_spheres.size()/3; //divide by 3 since a point has 3 coordinates
            }
        }
    }
    }
    
   //update index buffers
    if(initializedGL){
        
        vertexBuffer.bind();
        if(!periodic){
            vertexBuffer.allocate(vertex_data.data(), data_points->size() * 3 * sizeof(GLfloat));
        }else{
            vertexBuffer.allocate(vertex_data.data(), data_points->size() * 3 * 8 * sizeof(GLfloat));
        }
        VAOPointsWrap.bind(); vertexBuffer.bind(); indexBufferPointsWrap.bind();
        indexBufferPointsWrap.allocate(indicesPointsWrap.data(), (getNumPointsWrapFull()) * sizeof(unsigned int));
        VAOEdgesWrap.bind(); vertexBuffer.bind(); indexBufferEdgesWrap.bind();
        indexBufferEdgesWrap.allocate(indicesEdgesWrap.data(), (2*getNumEdgesWrapFull()) * sizeof(unsigned int));
        VAOTrianglesWrap.bind(); vertexBuffer.bind(); indexBufferTrianglesWrap.bind();
        indexBufferTrianglesWrap.allocate(indicesTrianglesWrap.data(), (3*getNumTrianglesWrapFull()) * sizeof(unsigned int)); 
        VAOTetrahedraWrap.bind(); vertexBuffer.bind(); indexBufferTetrahedraWrap.bind();
        indexBufferTetrahedraWrap.allocate(indicesTetrahedraWrap.data(), (12*getNumTetrahedraWrapFull()) * sizeof(unsigned int)); 
        //indexBufferTetrahedraWrap.release(); //do not release!
        VAOPointsAlpha.bind(); vertexBuffer.bind(); indexBufferPointsAlpha.bind();
        indexBufferPointsAlpha.allocate(indicesPointsAlpha.data(), (getNumPointsAlphaFull()) * sizeof(unsigned int));
        VAOEdgesAlpha.bind(); vertexBuffer.bind(); indexBufferEdgesAlpha.bind();
        indexBufferEdgesAlpha.allocate(indicesEdgesAlpha.data(), (2*getNumEdgesAlphaFull()) * sizeof(unsigned int));
        VAOTrianglesAlpha.bind(); vertexBuffer.bind(); indexBufferTrianglesAlpha.bind();
        indexBufferTrianglesAlpha.allocate(indicesTrianglesAlpha.data(), (3*getNumTrianglesAlphaFull()) * sizeof(unsigned int));  
        VAOTetrahedraAlpha.bind(); vertexBuffer.bind(); 
        indexBufferTetrahedraAlpha.bind();
        indexBufferTetrahedraAlpha.allocate(indicesTetrahedraAlpha.data(), (12*getNumTetrahedraAlphaFull()) * sizeof(unsigned int));  
        vertexBuffer.release();
        if(weighted || bregman){
            VBOWeightedPointsSpheres.bind();
            VBOWeightedPointsSpheres.allocate(vertex_data_weighted_points_spheres.data(), vertex_data_weighted_points_spheres.size() * sizeof(GLfloat) );
            VAOWeightedPointsSpheres.bind(); VBOWeightedPointsSpheres.bind(); indexBufferWeightedPointsSpheres.bind();
            indexBufferWeightedPointsSpheres.allocate(indicesWeightedPointsSpheres.data(),indicesWeightedPointsSpheres.size()*sizeof(unsigned int));
            VBOWeightedPointsSpheres.release();
        }
    }else{
        initializeGL();
    }
    
    pointsWereSet = true;
}

//paint objects
void OpenGL_Widget::paintGL()
{
    if(!initializedGL){
        initializeGL();
    }
    if(!pointsWereSet){
        return;
    }
    
    //draw 2d content over 3d opengl content with QPpainter
    QPainter painter(this);
    painter.beginNativePainting();  //start opengl drawing 
    
    //clear buffer   
    glClearColor(1, 1, 1, 1); //specify color to clear the color buffers (white background) 
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  

    //rotate and scale scene (model matrix) depending on mouse input
    modelMatrix.setToIdentity();
    //rotate scene
    modelMatrix.rotate(-rotate_x, 1, 0, 0); //x-axis //minus to ensure that loaded models are displayed from correct side
    modelMatrix.rotate(rotate_y, 0, 1, 0); //y-axis
    modelMatrix.rotate(rotate_z, 0, 0, 1); //z-axis
    //scale scene (zoom)
    modelMatrix.scale(scale, scale, scale);
    //move center to origin
    if(!draw_bregman_on_transformed_points_bool){
        modelMatrix.translate(-center);
    }else{
        modelMatrix.translate(-center_bregman_transformed);
    }
    //rotate light source
    lightMatrix.setToIdentity();
    lightMatrix.rotate(-rotate_x_light, 1, 0, 0); //x-axis //minus to ensure that loaded models are displayed from correct side
    lightMatrix.rotate(rotate_y_light, 0, 1, 0); //y-axis
    lightMatrix.rotate(rotate_z_light, 0, 0, 1); //z-axis
    
    //move camera depending on keyboard input (arrow keys)
    viewMatrix.setToIdentity();
    viewMatrix.translate(QVector3D(translate_x*0.01,translate_y*0.01,-1));
    //setup shaders, input matrices
    shader_program->bind();
    shader_program->setUniformValue(projMatrix_loc, projMatrix);
    shader_program->setUniformValue(modelViewMatrix_loc, viewMatrix * modelMatrix);
    shader_program->setUniformValue(pointsize_loc, (GLfloat)pointsize);
    shader_program->setUniformValue(shader_program->uniformLocation("lightPos"), lightMatrix.mapVector(QVector3D(-1000, 0, 1000)));
    
    glLineWidth(linewidth);
    
    glEnable(GL_DEPTH_TEST); //enable use of the depth buffer //must be set here, not just for initialization!!
    //glEnable(GL_CULL_FACE); //back-face culling (polygons facing the back are not drawn)// cannot be used since vertices are not consistently ordered
    glEnable(GL_PROGRAM_POINT_SIZE); //increase point size in vertex shader
    glEnable(GL_POLYGON_OFFSET_FILL); //if triangles and bordering edges overlap, draw edges on top
    glPolygonOffset(1, 1); 

    //draw objects (data stored in buffers)
    //for point manipulation
    if(show_oldpoint || show_newpoint){
        shader_program->setUniformValue(pointsize_loc, (GLfloat)pointsize*2);
        //old point
        if(show_oldpoint){
            shader_program->setUniformValue(color_loc, QVector3D(qRed(color_old_point.rgb())/255., qGreen(color_old_point.rgb())/255., qBlue(color_old_point.rgb())/255.)); //orange
            VAOOldPoint.bind();
            glDrawArrays(GL_POINTS, 0, 1); 
            VAOOldPoint.release();  
        }
        //new point
        if(show_newpoint){
            shader_program->setUniformValue(color_loc, QVector3D(qRed(color_new_point.rgb())/255., qGreen(color_new_point.rgb())/255., qBlue(color_new_point.rgb())/255.)); //black
            VAONewPoint.bind();
            glDrawArrays(GL_POINTS, 0, 1);  
            VAONewPoint.release();  
        }
        shader_program->setUniformValue(pointsize_loc, (GLfloat)pointsize);
    }
   if(num_data_points>0){ 
    if(weighted_points_spheres_bool && ((weighted && !bregman) || (bregman && draw_bregman_on_transformed_points_bool))){ 
        shader_program->setUniformValue(color_loc, QVector3D(qRed(color_weighted_points.rgb())/255., qGreen(color_weighted_points.rgb())/255., qBlue(color_weighted_points.rgb())/255.)); //red 
        shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
        VAOWeightedPointsSpheres.bind();
        glDrawElements(GL_TRIANGLES, indicesWeightedPointsSpheres.size(), GL_UNSIGNED_INT, 0);
        VAOWeightedPointsSpheres.release();
    }
   if(highlight_mode>=1){
    //COLOR 1
    //points
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color1.rgb())/255., qGreen(highlight_color1.rgb())/255., qBlue(highlight_color1.rgb())/255.)); 
    VAOPointsHighlighted1.bind();
    glDrawElements(GL_POINTS, getNumPointsHighlighted1()*1, GL_UNSIGNED_INT, 0);
    VAOPointsHighlighted1.release();
    //Highlighted tetrahedra 
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color1.rgb())/255., qGreen(highlight_color1.rgb())/255., qBlue(highlight_color1.rgb())/255.)); 
    shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
    VAOTetrahedraHighlighted1.bind();
    glDrawElements(GL_TRIANGLES, getNumTetrahedraHighlighted1()*12, GL_UNSIGNED_INT, 0);
    VAOTetrahedraHighlighted1.release();
    //Highlighted triangles
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color1.rgb())/255., qGreen(highlight_color1.rgb())/255., qBlue(highlight_color1.rgb())/255.)); 
    shader_program->setUniformValue(shading_loc, true);
    VAOTrianglesHighlighted1.bind();
    glDrawElements(GL_TRIANGLES, getNumTrianglesHighlighted1()*3, GL_UNSIGNED_INT, 0);
    VAOTrianglesHighlighted1.release(); 
    //Highlighted edges
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color1.rgb())/255., qGreen(highlight_color1.rgb())/255., qBlue(highlight_color1.rgb())/255.)); 
    shader_program->setUniformValue(shading_loc, false); 
    VAOEdgesHighlighted1.bind();
    glDrawElements(GL_LINES, getNumEdgesHighlighted1()*2, GL_UNSIGNED_INT, 0);
    VAOEdgesHighlighted1.release(); 
    //COLOR 2
    //points
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color2.rgb())/255., qGreen(highlight_color2.rgb())/255., qBlue(highlight_color2.rgb())/255.)); 
    VAOPointsHighlighted2.bind();
    glDrawElements(GL_POINTS, getNumPointsHighlighted2(), GL_UNSIGNED_INT, 0);
    VAOPointsHighlighted2.release();
    //Highlighted tetrahedra 
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color2.rgb())/255., qGreen(highlight_color2.rgb())/255., qBlue(highlight_color2.rgb())/255.)); 
    shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
    VAOTetrahedraHighlighted2.bind();
    glDrawElements(GL_TRIANGLES, getNumTetrahedraHighlighted2()*12, GL_UNSIGNED_INT, 0);
    VAOTetrahedraHighlighted2.release();
    //Highlighted triangles
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color2.rgb())/255., qGreen(highlight_color2.rgb())/255., qBlue(highlight_color2.rgb())/255.)); 
    shader_program->setUniformValue(shading_loc, true);
    VAOTrianglesHighlighted2.bind();
    glDrawElements(GL_TRIANGLES, getNumTrianglesHighlighted2()*3, GL_UNSIGNED_INT, 0);
    VAOTrianglesHighlighted2.release(); 
    //Highlighted edges
    shader_program->setUniformValue(color_loc, QVector3D(qRed(highlight_color2.rgb())/255., qGreen(highlight_color2.rgb())/255., qBlue(highlight_color2.rgb())/255.)); 
    shader_program->setUniformValue(shading_loc, false); 
    VAOEdgesHighlighted2.bind();
    glDrawElements(GL_LINES, getNumEdgesHighlighted2()*2, GL_UNSIGNED_INT, 0);
    VAOEdgesHighlighted2.release(); //important that painter works!!
   }
   if(highlight_mode<2){
    //Wrap complex
    if(wrap_bool){   
        //points
        shader_program->setUniformValue(color_loc, QVector3D(qRed(color_points.rgb())/255., qGreen(color_points.rgb())/255., qBlue(color_points.rgb())/255.)); //red
        shader_program->setUniformValue(shading_loc, false); //shading without normal vector (for points and edges)
        VAOPointsWrap.bind();
        if(full_bool){
            glDrawElements(GL_POINTS, getNumPointsWrapFull(), GL_UNSIGNED_INT, 0); 
        }else{
            glDrawElements(GL_POINTS, getNumPointsWrap(), GL_UNSIGNED_INT, 0); 
        }
        VAOPointsWrap.release();
        if(!color_by_radius_bool){
            //Wrap tetrahedra
            shader_program->setUniformValue(color_loc, QVector3D(qRed(color_wrap_light.rgb())/255., qGreen(color_wrap_light.rgb())/255., qBlue(color_wrap_light.rgb())/255.)); //light green 
            shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
            VAOTetrahedraWrap.bind();
            if(full_bool){
                glDrawElements(GL_TRIANGLES, getNumTetrahedraWrapFull()*12, GL_UNSIGNED_INT, 0);
            }else{
                glDrawElements(GL_TRIANGLES, getNumTetrahedraWrap()*12, GL_UNSIGNED_INT, 0);
            }
            VAOTetrahedraWrap.release();
            //Wrap triangles
            if(pale_triangles_bool){
                shader_program->setUniformValue(color_loc, QVector3D(qRed(color_wrap_pale.rgb())/255., qGreen(color_wrap_pale.rgb())/255., qBlue(color_wrap_pale.rgb())/255.)); //pale green
                shader_program->setUniformValue(shading_loc, false);
            }else{
                shader_program->setUniformValue(color_loc, QVector3D(qRed(color_wrap_light.rgb())/255., qGreen(color_wrap_light.rgb())/255., qBlue(color_wrap_light.rgb())/255.)); //light green 
                shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
            }
            VAOTrianglesWrap.bind();
            if(full_bool){
                glDrawElements(GL_TRIANGLES, getNumTrianglesWrapFull()*3, GL_UNSIGNED_INT, 0);
            }else{
                glDrawElements(GL_TRIANGLES, getNumTrianglesWrap()*3, GL_UNSIGNED_INT, 0);//(void*)((num_data_points+2*num_edges)*sizeof(unsigned int)))
            }
            VAOTrianglesWrap.release();
        }else{
            int triangle_index = 0; 
            int tetrahedron_index = 0;
            for(int i=0; i<filtration_wrap->size(); i++){
                double color_value = 0;
                int element_dim = filtration_wrap->getElementDim(i,simplices);
                if(element_dim == 2 || element_dim == 3){
                    if(!full_bool && ((triangle_index>=getNumTrianglesWrap() && element_dim ==2) || (tetrahedron_index>=getNumTetrahedraWrap() && element_dim ==3))){
                        break;
                    }
                    double simplex_radius = CGAL::to_double(wrap_filtration_values->at(filtration_wrap->getElementIndex(i)));
                    double min_value = 0;
                    if(filtration_alpha->size()>0){
                        min_value = CGAL::to_double(filtration_alpha->getMinValue());
                    }
                    double max_value = max_value_color_gradient;
                    if(max_value==-1 || max_value<=min_value){
                        max_value = CGAL::to_double(filtration_alpha->getMaxValue());
                    }
                    color_value = std::max(std::min(std::log(std::min((simplex_radius-min_value)/(max_value-min_value),1.)+1.)/std::log(2),1.),0.);

                    shader_program->setUniformValue(color_loc, QVector3D(color_value, color_value, color_value)); //grayscale value
                    shader_program->setUniformValue(shading_loc, false);

                    if(element_dim == 2){
                        VAOTrianglesWrap.bind();
                        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, static_cast<char*>(0) + (triangle_index*3*sizeof(GLuint)));
                        VAOTrianglesWrap.release(); 
                        triangle_index++;
                    }else if(element_dim == 3){
                        VAOTetrahedraWrap.bind();
                        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, static_cast<char*>(0) + (tetrahedron_index*12*sizeof(GLuint)));
                        VAOTetrahedraWrap.release();
                        tetrahedron_index++;
                    }
                }
                
                
            }
        }
        //Wrap edges
        shader_program->setUniformValue(color_loc, QVector3D(qRed(color_wrap_dark.rgb())/255., qGreen(color_wrap_dark.rgb())/255., qBlue(color_wrap_dark.rgb())/255.)); //dark green
        shader_program->setUniformValue(shading_loc, false); 
        VAOEdgesWrap.bind();
        if(full_bool){ //draw all
            glDrawElements(GL_LINES, getNumEdgesWrapFull()*2, GL_UNSIGNED_INT, 0);
        }else{ //draw subset depending on filtration value
            glDrawElements(GL_LINES, getNumEdgesWrap()*2, GL_UNSIGNED_INT, 0);
        }
        VAOEdgesWrap.release(); //important that painter works!!
    }
    //Alpha complex
    if(alpha_bool){  
        //points
        shader_program->setUniformValue(color_loc, QVector3D(qRed(color_points.rgb())/255., qGreen(color_points.rgb())/255., qBlue(color_points.rgb())/255.)); //red
        shader_program->setUniformValue(shading_loc, false); //shading without normal vector (for points and edges)
        VAOPointsAlpha.bind();
        if(full_bool){
            glDrawElements(GL_POINTS, getNumPointsAlphaFull(), GL_UNSIGNED_INT, 0); 
        }else{
            glDrawElements(GL_POINTS, getNumPointsAlpha(), GL_UNSIGNED_INT, 0); 
        }
        VAOPointsAlpha.release();
        if(!color_by_radius_bool){
            //Alpha tetrahedra 
            //shader_program->setUniformValue(color_loc, QVector3D(0.2f, 0.2f, 1.0f)); //light blue 
            shader_program->setUniformValue(color_loc, QVector3D(qRed(color_alpha_light.rgb())/255., qGreen(color_alpha_light.rgb())/255., qBlue(color_alpha_light.rgb())/255.)); //light blue 
            shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
            VAOTetrahedraAlpha.bind();
            if(full_bool){
                glDrawElements(GL_TRIANGLES, getNumTetrahedraAlphaFull()*12, GL_UNSIGNED_INT, 0);
            }else{
                glDrawElements(GL_TRIANGLES, getNumTetrahedraAlpha()*12, GL_UNSIGNED_INT, 0);
            }
            VAOTetrahedraAlpha.release();
            //Alpha triangles
            if(pale_triangles_bool){
                //shader_program->setUniformValue(color_loc, QVector3D(0.5f, 0.5f, 1.0f)); //pale blue
                shader_program->setUniformValue(color_loc, QVector3D(qRed(color_alpha_pale.rgb())/255., qGreen(color_alpha_pale.rgb())/255., qBlue(color_alpha_pale.rgb())/255.)); //pale blue
                shader_program->setUniformValue(shading_loc, false); 
            }else{
                shader_program->setUniformValue(color_loc, QVector3D(qRed(color_alpha_light.rgb())/255., qGreen(color_alpha_light.rgb())/255., qBlue(color_alpha_light.rgb())/255.)); //light blue 
                shader_program->setUniformValue(shading_loc, true); //shading with normal vector ('flat shading') 
            }
            VAOTrianglesAlpha.bind();
            if(full_bool){
                glDrawElements(GL_TRIANGLES, getNumTrianglesAlphaFull()*3, GL_UNSIGNED_INT, 0);
            }else{
                glDrawElements(GL_TRIANGLES, getNumTrianglesAlpha()*3, GL_UNSIGNED_INT, 0);
            }
            VAOTrianglesAlpha.release(); 
        }else{
            int triangle_index = 0; 
            int tetrahedron_index = 0;
            for(int i=0; i<filtration_alpha->size(); i++){
                double color_value = 0;
                int element_dim = filtration_alpha->getElementDim(i,simplices);
                if(element_dim == 2 || element_dim == 3){
                    if(!full_bool && ((triangle_index>=getNumTrianglesAlpha() && element_dim ==2) || (tetrahedron_index>=getNumTetrahedraAlpha() && element_dim ==3))){
                        break;
                    }
                    double simplex_radius = CGAL::to_double(alpha_filtration_values->at(filtration_alpha->getElementIndex(i)));
                    double min_value = 0;
                    if(filtration_alpha->size()>0){
                        min_value = CGAL::to_double(filtration_alpha->getMinValue());
                    }
                    double max_value = max_value_color_gradient;
                    if(max_value==-1 || max_value<=min_value){
                        max_value = CGAL::to_double(filtration_alpha->getMaxValue());
                    }
                    color_value = std::max(std::min(std::log(std::min((simplex_radius-min_value)/(max_value-min_value),1.)+1.)/std::log(2),1.),0.);

                    shader_program->setUniformValue(color_loc, QVector3D(color_value, color_value, color_value)); //grayscale value
                    shader_program->setUniformValue(shading_loc, false);

                    if(element_dim == 2){
                        VAOTrianglesAlpha.bind();
                        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, static_cast<char*>(0) + (triangle_index*3*sizeof(GLuint)));
                        VAOTrianglesAlpha.release(); 
                        triangle_index++;
                    }else if(element_dim == 3){
                        VAOTetrahedraAlpha.bind();
                        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, static_cast<char*>(0) + (tetrahedron_index*12*sizeof(GLuint)));
                        VAOTetrahedraAlpha.release();
                        tetrahedron_index++;
                    }
                }
                
                
            }
        }
        //Alpha edges
        //shader_program->setUniformValue(color_loc, QVector3D(0.0f, 0.0f, 0.5f)); //dark blue
        shader_program->setUniformValue(color_loc, QVector3D(qRed(color_alpha_dark.rgb())/255., qGreen(color_alpha_dark.rgb())/255., qBlue(color_alpha_dark.rgb())/255.)); //dark blue
        shader_program->setUniformValue(shading_loc, false); 
        VAOEdgesAlpha.bind();
        if(full_bool){ //draw all
            glDrawElements(GL_LINES, getNumEdgesAlphaFull()*2, GL_UNSIGNED_INT, 0);
        }else{ //draw subset depending on filtration value
            glDrawElements(GL_LINES, getNumEdgesAlpha()*2, GL_UNSIGNED_INT, 0);
        }
        VAOEdgesAlpha.release(); //important that painter works!!
    } 
    //draw all points (inclusive hidden) in gray, will be overdrawn by red points of filtration
    shader_program->setUniformValue(pointsize_loc, (GLfloat)pointsize_input);
    shader_program->setUniformValue(color_loc, QVector3D(0.5f, 0.5f, 0.5f)); //gray
    shader_program->setUniformValue(shading_loc, false);
    VAOPointsAlpha.bind();
    glDrawElements(GL_POINTS, getNumPointsAlphaFull(), GL_UNSIGNED_INT, 0); 
    VAOPointsAlpha.release();
   }
   }
    shader_program->release();  
    glDisable(GL_DEPTH_TEST); //otherwise 2D content not drawn or disappears after update !!
    painter.endNativePainting(); //end opengl drawing
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    QFont font_label = QFont();
    font_label.setPixelSize(labelsize);
    painter.setFont(font_label);
    painter.setPen(Qt::darkGray);
    if(labels_intervals_bool){
        for(int i=0; i<labels_intervals_coords.size(); i++){
            if(!draw_bregman_on_transformed_points_bool){
                renderText(labels_intervals_coords[i].second,QString::number(labels_intervals_coords[i].first),&painter);
            }else{
                renderText(labels_intervals_coords_bregman_transformed[i].second,QString::number(labels_intervals_coords[i].first),&painter);
            }
        }
    }
    if(labels_points_bool){
        for(int i=0; i<data_points->size(); i++){
            if(!((data_points->at(i)).isDeleted())){
                //non-deleted point
                if(!draw_bregman_on_transformed_points_bool){
                    renderText(labels_points_coords[i],QString::number(i),&painter);
                }else{
                    renderText(labels_points_coords_bregman_transformed[i],QString::number(i),&painter);
                }
            }
        }
    }
    painter.end();
}

//change filtration value alpha, filtration indices given
void OpenGL_Widget::updateAlpha(int filtration_index_wrap_, int filtration_index_alpha_)
{
    if(filtration_index_alpha!=filtration_index_alpha_){
        filtration_index_wrap=filtration_index_wrap_;
        filtration_index_alpha=filtration_index_alpha_;
        update();
    }
}

//show/hide Wrap complex
void OpenGL_Widget::drawWrap(bool checked)
{
    wrap_bool = checked;
    update();
}

//show/hide Alpha complex
void OpenGL_Widget::drawAlpha(bool checked)
{
    alpha_bool = checked;
    update();
}

//draw full Wrap and Alpha complex
void OpenGL_Widget::drawFull(bool checked)
{
    full_bool = checked;
    update();
}

//show/hide tetrahedron labels = interval labels
void OpenGL_Widget::showLabelsIntervals(bool checked)
{
    labels_intervals_bool = checked;
    update();
}

//show/hide point labels
void OpenGL_Widget::showLabelsPoints(bool checked)
{
    labels_points_bool = checked;
    update();
}

//show/hide weighted points spheres
void OpenGL_Widget::showWeightedPointsSpheres(bool checked)
{
    weighted_points_spheres_bool = checked;
    update();
}

//whether to draw Bregman-Delaunay on transformed points or input points
void OpenGL_Widget::drawBregmanOnTransformedPoints(bool checked)
{
    if(checked != draw_bregman_on_transformed_points_bool){
        draw_bregman_on_transformed_points_bool = checked;
        //change data in vertex buffer (exchange input points with transformed points)
        vertexBuffer.bind();
        if(draw_bregman_on_transformed_points_bool){
            if(!periodic){
                vertexBuffer.write(0,vertex_data_bregman_transformed.data(), data_points->size() * 3 * sizeof(GLfloat)); 
            }else {
                vertexBuffer.write(0,vertex_data_bregman_transformed.data(), data_points->size() * 3 * 8 * sizeof(GLfloat)); 
            }
        }else{
            if(!periodic){
                vertexBuffer.write(0,vertex_data.data(), data_points->size() * 3 * sizeof(GLfloat)); 
            }else {
                vertexBuffer.write(0,vertex_data.data(), data_points->size() * 3 * 8 * sizeof(GLfloat)); 
            }
        }
        vertexBuffer.release();
        update();
    }
}

//update old point for point manipulation
void OpenGL_Widget::updateOldPoint(bool show, double x, double y, double z)
{
    show_oldpoint = show;
    if(show){
        std::vector<GLfloat> coords;
        coords.push_back((GLfloat) x);
        coords.push_back((GLfloat) y);
        coords.push_back((GLfloat) z);
        VAOOldPoint.bind();
        VBOOldPoint.bind();
        VBOOldPoint.allocate(coords.data(), 3*sizeof(GLfloat));
        VBOOldPoint.release();
        VAOOldPoint.release();
    }
    update();
}

//update new point for point manipulation
void OpenGL_Widget::updateNewPoint(bool show, double x, double y, double z)
{
    show_newpoint = show;
    if(show){
        std::vector<GLfloat> coords;
        coords.push_back((GLfloat) x);
        coords.push_back((GLfloat) y);
        coords.push_back((GLfloat) z);
        VAONewPoint.bind();
        VBONewPoint.bind();
        VBONewPoint.allocate(coords.data(), 3*sizeof(GLfloat));
        VBONewPoint.release();
        VAONewPoint.release();
    }
    update();
}

//highlight given simplices
void OpenGL_Widget::highlightSimplices(std::vector<int>* simplices_to_highlight1, std::vector<int>* simplices_to_highlight2, bool highlight_mode_, bool wrap, QColor color1, QColor color2, std::vector<DataPoint>* data_points, std::vector<Simplex>* simplices)
{
    clearHighlighted();
    
    highlight_mode = 1;
    if(!highlight_mode_){
        highlight_mode = 2;
    }
    
    highlight_color1 = color1;
    highlight_color2 = color2;
    
    num_points_highlighted1_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_edges_highlighted1_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_triangles_highlighted1_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_tetrahedra_highlighted1_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_points_highlighted2_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_edges_highlighted2_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_triangles_highlighted2_filtration = std::vector<int>(filtration_alpha->size(),0);
    num_tetrahedra_highlighted2_filtration = std::vector<int>(filtration_alpha->size(),0);
    
    //COLOR 1
    for(int i=0; i<simplices_to_highlight1->size(); i++){
        int element_index = (simplices_to_highlight1->at(i));
        int element_dim = (simplices->at(element_index)).getDim();
        Simplex simplex = simplices->at(element_index);
        double simplex_radius = CGAL::to_double(alpha_filtration_values->at(element_index));
        if(element_dim==0){ //point
            indicesPointsHighlighted1.push_back(simplex.getVertex(0));
        }else if(element_dim==1){ //edge
            if(!periodic){
                indicesEdgesHighlighted1.push_back(simplex.getVertex(0));
                indicesEdgesHighlighted1.push_back(simplex.getVertex(1));
            }else{
                indicesEdgesHighlighted1.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesEdgesHighlighted1.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
            }
        }else if(element_dim==2){ //triangle
            if(!periodic){
                indicesTrianglesHighlighted1.push_back(simplex.getVertex(0));
                indicesTrianglesHighlighted1.push_back(simplex.getVertex(1));
                indicesTrianglesHighlighted1.push_back(simplex.getVertex(2));
            }else{
                indicesTrianglesHighlighted1.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesTrianglesHighlighted1.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
                indicesTrianglesHighlighted1.push_back(simplex.getVertex(2)+simplex.getOffset(2).toInt()*data_points->size());
            }
        }else{ //tetrahedron
            std::vector<int> triangles_ind = simplex.getFacetIndices();
            if(triangles_ind.size()!=4){
                std::cerr << "ERROR (OpenGL_Widget::setPoints)" << std::endl;
            }
            for(int i=0; i<4; i++){
                Simplex triangle = simplices->at(triangles_ind[i]);
                if(!periodic){
                    indicesTetrahedraHighlighted1.push_back(triangle.getVertex(0));
                    indicesTetrahedraHighlighted1.push_back(triangle.getVertex(1));
                    indicesTetrahedraHighlighted1.push_back(triangle.getVertex(2));
                }else{
                    indicesTetrahedraHighlighted1.push_back(triangle.getVertex(0)+triangle.getOffset(0).toInt()*data_points->size());
                    indicesTetrahedraHighlighted1.push_back(triangle.getVertex(1)+triangle.getOffset(1).toInt()*data_points->size());
                    indicesTetrahedraHighlighted1.push_back(triangle.getVertex(2)+triangle.getOffset(2).toInt()*data_points->size());
                }
                //add tetrahedron triangle vertices in the same order as for individual triangles to avoid z-fighting
            }
        }
    }
    //COLOR 2
    for(int i=0; i<simplices_to_highlight2->size(); i++){
        int element_index = (simplices_to_highlight2->at(i));
        int element_dim = (simplices->at(element_index)).getDim();
        Simplex simplex = simplices->at(element_index);
        double simplex_radius = CGAL::to_double(alpha_filtration_values->at(element_index));
        if(element_dim==0){ //point
            indicesPointsHighlighted2.push_back(simplex.getVertex(0));
        }else if(element_dim==1){ //edge
            if(!periodic){
                indicesEdgesHighlighted2.push_back(simplex.getVertex(0));
                indicesEdgesHighlighted2.push_back(simplex.getVertex(1));
            }else{
                indicesEdgesHighlighted2.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesEdgesHighlighted2.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
            }
        }else if(element_dim==2){ //triangle
            if(!periodic){
                indicesTrianglesHighlighted2.push_back(simplex.getVertex(0));
                indicesTrianglesHighlighted2.push_back(simplex.getVertex(1));
                indicesTrianglesHighlighted2.push_back(simplex.getVertex(2));
            }else{
                indicesTrianglesHighlighted2.push_back(simplex.getVertex(0)+simplex.getOffset(0).toInt()*data_points->size());
                indicesTrianglesHighlighted2.push_back(simplex.getVertex(1)+simplex.getOffset(1).toInt()*data_points->size());
                indicesTrianglesHighlighted2.push_back(simplex.getVertex(2)+simplex.getOffset(2).toInt()*data_points->size());
            }
        }else{ //tetrahedron
            std::vector<int> triangles_ind = simplex.getFacetIndices();
            if(triangles_ind.size()!=4){
                std::cerr << "ERROR (OpenGL_Widget::setPoints)" << std::endl;
            }
            for(int i=0; i<4; i++){
                Simplex triangle = simplices->at(triangles_ind[i]);
                if(!periodic){
                    indicesTetrahedraHighlighted2.push_back(triangle.getVertex(0));
                    indicesTetrahedraHighlighted2.push_back(triangle.getVertex(1));
                    indicesTetrahedraHighlighted2.push_back(triangle.getVertex(2));
                }else{
                    indicesTetrahedraHighlighted2.push_back(triangle.getVertex(0)+triangle.getOffset(0).toInt()*data_points->size());
                    indicesTetrahedraHighlighted2.push_back(triangle.getVertex(1)+triangle.getOffset(1).toInt()*data_points->size());
                    indicesTetrahedraHighlighted2.push_back(triangle.getVertex(2)+triangle.getOffset(2).toInt()*data_points->size());
                }
                //add tetrahedron triangle vertices in the same order as for individual triangles to avoid z-fighting
            }
        }
    }
    //go through filtration, check which of the simplices is in set of highlighted ones
    for(int i=0; i<filtration_alpha->size(); i++){
        std::vector<int>::iterator it = std::find(simplices_to_highlight1->begin(),simplices_to_highlight1->end(),filtration_alpha->getElementIndex(i));
        if(it!=simplices_to_highlight1->end()){
            //this simplices is in set of highlighted simplices
            int dim = (simplices->at(*it)).getDim();
            if(dim==0){
                num_points_highlighted1_filtration[i]=1;
            }else if(dim==1){
                num_edges_highlighted1_filtration[i]=1;
            }else if(dim==2){
                num_triangles_highlighted1_filtration[i]=1;
            }else if(dim==3){
                num_tetrahedra_highlighted1_filtration[i]=1;
            }
        }else{
            std::vector<int>::iterator it = std::find(simplices_to_highlight2->begin(),simplices_to_highlight2->end(),filtration_alpha->getElementIndex(i));
            if(it!=simplices_to_highlight2->end()){
                //this simplices is in set of highlighted simplices
                int dim = (simplices->at(*it)).getDim();
                if(dim==0){
                    num_points_highlighted2_filtration[i]=1;
                }else if(dim==1){
                    num_edges_highlighted2_filtration[i]=1;
                }else if(dim==2){
                    num_triangles_highlighted2_filtration[i]=1;
                }else if(dim==3){
                    num_tetrahedra_highlighted2_filtration[i]=1;
                }
            }
        }
    }
    //get cumulative numbers
    for(int i=1; i<filtration_alpha->size(); i++){
        num_points_highlighted1_filtration[i]=num_points_highlighted1_filtration[i]+num_points_highlighted1_filtration[i-1];
        num_edges_highlighted1_filtration[i]=num_edges_highlighted1_filtration[i]+num_edges_highlighted1_filtration[i-1];
        num_triangles_highlighted1_filtration[i]=num_triangles_highlighted1_filtration[i]+num_triangles_highlighted1_filtration[i-1];
        num_tetrahedra_highlighted1_filtration[i]=num_tetrahedra_highlighted1_filtration[i]+num_tetrahedra_highlighted1_filtration[i-1];
        num_points_highlighted2_filtration[i]=num_points_highlighted2_filtration[i]+num_points_highlighted2_filtration[i-1];
        num_edges_highlighted2_filtration[i]=num_edges_highlighted2_filtration[i]+num_edges_highlighted2_filtration[i-1];
        num_triangles_highlighted2_filtration[i]=num_triangles_highlighted2_filtration[i]+num_triangles_highlighted2_filtration[i-1];
        num_tetrahedra_highlighted2_filtration[i]=num_tetrahedra_highlighted2_filtration[i]+num_tetrahedra_highlighted2_filtration[i-1];
    }
    
    
    vertexBuffer.bind();
    //COLOR 1
    VAOPointsHighlighted1.bind(); vertexBuffer.bind(); indexBufferPointsHighlighted1.bind();
    indexBufferPointsHighlighted1.allocate(indicesPointsHighlighted1.data(), indicesPointsHighlighted1.size() * sizeof(unsigned int));
    VAOEdgesHighlighted1.bind(); vertexBuffer.bind(); indexBufferEdgesHighlighted1.bind();
    indexBufferEdgesHighlighted1.allocate(indicesEdgesHighlighted1.data(), indicesEdgesHighlighted1.size() * sizeof(unsigned int));
    VAOTrianglesHighlighted1.bind(); vertexBuffer.bind(); indexBufferTrianglesHighlighted1.bind();
    indexBufferTrianglesHighlighted1.allocate(indicesTrianglesHighlighted1.data(), indicesTrianglesHighlighted1.size() * sizeof(unsigned int)); 
    VAOTetrahedraHighlighted1.bind(); vertexBuffer.bind(); indexBufferTetrahedraHighlighted1.bind();
    indexBufferTetrahedraHighlighted1.allocate(indicesTetrahedraHighlighted1.data(), indicesTetrahedraHighlighted1.size() * sizeof(unsigned int)); 
    indexBufferTetrahedraHighlighted1.release();
    //COLOR 2
    VAOPointsHighlighted2.bind(); vertexBuffer.bind(); indexBufferPointsHighlighted2.bind();
    indexBufferPointsHighlighted2.allocate(indicesPointsHighlighted2.data(), indicesPointsHighlighted2.size() * sizeof(unsigned int));
    VAOEdgesHighlighted2.bind(); vertexBuffer.bind(); indexBufferEdgesHighlighted2.bind();
    indexBufferEdgesHighlighted2.allocate(indicesEdgesHighlighted2.data(), indicesEdgesHighlighted2.size() * sizeof(unsigned int));
    VAOTrianglesHighlighted2.bind(); vertexBuffer.bind(); indexBufferTrianglesHighlighted2.bind();
    indexBufferTrianglesHighlighted2.allocate(indicesTrianglesHighlighted2.data(), indicesTrianglesHighlighted2.size() * sizeof(unsigned int)); 
    VAOTetrahedraHighlighted2.bind(); vertexBuffer.bind(); indexBufferTetrahedraHighlighted2.bind();
    indexBufferTetrahedraHighlighted2.allocate(indicesTetrahedraHighlighted2.data(), indicesTetrahedraHighlighted2.size() * sizeof(unsigned int)); 
    indexBufferTetrahedraHighlighted2.release();
    vertexBuffer.release();
    
    update();
}

//delete all highlighted simplices
void OpenGL_Widget::clearHighlighted()
{
    highlight_mode = 0;
    indicesPointsHighlighted1.clear();
    indicesEdgesHighlighted1.clear();
    indicesTrianglesHighlighted1.clear();
    indicesTetrahedraHighlighted1.clear();
    indicesPointsHighlighted2.clear();
    indicesEdgesHighlighted2.clear();
    indicesTrianglesHighlighted2.clear();
    indicesTetrahedraHighlighted2.clear();
    
    update();
}

//update after resizing the widget
void OpenGL_Widget::resizeGL(int width, int height)
{
    //modify projection matrix to new screen size
    projMatrix.setToIdentity();
    projMatrix.perspective(45.0f, GLfloat(width) / height, 0.01f, 100.0f);
}

//when pressing a mouse button update its last position (necessary for mouseMoveEvent)
void OpenGL_Widget::mousePressEvent(QMouseEvent *event)
{
    last_mouse_pos = event->pos();
}

//when moving the pressed mouse, rotate the scene, left button: z-axis, right button: y-axis
void OpenGL_Widget::mouseMoveEvent(QMouseEvent *event)
{
    //how far did we move the mouse in x/y-direction after pressing a button
    int dx = event->x() - last_mouse_pos.x();
    int dy = event->y() - last_mouse_pos.y();

    //ctrl-key was not pressed -> object rotation
    if(!(QApplication::keyboardModifiers().testFlag(Qt::ControlModifier))){
        //press left button: rotate around z-axis
        if (event->buttons() & Qt::LeftButton) {
            setXRotation(rotate_x + dy);
            setYRotation(rotate_y + dx);
        //press right button: rotate around y-axis
        } else if (event->buttons() & Qt::RightButton) {
            setXRotation(rotate_x + dy);
            setZRotation(rotate_z + dx);
        }
    //ctrl-key was pressed -> light source rotation
    }else{
        //press left button: rotate around z-axis
        if (event->buttons() & Qt::LeftButton) {
            setXRotationLight(rotate_x_light + dy);
            setYRotationLight(rotate_y_light + dx);
        //press right button: rotate around y-axis
        } else if (event->buttons() & Qt::RightButton) {
            setXRotationLight(rotate_x_light + dy);
            setZRotationLight(rotate_z_light + dx);
        }
    }
    //update mouse position
    last_mouse_pos = event->pos();
}

//zoom in and out with mouse wheel
void OpenGL_Widget::wheelEvent(QWheelEvent *event)
{
    //depending on direction of wheel movement zoom in or out
    event->delta() > 0 ? scale += scale*0.1f : scale -= scale*0.1f;
    
    //update scene
    update(); 
}

//move camera with arrow keys
void OpenGL_Widget::keyPressEvent(QKeyEvent *keyEvent)
{
    if(keyEvent->key()==Qt::Key_Up){
        translate_y = translate_y+1;
    }else if(keyEvent->key()==Qt::Key_Down){
        translate_y = translate_y-1;
    }else if(keyEvent->key()==Qt::Key_Left){
        translate_x = translate_x-1;
    }
    else if(keyEvent->key()==Qt::Key_Right){
        translate_x = translate_x+1;
    }
    update();
}

//set rotation angle for x-direction
void OpenGL_Widget::setXRotation(int angle)
{
    angle = angle%360; //normalize angle (bring into interval [0,360) )  
    if (angle != rotate_x) {
        rotate_x = angle;
        update();
    }
}

//set rotation angle for y-direction
void OpenGL_Widget::setYRotation(int angle)
{
    angle = angle%360; //normalize angle (bring into interval [0,360) )  
    if (angle != rotate_y) {
        rotate_y = angle;
        update();
    }
}

//set rotation angle for z-direction
void OpenGL_Widget::setZRotation(int angle)
{
    angle = angle%360; //normalize angle (bring into interval [0,360) )  
    if (angle != rotate_z) {
        rotate_z = angle;
        update();
    }
}

//set rotation angle for x-direction for light source
void OpenGL_Widget::setXRotationLight(int angle)
{
    angle = angle%360; //normalize angle (bring into interval [0,360) )  
    if (angle != rotate_x_light) {
        rotate_x_light = angle;
        update();
    }
}

//set rotation angle for y-direction for light source
void OpenGL_Widget::setYRotationLight(int angle)
{
    angle = angle%360; //normalize angle (bring into interval [0,360) )  
    if (angle != rotate_y_light) {
        rotate_y_light = angle;
        update();
    }
}

//set rotation angle for z-direction for light source
void OpenGL_Widget::setZRotationLight(int angle)
{
    angle = angle%360; //normalize angle (bring into interval [0,360) )  
    if (angle != rotate_z_light) {
        rotate_z_light = angle;
        update();
    }
}

//draw 2d text at given projected 3d location
void OpenGL_Widget::renderText(QVector3D world_coords, QString text, QPainter* painter)
{     
    QVector4D world_coords_hom = QVector4D(world_coords,1.0); //homogeneous coordinates
    QVector4D projected_coords_hom = projMatrix*viewMatrix*modelMatrix*world_coords_hom; //project
    if(projected_coords_hom.w()==0){
        std::cerr << "ERROR (OpenGL_Widget::renderText)" << std::endl;
    }
    QVector3D projected_coords = projected_coords_hom.toVector3DAffine(); //divide by w coordinate to get 3D vector
    //viewport transformation
    double screen_x = (1+projected_coords.x())*this->width()/2;
    double screen_y = this->height()-((1+projected_coords.y())*this->height()/2);    
    painter->drawText(screen_x,screen_y,text);   
}

//redraw with new parameters
void OpenGL_Widget::setDrawingParameters(double pointsize_, double pointsize_input_, double linewidth_, double labelsize_, bool draw)
{
    pointsize=pointsize_;
    pointsize_input=pointsize_input_;
    linewidth=linewidth_;
    labelsize=labelsize_;
    
    if(draw){
        update();
    }
}

///fit scale of the scene to bounding sphere of points, reset translation and rotation
void OpenGL_Widget::fitView()
{
    if(!draw_bregman_on_transformed_points_bool){
        if(max_coord>0){
            scale = 1./(4*max_coord);
        }else{
            scale = 1.;
        }
    }else{
        if(max_coord_bregman_transformed>0){
            scale = 1./(4*max_coord_bregman_transformed);
        }else{
            scale = 1.;
        }
    }
    
    rotate_x = 0; rotate_y = 0; rotate_z = 0;
    translate_x = 0; translate_y = 0; 
    
    update();
}

//set default colors for drawing
void OpenGL_Widget::setDefaultColors()
{
    color_points = Qt::red;
    color_alpha_dark = QColor(0,51,178.5);
    color_alpha_light = QColor(89.25,127.5,255);
    color_alpha_pale = QColor(153,183.6,255);
    color_wrap_dark = QColor(0,127.5,0);
    color_wrap_light = QColor(25.5,255,25.5);
    color_wrap_pale = QColor(153,255,153);
    //color_wrap_light = QColor(150,255,150); //light green
    color_old_point = QColor(255,180,0); //orange
    color_new_point = Qt::black;
    color_weighted_points = Qt::red;
}