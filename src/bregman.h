//bregman.h
//author: koelsboe

#ifndef BREGMAN_H
#define BREGMAN_H

#include "alpha_complex.h"

//Bregman-Delaunay mosaic, Bregman-Alpha complex, Bregman-Wrap complex
class Bregman : public Alpha_Complex
{      
    private:
        std::string type; //"halfEuclidean", "Shannon", ...
        std::vector<DataPoint> bregman_points; // points for which weighted Delaunay mosaic is the Bregman-Delaunay mosaic for given points
        std::vector<DataPoint>* input_points;
        bool primal; //grow primal or dual balls for Bregman-Delaunay
        
    public: 
        Bregman(double alpha2, std::string type_):Alpha_Complex(alpha2), type(type_){} //initialize, set type (function F)
        
        void clearPoints(); //clear list of points
        
        std::vector<DataPoint>* getBregmanPoints(){return &bregman_points;}
        void setType(std::string type_){type=type_;}
        void setPrimal(bool primal_){primal=primal_;}
        
        bool computeBregmanDelaunay(std::vector<DataPoint> *new_points, bool weighted_euclidean_radii, int num_data_points_, int periodic_size, bool dim3, bool compute_persistence, bool printstats); //compute Bregman-Delaunay triangulation
        bool computeWithFisherMetric(std::vector<DataPoint> *new_points, int num_data_points_, int periodic_size, bool dim3, bool compute_persistence, bool printstats); //compute Delaunay triangulation in Fisher geometry
        
        void computeCircumcentersMaxSimplicesBregman(bool restrict_to_domain, bool print); //compute circumcenters of maximum simplices in Bregman-Delaunay triangulation, either restrict to domain (delete all simplices with circumcenter outside of domain (positive quadrant/orthant or standard simplex)) or print as output
        
    private:
        exact bregF(DataPoint x, bool conjugate); //Legendre type function determined by type
        std::vector<exact> bregGradF(DataPoint x, bool conjugate); //gradient of F
        exact bregDiv(DataPoint x, DataPoint y); //bregman divergence of x and y
        std::pair<double,vec> compute_circumradius_bregman(Simplex* simplex, bool conjugate); //compute radius of circumsphere (not caring about emptiness)
        bool isNeighborInside(std::pair<double,vec> circumsphere_r_c, Simplex* simplex, bool conjugate); //check whether any neighboring vertex is inside circumsphere
        std::pair<exact,int> getSmallestCofacetValue(std::vector<exact>* numeric_filtration_values, Simplex* simplex); //compute value and index of smallest cofacet
        double sphericalDistance2(CPoint3 x, CPoint3 y); //compute geodesic distance between points lying on sphere of radius sqrt(2)
       
        void computeBregmanPoints(std::vector<DataPoint> *new_points); //compute weighted Euclidean points corresponding to Bregman geometry
};     

#endif // ALPHA_COMPLEX_H