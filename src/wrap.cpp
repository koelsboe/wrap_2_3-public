//wrap.cpp
//author: koelsboe

#include "basics.h"
#include "wrap.h"
#include "alpha_complex.h"

//clear complex
void Wrap::clear()
{
    clear0();
    
    intervals.clear();
    interval_indices.clear();
    free_indices_intervals.clear();
    
    descendants_computed = false;
    
    num_crit_simplices_neg_filtration.clear();
    num_crit_simplices_pos_filtration.clear();
}

//compute Wrap complex with information provided by Alpha complex (Delaunay radii, criticality, what is in same interval)
Wrap::Wrap(std::vector<exact>* filtration_values_alpha_, std::vector<bool>* critical_, std::vector<std::vector<int> >* interval_faces_, bool dim3_, bool weighted_, int periodic_size_, int num_data_points_, std::vector<DataPoint>* data_points_, std::vector<Simplex>* simplices_, bool printstats)
{ 
    //initialize
    //----------------------------------------------
    
    clear();
    
    name="Wrap";name_uppercase ="WRAP";
    
    filtration_values_alpha = filtration_values_alpha_;
    critical_ptr = critical_;
    interval_faces = interval_faces_;
    dim3=dim3_;
    weighted=weighted_;
    periodic_size=periodic_size_;
    num_data_points=num_data_points_;
    data_points = data_points_;
    simplices_ptr = simplices_;
    
    descendants_computed=false;
    exhaustive_persistence=true;
    persistence_computed = false;
    
    in_complex = std::vector<bool>(simplices_ptr->size(),false);
    filtration_values = std::vector<exact>(simplices_ptr->size(),-1);
    interval_indices = std::vector<int>(simplices_ptr->size(),-1);
    excluded = std::vector<bool>(simplices_ptr->size(),false);
    included = std::vector<bool>(simplices_ptr->size(),false);
    current_alpha2 = -1;
    
    double time1;
    if(printstats){
        std::cout << "COMPUTE WRAP COMPLEX" << std::endl;
        time1 = getCPUTime();
    }
    
    //compute intervals
    //----------------------------------------------
    
    //create intervals for simplices of decreasing dimension
    // (lower dimensional simplices will be added to existing intervals)
    int maxdim = 2;
    if(dim3){
        maxdim = 3;
    }
    for(int d=maxdim; d>=0; d--){
        for(int i=0; i<simplices_ptr->size(); i++){
            if(simplices_ptr->at(i).getDim()==d){
                createInterval(&simplices_ptr->at(i));
            }
        }
    }
    
    //compute predecessors for intervals
    for(int i=0; i<intervals.size(); i++){
        computeIntervalPredecessors(i);
    }
    
    //compute filtration values for Wrap complex
    //--------------------------------------------------
    int count_updates = computeFiltrationValuesWrap(true);
    if(printstats){
        std::cout << " # calls of alpha updates, " << count_updates;
    }
    
    //compute filtration: simplices sorted by wrap alpha value
    //---------------------------------------------------
    computeFiltration(false);
    
    
    if(printstats){
        std::cout << " Time, " << getCPUTime()-time1 << std::endl;
    }
    
    //TEST 
    //printIntervals();
    //std::cout << "Wrap" <<std::endl;
    //filtration.print(simplices_ptr); 
}

//create interval of given simplex, start with higher-dim simplices, lower-dim simplices and references to predecessors are taken care of later, return index and list of deleted intervals (now in same interval as this one)
std::pair<int,std::vector<int> > Wrap::createInterval(Simplex* simplex)
{
    int interval_index = -1;
    std::vector<int> deleted_intervals;
    if(!simplex->isDeleted()){
        //we assume that simplices that are in interval with coface were already taken care of
        //so we are left with simplices which are maximal in their interval (critical or not)
        std::vector<int> interval_faces_this = interval_faces->at(simplex->getIndex());
        if(critical_ptr->at(simplex->getIndex()) || interval_faces_this.size()>0){
            //determine index: new interval either at end of list or at free position
            interval_index = intervals.size(); 
            if(free_indices_intervals.size()>0){
                interval_index = free_indices_intervals.back();
                free_indices_intervals.pop_back();
            }
            //create interval
            Interval interval(simplex,interval_index); 
            interval_indices[simplex->getIndex()] = interval_index; //store index

            //if simplex is non-critical, add interval faces
            if(!critical_ptr->at(simplex->getIndex())){
                for(int i=0; i<interval_faces_this.size(); i++){
                    int face_simplex_index = interval_faces_this[i];
                    //if simplex had interval assigned, delete that interval
                    //(happens for critical boundary simplices that become non-critical after point manipulation)
                    int old_interval_index = interval_indices[face_simplex_index];
                    if(old_interval_index>=0 && !intervals[old_interval_index].isDeleted()){
                        //delete simplices in old interval from filtration
                        std::vector<int> old_interval_simplices = intervals[old_interval_index].getSimplices();
                        for(int i=0; i<old_interval_simplices.size(); i++){
                            filtration_current.deleteSimplex(old_interval_simplices[i],getAlpha2Simplex(old_interval_simplices[i]),simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
                        }
                        //delete old interval
                        intervals[old_interval_index]=Interval(); //set empty interval
                        deleted_intervals.push_back(old_interval_index);
                    }
                    //add face simplex to interval
                    interval.addSimplex(face_simplex_index);
                    interval_indices[face_simplex_index] = interval_index;
                }
            }

            //save interval
            if(interval_index == intervals.size()){
                intervals.push_back(interval);
            }else{
                intervals[interval_index]=interval;
            }
        }
    }
    return std::make_pair(interval_index,deleted_intervals);
}

//compute predecessors for given interval
void Wrap::computeIntervalPredecessors(int interval_index)
{
    //check if index in correct range
    if(interval_index<0 || interval_index>=intervals.size()){
        return;
    }
    //check if interval not deleted
    Interval* interval = &(intervals[interval_index]);
    if(interval->isDeleted()){
        return;
    }
    
    Simplex max_simplex = simplices_ptr->at(interval->getMaxSimplexIndex());
    
    //add intervals of facets of maximal simplex as predecessors (lower dimensional faces will be indirect predecessors)
    std::vector<int> facet_indices = max_simplex.getFacetIndices();
    for(int i=0; i<facet_indices.size(); i++){
        //add interval of facet as predecessor if it is not the same interval anyway
        if(interval_indices[facet_indices[i]]!=interval_index && interval_indices[facet_indices[i]]>=0){ //empty simplex has no interval assigned
            interval->addPredecessor(interval_indices[facet_indices[i]]);
        }
    }
}

//add given interval as predecessor to its descendants
void Wrap::addIntervalAsPredecessor(int interval_index)
{
    //check if interval index in correct range
    if(interval_index<0 || interval_index>=intervals.size()){
        return;
    }
    //get direct descendant intervals (intervals of cofacets of simplices of this interval)
    std::set<int> descendant_intervals = intervals[interval_index].getDescendantIntervals(interval_index, &intervals, &interval_indices, simplices_ptr);
    //add as predecessor to direct descendants
    for(std::set<int>::iterator it = descendant_intervals.begin(); it != descendant_intervals.end(); ++it){
        intervals[*it].addPredecessor(interval_index);
    }
}

//compute filtration values for Wrap simplices, optionally also compute critical descendants
int Wrap::computeFiltrationValuesWrap(bool compute_descendants)
{
    descendants_computed = compute_descendants;
    
    int count = 0; //count number of updates of filtration values
    
    //initialize all filtration values, delete critical descendants for intervals (reset from previous computations)
    for(int i=0; i<simplices_ptr->size(); i++){
        in_complex[i]=false;
        filtration_values[i]=false;
    }    
    for(int i=0; i<intervals.size(); i++){
        intervals[i].clearDescendants();
    }
    
    //put empty simplex into complex, there was no interval created
    in_complex[0]=true;
    filtration_values[0]=-1;
    
    double time1 = getCPUTime();
    
    //iterate over all intervals
    //if singular and not excluded, set alpha2 for this node and update alpha2 in lower set recursively, store in which lower sets an interval is contained
    for(int i=0; i<intervals.size(); i++){
        Interval interval = intervals[i]; 
        if(!interval.isDeleted()){
            if(interval.isSingular()){
                count ++;

                //critical simplex has same filtration value as in Delaunay complex
                exact crit_alpha2 = filtration_values_alpha->at(interval.getMaxSimplexIndex());
                filtration_values[interval.getMaxSimplexIndex()]=crit_alpha2;
                in_complex[interval.getMaxSimplexIndex()]=true;

                //all non-singular intervals in lower set have filtration value at most as high as this one, update recursively
                std::set<int> predecessors = interval.getPredecessors();
                for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end(); ++it){
                    count += setWrapAlpha2Recursively(i,(*it),crit_alpha2,descendants_computed,false,true,0);
                }
            }
        }
    }

    //std::cout << "Elapsed time (compute Wrap radii): " << getCPUTime()-time1 << std::endl;

    return count;
}

//update filtration value for given interval and its lower set recursively, return number of recursive calls
int Wrap::setWrapAlpha2Recursively(int root_interval_index, int current_interval_index, exact alpha2, bool compute_descendants, bool update, bool stop_when_already_found, int count)
{  
    Interval current_interval = intervals[current_interval_index];
    std::vector<int> interval_simplices = intervals[current_interval_index].getSimplices();
    //for local update after point manipulation we need to check if simplex was in filtration with different value before, store old values
    std::vector<int> old_in_complex; std::vector<exact> old_filtration_values;
    for(int i=0; i<interval_simplices.size(); i++){
        old_in_complex.push_back(in_complex[interval_simplices[i]]);
        old_filtration_values.push_back(filtration_values[interval_simplices[i]]);
    }
    //add critical interval to set of descendants
    std::pair<bool,bool> out_added = intervals[current_interval_index].addCriticalDescendant(root_interval_index, alpha2, &in_complex, &filtration_values);
    bool already_added = out_added.first;
    bool new_first = out_added.second;
    
    //only need to recurse for non-singular intervals (critical simplex which is face of other critical simplex necessarily has lower alpha^2 value, lower set is nested)       
    if((!already_added||!stop_when_already_found) && !current_interval.isSingular()){
        count ++;
        
        //update wrap filtration value for this interval after point manipulation
        //(for full computation, filtration is computed afterwards)
        if(update && new_first){
            //insert simplices of interval with new alpha value in filtration
            //remove with old value if inserted before
            for(int i=0; i<interval_simplices.size(); i++){
                if(old_in_complex[i]){
                    filtration_current.deleteSimplex(interval_simplices[i],old_filtration_values[i],simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
                }
                in_complex[interval_simplices[i]]=true;
                filtration_current.insertElement(alpha2,interval_simplices[i],simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
            }
        }

        //call function recursively on predecessors of this interval
        //if value was improved or if we want to compute all critical descendants
        if(new_first || compute_descendants){      
            std::set<int> predecessors = current_interval.getPredecessors();
            for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end(); ++it){
                count = setWrapAlpha2Recursively(root_interval_index,(*it),alpha2,compute_descendants,update,true,count);
            }
        }
    }
    return count;
}

//compute filtration: simplices sorted by wrap filtration value
void Wrap::computeFiltration(bool holeOperation) 
{
    filtration_current.clear();
    for(int i=0; i<simplices_ptr->size(); i++){
        if(isInComplex(i)){
            filtration_current.appendElement(getAlpha2Simplex(i),i);
        }
    }
    filtration_current.sort(simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
    setAlpha2(current_alpha2); //compute index of current subcomplex
    
    if(!holeOperation){
        filtration_original = filtration_current;
    }
}

//store unadapted filtration and critical descendants before any hole operations
void Wrap::storeUnadapted()
{
    //store unadapted filtration
    filtration_original = filtration_current;
    
    //compute critical descendants if this was not done yet (with another run of wrap filtration values computation)
    if(!descendants_computed){
        computeFiltrationValuesWrap(true);
    }
}

//lock (fill) cycle for full complex by including canonical cycle (chain) of the birth (death) simplex, wrap adaptation: include first critical descendants of these simplices and corresponding lower sets
void Wrap::lockOrFillCycleFullComplex(bool lock, PersistencePair persistence_pair, exact inclusion_alpha2, bool printinfo, bool printstats)
{
    int filtration_index_of_main_simplex;
    if(lock){
        filtration_index_of_main_simplex = persistence_pair.getBirthIndexFiltration();
    }else{
        filtration_index_of_main_simplex = persistence_pair.getDeathIndexFiltration();
    }
    if(printinfo){
        if(lock){
            std::cout << " birth simplex " << filtration_index_of_main_simplex << std::endl;
        }else{
            std::cout << " death simplex " << filtration_index_of_main_simplex << std::endl;
        }
        Simplex* main_simplex = filtration_original.getSimplex(filtration_index_of_main_simplex,simplices_ptr);
        main_simplex->print();
    }
    
    std::set<int> nonsingular_intervals_to_include;
    
    //get canonical cycle (chain) that should be included
    std::vector<int> chain = persistence.getChain(filtration_index_of_main_simplex);
    std::string chain_name = "chain";
    if(lock){
        chain_name = "cycle";
    }
    if(printstats || printinfo){
        std::cout << " # " << chain_name << " simplices included, " << chain.size() << std::endl;
    }
    if(printinfo){
       std::cout << chain_name << ": "  << std::endl;
    }
    int count_critical_chain_intervals_included = 0;
    for(int i=0; i<chain.size(); i++){
        if(printinfo){
            std::cout << chain[i] << " ";
        }
        int included_filtration_index = chain[i];
        int included_simplex_index = filtration_original.getElementIndex(included_filtration_index);
        int included_interval_index = interval_indices[included_simplex_index];
        //if critical simplex include immediately, also its full lower set
        if(intervals[included_interval_index].isSingular()){
            bool incl = setIncludedLowerSetRecursively(included_interval_index,inclusion_alpha2,printinfo);      
            if(incl){
                count_critical_chain_intervals_included++;
            }
        //nonsingular intervals are handled later
        }else{
            nonsingular_intervals_to_include.insert(included_interval_index);
        }
        
        //faces of included simplices are in their lower sets, do not need to be included extra
    }
    if(printinfo){
        std::cout << std::endl;
        std::cout << "include non-critical" << std::endl;
    }
    int count_critical_descendant_intervals_included = 0;
    
    //include nonsingular intervals which have not been included together with a singular interval yet by including its first critical descendant
    for(std::set<int>::iterator it = nonsingular_intervals_to_include.begin(); it != nonsingular_intervals_to_include.end(); ++it){
        Interval* interval = &intervals[(*it)];
        Simplex* max_simplex = interval->getMaxSimplex(simplices_ptr);
        //check if interval (its maximum simplex) was included already or is already earlier in filtration
        if(!included[max_simplex->getIndex()]||getIncludedWithAlpha2(max_simplex->getIndex())>inclusion_alpha2){
            //include first critical descendant of this nonsingular interval, with lower set
            int descendant = intervals[(*it)].getFirstCriticalDescendant();
            if(printinfo){
                std::cout << (*it);
                std::cout << "p" << descendant;
            }
            bool incl = setIncludedLowerSetRecursively(descendant,inclusion_alpha2,printinfo);      
            if(incl){
                count_critical_descendant_intervals_included++;
            }
            if(printinfo){
                std::cout<< " " << std::endl;
            }
        }
    }
    
    if(printstats){
        std::cout << " # critical " << chain_name << " intervals included, " << count_critical_chain_intervals_included << std::endl;
        std::cout << " # critical descendant intervals included, " << count_critical_descendant_intervals_included << std::endl;
    }
}

//unlock (unfill) cycle for full complex by excluding canonical cochain (cocycle) of birth simplex (death), wrap adaptation: exclude critical intervals in upper sets, keep wrap structure
void Wrap::unlockOrUnfillCycleFullComplex(bool lock, PersistencePair persistence_pair, bool printinfo, bool printstats)
{
    int filtration_index_of_main_simplex;
    if(lock){
        filtration_index_of_main_simplex = persistence_pair.getBirthIndexFiltration();
    }else{
        filtration_index_of_main_simplex = persistence_pair.getDeathIndexFiltration();
    }
    if(printinfo){
        if(lock){
            std::cout << " birth simplex " << filtration_index_of_main_simplex << std::endl;
        }else{
            std::cout << " death simplex " << filtration_index_of_main_simplex << std::endl;
        }
        Simplex* main_simplex = filtration_original.getSimplex(filtration_index_of_main_simplex,simplices_ptr);
        main_simplex->print();
    }
    
    std::set<int> nonsingular_intervals_to_exclude;
    
    //get canonical cochain (cocycle) that should be included
    std::vector<int> cochain = persistence.getCocycle(filtration_index_of_main_simplex);
    std::string chain_name = "cochain";
    if(!lock){
        chain_name = "cocycle";
    }
    if(printstats){
        std::cout << " # " << chain_name << " simplices excluded, " << cochain.size() << std::endl;
    }
    if(printinfo){
       std::cout << chain_name << ": "  << std::endl;
    }
    
    //exclude critical intervals in cochain
    int count_critical_cochain_intervals_excluded = 0;
    std::set<int> excluded_intervals_singular; ///set of excluded intervals of cochain, later take care of their upper sets
    for(int j=0; j<cochain.size(); j++){
        int excluded_filtration_index =  cochain[j];
        int excluded_simplex_index = filtration_original.getElementIndex(excluded_filtration_index);
        int excluded_interval_index = interval_indices[excluded_simplex_index];
        if(printinfo){
            std::cout << cochain[j] << " "; 
        }
        //exclude singular intervals
        if(intervals[excluded_interval_index].isSingular()){
            excluded[excluded_simplex_index]=true;
            excluded_intervals_singular.insert(excluded_interval_index);
            count_critical_cochain_intervals_excluded++;
        }else{
            //nonsingular intervals are handled later
            nonsingular_intervals_to_exclude.insert(excluded_interval_index);
        }
    }
    
    int count_critical_descendant_intervals_excluded = 0;
    
    //exclude critical descendants of nonsingular intervals in cochain
    for(std::set<int>::iterator it = nonsingular_intervals_to_exclude.begin(); it != nonsingular_intervals_to_exclude.end(); ++it){
        int excluded_interval_index = (*it);
        std::vector<int> descendants = intervals[excluded_interval_index].getCriticalDescendants();
        int count_current_descendants_excluded = 0;
        for(int j=0; j<descendants.size(); j++){
            if(!excluded[intervals[descendants[j]].getMaxSimplexIndex()]){
                count_current_descendants_excluded++;
                excluded[intervals[descendants[j]].getMaxSimplexIndex()]=true;
                excluded_intervals_singular.insert(descendants[j]);
                if(printinfo){
                    std::cout << "    " << descendants[j] << " ";
                }
            }
        }
        if(count_current_descendants_excluded>0){
            count_critical_descendant_intervals_excluded += count_current_descendants_excluded;
        }
    }
    
    if(printinfo){
        std::cout << std::endl;
        std::cout << "upper set excluded:" << std::endl;
    }
    //exclude all critical simplices which are in upper set of any of the excluded intervals
    //get all singular intervals in upper set of any excluded interval
    std::set<int> excluded_intervals_singular_upperset;
    for(std::set<int>::iterator it = excluded_intervals_singular.begin(); it != excluded_intervals_singular.end(); ++it){
        std::set<int> upperset = intervals[*it].getSingularUpperSetRecursively(&intervals);
        excluded_intervals_singular_upperset.insert(upperset.begin(),upperset.end());
    }
    //exclude upper set critical intervals
    int count_critical_upperset_intervals_excluded = 0;
    for(std::set<int>::iterator it = excluded_intervals_singular_upperset.begin(); it != excluded_intervals_singular_upperset.end(); ++it){
        if(!excluded[intervals[*it].getMaxSimplexIndex()]){
            excluded[intervals[*it].getMaxSimplexIndex()]=true;
            count_critical_upperset_intervals_excluded++;
        }
    }
    //update lower sets of excluded critical intervals
    int num_noncritical_intervals_adapted = adaptNonCritialIntervalsAfterExcludingCritical();
    
    if(printstats){
        std::cout << " # critical cochain intervals excluded, " << count_critical_cochain_intervals_excluded << std::endl;
        std::cout << " # critical descendant intervals excluded, " << count_critical_descendant_intervals_excluded << std::endl;
        std::cout << " # critical upperset intervals excluded, " << count_critical_upperset_intervals_excluded << std::endl;
        std::cout << " # non-critical intervals adapted, " << num_noncritical_intervals_adapted << std::endl;
    }
}

//include given interval and its lower set earlier in filtration, for hole manipulation
bool Wrap::setIncludedLowerSetRecursively(int interval_index, exact inclusion_alpha2, bool printinfo){
    //include max simplex in filtration
    bool included = setSimplexIncludedWithAlpha2(intervals[interval_index].getMaxSimplexIndex(),inclusion_alpha2,true);
    if(included){
        std::vector<int> interval_simplices = intervals[interval_index].getSimplices();
        //if was not included, include other interval simplices
        for(int i=0; i<interval_simplices.size();i++){
            setSimplexIncludedWithAlpha2(interval_simplices[i],inclusion_alpha2,true);
        }
        //recursively include simplices in predecessor intervals
        std::set<int> predecessors = intervals[interval_index].getPredecessors();
        for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end(); ++it){
            if(printinfo){
                std::cout << (*it) << " ";
            }
            setIncludedLowerSetRecursively((*it),inclusion_alpha2,printinfo);
        }
    }
    return included;
}

//adapt filtration values (exclude or include with new value) of non critical intervals that are in lower set of excluded critical intervals
int Wrap::adaptNonCritialIntervalsAfterExcludingCritical(){
    int num_noncritical_intervals_adapted = 0;
    for(int i=0; i<intervals.size(); i++){
        Interval interval = intervals[i];
        if(!interval.isSingular()){
            int first_critical_descendant = interval.getFirstCriticalDescendant();
            std::set<int>* other_descendants = interval.getOtherCriticalDescendants();
            //first critical descendant was excluded
            if(first_critical_descendant>=0 && excluded[intervals[first_critical_descendant].getMaxSimplexIndex()]){
                num_noncritical_intervals_adapted++;
                int new_first_descendant = -1;
                exact new_alpha2 = -1;
                for(std::set<int>::iterator it = other_descendants->begin(); it != other_descendants->end(); ++it){
                    int simplex_index = intervals[*it].getMaxSimplexIndex();
                    if(!excluded[simplex_index]){
                        exact descendant_alpha2 = filtration_values[simplex_index];
                        if(included[simplex_index]){
                            descendant_alpha2 = inclusion_values_and_counter[simplex_index].first;
                        }
                        if(new_first_descendant<0 || descendant_alpha2<new_alpha2){
                            //this is the current candidate for the new first descendant
                            new_first_descendant = (*it);
                            new_alpha2 = descendant_alpha2;
                        }
                    }
                }
                std::vector<int> interval_simplices = interval.getSimplices();
                if(new_first_descendant<0){
                    //no new first descendant found
                    //exclude interval simplices
                    for(int j=0; j<interval_simplices.size(); j++){
                        excluded[interval_simplices[j]]=true;
                    }
                }else{
                    //new first descendant, include with new value in filtration
                    for(int j=0; j<interval_simplices.size(); j++){
                        included[interval_simplices[j]]=true;
                        inclusion_values_and_counter[interval_simplices[j]]=std::make_pair(new_alpha2,0);
                    }
                }
            }
        }
    }
}

//update size of lists after change of simplices number
void Wrap::updateListSizes()
{
    int size_increase = simplices_ptr->size()-in_complex.size();
    if(size_increase>0){
        std::vector<bool> extra_bool = std::vector<bool>(size_increase,false);
        std::vector<int> extra_values = std::vector<int>(size_increase,-1);
        in_complex.insert(in_complex.end(),extra_bool.begin(),extra_bool.end());
        filtration_values.insert(filtration_values.end(),extra_values.begin(),extra_values.end());
        interval_indices.insert(interval_indices.end(),extra_values.begin(),extra_values.end());
        excluded.insert(excluded.end(),extra_bool.begin(),extra_bool.end());
        included.insert(included.end(),extra_bool.begin(),extra_bool.end());
    }
}

//update lower sets of new intervals, add new critical intervals as descendants, update filtration value if new first_critical_descendant, update in filtration then
int Wrap::updateFiltrationValuesInLowerSetsOf(std::vector<int>* considered_intervals)
{
    int count = 0; //count number of updates
    for(std::vector<int>::iterator it = considered_intervals->begin(); it != considered_intervals->end(); ++it){
        Interval interval = intervals[*it]; 
        if(interval.isSingular()){
            count ++;
            //set filtration value for new critical intervals
            exact crit_alpha2 = interval.getAlpha2Wrap(&filtration_values);
            if(crit_alpha2<0){
                //was not set before
                //critical simplex has same alpha value as in Delaunay complex
                crit_alpha2 = filtration_values_alpha->at(interval.getMaxSimplexIndex());
                filtration_values[interval.getMaxSimplexIndex()]=crit_alpha2;
                in_complex[interval.getMaxSimplexIndex()]=true;
                //insert simplex of interval with new alpha value in filtration
                filtration_current.insertElement(crit_alpha2,interval.getMaxSimplexIndex(),simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
            }
            //all non-singular intervals in lower set have filtration value at most as high as this one, update recursively until critical intervals reached
            std::set<int> predecessors = interval.getPredecessors();
            for(std::set<int>::iterator it2 = predecessors.begin(); it2 != predecessors.end(); ++it2){
                count += setWrapAlpha2Recursively((*it),(*it2),crit_alpha2,true,true,true,0);
            }
        }
    }
    return count;
}

//update lower sets of (now) non-singular boundary intervals (with old critical descendants)
    //propagate values of old critical descendants through non-critical boundary into conflict zone (and maybe beyond), their lower set might contain additional intervals now
int Wrap::updateFiltrationValuesInLowerSetOfNonCriticalBoundary(int interval_index)
{
    int count = 0;
    Interval interval = intervals[interval_index];
    if(!interval.isSingular()){
        count ++;
        //get critical descendants outside of conflict zone (old intervals)
        std::set<int> old_critical_descendants;
        std::set<int> descendant_intervals = interval.getDescendantIntervals(interval_index,&intervals,&interval_indices,simplices_ptr);
        for(std::set<int>::iterator it = descendant_intervals.begin(); it != descendant_intervals.end(); ++it){
            int descendant_interval_index = (*it);
            if(descendant_interval_index>=0){
                Interval descendant_interval = intervals[descendant_interval_index];
                bool descendant_in_wrap = isInComplex(descendant_interval.getMaxSimplexIndex());
                if(descendant_in_wrap){
                    //old interval that is in lower set of some critical simplex (maybe critical itself)
                    //assumption: alpha2 was not set yet for new intervals
                    if(descendant_interval.isSingular()){
                        old_critical_descendants.insert(descendant_interval_index);
                    }else{
                        if(descendant_interval.getFirstCriticalDescendant()<0){
                            std::cerr <<"ERR(Wrap::updateFiltrationValuesInLowerSetOfNonCriticalBoundary): non-critical interval in Wrap has no first critical descendant" << std::endl;
                        }
                        old_critical_descendants.insert(descendant_interval.getFirstCriticalDescendant());
                    }
                }
            }
        }
        for(std::set<int>::iterator it = old_critical_descendants.begin(); it != old_critical_descendants.end(); ++it){
            //start recursion at boundary interval (no change in lower set between old descendant and boundary, also recursion would stop because descendant already known there)
            //advance recursion at boundary interval even if it already knows this critical descendant, want to propagate to interior of conflict zone, there the already_added rule applies
            count += setWrapAlpha2Recursively((*it),interval_index,intervals[*it].getAlpha2Wrap(&filtration_values),true,true,false,0);
        }
    }
    return count;
}

//delete simplex and other simplices in its interval from wrap filtration, delete interval (for point manipulation)
std::vector<int> Wrap::deleteSimplex(Simplex* simplex)
{
    std::vector<int> deleted_simplices;
    int interval_index = interval_indices[simplex->getIndex()];
    if(interval_index>=0){
        //delete simplices in interval from filtration and reset information about them
        std::vector<int> interval_simplices = intervals[interval_index].getSimplices();
        for(int i=0; i<interval_simplices.size(); i++){
            int index = interval_simplices[i];
            
            //delete from filtration
            bool deleted = filtration_current.deleteSimplex(index,getAlpha2Simplex(index),simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter);
            
            //reset all stored information about this simplex
            interval_indices[index]=-1;
            filtration_values[index]=-1;
            in_complex[index]=false;
            if(included[interval_simplices[i]]){
                if(inclusion_values_and_counter.find(index)!=inclusion_values_and_counter.end()){
                    inclusion_values_and_counter.erase(index);
                }
            }
            included[index]=false;
            excluded[index]=false;
            
            deleted_simplices.push_back(interval_simplices[i]);
        }
        //delete interval
        intervals[interval_index]=Interval(); //set empty interval
    }
    return deleted_simplices;
}   

//clear predecessor references to deleted intervals in all considered intervals
void Wrap::clearDeletedPredecessors(std::set<int>* considered_intervals, std::set<int>* deleted_intervals){
    for(std::set<int>::iterator it = considered_intervals->begin(); it != considered_intervals->end(); ++it){
        intervals[(*it)].clearDeletedPredecessors(deleted_intervals);
    }
}

//if interval is non-singular delete references to deleted critical descendants (and update first_critical_descendant and filtration value if necessary), recursively go to non-singular predecessors
int Wrap::deleteOldReferencesFromLowerSet(int interval_index,std::set<int>* deleted_intervals)
{
    int count = 0;
    if(interval_index>=0){
        count += intervals[interval_index].deleteOldReferencesFromLowerSetRecursively(deleted_intervals,&intervals,true,&filtration_current,&in_complex,&filtration_values,simplices_ptr,critical_ptr,&included,&inclusion_values_and_counter,0); 
    }else{
        std::cerr << "ERROR(Wrap::deleteOldReferencesFromLowerSet) invalid interval index" << std::endl;
    }
    return count;
}

//compute statistics (# simplices, ...) for whole filtration (for every filtration value)
void Wrap::computeFiltrationStatistics(bool printstats, bool after_hole_operation)
{
    //basic filtered complex statistics
    computeFiltrationStatistics0(printstats);
    
    //compute number of critical simplices
    num_crit_simplices_neg_filtration.clear();
    num_crit_simplices_pos_filtration.clear();
    std::vector<int> num_crit_edges_neg_filtration; //negative critical edges
    std::vector<int> num_crit_edges_pos_filtration; //positive critical edges
    std::vector<int> num_crit_triangles_neg_filtration; //negative critical triangles 
    std::vector<int> num_crit_triangles_pos_filtration; //positive critical triangles 
    std::vector<int> num_crit_tetrahedra_neg_filtration; //negative critical tetrahedra
    //only last critical triangle is positive for dim3 periodic
    if(!after_hole_operation){
        num_crit_edges_pos_filtration = std::vector<int>(filtration_current.size(),0);
        num_crit_edges_neg_filtration = std::vector<int>(filtration_current.size(),0);
        num_crit_triangles_neg_filtration = std::vector<int>(filtration_current.size(),0);
        num_crit_triangles_pos_filtration = std::vector<int>(filtration_current.size(),0); 
        if(dim3){
            num_crit_tetrahedra_neg_filtration = std::vector<int>(filtration_current.size(),0);
        }else{ //clear 3-dim statistics in 2-dim case 
            num_crit_tetrahedra_neg_filtration.clear();
        }
        int num_crit_edges_pos = 0; int num_crit_edges_neg = 0; 
        int num_crit_triangles_pos = 0; int num_crit_triangles_neg = 0;
        int num_crit_tetrahedra_neg = 0;
        //iterate over filtration
        for(int i=0; i<filtration_current.size(); i++){
            //if simplex critical update statistics, else they are not changed
            if(critical_ptr->at(filtration_current.getElementIndex(i))){
                int simplex_dim = filtration_current.getElementDim(i,simplices_ptr);
                if(simplex_dim>=0){
                    if(persistence.isPositiveSimplexCurrent(i)){ //positive simplex
                        if(simplex_dim==1){
                            //positive edge
                            num_crit_edges_pos++;
                        }else if(simplex_dim==2){
                            //positive triangle
                            num_crit_triangles_pos++;
                        }
                    }else{ //negative simplex
                        if(simplex_dim==1){ //edge
                            num_crit_edges_neg++;
                        }else if(simplex_dim==2){ //triangle
                            num_crit_triangles_neg++;
                        }else if(simplex_dim==3){ //tetrahedron
                            num_crit_tetrahedra_neg++; 
                        }
                    }
                }
            }
            num_crit_edges_pos_filtration[i]=num_crit_edges_pos;
            num_crit_edges_neg_filtration[i]=num_crit_edges_neg;
            num_crit_triangles_neg_filtration[i]=num_crit_triangles_neg;
            num_crit_triangles_pos_filtration[i]=num_crit_triangles_pos;
            if(dim3){
                num_crit_tetrahedra_neg_filtration[i]=num_crit_tetrahedra_neg;
            }
        }
    }else{
        //# of critical simplices not valid any more (criticality might change because of hole operation)
        num_crit_edges_pos_filtration = std::vector<int>(filtration_current.size(),-1);
        num_crit_edges_neg_filtration = std::vector<int>(filtration_current.size(),-1);
        num_crit_triangles_neg_filtration = std::vector<int>(filtration_current.size(),-1);
        num_crit_triangles_pos_filtration = std::vector<int>(filtration_current.size(),-1);
        num_crit_tetrahedra_neg_filtration = std::vector<int>(filtration_current.size(),-1);
    }
    
    num_crit_simplices_neg_filtration.push_back(num_crit_edges_neg_filtration);
    num_crit_simplices_neg_filtration.push_back(num_crit_triangles_neg_filtration);
    if(dim3){
        num_crit_simplices_neg_filtration.push_back(num_crit_tetrahedra_neg_filtration);
    }
    num_crit_simplices_pos_filtration.push_back(num_crit_edges_pos_filtration);
    num_crit_simplices_pos_filtration.push_back(num_crit_triangles_pos_filtration);
    
   filtration_statistics_computed = true;
}

//compute and print statistics for all intervals in filtration order, also print statistics about full complex and overlapping lower sets
void Wrap::computeIntervalStatisticsPaper()
{
    if(!descendants_computed){
        computeFiltrationValuesWrap(true);
    }
    //----------------- COMPUTE --------------------
    //compute statistics for intervals
    std::vector<std::vector<int> > nested_intervals = std::vector<std::vector<int> >(intervals.size(),std::vector<int>(0)); //for singular intervals: indices of singular intervals for which lower set is nested inside this lower set
    std::vector<std::vector<int> > lowerset_intervals = std::vector<std::vector<int> >(intervals.size(),std::vector<int>(0)); //for singular interval: indices of non-singular intervals that are in the lower set (without nested lower sets)
    
    std::vector<int> size_lowerset_simplices_full = std::vector<int>(intervals.size(),-1); //for singular intervals: how many simplices are in full lower set (including nesting), -1 for non-singular
    std::vector<int> size_lowerset_nested_full = std::vector<int>(intervals.size(),-1);    //for singular intervals: how many lower sets of critical simplices are nested (also recursively), -1 for non-singular
    std::vector<int> size_lowerset_simplices = std::vector<int>(intervals.size(),-1); //how many simplices are in lower set of singular interval (without nested)
    
    std::vector<double> diameter_lowerset = std::vector<double>(intervals.size(),-1); //diameter of lower set
    
    //compute intervals in lower set, if singular: store in nested_intervals, if non-singular: store in lowerset_intervals
    for(int i=0; i<intervals.size(); i++){ 
        Interval interval = intervals[i];
        int interval_index = i;
        if(!interval.isDeleted()){
            std::vector<int> in_lowerset_of = interval.getCriticalDescendants(); //get all critical descendants
            for(std::vector<int>::iterator it = in_lowerset_of.begin(); it<in_lowerset_of.end(); ++it){
                int descendant_index = (*it);
                if(!interval.isSingular()){
                    lowerset_intervals[descendant_index].push_back(interval_index);
                }else{  //critical simplex with nested lower set
                    nested_intervals[descendant_index].push_back(interval_index);
                } 
            }
        }
    }
    //compute number of simplices in lower set of critical simplex (without nested lower sets)
    for(int i=0; i<intervals.size(); i++){
        if(!intervals[i].isDeleted() && intervals[i].isSingular()){
            size_lowerset_simplices[i]=1; //we also count critical simplex itself
            for(int j=0; j<lowerset_intervals[i].size(); j++){
                size_lowerset_simplices[i] = size_lowerset_simplices[i]+intervals[lowerset_intervals[i][j]].getSize();
            }
        }
    }
    //compute size (# simplices, # nested) of full lower set (with nested)
    for(int i=0; i<intervals.size(); i++){
        if(!intervals[i].isDeleted() && intervals[i].isSingular()){
            //recursively get all nested intervals
            std::set<int> nested_intervals_full = intervals[i].getNestedIntervalsFull(&nested_intervals,&intervals);
            //compute size
            size_lowerset_nested_full[i] = nested_intervals_full.size();  //how many lower sets of critical simplices are nested (also recursively)
            //how many simplices are in full lower set (including nesting)
            int size_lowerset_simplices_full_i = size_lowerset_simplices[i]; //get size of this lowerset without nested
            for(std::set<int>::iterator it = nested_intervals_full.begin(); it != nested_intervals_full.end(); ++it){
                size_lowerset_simplices_full_i += size_lowerset_simplices[*it]; //add size of nested
            }
            size_lowerset_simplices_full[i] = size_lowerset_simplices_full_i;
        }
    }
    //within the set of all points in the lower set, find the ones with maximum distance, they define the diameter
    for(int i=0; i<intervals.size(); i++){
        if(!intervals[i].isDeleted() && intervals[i].isSingular()){
            //get all points in lower set
            std::set<int> points_lowerset=intervals[i].getPointsLowerSetFull(&lowerset_intervals,&nested_intervals,&intervals,simplices_ptr);
            
            exact diam2=0;
            //iterate over all pairs of points in lower set
            for(std::set<int>::iterator it1 = points_lowerset.begin(); it1!= points_lowerset.end(); ++it1){
                //second iteration stops at current element => it2 <= it1, only unique pairs
                for(std::set<int>::iterator it2 = points_lowerset.begin(); it2!= it1; ++it2){
                    //get points
                    DataPoint vert0 = data_points->at(*it2);
                    DataPoint vert1 = data_points->at(*it1);
                    CPoint3 cpoint0 = CPoint3(vert0.getX(),vert0.getY(),vert0.getZ());
                    CPoint3 cpoint1 = CPoint3(vert1.getX(),vert1.getY(),vert1.getZ());
                    exact dist2;
                    //compute distance
                    if(periodic_size >= 0){
                        //periodic case: for each pair of points compute minimum distance over all possible offsets
                        //we can compute the minimum distance independently for each coordinate (we look at ALL possible offsets): 
                        //      (point0_coord - point1_coord (+- periodic_size))^2
                        exact min_x2 = CGAL::min(CGAL::min(CGAL::square(cpoint0.x()-cpoint1.x()), CGAL::square(cpoint0.x()-cpoint1.x()+periodic_size)), CGAL::square(cpoint0.x()-cpoint1.x()-periodic_size));
                        exact min_y2 = CGAL::min(CGAL::min(CGAL::square(cpoint0.y()-cpoint1.y()), CGAL::square(cpoint0.y()-cpoint1.y()+periodic_size)), CGAL::square(cpoint0.y()-cpoint1.y()-periodic_size));
                        if(dim3){
                            exact min_z2 = CGAL::min(CGAL::min(CGAL::square(cpoint0.z()-cpoint1.z()), CGAL::square(cpoint0.z()-cpoint1.z()+periodic_size)), CGAL::square(cpoint0.z()-cpoint1.z()-periodic_size));
                            dist2 = min_x2+min_y2+min_z2;
                        }else{
                            dist2 = min_x2+min_y2;
                        }
                    }else{
                        //non-periodic case: compute distance directly
                        dist2 = CGAL::squared_distance(cpoint0,cpoint1);
                    }
                    if(dist2>diam2){
                        //new candidates found
                        diam2=dist2;
                    }
                }
            }
            diameter_lowerset[i] = CGAL::to_double(diam2);
        }
    }
    

    //----------------- PRINT --------------------
    
    //get list of intervals and sort it by alpha2 values (of Alpha complex)
    std::vector<std::pair<exact,Interval*> > intervals_with_alpha2;
    for(int i=0; i<intervals.size(); i++){
        Interval* interval = &(intervals[i]);
        if(!interval->isDeleted()){
            intervals_with_alpha2.push_back(std::make_pair(filtration_values_alpha->at(interval->getMaxSimplexIndex()),interval));
        }
    }
    std::sort(intervals_with_alpha2.begin(),intervals_with_alpha2.end());
    
    std::vector<int> filtration_indices = std::vector<int>(simplices_ptr->size(),-1);
    for(int i=0; i<filtration_current.size(); i++){
        filtration_indices[filtration_current.getElementIndex(i)]=i;
    }
    
    //print values that do not depend on alpha: # points, # intervals
    std::cout << std::setw(10) << std::right << "# points" << ", ";
    std::cout << std::setw(11) << std::right << "# intervals" << std::endl;
   
    std::cout << std::setw(10) << std::right << num_data_points << ", ";
    std::cout << std::setw(11) << std::right << getNumIntervals() << std::endl;
    std::cout << std::endl;
    
    //for each interval print in a row: 
    //  alpha in Alpha complex, alpha in Wrap complex, dimension of lower bound, dimension of upper bound
    //if interval is singular also print:
    //   simplex is positive?, persistence of corresponding pair, # simplices in full lower set, 
    //   diameter of critical simplex, diameter of lower set (including nested)
    if(!weighted){
        std::cout <<   "r_alpha, r_wrap, lower dimension, upper dimension, positive?, persistence, # simplices in lower set, # nested lower sets, diameter of simplex, diameter of lower set" << std::endl;
    }else{
        std::cout << "r2_alpha, r2_wrap, lower dimension, upper dimension, positive?, persistence, # simplices in lower set, # nested lower sets, diameter of simplex, diameter of lower set" << std::endl;
    }
    for(int i=0; i<intervals_with_alpha2.size(); i++){
        Interval* interval = intervals_with_alpha2[i].second;
        if(!interval->isDeleted()){
            if(!weighted){
                std::cout << std::sqrt(CGAL::to_double(intervals_with_alpha2[i].first)) << "," << std::sqrt(interval->getAlpha2Wrap_double(&filtration_values)) << ",";
            }else{
               std::cout << CGAL::to_double(intervals_with_alpha2[i].first) << "," << interval->getAlpha2Wrap_double(&filtration_values) << ","; 
            }
            std::cout << interval->getMinDim(simplices_ptr) << "," << interval->getMaxDim();
            if(interval->isSingular() && !excluded[interval->getMaxSimplexIndex()] && (interval->getMaxDim())>0){
                PersistencePair persPair = persistence.getPersistencePairForSimplexCurrent(interval->getMaxSimplexIndex());
                std::cout << "," << persistence.isPositiveSimplexCurrent(filtration_indices[interval->getMaxSimplexIndex()]) << ",";
                if(persPair.isInfinite()){
                    std::cout << (-1) << "," ;
                }else{
                    if(!weighted){
                        std::cout << persPair.getPersistence_double(&filtration_current) << "," ;
                    }else{
                        std::cout << persPair.getPersistence2_double(&filtration_current) << "," ;
                    }
                }
                std::cout << size_lowerset_simplices_full[interval->getIndex()] << "," << size_lowerset_nested_full[interval->getIndex()] << ",";
                std::cout << interval->getDiameter(data_points,periodic_size,simplices_ptr) << "," << diameter_lowerset[interval->getIndex()];  
            }
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
    
    //compute # intervals and simplices in overlapping lower sets (for unweighted points!, intervals with points ignored)
    std::cout << "overlapping lower sets" << std::endl;
    std::cout << std::setw(14) << std::right << "# intervals 12" << ", " << std::setw(14) << std::right << "# intervals 23" << ", " << std::setw(14) << std::right << "# intervals 13" << ", ";
    std::cout << std::setw(23) << std::right << "# noncritical simplices" << ", ";
    std::cout << std::setw(20) << std::right << "# critical simplices";
    std::cout << std::endl;
    int overlap_intervals12 = 0;
    int overlap_intervals23 = 0;
    int overlap_intervals13 = 0;
    int overlap_critical = 0;
    for(int i=0; i<intervals.size(); i++){
        if(!intervals[i].isDeleted()){
            if(intervals[i].inOverlap()){
                Interval interval = intervals[i];
                if(!interval.isSingular()){
                    if(interval.getMaxDim()==2){
                        if(interval.getMinDim(simplices_ptr)==1){
                            overlap_intervals12++;
                        }
                    }else if(interval.getMaxDim()==3){
                        if(interval.getMinDim(simplices_ptr)==2){
                            overlap_intervals23++;
                        }else if(interval.getMinDim(simplices_ptr)==1){
                            overlap_intervals13++;
                        }
                    }
                }else{
                    overlap_critical++;
                }
            }
        }
    }
    int overlap_noncritical = overlap_intervals12*2+overlap_intervals23*2+overlap_intervals13*4;
    std::cout << std::setw(14) << std::right << overlap_intervals12 << ", " << std::setw(14) << std::right << overlap_intervals23 << ", " << std::setw(14) << std::right << overlap_intervals13 << ", ";
    std::cout << std::setw(23) << std::right << overlap_noncritical << ", ";
    std::cout << std::setw(20) << std::right << overlap_critical;
    std::cout << std::endl;
}

//print detailed information about intervals (set of related simplices with same alpha value)
void Wrap::printIntervals()
{
    for(int i=0; i<intervals.size(); i++){
        std::cout << i << " ";
        if(!intervals[i].isDeleted()){
            intervals[i].print(&filtration_values,simplices_ptr);
        }else{
            std::cout << "deleted" << std::endl;
        }
    }
}

//print points, filtration and persistence pairs as output
void Wrap::printOutput(){
    //points
    std::cout << "POINTS: index, x, y";
    if(dim3){
        std::cout << ", z";
    }
    if(weighted){
        std::cout << ", weight";
    }
    std::cout << std::endl;
    for(int i=0; i<data_points->size(); i++){
        DataPoint point = data_points->at(i);
        if(!(point.isDeleted())){
            std::cout << i << ", " << point.getX_double() << ", " << point.getY_double();
            if(dim3){
                std::cout << ", " << point.getZ_double();
            }
            if(weighted){
                std::cout << ", " << point.getWeight_double();
            }
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
    if(!weighted){
        std::cout << "FILTRATION: index, dim, r_wrap, r_alpha, point1 point2 ... " << std::endl;
    }else{
        std::cout << "FILTRATION: index, dim, r2_wrap, r2_alpha, point1 point2 ... " << std::endl;
    }
    for(int i=1; i<filtration_current.size();i++){ //skip empty simplex
        exact value = filtration_current.getElement(i).first;
        Simplex* simplex = filtration_current.getSimplex(i,simplices_ptr);
        std::cout << i << ", " << simplex->getDim() << ", ";
        if(!weighted){
            std::cout << std::sqrt(CGAL::to_double(value)) << ", " << std::sqrt(CGAL::to_double(filtration_values_alpha->at(simplex->getIndex()))) << ", ";
        }else{
            std::cout << (CGAL::to_double(value)) << ", " << CGAL::to_double(filtration_values_alpha->at(simplex->getIndex())) << ", ";
        }
        std::vector<int> vertices = simplex->getVertices();
        for(int j=0; j<vertices.size()-1; j++){
            std::cout << vertices[j] << " ";
        }
        if(vertices.size()>0){
            std::cout << vertices[vertices.size()-1];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    persistence.printPersistencePairs(std::cout,&filtration_current);
}

//print info about full complex
void Wrap::printFull(bool header, std::ostream& output){
    
    //optionally print header line
    if(header){
       output << std::setw(9) << std::right << "# Wrap ed" << ", " << std::setw(10) << std::right << "# Wrap tri" << ", " ;
       if(dim3){
           output << std::setw(12) << std::right << "# Wrap tetra" << ", ";
       }
       output << std::setw(11) << std::right << "# crit ed -" << ", " << std::setw(11) << std::right << "# crit ed +" << ", ";
       if(dim3){
           output << std::setw(12) << std::right << "# crit tri -" << ", " << std::setw(12) << std::right << "# crit tri +" << ", ";
           output << std::setw(12) << std::right << "# crit tetra" << ", ";
       }else{
           output << std::setw(10) << std::right << "# crit tri" << ", ";
       }
       output << std::setw(6) << std::right << "Betti0" << ", " << std::setw(6) << std::right << "Betti1";
       if(dim3){
           output << ", " << std::setw(6) << std::right << "Betti2";
       }
       output << std::endl;
    }
    
    //print statistics of full complex
    output << std::setw(9) << std::right << getNumEdgesFull() << ", " << std::setw(10) << std::right << getNumTrianglesFull() << ", "; 
    if(dim3){
       output << std::setw(12) << std::right << getNumTetrahedraFull() << ", ";
    }  
    output << std::setw(11) << std::right << getNumCriticalEdgesNegFull() << ", " << std::setw(11) << std::right << getNumCriticalEdgesPosFull() << ", ";
    if(dim3){
        output << std::setw(12) << std::right << getNumCriticalTrianglesNegFull() << ", " << std::setw(12) << std::right << getNumCriticalTrianglesPosFull() << ", ";
        output << std::setw(12) << std::right << getNumCriticalTetrahedraFull() << ", ";
    }else{
        output << std::setw(10) << std::right << getNumCriticalTrianglesNegFull() << ", ";
    }  
    output << std::setw(6) << std::right << getBetti0Full() << ", " << std::setw(6) << std::right << getBetti1Full();
    if(dim3){
        output << ", " << std::setw(6) << std::right << getBetti2Full();
    }
    output << std::endl;
}