//main.cpp
//author: koelsboe

//main file
//application is started here

#include "alpha_complex.h"
#include "basics.h" 
#include "bregman.h"
//#include <CGAL/config.h> //TEST CGAL Version

#ifndef NOQT5
    #include <QApplication>
    #include <QSurfaceFormat>
    #include "window.h" 
#endif
 
int main(int argc, char **argv)
{
    std::cout << "WELCOME TO WRAP_2_3! :) (last changed: 04.02.2020, license: GPLv3)" << std::endl;
    //std::cout << "My CGAL library is " << CGAL_VERSION_NR << " (1MMmmb1000)" << std::endl; 
    
    if(argc>1){
        //COMMAND LINE INPUT
        
        std::vector<std::string> input;
        bool correct_input = true;
        
        //initialize
        int alpha_init=1;
        Alpha_Complex alpha_complex(alpha_init);
        std::vector<DataPoint> data_points;
        bool dim3 = false;
        bool weighted = false;
        bool draw = false;
        int window_size = -1;
        int periodic_size = -1;
        bool bregman = false;
        Bregman bregman_complexes = Bregman(1,""); //initialize Bregman complexes
        
        std::cout << "input: ";
        for(int i=1; i<argc; i++){
            std::string input_element = argv[i];
            input.push_back(input_element);
            std::cout << argv[i] << " ";
        }
        std::cout << std::endl;
        
        std::vector<std::string> input_input;
        std::vector<std::string> input_computation;
        std::vector<std::string> input_output;
        std::vector<std::string> input_draw;
        
        for(int i=0; i<input.size(); ){
            if(input[i]=="-i"){
                i++;
                while(i<input.size() && input[i]!="-i" && input[i]!="-c" && input[i]!="-o" && input[i]!="-d"){
                    input_input.push_back(input[i]);
                    i++;
                }
            }else if(input[i]=="-c"){
                i++;
                while(i<input.size() && input[i]!="-i" && input[i]!="-c" && input[i]!="-o" && input[i]!="-d"){
                    input_computation.push_back(input[i]);
                    i++;
                }
            }else if(input[i]=="-o"){
                i++;
                while(i<input.size() && input[i]!="-i" && input[i]!="-c" && input[i]!="-o" && input[i]!="-d"){
                    input_output.push_back(input[i]);
                    i++;
                }
            }else if(input[i]=="-d"){
                i++;
                while(i<input.size() && input[i]!="-i" && input[i]!="-c" && input[i]!="-o" && input[i]!="-d"){
                    input_draw.push_back(input[i]);
                    i++;
                }
            }else{
                correct_input=false;
                break;
            }
        }
        
        if(correct_input){
            
            //NEW INPUT FORMAT
            
            double time1 = getCPUTime();  
            
            bool compute_persistence = true;
            
            //name for output files
            std::string output_filename_input;
            
            //INPUT: (0) default point set, (1) PPP, (2) file, (3) filtered complex
            
            if(input_input.size()==0){
                //(0) default point set
                
                data_points.push_back(DataPoint(2.37037, 4.76296));
                data_points.push_back(DataPoint(1.19259, 4.44444));
                data_points.push_back(DataPoint(0.585185, 3.94815));
                data_points.push_back(DataPoint(0.251852, 3.11852));
                data_points.push_back(DataPoint(0.251852, 2.13333));
                data_points.push_back(DataPoint(0.622222, 1.14074));
                data_points.push_back(DataPoint(1.57778, 0.6));
                data_points.push_back(DataPoint(2.65185, 0.318519));
                data_points.push_back(DataPoint(3.93333, 0.666667));
                data_points.push_back(DataPoint(4.4, 1.60741));
                data_points.push_back(DataPoint(4.65185, 2.57778));
                data_points.push_back(DataPoint(4.31111, 3.47407));
                data_points.push_back(DataPoint(3.74815, 4.17037));
                data_points.push_back(DataPoint(3.08889, 4.57037));
                data_points.push_back(DataPoint(1.48148, 3.68148));
                data_points.push_back(DataPoint(1.08148, 3.02963));
                data_points.push_back(DataPoint(1.08889, 2.2963));
                data_points.push_back(DataPoint(1.37037, 1.65185));
                data_points.push_back(DataPoint(2.34815, 1.27407));
                data_points.push_back(DataPoint(3.15556, 1.43704));
                data_points.push_back(DataPoint(3.62222, 1.91852));
                data_points.push_back(DataPoint(3.67407, 2.77037));
                data_points.push_back(DataPoint(3.22222, 3.37037));
                data_points.push_back(DataPoint(2.25926, 3.77778));
                data_points.push_back(DataPoint(1.8614,  4.3674));
                data_points.push_back(DataPoint(3.28099, 4.14288));
                data_points.push_back(DataPoint(4.20082, 2.95506));
                data_points.push_back(DataPoint(3.91111, 1.33267));
                data_points.push_back(DataPoint(2.15835, 0.731522));
                data_points.push_back(DataPoint(0.666337, 1.64411));
                data_points.push_back(DataPoint(0.637366, 3.34617));
                data_points.push_back(DataPoint(2.63638, 4.4688));
                data_points.push_back(DataPoint(3.80971, 3.74452));
                data_points.push_back(DataPoint(4.3674,  2.10041));
                data_points.push_back(DataPoint(3.24477, 0.774979));
                data_points.push_back(DataPoint(1.1516,  1.11539));
                data_points.push_back(DataPoint(0.470782, 2.54946));
                data_points.push_back(DataPoint(1.08642, 4.05596));
                
                bool valid_complex = alpha_complex.setPoints(&data_points,data_points.size(),periodic_size,dim3,false,compute_persistence,false); 
                
                output_filename_input="default";
                
            }else if(input_input[0]=="ppp" && input_input.size()==5){
                
                //(1) PPP: "-i ppp < dim (2/3)> < window size (int)> < lambda (double)> < periodic? (0/1)>"
                
                int dim = std::stoi(input_input[1]);
               
                window_size = std::stoi(input_input[2]);
                double lambda = std::stod(input_input[3]);
                int periodic_int = std::stoi(input_input[4]);
               
                
                if((dim==2||dim==3) && (periodic_int==0||periodic_int==1) && window_size>0){
                    
                    dim3 = (dim==3);
                    bool periodic = (periodic_int==1);
                    
                    //see Geometry::generatePointsPPP()
                    //------------------------------------------------------

                    //initialize
                    int volume = window_size*window_size; //2-dimensional window
                    if(dim3){
                        volume = volume * window_size; //3-dimensional window
                    }

                    //seed random number generator
                    std::mt19937 mrand(std::time(0));
                    //std::default_random_engine generator; //pseudo-random number generator

                    //number of points in window is random Poisson variable with parameter lambda*volume
                    std::poisson_distribution<int> poisson_dist(lambda*volume);
                    //points are uniformly distributed in the window
                    std::uniform_real_distribution<double> uniform_dist(0.0,(double)window_size);  

                    //generate points  
                    int num_points = poisson_dist(mrand);
                    
                    if(periodic){
                        periodic_size=window_size;
                    } 

                    bool valid_complex=false;
                    int count_invalid=0;
                    while(!valid_complex && count_invalid<10){ //multiple trials if we do not get valid complex immediately, can happen for periodic points
                        
                        data_points.clear();
                        for(int i = 0; i<num_points; i++){
                            if(!dim3){
                                double randx = uniform_dist(mrand); 
                                double randy = uniform_dist(mrand);
                                data_points.push_back(DataPoint(randx,randy));
                            }else{
                                double randx = uniform_dist(mrand); 
                                double randy = uniform_dist(mrand);
                                double randz = uniform_dist(mrand);
                                data_points.push_back(DataPoint(false,randx,randy,randz));
                            }          
                        }
                        valid_complex = alpha_complex.setPoints(&data_points,data_points.size(),periodic_size,dim3,false,compute_persistence,false); 
                        if(!valid_complex){
                            count_invalid++;
                        }
                    }
                    if(count_invalid>0){
                        std::cerr << "ERROR - # invalid trials: " << count_invalid;
                        if(!valid_complex){
                            std::cout << " - CANCELLED" << std::endl;
                            std::cout << "--> PLEASE USE A LARGER WINDOW SIZE OR LAMBDA! <--" << std::endl;
                            return 0;
                        }
                        std::cout << std::endl;
                    }
                    
                    output_filename_input = "ppp";
                    std::ostringstream ss_size, ss_lambda;
                    ss_size << window_size;
                    ss_lambda << lambda;
                    output_filename_input = output_filename_input + "_" + ss_lambda.str() + "_" + ss_size.str();
                    if(periodic){
                        output_filename_input = output_filename_input + "_p";
                    }
                      
                }else{
                    correct_input=false;
                }
                
            }else if(input_input[0]=="file" && input_input.size()==2){
                
                // (2) file "-i file < filename >"
                
                std::string filename = input_input[1];

                std::ifstream ifs(filename.c_str());

                if(ifs.fail()){
                    std::cout << "Error reading input file!" << std::endl;
                    return 0;
                }

                std::string firstline;
                std::getline(ifs, firstline);
                std::istringstream streamLine(firstline);
                int dim=0;
                std::string s;
                for(; streamLine >> s;){
                    dim++;
                }
                //first line optionally contains dimension, important for weighted input
                if(dim==1){
                    dim=atoi(s.c_str());
                    //check length of second line to know if weighted points
                    std::getline(ifs, firstline);
                    std::istringstream streamLine2(firstline);
                    int linelength=0;
                    std::string s2;
                    for(; streamLine2 >> s2;){
                        linelength++;
                    }
                    if(linelength>dim){
                        weighted=true;
                    }
                }

                dim3=false;
                if(dim==2){
                    CPoint2 p;
                    exact weight;
                    //save first point
                    if(!weighted){
                        std::istringstream(firstline) >> p;
                    }else{
                        std::istringstream(firstline) >> p >> weight;
                    }
                    DataPoint point = DataPoint(p.x(),p.y());
                    if(weighted){
                        point.setWeight(weight);
                    }
                    data_points.push_back(point);
                    //save points from input stream 
                    //(important: directly stream to CGAL points to ensure exact computation)          
                    int i = 1;
                    if(!weighted){
                        while(ifs >> p){
                            point = DataPoint(p.x(),p.y());
                            data_points.push_back(point);
                            i++;
                        }
                    }else{
                        while(ifs >> p >> weight){
                            point = DataPoint(p.x(),p.y());
                            point.setWeight(weight);
                            data_points.push_back(point);
                            i++;
                        }
                    }
                }else if(dim==3){
                    dim3=true;
                    CPoint3 p;
                    exact weight;
                    //save first point
                    if(!weighted){
                        std::istringstream(firstline) >> p;
                    }else{
                        std::istringstream(firstline) >> p >> weight;
                    }
                    DataPoint point = DataPoint(false,p.x(),p.y(),p.z());
                    if(weighted){
                        point.setWeight(weight);
                    }
                    data_points.push_back(point);
                    //save points from input stream 
                    //(important: directly stream to CGAL points to ensure exact computation)
                    int i = 1;
                    if(!weighted){
                        while(ifs >> p){
                           point = DataPoint(false,p.x(),p.y(),p.z());
                           data_points.push_back(point);
                           i++;
                        }
                    }else{
                        while(ifs >> p >> weight){
                           point = DataPoint(false,p.x(),p.y(),p.z());
                           point.setWeight(weight);
                           data_points.push_back(point);
                           i++;
                        }
                    }
                
                }else{
                    correct_input = false;
                }

                if(correct_input){
                    
                    bool valid_complex = alpha_complex.setPoints(&data_points,data_points.size(),false,dim3,weighted,compute_persistence,false);

                    if(!valid_complex){
                        std::cout << "Error: This point set does not give a valid complex!" << std::endl;
                        return 0;
                    }

                    output_filename_input = filename.substr(filename.find_last_of("/\\") + 1); //remove path
                    output_filename_input = output_filename_input.substr(0, output_filename_input.find_last_of(".")); //remove file extension
                
                }
                
            }else if(input_input[0]=="complex" && input_input.size()==2){
                
                // (3) filtered complex "-i complex < filename >"
                
                std::string filename = input_input[1];
                
                data_points = alpha_complex.importFilteredComplexFromFile(filename,true,compute_persistence); //import

                //get info from alpha
                dim3 = alpha_complex.isDim3();
                alpha_complex.updateDataPointsPtr(&data_points); //for some reason we need to update pointer
                
                output_filename_input = filename.substr(filename.find_last_of("/\\") + 1);
                output_filename_input = output_filename_input.substr(0, output_filename_input.find_last_of(".")); //remove file extension
                
            }else{
                correct_input=false;
            }
            
            

            double radius2 = CGAL::to_double(alpha_complex.getWrapComplex()->getMaxAlpha2())*1.001;
            
                
            //COMPUTATION: (0) default: Euclidean Alpha and Wrap complexes, (1) Bregman geometries: Shannon, conjugate Shannon, or Fisher,
            //             (2) hole operation, (3) relaxed Wrap complex

            if(correct_input){
            
                if(input_computation.size()==0){

                    //(0) default: Euclidean Alpha and Wrap complexes

                    //we already computed them

                }else if(input_computation[0]=="bregman" && input_computation.size()==2 && 
                        (input_computation[1]=="shannon"||input_computation[1]=="conjugate_shannon"||input_computation[1]=="fisher")){

                    //(1) Bregman geometries: "-c bregman < "shannon" / "conjugate_shannon" / "fisher" >

                    //only for unweighted, not periodic point set
                    periodic_size = -1;
                    weighted = false;

                    bregman = true;
                    bregman_complexes.setOnStandardSimplex(alpha_complex.isOnStandardSimplex());

                    std::string bregman_type = input_computation[1];
                    if(bregman_type=="shannon" || bregman_type=="conjugate_shannon"){

                        bregman_complexes.setType("Shannon");
                        bregman_complexes.setPrimal(true);
                        if(bregman_type=="conjugate_shannon"){
                            bregman_complexes.setPrimal(false);
                        }

                        bool valid = bregman_complexes.computeBregmanDelaunay(&data_points,false,data_points.size(),periodic_size,dim3,compute_persistence,false);

                    }else if(bregman_type=="fisher"){

                        bool valid = bregman_complexes.computeWithFisherMetric(&data_points,data_points.size(),periodic_size,dim3,compute_persistence,false);  
                        alpha_complex = (Alpha_Complex)bregman_complexes;

                    }

                    //culling
                    if(bregman_type=="fisher" || bregman_type=="conjugate_shannon"){
                        bregman_complexes.computeCircumcentersMaxSimplicesBregman(true,false);
                    }
                    //use Bregman complexes as Alpha and Wrap complex
                    alpha_complex = (Alpha_Complex)bregman_complexes;
                    alpha_complex.updateDataPointsPtr(&data_points); //for some reason we need to update pointers
                    alpha_complex.updateSimplicesPtr(bregman_complexes.getSimplices());
                    alpha_complex.updateCritical(bregman_complexes.getCritical());

                }else if(input_computation[0]=="hole" && input_computation.size()==5){

                    //(2) hole "-c hole < "lock" / "fill" / "unlock" / "unfill" > < dim (int)> < "rank" / "below" / "above" > < rank (int) / threshold (double) >"
                    //first approach, using basis vectors, for full complex

                    std::string operation_type = input_computation[1];
                    int dim = std::stoi(input_computation[2]);
                    std::string rank_or_threshold = input_computation[3];
                    int rank=1; double threshold=1;
                    if(rank_or_threshold=="rank"){
                        rank=std::stoi(input_computation[4]);
                    }else{
                        threshold=std::stod(input_computation[4]);
                    }

                    if((operation_type=="lock" || operation_type=="fill" || operation_type=="unlock" || operation_type=="unfill") && dim>=0 
                            && (rank_or_threshold=="rank" || rank_or_threshold=="below" || rank_or_threshold=="above") && rank>0 && threshold>=0){

                        bool lock = false;
                        if(operation_type=="lock" || operation_type=="unlock"){
                            lock = true;
                        }
                        bool un = false;
                        if(operation_type=="unlock" || operation_type=="unfill"){
                            un = true;
                        }
                        bool subcomplex=false;
                        double persistence_bound=-1;
                        bool lowerbound=false;
                        if(rank_or_threshold=="below"||rank_or_threshold=="above"){
                            persistence_bound=threshold;
                            if(rank_or_threshold=="above"){
                                lowerbound=true;
                            }
                        }
                        alpha_complex.holeOperation(lock,un,dim,rank,subcomplex,persistence_bound,lowerbound, false, false, true, false);
                        alpha_complex.getWrapComplex()->holeOperation(lock,un,dim,rank,subcomplex,persistence_bound,lowerbound,false, false, true, false);

                        //set alpha2 so that effect gets nicely visible
                        if(lock && !un){ //lock
                            radius2 = CGAL::to_double(alpha_complex.getMinAlpha2());
                        }
                        if((!lock && !un) && rank_or_threshold=="rank"){ //fill
                            //set alpha to birth value of hole (simplices inserted with this filtration value)
                            int birth_filtration_index = alpha_complex.getPersistencePairOfDim(dim,rank).getBirthIndexFiltration();
                            exact birth_alpha2=(alpha_complex.getFiltrationFull()->getElement(birth_filtration_index)).first;
                            radius2 = CGAL::to_double(birth_alpha2);
                        }
                    }else{
                        correct_input = false;
                    }

                }else if(input_computation[0]=="relaxed" && input_computation.size()==2){

                    //(3) relaxed Wrap complex "-c relaxed <threshold (double>=0)> "

                    double threshold = std::stod(input_computation[1]);

                    if(threshold>=0){
                        alpha_complex.computeIntervalsFromNoisyFiltration(alpha_complex.getFiltrationValues(),threshold,alpha_complex.isWeighted(),false,false,true,true,false,false);
                    }else{
                        correct_input = false;
                    }
                }else{
                    correct_input = false;
                }

                //compute statistics
                alpha_complex.computeFiltrationStatistics(false,false);
                alpha_complex.getWrapComplex()->computeFiltrationStatistics(false,false);

                std::cout << "Elapsed time: " << getCPUTime()-time1 << std::endl; 
            }
                
            //OUTPUT first argument: prefix for files (can be empty), other arguments: different export options, can choose multiple
            //          export options: "points", full_info", "filtered_complex_wrap", "filtered_complex_alpha", "interval_statistics", "interval_info", 
            //                          "persistence_barcode", "persistence_pairs_wrap", "persistence_pairs_alpha", "boundary_matrix_wrap", "boundadry_matrix_alpha",
            //                          "persistence_matrices_wrap", "persistence_matrices_alpha", "tripartition"

            //set up output filename

            std::string output_folder = "output/";

            //default prefix for output filename
            std::string prefix = "output";
            if(dim3){
                prefix = prefix + "3";
            }else{
                prefix = prefix + "2";
            }

            std::string output_filename_basis = output_filename_input + "_" + getCurrentDate();
            
            if(correct_input){

                if(input_output.size()==0){

                    //default option: export full information, default prefix "output2/3"

                    std::string output_filename = output_folder+prefix+"_"+output_filename_basis+"_full_info.txt";
                    std::cout << output_filename << std::endl;

                    //open file and redirect output to it
                    std::ofstream myfile(output_filename.c_str());
                    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
                    std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile   

                    alpha_complex.printFullInformation(std::cout);

                    myfile.close(); //close output file
                    std::cout.rdbuf(coutbuf); //reset to standard output again

                }else{

                    int start_index=0;
                    if(input_output.size()>1){

                        //more than 1 string: first string is interpreted as prefix for output file
                        prefix = input_output[0];
                        start_index = 1;
                    }

                    for(int i=start_index; i<input_output.size(); i++){

                        if(input_output[i]=="points" || input_output[i]=="full_info" || input_output[i]=="filtered_complex_wrap" || input_output[i]=="filtered_complex_alpha"
                                || input_output[i]=="interval_statistics" || input_output[i]=="interval_info" || input_output[i]=="persistence_barcode" 
                                || input_output[i]=="persistence_pairs_wrap" || input_output[i]=="persistence_pairs_alpha" || input_output[i]=="boundary_matrix_wrap"
                                || input_output[i]=="boundary_matrix_alpha" || input_output[i]=="persistence_matrices_wrap" || input_output[i]=="persistence_matrices_alpha"
                                || input_output[i]=="tripartition"){

                            std::string output_filename = output_folder+prefix+"_"+output_filename_basis+"_"+input_output[i]+".txt";
                            std::cout << output_filename << std::endl;

                            //open file and redirect output to it
                            std::ofstream myfile(output_filename.c_str());
                            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
                            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile   

                            if(input_output[i]=="points"){

                                if(!dim3){
                                    std::cout << "2" << std::endl;
                                }else{
                                    std::cout << "3" << std::endl;
                                }
                                //write point coordinates to file
                                for(int i=0;i<data_points.size();i++){
                                    DataPoint point = data_points[i];
                                    if(!point.isDeleted()){
                                        std::cout << point.getX() << " " << point.getY();
                                        if(dim3){
                                            std::cout << " " << point.getZ();
                                        }
                                        if(alpha_complex.isWeighted()){
                                            std::cout << " " << point.getWeight();
                                        }
                                        std::cout << std::endl;
                                    }
                                }

                            }else if(input_output[i]=="full_info"){

                                alpha_complex.printFullInformation(std::cout);

                            }else if(input_output[i]=="filtered_complex_wrap"){

                                alpha_complex.printWrapComplexes(std::cout);

                            }else if(input_output[i]=="filtered_complex_alpha"){

                                alpha_complex.printAlphaComplexes(std::cout);

                            }else if(input_output[i]=="interval_statistics"){

                                alpha_complex.getWrapComplex()->computeIntervalStatisticsPaper();

                            }else if(input_output[i]=="interval_info"){

                                alpha_complex.getWrapComplex()->printIntervals();

                            }else if(input_output[i]=="persistence_barcode" ){

                                alpha_complex.getWrapComplex()->printPersistenceBarcode();

                            }else if(input_output[i]=="persistence_pairs_wrap"){

                                alpha_complex.getWrapComplex()->printPersistencePairs(std::cout);

                            }else if(input_output[i]=="persistence_pairs_alpha"){

                                alpha_complex.printPersistencePairs(std::cout);

                            }else if(input_output[i]=="boundary_matrix_wrap"){

                                alpha_complex.getWrapComplex()->printBoundaryMatrix(std::cout);

                            }else if(input_output[i]=="boundary_matrix_alpha"){

                                alpha_complex.printBoundaryMatrix(std::cout);

                            }else if(input_output[i]=="persistence_matrices_wrap"){

                                alpha_complex.getWrapComplex()->printPersistenceMatrices(std::cout);

                            }else if(input_output[i]=="persistence_matrices_alpha"){

                                alpha_complex.printPersistenceMatrices(std::cout);

                            }else if(input_output[i]=="tripartition"){

                                alpha_complex.printAlphaSimplicesPartition();

                            }

                            myfile.close(); //close output file
                            std::cout.rdbuf(coutbuf); //reset to standard output again

                        }else{
                            std::cout << "Error: This export option does not exist: " << input_output[i] << std::endl;
                            correct_input = false;
                        }

                    }

                }
            }
                
            //DRAW (0) default: do not draw, (1) start interactive window, (2) export image of alpha or wrap, possibly also start interactive window
            
            #ifndef NOQT5 //check if compiled with QT5

            //initialize QApplication
            QApplication app (argc, argv);
            bool start_interactive = false;
            
            if(correct_input){
                
                bool radius_max_wrap = false;
                bool export_alpha = false;
                bool export_wrap = false;

                if(input_draw.size()==0){

                    draw = false;

                }else if(input_draw.size()==1){

                    //(1) start interactive window: "-d start"

                    draw = true;

                    if(input_draw[0]=="start"){

                        start_interactive = true;

                    }else{
                        correct_input = false;
                    }

                }else{

                    //(2) export image of alpha or wrap for given radius, possibly also start interactive window
                    //    "-d (start) < alpha (double) / "max_wrap" > <"alpha" and/or "wrap">

                    draw = true;

                    int j=0;

                    if(input_draw[0]=="start"){

                        start_interactive = true;
                        j++;        
                    }

                    if(input_draw[j]!="max_wrap"){

                        radius2 = std::stod(input_draw[j]);

                    }
                    j++;

                    for(;j<input_draw.size();j++){

                        if(input_draw[j]=="alpha"){
                            export_alpha = true;
                        }else if(input_draw[j]=="wrap"){
                            export_wrap = true;
                        }else{
                            correct_input = false;
                        }

                    }
                }

                if(draw){

                    //open interactive window and draw 

                    //for OpenGL Window: set default format
                    QSurfaceFormat format;
                    format.setDepthBufferSize(24); //minimum depth buffer size
                    QSurfaceFormat::setDefaultFormat(format);

                    Alpha_Complex* alpha_pointer = &alpha_complex;
                    std::vector<DataPoint> data_points2;
                    Alpha_Complex alpha_complex2d(1);
                    if(dim3){
                        //we create small alpha complex in 2d to initialize window, because it does not work to start drawing in 3d for some reason..
                        data_points2.push_back(DataPoint(2.37037, 4.76296));
                        data_points2.push_back(DataPoint(1.19259, 4.44444));
                        data_points2.push_back(DataPoint(0.585185, 3.94815));
                        data_points2.push_back(DataPoint(0.251852, 3.11852));
                        alpha_complex2d.setPoints(&data_points2,data_points2.size(),-1,false,false, true,false);
                        alpha_complex2d.computeFiltrationStatistics(false,false);
                        alpha_complex2d.getWrapComplex()->computeFiltrationStatistics(false,false);
                        alpha_pointer = &alpha_complex2d;
                    }else{
                        data_points2=data_points;
                    }

                    //create main window and start application
                    Window window(data_points2,alpha_pointer,window_size);
                    window.show();
                    if(alpha_complex.isPeriodic()){
                        window_size = window_size *1.2; //increase size of drawing window
                    }
                    window.init2(window_size); //some initial commands that only work after window.show()

                    //change to the complex we actually want to draw
                    Geometry* geometry = window.getGeometry();
                    
                    if(dim3){
                        geometry->setAlphaComplex(data_points,&alpha_complex,window_size,1,false);
                    }
                    geometry->setStandardSimplex(alpha_complex.isOnStandardSimplex());
                    geometry->setBregman(bregman);
                    
                    window.fitAlphaRangeWrap();
                    window.updateGeometry(geometry);
                    
                    window.updateLayoutDim(geometry->isDim3());
                    window.updateLayoutWeighted(geometry->isWeighted()||geometry->isUseRadius2());
                    window.updateLayoutBregman(geometry->isBregman());
                    window.assignLayoutShowHideStatistics();
                    window.assignLayoutShowHideStatistics();
                    window.raise();
                    
                    geometry->setUseRadius2(true); //so that updateAlpha interprets value as alpha^2
                    geometry->updateAlpha(radius2);

                    geometry->setUseRadius2(bregman||weighted);
                    geometry->drawDelaunay(false);
                    geometry->fitView();

                    if(export_alpha || export_wrap){

                        for(int i=0; i<2; i++){
                            if((i==0 && export_alpha)||(i==1 && export_wrap)){

                                std::string name = "alpha";
                                //draw wrap or alpha complex
                                if(i==0 && export_alpha){
                                    geometry->drawWrap(false);
                                    geometry->drawAlpha(true);
                                }else{
                                    name = "wrap";
                                    geometry->drawWrap(true);
                                    geometry->drawAlpha(false);
                                }
                                 
                                //open file
                                char str_buffer[32]; //nice formatting for float
                                memset(str_buffer, 0, sizeof(str_buffer));
                                snprintf(str_buffer, sizeof(str_buffer), "%g", radius2);
                                std::string radius_str(str_buffer);
                                std::string output_filename_png = output_folder+prefix+"_"+output_filename_basis+"_"+name+"_"+radius_str+".png";
                                std::string output_filename_svg = output_folder+prefix+"_"+output_filename_basis+"_"+name+"_"+radius_str+".svg";

                                QString outputname_png = QString::fromStdString(output_filename_png);
                                QString outputname_svg = QString::fromStdString(output_filename_svg);

                                QFile outputfile_png(outputname_png);
                                outputfile_png.open(QIODevice::WriteOnly);
                                QPixmap pixmap;
                                if(geometry->isDim3()){
                                    pixmap = QPixmap(geometry->getOpenGLWidget()->size());
                                    geometry->getOpenGLWidget()->render(&pixmap);
                                }else{
                                    pixmap = geometry->grab();
                                }
                                pixmap.save(&outputfile_png,"PNG");

                                if(!dim3){
                                    int size = 500;
                                    QSvgGenerator generator;
                                    generator.setFileName(outputname_svg);       
                                    generator.setSize(QSize(size, size));
                                    generator.setViewBox(QRect(0, 0, size, size));
                                    generator.setTitle(QString::fromStdString("wrap_2_3 example"));
                                    QPainter painter;
                                    painter.begin(&generator);
                                    geometry->render(&painter);
                                    painter.end();
                                }

                            }
                        }
                    }
                    
                    if(!correct_input){

                        std::cout << "INPUT FALSE!" << std::endl;
                        std::cout << "Expected input: './wrap_2_3 -i <input_options> -c <computation_options> -o <output_options> -d <draw_options>'" << std::endl;
                        std::cout << "  (all of them are optional)" << std::endl;
                        std::cout << " input_options:" << std::endl;
                        std::cout << "   (0) default point set: empty"  << std::endl;
                        std::cout << "   (1) generate random points with PPP: 'ppp <dim(2/3)> <window_size(int)> <lambda(double)> <periodic?(0/1)>'" << std::endl;
                        std::cout << "   (2) load points from file: 'file <filename>' " << std::endl;
                        std::cout << "   (3) load filtered complex from file: 'complex <filename>'" << std::endl;
                        std::cout << " computation_options:" << std::endl;
                        std::cout << "   (0) Euclidean Alpha and Wrap complexes: empty (default)" << std::endl;
                        std::cout << "   (1) Bregman geometries: 'bregman <\"shannon\"/\"conjugate_shannon\"/\"fisher\">'" << std::endl;
                        std::cout << "   (2) hole manipulation with basis vectors: 'hole <\"lock\"/\"fill\"/\"unlock\"/\"unfill\"> <dim(int)> <\"rank\"/\"below\"/\"above\"> <rank(int)/threshold(double)>'" << std::endl;
                        std::cout << "   (3) relaxed Wrap complex: 'relaxed <threshold(double)>'" << std::endl;
                        std::cout << " output_options:" << std::endl;
                        std::cout << "   (0) export full information in one file, default prefix \"output2/3\": empty" << std::endl;
                        std::cout << "   (1) if 1 string provided: interpret as export option (see (2)), default prefix" << std::endl;
                        std::cout << "   (2) first string used as prefix for output files, other strings: different export options, can choose multiple" << std::endl;
                        std::cout << "   export options: \"points\", \"full_info\", \"filtered_complex_wrap\", \"filtered_complex_alpha\", \"interval_statistics\", \"interval_info\", ";
                        std::cout << "\"persistence_barcode\", \"persistence_pairs_wrap\", \"persistence_pairs_alpha\", \"boundary_matrix_wrap\", \"boundadry_matrix_alpha\", ";
                        std::cout << "\"persistence_matrices_wrap\", \"persistence_matrices_alpha\", \"tripartition\"" << std::endl;
                        std::cout << " draw_options" << std::endl;
                        std::cout << "  (0) do not draw: empty" << std::endl;
                        std::cout << "  (1) start interactive window: 'start'" << std::endl;
                        std::cout << "  (2) export image of alpha or wrap complex of given radius and possibly also start window: '(start) <radius^2(double)/\"max_wrap\"> <\"alpha\" and/or \"wrap\">'" << std::endl;
                        std::cout << std::endl;
                    }

                    if(draw && start_interactive){
                        geometry->setUseRadius2(true); //so that updateAlpha interprets value as alpha^2
                        geometry->updateAlpha(radius2);
                        geometry->setUseRadius2(bregman || weighted); 
                        window.initAlphaSliderValue();
                        return app.exec();
                    }else{
                        return 0;
                    }
                }
            }
            
            
            

            #endif

            if(!correct_input){

                std::cout << "INPUT FALSE!" << std::endl;
                std::cout << "Expected input: './wrap_2_3 -i <input_options> -c <computation_options> -o <output_options> -d <draw_options>'" << std::endl;
                std::cout << "  (all of them are optional)" << std::endl;
                std::cout << " input_options:" << std::endl;
                std::cout << "   (0) default point set: empty"  << std::endl;
                std::cout << "   (1) generate random points with PPP: 'ppp <dim(2/3)> <window_size(int)> <lambda(double)> <periodic?(0/1)>'" << std::endl;
                std::cout << "   (2) load points from file: 'file <filename>' " << std::endl;
                std::cout << "   (3) load filtered complex from file: 'complex <filename>'" << std::endl;
                std::cout << " computation_options:" << std::endl;
                std::cout << "   (0) Euclidean Alpha and Wrap complexes: empty (default)" << std::endl;
                std::cout << "   (1) Bregman geometries: 'bregman <\"shannon\"/\"conjugate_shannon\"/\"fisher\">'" << std::endl;
                std::cout << "   (2) hole manipulation with basis vectors: 'hole <\"lock\"/\"fill\"/\"unlock\"/\"unfill\"> <dim(int)> <\"rank\"/\"below\"/\"above\"> <rank(int)/threshold(double)>'" << std::endl;
                std::cout << "   (3) relaxed Wrap complex: 'relaxed <threshold(double)>'" << std::endl;
                std::cout << " output_options:" << std::endl;
                std::cout << "   (0) export full information in one file, default prefix \"output2/3\": empty" << std::endl;
                std::cout << "   (1) if 1 string provided: interpret as export option (see (2)), default prefix" << std::endl;
                std::cout << "   (2) first string used as prefix for output files, other strings: different export options, can choose multiple" << std::endl;
                std::cout << "   export options: \"points\", \"full_info\", \"filtered_complex_wrap\", \"filtered_complex_alpha\", \"interval_statistics\", \"interval_info\", " ;
                std::cout << "\"persistence_barcode\", \"persistence_pairs_wrap\", \"persistence_pairs_alpha\", \"boundary_matrix_wrap\", \"boundadry_matrix_alpha\", ";
                std::cout << "\"persistence_matrices_wrap\", \"persistence_matrices_alpha\", \"tripartition\"" << std::endl;
                std::cout << " draw_options" << std::endl;
                std::cout << "  (0) do not draw: empty" << std::endl;
                std::cout << "  (1) start interactive window: 'start'" << std::endl;
                std::cout << "  (2) export image of alpha or wrap complex of given radius and possibly also start window: '(start) <radius^2(double)/\"max_wrap\"> <\"alpha\" and/or \"wrap\">'" << std::endl;
                std::cout << std::endl;
            }
            
            return 0;
            
        }else{
            correct_input=true;
            
            //OLD INPUT FORMAT
        
            std::string input = argv[1];
            if(input=="ppp" || input=="file" || input=="test_dynamic_runtime" || input=="statistics_hole_operations" || input=="tripartition_ppp" || input =="tripartition_file"){
                if(input == "ppp" || input == "test_dynamic_runtime" || input=="statistics_hole_operations" || input=="tripartition_ppp"){
                    if(argc==8 || argc==9){
                        int dim = atoi(argv[2]);
                        window_size = atoi(argv[3]);
                        double lambda = atof(argv[4]);
                        int ntrials = atoi(argv[5]);
                        int periodic_int = atoi(argv[6]);
                        int draw_int = atoi(argv[7]);
                        std::string filename_suffix = "";
                        if(argc==9){
                            filename_suffix = argv[8];
                        }

                        if((dim==2||dim==3) && (periodic_int==0||periodic_int==1) && window_size>0 && ntrials>0 && (draw_int==0||draw_int==1)){
                            dim3 = (dim==3);
                            bool periodic = (periodic_int == 1);
                            draw = (draw_int == 1);

                            //see Geometry::generatePointsPPP()
                            //------------------------------------------------------
                            double time1 = getCPUTime();  

                            //initialize
                            int volume = window_size*window_size; //2-dimensional window
                            if(dim3){
                                volume = volume * window_size; //3-dimensional window
                            }

                            //seed random number generator
                            std::mt19937 mrand(std::time(0));
                            //std::default_random_engine generator; //pseudo-random number generator

                            //number of points in window is random Poisson variable with parameter lambda*volume
                            std::poisson_distribution<int> poisson_dist(lambda*volume);
                            //points are uniformly distributed in the window
                            std::uniform_real_distribution<double> uniform_dist(0.0,(double)window_size);  

                            std::vector<DataPoint> generated_points;

                            //open output file
                            std::ostringstream ss_size, ss_ntrials;
                            ss_size << window_size;
                            ss_ntrials << ntrials;
                            std::string s = "../output/output";
                            if(input=="test_dynamic_runtime"){
                                s = "../output/test_dynamic";
                            }
                            if(input=="statistics_hole_operations"){
                                s = "../output/statistics_hole_operations";
                            }
                            if(input=="tripartition_ppp"){
                                s = "../output/tripartition_ppp";
                            }
                            if(!dim3){
                                s = s + "2";
                            }else{
                                s = s + "3";
                            }
                            s = s + "_" + getCurrentDate() + "_" + ss_size.str() + "_" + ss_ntrials.str();
                            if(periodic){
                                s = s + "_p";
                            }
                            s=s+filename_suffix+".txt";
                            char const* filename = s.c_str();
                            std::ofstream myfile(filename);

                            std::cout << s << std::endl;

                            //redirect std::cout output to file
                            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
                            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile   

                            std::cout << "POISSON POINT PROCESS" << std::endl;


                            std::cout <<  std::setw(8) << std::right << "# trials" << ", " << std::setw(3) << std::right << "dim" << ", ";
                            std::cout << std::setw(11) << std::right << "window size" << ", " << std::setw(10) << std::right << "lambda" << ", " ;
                            std::cout << std::setw(8) << std::right << "periodic" << std::endl;
                            std::cout <<  std::setw(8) << std::right << ntrials << ", ";
                            if(!dim3){
                                std::cout << std::setw(3) << std::right << "2";
                            }else{
                                std::cout << std::setw(3) << std::right << "3";
                            }
                            std::cout << ", " << std::setw(11) << std::right << window_size << ", " << std::setw(10) << std::right << lambda << ", ";
                            std::cout << std::setw(8) << std::right << periodic << std::endl;

                            int count_invalid = 0;
                            bool repeated = false;

                            //generate points, compute complexes, repeat 
                            for(int t = 0; t<ntrials; t++){      
                                std::cout << std::endl;
                                if(!repeated){
                                    std::cout << "TRIAL, " << t << std::endl;
                                }
                                repeated = false;
                                int num_points = poisson_dist(mrand);

                                for(int i = 0; i<num_points; i++){
                                    if(!dim3){
                                        double randx = uniform_dist(mrand); 
                                        double randy = uniform_dist(mrand);
                                        generated_points.push_back(DataPoint(randx,randy));
                                    }else{
                                        double randx = uniform_dist(mrand); 
                                        double randy = uniform_dist(mrand);
                                        double randz = uniform_dist(mrand);
                                        generated_points.push_back(DataPoint(false,randx,randy,randz));
                                    }          
                                }
                                if(periodic){
                                    periodic_size=window_size;
                                }  
                                data_points = generated_points;
                                //compute alpha and wrap complex
                                bool compute_persistence = true;
                                if(input == "test_dynamic_runtime"){
                                    compute_persistence = false; //disable persistence computation because it takes so long
                                }
                                bool valid = alpha_complex.setPoints(&data_points,data_points.size(),periodic_size,dim3,false,compute_persistence,false); 
                                //for periodic point sets the result might be invalid
                                if(valid){
                                    if(input=="ppp"){
                                        (alpha_complex.getWrapComplex())->computeIntervalStatisticsPaper();
                                    }else if(input == "test_dynamic_runtime"){
                                        //dynamic point addition and deletion

                                        double randx = uniform_dist(mrand); 
                                        double randy = uniform_dist(mrand);
                                        double randz = uniform_dist(mrand);

                                        std::cout << "ADD RANDOM POINT" << std::endl;
                                        //see Geometry::addPoint(randx,randy,randz,false,true,false);
                                        //add point to list of data_points
                                        int new_index = data_points.size();
                                        if(!dim3){
                                            data_points.push_back(DataPoint(randx,randy));
                                        }else{
                                            data_points.push_back(DataPoint(false,randx,randy,randz));
                                        }
                                        //first add point by local updates, then recompute, output running times
                                        alpha_complex.addPoint(&data_points,new_index,false,false,false,false); 
                                        double time1 = getCPUTime();          
                                        alpha_complex.setPoints(&data_points,data_points.size(),periodic_size,dim3,false,false,false);
                                        std::cout << "Elapsed time: " << getCPUTime()-time1 << std::endl; 

                                        std::cout << "DELETE POINT 0" << std::endl;
                                        //deleting point from list (formally, invalidate)
                                        DataPoint old_point = data_points[0]; //keep old point for now
                                        //set empty/deleted point at given index
                                        data_points[0] = DataPoint(); 
                                        //first add by local updates
                                        alpha_complex.deletePoint(old_point,0,false,false,false,false); 
                                        //then try recomputing
                                        time1 = getCPUTime();          
                                        alpha_complex.setPoints(&data_points,data_points.size(),periodic_size,dim3,false,false,false);
                                        std::cout << "Elapsed time: " << getCPUTime()-time1 << std::endl; 

                                        data_points[0]=old_point; //we do not want deleted points for drawing
                                    }else if(input == "statistics_hole_operations"){
                                        alpha_complex.computeFiltrationStatistics(false,false);
                                        alpha_complex.getWrapComplex()->computeFiltrationStatistics(false,false);
                                        alpha_complex.computeResultsPaperHoles(std::cout);
                                    }else if(input == "tripartition_ppp"){
                                        alpha_complex.computeFiltrationStatistics(false,false);
                                        alpha_complex.printAlphaSimplicesPartition();
                                    }
                                }else{ //if invalid
                                    count_invalid++;
                                    if(count_invalid<ntrials*2){ //avoid infinite loop
                                        t--; //repeat trial
                                        repeated = true;
                                    }
                                }    
                                generated_points.clear();  
                            }

                            double time2 = getCPUTime();

                            myfile.close(); //close output file
                            std::cout.rdbuf(coutbuf); //reset to standard output again

                            std::cout << "Elapsed time: " << time2-time1 << std::endl; 
                            if(periodic && count_invalid>0){
                                std::cerr << "ERROR - # invalid trials: " << count_invalid;
                                if(count_invalid>=2*ntrials){
                                    std::cout << " - CANCELLED" << std::endl;
                                    std::cout << "--> PLEASE USE A LARGER WINDOW SIZE OR LAMBDA! <--";
                                    draw = false;
                                }
                                std::cout << std::endl;
                            }
                            //------------------------------------------------------

                        }else{
                            correct_input=false;
                        }
                    }else{
                        correct_input = false;
                    }
                }
                if(input == "file"||input == "tripartition_file"){
                    if(argc == 4){
                        //read input parameters
                        std::string filename = argv[2];
                        int draw_int = atoi(argv[3]);

                        draw = (draw_int == 1);

                        std::ifstream ifs(filename.c_str());

                        if(ifs.fail()){
                            std::cout << "Error reading input file!" << std::endl;
                            return 0;
                        }

                        std::string firstline;
                        std::getline(ifs, firstline);
                        std::istringstream streamLine(firstline);
                        int dim=0;
                        std::string s;
                        for(; streamLine >> s;){
                            dim++;
                        }
                        bool weighted=false;
                        //first line optionally contains dimension, important for weighted input
                        if(dim==1){
                            dim=atoi(s.c_str());
                            //check length of second line to know if weighted points
                            std::getline(ifs, firstline);
                            std::istringstream streamLine2(firstline);
                            int linelength=0;
                            std::string s2;
                            for(; streamLine2 >> s2;){
                                linelength++;
                            }
                            if(linelength>dim){
                                weighted=true;
                            }
                        }

                        bool dim3=false;
                        if(dim==2){
                            CPoint2 p;
                            exact weight;
                            //save first point
                            if(!weighted){
                                std::istringstream(firstline) >> p;
                            }else{
                                std::istringstream(firstline) >> p >> weight;
                            }
                            DataPoint point = DataPoint(p.x(),p.y());
                            if(weighted){
                                point.setWeight(weight);
                            }
                            data_points.push_back(point);
                            //save points from input stream 
                            //(important: directly stream to CGAL points to ensure exact computation)          
                            int i = 1;
                            if(!weighted){
                                while(ifs >> p){
                                    point = DataPoint(p.x(),p.y());
                                    data_points.push_back(point);
                                    i++;
                                }
                            }else{
                                while(ifs >> p >> weight){
                                    point = DataPoint(p.x(),p.y());
                                    point.setWeight(weight);
                                    data_points.push_back(point);
                                    i++;
                                }
                            }
                        }else if(dim==3){
                            dim3=true;
                            CPoint3 p;
                            exact weight;
                            //save first point
                            if(!weighted){
                                std::istringstream(firstline) >> p;
                            }else{
                                std::istringstream(firstline) >> p >> weight;
                            }
                            DataPoint point = DataPoint(false,p.x(),p.y(),p.z());
                            if(weighted){
                                point.setWeight(weight);
                            }
                            data_points.push_back(point);
                            //save points from input stream 
                            //(important: directly stream to CGAL points to ensure exact computation)
                            int i = 1;
                            if(!weighted){
                                while(ifs >> p){
                                   point = DataPoint(false,p.x(),p.y(),p.z());
                                   data_points.push_back(point);
                                   i++;
                                }
                            }else{
                                while(ifs >> p >> weight){
                                   point = DataPoint(false,p.x(),p.y(),p.z());
                                   point.setWeight(weight);
                                   data_points.push_back(point);
                                   i++;
                                }
                            }
                        }else{
                            correct_input = false;
                        }

                        bool valid = alpha_complex.setPoints(&data_points,data_points.size(),false,dim3,weighted,true,false);
                        if(valid){
                            //open output file
                            s = "../output/output";
                            if(input=="tripartition_file"){
                                s = "../output/tripartition";
                            }
                            if(!dim3){
                                s = s + "2";
                            }else{
                                s = s + "3";
                            }
                            std::string base_filename = filename.substr(filename.find_last_of("/\\") + 1);
                            s = s + "_" + getCurrentDate() + "_" + base_filename;
                            char const* output_filename = s.c_str();
                            std::ofstream myfile(output_filename);
                            std::cout << s << std::endl;

                            //redirect std::cout output to file
                            std::streambuf *coutbuf = std::cout.rdbuf(); //save old buffer
                            std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile   

                            alpha_complex.computeFiltrationStatistics(false,false);
                            if(input=="file"){
                                alpha_complex.getWrapComplex()->computeFiltrationStatistics(false,false);
                                (alpha_complex.getWrapComplex())->printOutput();
                            }else if(input=="tripartition_file"){
                                alpha_complex.printAlphaSimplicesPartition();
                            }

                            myfile.close(); //close output file
                            std::cout.rdbuf(coutbuf); //reset to standard output again
                        }
                    }else{
                        correct_input = false;
                    }
                }
                if(draw && !dim3){
                    //REMARK: segmentation fault for 3d
                    if(input=="test_dynamic_runtime"){
                        //we do not want deleted points for drawing
                        alpha_complex.setPoints(&data_points,data_points.size(),periodic_size,dim3,false, true,false);
                    }
                    alpha_complex.computeFiltrationStatistics(false,false);
                    alpha_complex.getWrapComplex()->computeFiltrationStatistics(false,false);
                    #ifndef NOQT5
                    //open interactive window and draw 

                    //initialize QApplication
                    QApplication app (argc, argv);

                    //for OpenGL Window: set default format
                    QSurfaceFormat format;
                    format.setDepthBufferSize(24); //minimum depth buffer size
                    QSurfaceFormat::setDefaultFormat(format);

                    //create main window and start application
                    Window window(data_points,&alpha_complex,window_size);
                    window.show();
                    if(alpha_complex.isPeriodic()){
                        window_size = window_size *1.2; //increase size of drawing window
                    }
                    window.init2(window_size); //some initial commands that only work after window.show()

                    return app.exec();
                    #endif
                    return 0;
                }else{
                    return 0;
                }
            }else{
                correct_input = false;
            }

            if(!correct_input){
                std::cout << "Input false!" << std::endl; 
                std::cout << "Expected input (OLD FORMAT): " << std::endl;
                std::cout << "   (1) 'ppp' < dim (2/3)> < window size (int)> < lambda (double) > < # trials (int)> < periodic? (0/1)> < draw? (0/1)> optionally: < outputfilename suffix (str)>" << std::endl;
                std::cout << "or (2) 'file' < filename > < draw? (0/1)>" << std::endl;
                std::cout << std::endl;
                std::cout << "or (3) 'test_dynamic_runtime' < dim (2/3)> < window size (int)> < lambda (double) > < # trials (int)> < periodic? (0/1)> < draw? (0/1)> optionally: < outputfilename suffix (str)>" << std::endl;
                std::cout << "or (4) 'statistics_hole_operations' < dim (2/3)> < window size (int)> < lambda (double) > < # trials (int)> < periodic? (0/1)> < draw? (0/1)> optionally: < outputfilename suffix (str)>" << std::endl;
                std::cout << std::endl;
                std::cout << "or (5) 'tripartition_ppp' < dim (2/3)> < window size (int)> < lambda (double) > < # trials (int)> < periodic? (0/1)> < draw? (0/1)> optionally: < outputfilename suffix (str)>" << std::endl;
                std::cout << "or (6) 'tripartition_file' < filename > < draw? (0/1)>" << std::endl;
            }
        }
    }else{
        
        #ifndef NOQT5
        //open interactive window and draw 

        //start with default point set
        int alpha_init=50;
        Alpha_Complex alpha_complex(alpha_init);
        std::vector<DataPoint> data_points;
        data_points.push_back(DataPoint(2.37037, 4.76296));
        data_points.push_back(DataPoint(1.19259, 4.44444));
        data_points.push_back(DataPoint(0.585185, 3.94815));
        data_points.push_back(DataPoint(0.251852, 3.11852));
        data_points.push_back(DataPoint(0.251852, 2.13333));
        data_points.push_back(DataPoint(0.622222, 1.14074));
        data_points.push_back(DataPoint(1.57778, 0.6));
        data_points.push_back(DataPoint(2.65185, 0.318519));
        data_points.push_back(DataPoint(3.93333, 0.666667));
        data_points.push_back(DataPoint(4.4, 1.60741));
        data_points.push_back(DataPoint(4.65185, 2.57778));
        data_points.push_back(DataPoint(4.31111, 3.47407));
        data_points.push_back(DataPoint(3.74815, 4.17037));
        data_points.push_back(DataPoint(3.08889, 4.57037));
        data_points.push_back(DataPoint(1.48148, 3.68148));
        data_points.push_back(DataPoint(1.08148, 3.02963));
        data_points.push_back(DataPoint(1.08889, 2.2963));
        data_points.push_back(DataPoint(1.37037, 1.65185));
        data_points.push_back(DataPoint(2.34815, 1.27407));
        data_points.push_back(DataPoint(3.15556, 1.43704));
        data_points.push_back(DataPoint(3.62222, 1.91852));
        data_points.push_back(DataPoint(3.67407, 2.77037));
        data_points.push_back(DataPoint(3.22222, 3.37037));
        data_points.push_back(DataPoint(2.25926, 3.77778));
        data_points.push_back(DataPoint(1.8614,  4.3674));
        data_points.push_back(DataPoint(3.28099, 4.14288));
        data_points.push_back(DataPoint(4.20082, 2.95506));
        data_points.push_back(DataPoint(3.91111, 1.33267));
        data_points.push_back(DataPoint(2.15835, 0.731522));
        data_points.push_back(DataPoint(0.666337, 1.64411));
        data_points.push_back(DataPoint(0.637366, 3.34617));
        data_points.push_back(DataPoint(2.63638, 4.4688));
        data_points.push_back(DataPoint(3.80971, 3.74452));
        data_points.push_back(DataPoint(4.3674,  2.10041));
        data_points.push_back(DataPoint(3.24477, 0.774979));
        data_points.push_back(DataPoint(1.1516,  1.11539));
        data_points.push_back(DataPoint(0.470782, 2.54946));
        data_points.push_back(DataPoint(1.08642, 4.05596));
        alpha_complex.setPoints(&data_points,data_points.size(),-1,false,false, true,false);
        alpha_complex.computeFiltrationStatistics(false,false);
        alpha_complex.getWrapComplex()->computeFiltrationStatistics(false,false);
                
        //initialize QApplication
        QApplication app (argc, argv);

        //for OpenGL Window: set default format
        QSurfaceFormat format;
        format.setDepthBufferSize(24); //minimum depth buffer size
        QSurfaceFormat::setDefaultFormat(format);

        //create main window and start application
        Window window(data_points,&alpha_complex,-1);
        window.show();
        window.init2(-1); //some initial commands that only work after window.show()

        return app.exec();
        #endif

        std::cout << "INPUT FALSE!" << std::endl;
        std::cout << "Expected input: './wrap_2_3 -i <input_options> -c <computation_options> -o <output_options>'" << std::endl;
        std::cout << "  (all of them are optional)" << std::endl;
        std::cout << " input_options:" << std::endl;
        std::cout << "   (0) default point set: empty"  << std::endl;
        std::cout << "   (1) generate random points with PPP: 'ppp <dim(2/3)> <window_size(int)> <lambda(double)> <periodic?(0/1)>'" << std::endl;
        std::cout << "   (2) load points from file: 'file <filename>' " << std::endl;
        std::cout << "   (3) load filtered complex from file: 'complex <filename>'" << std::endl;
        std::cout << " computation_options:" << std::endl;
        std::cout << "   (0) Euclidean Alpha and Wrap complexes: empty (default)" << std::endl;
        std::cout << "   (1) Bregman geometries: 'bregman <\"shannon\"/\"conjugate_shannon\"/\"fisher\">'" << std::endl;
        std::cout << "   (2) hole manipulation with basis vectors: 'hole <\"lock\"/\"fill\"/\"unlock\"/\"unfill\"> <dim(int)> <\"rank\"/\"below\"/\"above\"> <rank(int)/threshold(double)>'" << std::endl;
        std::cout << "   (3) relaxed Wrap complex: 'relaxed < threshold(double)>'" << std::endl;
        std::cout << " output_options:" << std::endl;
        std::cout << "   (0) export full information in one file, default prefix \"output2/3\": empty" << std::endl;
        std::cout << "   (1) if 1 string provided: interpret as export option (see (2)), default prefix" << std::endl;
        std::cout << "   (2) first string used as prefix for output files, other strings: different export options, can choose multiple" << std::endl;
        std::cout << "   export options: \"points\", \"full_info\", \"filtered_complex_wrap\", \"filtered_complex_alpha\", \"interval_statistics\", \"interval_info\", " ;
        std::cout << "\"persistence_barcode\", \"persistence_pairs_wrap\", \"persistence_pairs_alpha\", \"boundary_matrix_wrap\", \"boundadry_matrix_alpha\", ";
        std::cout << "\"persistence_matrices_wrap\", \"persistence_matrices_alpha\", \"tripartition\"" << std::endl;
        std::cout << std::endl;
        return 0;
    }
}