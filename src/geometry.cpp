//geometry.cpp
//author: koelsboe

#include "geometry.h"

#include <iostream>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <fstream>

//Qt libraries
#include <QPainter>
#include <QPolygon>
#include <QTimeLine>
#include <QtOpenGL>

//constructor for default input
Geometry::Geometry(double alpha2, QWidget *parent)
    : DrawingPlane(parent)
{  
    alpha_complex = new Alpha_Complex(alpha2); //initialize Alpha complex with given alpha^2
    bregman_complexes = new Bregman(alpha2,""); //initialize Bregman complexes
    
    bregman=false;
    draw_on_input_points = true;
    Q_EMIT(bregmanChanged(false));
    
    //initialize scenes and drawing
    init();
    
    //default input points
    input_data_points.push_back(DataPoint(2.37037, 4.76296));
    input_data_points.push_back(DataPoint(1.19259, 4.44444));
    input_data_points.push_back(DataPoint(0.585185, 3.94815));
    input_data_points.push_back(DataPoint(0.251852, 3.11852));
    input_data_points.push_back(DataPoint(0.251852, 2.13333));
    input_data_points.push_back(DataPoint(0.622222, 1.14074));
    input_data_points.push_back(DataPoint(1.57778, 0.6));
    input_data_points.push_back(DataPoint(2.65185, 0.318519));
    input_data_points.push_back(DataPoint(3.93333, 0.666667));
    input_data_points.push_back(DataPoint(4.4, 1.60741));
    input_data_points.push_back(DataPoint(4.65185, 2.57778));
    input_data_points.push_back(DataPoint(4.31111, 3.47407));
    input_data_points.push_back(DataPoint(3.74815, 4.17037));
    input_data_points.push_back(DataPoint(3.08889, 4.57037));
    input_data_points.push_back(DataPoint(1.48148, 3.68148));
    input_data_points.push_back(DataPoint(1.08148, 3.02963));
    input_data_points.push_back(DataPoint(1.08889, 2.2963));
    input_data_points.push_back(DataPoint(1.37037, 1.65185));
    input_data_points.push_back(DataPoint(2.34815, 1.27407));
    input_data_points.push_back(DataPoint(3.15556, 1.43704));
    input_data_points.push_back(DataPoint(3.62222, 1.91852));
    input_data_points.push_back(DataPoint(3.67407, 2.77037));
    input_data_points.push_back(DataPoint(3.22222, 3.37037));
    input_data_points.push_back(DataPoint(2.25926, 3.77778));
    input_data_points.push_back(DataPoint(1.8614,  4.3674));
    input_data_points.push_back(DataPoint(3.28099, 4.14288));
    input_data_points.push_back(DataPoint(4.20082, 2.95506));
    input_data_points.push_back(DataPoint(3.91111, 1.33267));
    input_data_points.push_back(DataPoint(2.15835, 0.731522));
    input_data_points.push_back(DataPoint(0.666337, 1.64411));
    input_data_points.push_back(DataPoint(0.637366, 3.34617));
    input_data_points.push_back(DataPoint(2.63638, 4.4688));
    input_data_points.push_back(DataPoint(3.80971, 3.74452));
    input_data_points.push_back(DataPoint(4.3674,  2.10041));
    input_data_points.push_back(DataPoint(3.24477, 0.774979));
    input_data_points.push_back(DataPoint(1.1516,  1.11539));
    input_data_points.push_back(DataPoint(0.470782, 2.54946));
    input_data_points.push_back(DataPoint(1.08642, 4.05596));
    last_point_inserted=input_data_points.size()-1;
        
    setPoints(input_data_points,true, -1, false, "", true);  //set points, compute geometry 
    
    wrap_complex = alpha_complex->getWrapComplex();
    
    
}

//constructor for given input
Geometry::Geometry(std::vector<DataPoint> input_data_points_, Alpha_Complex* alpha_complex_, int drawing_window_size, double alpha2, QWidget *parent)
    : DrawingPlane(parent), input_data_points(input_data_points_),alpha_complex(alpha_complex_)
{  
    transformed_data_points = &input_data_points; //no transformation in Euclidean (non-Bregman) geometry
    bregman = false;
    draw_on_input_points = true;
    last_point_inserted=input_data_points.size()-1;
    Q_EMIT(bregmanChanged(false));
    
    dim3 = alpha_complex->isDim3();
    
    wrap_complex = alpha_complex->getWrapComplex();
    
    bregman_complexes = new Bregman(alpha2,""); //initialize Bregman complexes
    
    alpha_complex->computeFiltrationStatistics(false,false);
    wrap_complex->computeFiltrationStatistics(false,false);
    
    alpha_complex->updateDataPointsPtr(&input_data_points); //update reference to points in alpha complex (somehow it is not stored correctly!)
    
    //initialize scenes and drawing
    init();
    
    //draw
    if(drawing_window_size>0 && !dim3){
    //for 2-dim points generated in window
        //draw with adapted parameters
        setDrawingParameters(drawing_window_size*0.01,drawing_window_size*0.005,drawing_window_size*0.01,labelsize,pointsize_persdiag,maxvalue_plots,color_delaunay_by_radius,true);
    }else{
    //else just draw
        drawScene(); 
    }
}

//alpha complex as input
void Geometry::setAlphaComplex(std::vector<DataPoint> input_data_points_, Alpha_Complex* alpha_complex_, int drawing_window_size, double alpha2, bool compute_statistics)
{  
    input_data_points=input_data_points_;
    alpha_complex=alpha_complex_;
    last_point_inserted=input_data_points.size()-1;
    
    transformed_data_points = &input_data_points; //no transformation in Euclidean (non-Bregman) geometry
    bregman = false;
    draw_on_input_points = true;
    Q_EMIT(bregmanChanged(false));
    
    dim3 = alpha_complex->isDim3();
    
    wrap_complex = alpha_complex->getWrapComplex();
    
    bregman_complexes = new Bregman(alpha2,""); //initialize Bregman complexes
    
    if(compute_statistics){
        alpha_complex->computeFiltrationStatistics(false,false);
        wrap_complex->computeFiltrationStatistics(false,false);
    }
    
    alpha_complex->updateDataPointsPtr(&input_data_points); //update reference to points in alpha complex (somehow it is not stored correctly!)
    
    //initialize scenes and drawing
    init();
    
    //draw
    if(drawing_window_size>0 && !dim3){
    //for 2-dim points generated in window
        //draw with adapted parameters
        setDrawingParameters(drawing_window_size*0.01,drawing_window_size*0.005,drawing_window_size*0.01,labelsize,pointsize_persdiag,maxvalue_plots,color_delaunay_by_radius,true);
    }else{
    //else just draw
        drawScene(); 
    }
}

//initialize scenes and drawing
void Geometry::init(){
    
     //OpenGL in 2D, no improvement
    //setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
    //setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    
    //initialize widget for opengl 3d-drawing
    opengl_widget = new OpenGL_Widget; 

    //optimize drawing
    scene.setItemIndexMethod(QGraphicsScene::NoIndex); //no indexing of scene item (useful for searching the scene)
    second_scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    third_scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    fourth_scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    //setOptimizationFlags(QGraphicsView::DontSavePainterState); //painter state (e.g. pen) not saved
    
    //enable anti-aliasing for drawing lines
    setRenderHint(QPainter::Antialiasing, true);
    
    //track mouse cursor coordinates
    setMouseTracking(true);  
    
    //connect with scene variable
    setScene(&scene);
    
    //initialize second drawing plane
    second_drawing_plane.setScene(&second_scene);
    second_drawing_plane.setRenderHint(QPainter::Antialiasing, true); 
    //second_drawing_plane.resize(600,300);
    second_drawing_plane.scale(3,3);
    //initialize third drawing plane
    third_drawing_plane.setScene(&third_scene);
    third_drawing_plane.setRenderHint(QPainter::Antialiasing, true); 
    third_drawing_plane.scale(3,3);
    //initialize fourth drawing plane
    fourth_drawing_plane.setScene(&fourth_scene);
    fourth_drawing_plane.setRenderHint(QPainter::Antialiasing, true); 
    fourth_drawing_plane.scale(3,3); 
    
        
    //initialize interactive elements
    labels_intervals_bool = false; //triangle labels checkbox
    labels_points_bool = false; //point labels checkbox
    input_points_bool = false; //input points checkbox
    weighted_point_spheres_bool = false; //weighted points as spheres
    delaunay_bool = false; //Delaunay checkbox
    alpha_bool = false; //Alpha complex checkbox
    wrap_bool = true; //Wrap complex checkbox
    full_bool = false; //infinite alpha checkbox
    convex_hull_bool = false;
    union_of_balls_bool = false;
    persDiag_wrap_bool = false; //radio buttons, persistence for Wrap or Alpha complex 
    persDiag_adapted_bool = true; //radio buttons, adapted or original persistence diagram 
    persDiag_status_bool = false;
    linewidth = 1;
    pointsize = 1;
    pointsize_input = 0.5;
    labelsize = 15;
    pointsize_persdiag = 1.5;
    maxvalue_plots = -1; //initially no maximum value
    setDefaultColors();
    color_delaunay_by_radius = 0;
    use_radius2 = false;
    
    //set thresholds to ensure fast computation
    intervalgraph_maxsize = 5000; //maximal number of intervals (containing a maximal simplex) s.t. interval graph is drawn
    persdiagfull_maxpairs = 5000; //maximal number of persistence pairs s.t. full diagram is drawn
    persdiag_minpers_rel = 1e-6; //if not full persistence diagram is drawn, only pairs with persistence above persdiag_minpers_rel*maxWrapAlpha2 are drawn
    
    scale(1,-1); //flip y-axis, origin is lower left 
}

//clear list of points
void Geometry::clearPoints()
{
    input_data_points.clear();  //clear list of points
    transformed_data_points->clear();
    free_indices_data_points.clear();
    bregman_complexes->clearPoints(); //clear bregman points
}

//clear Alpha complex, scene items
void Geometry::clearScene(){
    bregman_complexes->clear(); //clear Bregman complexes (if they were computed)
    alpha_complex->clear(); //clear Alpha complex (and Wrap complex)
    delaunay_items.clear(); //clear Delaunay triangle group for graphics scene     
    labels_intervals_items.clear();   //triangle labels 
    labels_points_items.clear();   //point labels 
    input_points_items.clear(); //input points
    weighted_point_spheres_items.clear(); //weighted point spheres
    alpha_items.clear(); //clear group of alpha complex items
    wrap_items.clear(); //clear group of wrap complex items
    convex_hull_items.clear();
    union_of_balls_items.clear();
    clearHighlightedItems();
    //clear Bregman geometry
    transformed_points_items.clear();
    labels_intervals_items_bregman.clear();
    labels_points_items_bregman.clear();
    delaunay_items_bregman.clear();
    alpha_items_bregman.clear();
    wrap_items_bregman.clear();
    //clear persistence diagram
    persistence_diag_wrap_items_original.clear();
    persistence_diag_alpha_items_original.clear();
    persistence_diag_wrap_items_adapted.clear();
    persistence_diag_alpha_items_adapted.clear();
    persistence_diag_wrap_items_status.clear();
    persistence_diag_alpha_items_status.clear();
    persistence_diag_wrap_items_subcomplex.clear();
    persistence_diag_alpha_items_subcomplex.clear();
    persistence_diag_connections_wrap.clear();
    persistence_diag_connections_alpha.clear();
    //clear statistics plots
    statistics_plots_titles.clear();
    statistics_plots.clear();
    statistics_plots_log.clear();
    statistics_plots_axislabel.clear();
    scene.clear();   //clear scene 
    second_scene.clear();
    third_scene.clear();
    fourth_scene.clear();
    emitInitialSignals();
}

//set new list of points, only draw if indicated by boolean
bool Geometry::setPoints(std::vector<DataPoint> input, bool draw, int periodic_size, bool weighted_, std::string bregman_type, bool compute_persistence_and_statistics)
{
    clearScene(); //clear
    input_data_points = input;  //save input points  
    if(input_data_points.size()>0){
        dim3 = (input_data_points[0].getDim()==3);
    }
    
    double time1 = getCPUTime();
    bool valid=false;
    
    
    //compute Alpha and Wrap complex
    //-----------------------
    if(!(bregman_type.rfind("bregman", 0) == 0 || bregman_type == "fisher")){ //
        
        bregman=false;
        Q_EMIT(bregmanChanged(false));
        
        transformed_data_points = &input_data_points; //for Euclidean metric points are not transformed before computing complexes
        
        alpha_complex->setOnStandardSimplex(on_standard_simplex);
        
        valid = alpha_complex->setPoints(&input_data_points,input_data_points.size(),periodic_size,dim3,weighted_,compute_persistence_and_statistics,false); 
        
    }else if(bregman_type == "fisher"){
        
        bregman=true;
        Q_EMIT(bregmanChanged(true));
        bregman_complexes->setOnStandardSimplex(on_standard_simplex);
        
        valid = bregman_complexes->computeWithFisherMetric(&input_data_points,input_data_points.size(),periodic_size,dim3,compute_persistence_and_statistics,false);  
        
        //store computed points and complexes
        transformed_data_points = bregman_complexes->getBregmanPoints();
        alpha_complex = (Alpha_Complex*)bregman_complexes;
        wrap_complex = alpha_complex->getWrapComplex();
        
    }else{
        bregman=true;
        Q_EMIT(bregmanChanged(true));
        bregman_complexes->setOnStandardSimplex(on_standard_simplex);
        
        if(bregman_type.rfind("bregman_halfEuclidean", 0) == 0){
            bregman_complexes->setType("halfEuclidean");
        }else if(bregman_type.rfind("bregman_Shannon", 0) == 0){
            bregman_complexes->setType("Shannon");
        }else if(bregman_type.rfind("bregman_conjugateShannon", 0) == 0){
            bregman_complexes->setType("conjugateShannon");
        }
        
        bool compute_weighted_euclidean_radii = false;
        if(bregman_type.find("weightedEuclidean",0) != std::string::npos){
            compute_weighted_euclidean_radii = true;
        }
        bregman_complexes->setPrimal(true);
        if(bregman_type.find("dual",0) != std::string::npos){
            bregman_complexes->setPrimal(false);
        }
        valid = bregman_complexes->computeBregmanDelaunay(&input_data_points,compute_weighted_euclidean_radii,input_data_points.size(),periodic_size,dim3,compute_persistence_and_statistics,true);
        transformed_data_points = bregman_complexes->getBregmanPoints();
        
        //TEST
        /*std::cout << "Bregman points" << std::endl;
        for(int i=0; i<transformed_data_points->size(); i++){
            std::cout << i << " "; (transformed_data_points->at(i)).print();
        }*/
        
        //use Bregman complexes as Alpha and Wrap complex
        alpha_complex = (Alpha_Complex*)bregman_complexes;
        wrap_complex = alpha_complex->getWrapComplex();
        
        //TEST
        //std::cout << "Bregman-Delaunay filtration" << std::endl;
        //alpha_complex->getFiltrationFull()->print(alpha_complex->getSimplices());
    }
    
    if(compute_persistence_and_statistics){
        alpha_complex->computeFiltrationStatistics(false,false);
        alpha_complex->getWrapComplex()->computeFiltrationStatistics(false,false);
    }
    std::cout << "Elapsed time: " << getCPUTime()-time1 << std::endl; 
    
    //draw and emit signals for interactive output
    //-----------------------------------------------
    if(draw){
        //draw complexes, interval graph and persistence diagrams
        drawScene(); 
        if(bregman){
            drawBregmanOnInputPoints(draw_on_input_points);
        }
    }
    
    //emit signals 
    emitInitialSignals();  
    
    return valid;   
}

//recompute complexes (from scratch) after point set has been changed
bool Geometry::recomputeWithUpdatedPoints(bool draw, bool compute_wrapstatistics, bool compute_persistence_and_statistics)
{
    clearScene(); //clear
    
    transformed_data_points = &input_data_points;
    bregman=false;
    Q_EMIT(bregmanChanged(false));
    if(getNumDataPoints()>0){
        dim3 = (input_data_points[0].getDim()==3);
    }
    
    //compute Alpha and Wrap complex
    //-----------------------
    double time1 = getCPUTime();          
    bool valid = alpha_complex->setPoints(&input_data_points,getNumDataPoints(),alpha_complex->getPeriodicSize(),dim3,alpha_complex->isWeighted(),compute_persistence_and_statistics,false); 
    
    if(compute_persistence_and_statistics){
        alpha_complex->computeFiltrationStatistics(false,false);
        alpha_complex->getWrapComplex()->computeFiltrationStatistics(false,false);
    }
    std::cout << "Elapsed time: " << getCPUTime()-time1 << std::endl; 
    
    //draw and emit signals for interactive output
    //-----------------------------------------------
    if(draw){
        //draw complexes, interval graph and persistence diagrams
        drawScene(); 
    }
    
    //emit signals 
    emitInitialSignals();  
    
    return valid;   
}

//import filtered complex from file
void Geometry::importFilteredComplexFromFile(std::string filename, bool draw){
    
    clearScene();
    input_data_points = alpha_complex->importFilteredComplexFromFile(filename,true,true); //import
    
    //not bregman
    transformed_data_points = &input_data_points; //no transformation
    bregman = false;
    draw_on_input_points=true;
    Q_EMIT(bregmanChanged(false));
    
    //get info from alpha
    dim3 = alpha_complex->isDim3();
    wrap_complex = alpha_complex->getWrapComplex();
    
    //compute statistics
    alpha_complex->computeFiltrationStatistics(false,false);
    wrap_complex->computeFiltrationStatistics(false,false);
    
    alpha_complex->updateDataPointsPtr(&input_data_points);
    
    //draw
    if(draw){
        drawScene();
    }
}


//draw complexes and interval graph
void Geometry::drawScene()
{
    clearHighlightedItems();
    delaunay_items.clear(); //clear Delaunay triangle group for graphics scene  
    weighted_point_spheres_items.clear(); //weighted point spheres
    labels_intervals_items.clear();   //triangle labels 
    labels_points_items.clear();   //point labels 
    input_points_items.clear();
    alpha_items.clear(); //clear group of alpha complex items
    wrap_items.clear(); //clear group of wrap complex items 
    convex_hull_items.clear();
    union_of_balls_items.clear();
    //clear Bregman geometry
    transformed_points_items.clear();
    labels_intervals_items_bregman.clear();
    labels_points_items_bregman.clear();
    delaunay_items_bregman.clear();
    alpha_items_bregman.clear();
    wrap_items_bregman.clear();
    persistence_diag_wrap_items_original.clear(); //clear groups of persistence diagram items
    persistence_diag_alpha_items_original.clear();
    persistence_diag_wrap_items_adapted.clear();
    persistence_diag_alpha_items_adapted.clear();
    persistence_diag_wrap_items_status.clear();
    persistence_diag_alpha_items_status.clear();
    persistence_diag_wrap_items_subcomplex.clear();
    persistence_diag_alpha_items_subcomplex.clear();
    persistence_diag_connections_wrap.clear();
    persistence_diag_connections_alpha.clear();
    statistics_plots_titles.clear(); //clear statistic plots
    statistics_plots.clear(); 
    statistics_plots_log.clear();
    statistics_plots_axislabel.clear(); 
    scene.clear();   //clear scene   
    second_scene.clear();
    third_scene.clear();
    fourth_scene.clear();
    
    std::vector<Simplex>* simplices = alpha_complex->getSimplices();
    
    Filtration* filtration_alpha = alpha_complex->getFiltration();
    Filtration* filtration_alpha_full = alpha_complex->getFiltrationFull();
    
    double rad = pointsize;
   
   if(!dim3){ //dim2

        //draw special points
        QPointF p = QPointF(0,0); //cast to Qt Point
        new_point = scene.addEllipse(p.x()-rad*2, p.y()-rad*2, rad*2*2.0, rad*2*2.0, 
            Qt::NoPen, QBrush(color_new_point,Qt::SolidPattern));
        new_point->setZValue(48); //z-value (whether to draw in back or front of scene)
        old_point = scene.addEllipse(p.x()-rad*2, p.y()-rad*2, rad*2*2.0, rad*2*2.0, 
            Qt::NoPen, QBrush(color_old_point,Qt::SolidPattern));
        old_point->setZValue(47); //z-value (whether to draw in back or front of scene)
        
       if(getNumDataPoints()>0){
        //add data points to scene (drawn as little circles)
        //------------------------------------------------  
        //input data points
       rad = pointsize_input;
        for(int i=0; i<input_data_points.size(); i++){
            DataPoint point = input_data_points[i];
            if(!point.isDeleted()){
                //all points are drawn by default in gray, including hidden, points in filtration will be highlighted
                QPointF p = QPointF(point.getX_double(),point.getY_double()); //cast to Qt Point
                //draw Ellipse with specified pen
                QGraphicsEllipseItem * ell_gray = scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                    Qt::NoPen, QBrush(color_delaunay_points,Qt::SolidPattern));
                ell_gray->setZValue(48); //z-value (whether to draw in back or front of scene)
                input_points_items.add(ell_gray);
                scene.addItem(ell_gray);

                //points as balls (radius will be adapted)
                double rad_ball = alpha_complex->getAlpha2_double();
                QGraphicsEllipseItem * point_ball = scene.addEllipse(0., 0., 1.*2.0, 1.*2.0, 
                    Qt::NoPen, QBrush(color_weighted_points_pos,Qt::SolidPattern));
                point_ball->setScale(rad_ball);
                point_ball->setZValue(49); //z-value (whether to draw in back or front of scene)
                union_of_balls_items.add(point_ball); 
                scene.addItem(point_ball);

                //label data points by index     
                QFont font_label = QFont();
                font_label.setPixelSize(labelsize);
                QGraphicsTextItem *text = scene.addText(QString::number(i),font_label);
                text->setFlag(QGraphicsItem::ItemIgnoresTransformations, true); //text is not scaled with the scene (keep font size constant)
                text->document()->setDocumentMargin(0); //no margin!
                text->setPos(point.getX_double(),point.getY_double()); 
                text->setDefaultTextColor(Qt::black);
                text->setZValue(100);
                labels_points_items.add(text);
                scene.addItem(text);

                DataPoint transformed_point = transformed_data_points->at(i);
                QPointF transformed_p = QPointF(transformed_point.getX_double(),transformed_point.getY_double()); //cast to Qt Point
                if(bregman){ //for Bregman-Delaunay complexes also draw transformed points
                    QGraphicsEllipseItem * ell_gray = scene.addEllipse(transformed_p.x()-rad, transformed_p.y()-rad, rad*2.0, rad*2.0, 
                        Qt::NoPen, QBrush(Qt::darkGray,Qt::SolidPattern));
                    ell_gray->setZValue(48); //z-value (whether to draw in back or front of scene)
                    transformed_points_items.add(ell_gray);
                    scene.addItem(ell_gray);

                    QGraphicsTextItem *text_bregman = scene.addText(QString::number(i),font_label);
                    text_bregman->setFlag(QGraphicsItem::ItemIgnoresTransformations, true); //text is not scaled with the scene (keep font size constant)
                    text_bregman->document()->setDocumentMargin(0); //no margin!
                    text_bregman->setPos(transformed_point.getX_double(),transformed_point.getY_double()); 
                    text_bregman->setDefaultTextColor(Qt::black);
                    text_bregman->setZValue(100);
                    labels_points_items_bregman.add(text_bregman);
                    scene.addItem(text_bregman);
                }

                //draw spheres for weighted points
                if(alpha_complex->isWeighted() || bregman){
                    double w = std::sqrt(std::abs(point.getWeight_double()));
                    if(bregman){
                        w = std::sqrt(std::abs(transformed_point.getWeight_double()));
                    }
                    QColor color = color_weighted_points_pos;
                    if((point.getWeight())<0){
                        color = color_weighted_points_neg;
                    }
                    QGraphicsEllipseItem * sphere;
                    if(!bregman){
                        sphere = scene.addEllipse(p.x()-w, p.y()-w, w*2.0, w*2.0,
                            QPen(color,0,Qt::DotLine),QBrush(Qt::white, Qt::NoBrush));
                    }else{
                        //for Bregman-Delaunay, only transformed points are weighted
                        sphere = scene.addEllipse(transformed_p.x()-w, transformed_p.y()-w, w*2.0, w*2.0,
                            QPen(color,0,Qt::DotLine),QBrush(Qt::white, Qt::NoBrush));
                    }
                    sphere->setZValue(90);
                    weighted_point_spheres_items.add(sphere);
                    scene.addItem(sphere);
                }
            }
        }
           
        rad = pointsize;
            
        //draw Delaunay triangulation, Alpha and Wrap complex
        //--------------------
        //add Delaunay triangles to group, add group to scene
        //
        //create graphics item groups for alpha and wrap complex, add all edges and triangles
        //alpha and wrap complexes are shown by changing visibility
        for(int i=0; i<simplices->size(); i++){
            Simplex simplex = simplices->at(i);
            if(!simplex.isDeleted()){
                if(simplex.getDim()==2){
                    //save as Qt polygon
                    QPolygonF triangle;
                    //get triangle points
                    std::vector<DataPoint> tri_points;
                    tri_points.push_back(input_data_points[simplex.getVertex(0)]);
                    tri_points.push_back(input_data_points[simplex.getVertex(1)]);
                    tri_points.push_back(input_data_points[simplex.getVertex(2)]);
                    //if periodic triangulation, need to draw points with offset
                    if(alpha_complex->isPeriodic()){
                        int periodic_size = alpha_complex->getPeriodicSize();
                        for(int i=0; i<3; i++){
                            Offset offset = simplex.getOffset(i);
                            tri_points[i]=DataPoint(tri_points[i].getX_double()+offset.getX()*periodic_size,tri_points[i].getY_double()+offset.getY()*periodic_size);
                        }
                    }
                    triangle << QPointF(tri_points[0].getX_double(),tri_points[0].getY_double()) << QPointF(tri_points[1].getX_double(),tri_points[1].getY_double()) << QPointF(tri_points[2].getX_double(),tri_points[2].getY_double());
                    
                    //also for transformed points in Bregman geometry
                    QPolygonF triangle_bregman;
                    std::vector<DataPoint> tri_points_bregman;
                    if(bregman){
                        tri_points_bregman.push_back(transformed_data_points->at(simplex.getVertex(0)));
                        tri_points_bregman.push_back(transformed_data_points->at(simplex.getVertex(1)));
                        tri_points_bregman.push_back(transformed_data_points->at(simplex.getVertex(2)));
                        //if periodic triangulation, need to draw points with offset
                        if(alpha_complex->isPeriodic()){
                            int periodic_size = alpha_complex->getPeriodicSize();
                            for(int i=0; i<3; i++){
                                Offset offset = simplex.getOffset(i);
                                tri_points_bregman[i]=DataPoint(tri_points_bregman[i].getX_double()+offset.getX()*periodic_size,tri_points_bregman[i].getY_double()+offset.getY()*periodic_size);
                            }
                        }
                        triangle_bregman << QPointF(tri_points_bregman[0].getX_double(),tri_points_bregman[0].getY_double()) << QPointF(tri_points_bregman[1].getX_double(),tri_points_bregman[1].getY_double()) << QPointF(tri_points_bregman[2].getX_double(),tri_points_bregman[2].getY_double());                       
                    }
                    
                    //define graphics item and add to group
                    //Delaunay
                    
                    
                    QColor color_delaunay_triangle = color_delaunay_light;
                    QColor color_delaunay_edge = color_delaunay_dark;
                    if(color_delaunay_by_radius>0){
                        double simplex_radius = CGAL::to_double(alpha_complex->getAlpha2Simplex(simplex.getIndex()));
                        if(color_delaunay_by_radius>1){
                           simplex_radius = CGAL::to_double(alpha_complex->getWrapComplex()->getAlpha2Simplex(simplex.getIndex()));
                        }
                        double min_value = CGAL::to_double(alpha_complex->getMinAlpha2());
                        if(!(alpha_complex->isWeighted()||use_radius2)){
                            simplex_radius = std::sqrt(simplex_radius);
                            min_value = std::sqrt(min_value);
                        }
                        double max_value = maxvalue_plots;
                        if(max_value==-1 || max_value<=min_value){
                            max_value = std::max(CGAL::to_double(alpha_complex->getMaxAlpha2()),0.000001);
                            if(!(alpha_complex->isWeighted()||use_radius2)){
                                max_value = std::sqrt(max_value);
                            }
                        }
                        double color_value = std::max(std::min(std::log(std::min((simplex_radius-min_value)/(max_value-min_value),1.)+1.)/std::log(2),1.),0.)*255.;
                        color_delaunay_triangle = QColor(color_value,color_value,color_value);
                    }
                    QGraphicsPolygonItem* triangle_item_delaunay;
                    if(color_delaunay_by_radius<2 || (alpha_complex->getWrapComplex()->isInComplex(simplex.getIndex()))){ //do not draw if color by Wrap complex and not included
                        triangle_item_delaunay = scene.addPolygon(triangle,QPen(color_delaunay_edge, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_delaunay_triangle, Qt::SolidPattern));
                        triangle_item_delaunay->setZValue(0); 
                        delaunay_items.add(triangle_item_delaunay); 
                        scene.addItem(triangle_item_delaunay);
                    }
                    QGraphicsPolygonItem* triangle_item_convex_hull = scene.addPolygon(triangle,QPen(color_delaunay_light, linewidth*0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_delaunay_light, Qt::SolidPattern));             
                    triangle_item_convex_hull->setZValue(1); 
                    convex_hull_items.add(triangle_item_convex_hull); 
                    scene.addItem(triangle_item_convex_hull);
                    if(bregman){
                        //also for transformed points in Bregman geometry
                        QGraphicsPolygonItem* triangle_item_delaunay_bregman = scene.addPolygon(triangle_bregman,QPen(color_delaunay_edge, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_delaunay_triangle, Qt::SolidPattern));             
                        triangle_item_delaunay_bregman->setZValue(0); 
                        delaunay_items_bregman.add(triangle_item_delaunay_bregman); 
                        scene.addItem(triangle_item_delaunay_bregman);
                    }

                    if(!alpha_complex->isExcluded(simplex.getIndex())){
                        //Alpha
                        QGraphicsPolygonItem* triangle_item_alpha;
                        triangle_item_alpha = scene.addPolygon(triangle,QPen(color_alpha_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_alpha_light, Qt::SolidPattern));     
                        triangle_item_alpha->setZValue(29); //z-value 

                        alpha_items.add(alpha_complex->getAlpha2Simplex(simplex.getIndex()),triangle_item_alpha); //add (together with alpha^2 value as key) to group of alpha complex
                        scene.addItem(triangle_item_alpha); //add to scene
                        
                        if(bregman){
                            //also for transformed points in Bregman geometry
                            QGraphicsPolygonItem* triangle_item_alpha_bregman;
                            triangle_item_alpha_bregman = scene.addPolygon(triangle_bregman,QPen(color_alpha_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_alpha_light, Qt::SolidPattern));     
                            triangle_item_alpha_bregman->setZValue(29); //z-value 
                            alpha_items_bregman.add(alpha_complex->getAlpha2Simplex(simplex.getIndex()),triangle_item_alpha_bregman); //add (together with alpha^2 value as key) to group of alpha complex
                            scene.addItem(triangle_item_alpha_bregman); //add to scene
                        }
                    }
                    //Wrap 
                    if(wrap_complex->isInComplex(simplex.getIndex())){
                        //triangle is in wrap complex for some alpha
                        QGraphicsPolygonItem* triangle_item_wrap = scene.addPolygon(triangle,QPen(color_wrap_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_wrap_light, Qt::SolidPattern));       
                        triangle_item_wrap->setZValue(30); //z-value         
                        wrap_items.add(wrap_complex->getAlpha2Simplex(simplex.getIndex()),triangle_item_wrap); //add (together with alpha^2 value as key) to group of wrap complex
                        scene.addItem(triangle_item_wrap); //add to scene
                        
                        if(bregman){
                            //also for transformed points in Bregman geometry
                            QGraphicsPolygonItem* triangle_item_wrap_bregman = scene.addPolygon(triangle_bregman,QPen(color_wrap_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color_wrap_light, Qt::SolidPattern));       
                            triangle_item_wrap_bregman->setZValue(30); //z-value         
                            wrap_items_bregman.add(wrap_complex->getAlpha2Simplex(simplex.getIndex()),triangle_item_wrap_bregman); //add (together with alpha^2 value as key) to group of wrap complex
                            scene.addItem(triangle_item_wrap_bregman); //add to scene
                        }
                    } 

                    //label Delaunay triangles by interval index     
                    QFont font_label = QFont();
                    font_label.setPixelSize(labelsize);
                    CPoint2 center = CGAL::centroid(CPoint2(tri_points[0].getX(),tri_points[0].getY()),CPoint2(tri_points[1].getX(),tri_points[1].getY()),CPoint2(tri_points[2].getX(),tri_points[2].getY()));
                    QGraphicsTextItem *text = scene.addText(QString::number(wrap_complex->getIntervalIndex(simplex.getIndex())),font_label);
                    text->setFlag(QGraphicsItem::ItemIgnoresTransformations, true); //text is not scales with the scene (keep font size constant)
                    text->document()->setDocumentMargin(0); //no margin!
                    text->setPos(CGAL::to_double(center.x()),CGAL::to_double(center.y())); 
                    text->setDefaultTextColor(Qt::black);
                    text->setZValue(101);
                    labels_intervals_items.add(text);
                    scene.addItem(text);
                    if(bregman){
                        //also for transformed points in Bregman geometry
                        CPoint2 center_bregman = CGAL::centroid(CPoint2(tri_points_bregman[0].getX(),tri_points_bregman[0].getY()),CPoint2(tri_points_bregman[1].getX(),tri_points_bregman[1].getY()),CPoint2(tri_points_bregman[2].getX(),tri_points_bregman[2].getY()));
                        QGraphicsTextItem *text_bregman = scene.addText(QString::number(wrap_complex->getIntervalIndex(simplex.getIndex())),font_label);
                        text_bregman->setFlag(QGraphicsItem::ItemIgnoresTransformations, true); //text is not scales with the scene (keep font size constant)
                        text_bregman->document()->setDocumentMargin(0); //no margin!
                        text_bregman->setPos(CGAL::to_double(center_bregman.x()),CGAL::to_double(center_bregman.y())); 
                        text_bregman->setDefaultTextColor(Qt::black);
                        text_bregman->setZValue(101);
                        labels_intervals_items_bregman.add(text_bregman);
                        scene.addItem(text_bregman);
                    }

                }else if(simplex.getDim()==1){
                    //edges for alpha and wrap complex (not necessary for Delaunay)
                    
                    //save as Qt Line
                    QLineF edge;
                    if(!alpha_complex->isPeriodic()){
                        edge = QLineF(QPointF(input_data_points[simplex.getVertex(0)].getX_double(),input_data_points[simplex.getVertex(0)].getY_double()), QPointF(input_data_points[simplex.getVertex(1)].getX_double(),input_data_points[simplex.getVertex(1)].getY_double()));
                    }else{
                        int periodic_size = alpha_complex->getPeriodicSize();
                        Offset offset0 = simplex.getOffset(0);
                        Offset offset1 = simplex.getOffset(1);
                        QPointF point0 = QPointF(input_data_points[simplex.getVertex(0)].getX_double()+offset0.getX()*periodic_size,input_data_points[simplex.getVertex(0)].getY_double()+offset0.getY()*periodic_size);
                        QPointF point1 = QPointF(input_data_points[simplex.getVertex(1)].getX_double()+offset1.getX()*periodic_size,input_data_points[simplex.getVertex(1)].getY_double()+offset1.getY()*periodic_size);
                        edge = QLineF(point0,point1);
                    }
                    //in bregman geometry there can be crossings, draw Delaunay edges separately to triangles
                    if(bregman){
                        QGraphicsLineItem* edge_item_delaunay = scene.addLine(edge,QPen(color_delaunay_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)); 
                        edge_item_delaunay->setZValue(1); 
                        delaunay_items.add(edge_item_delaunay); 
                        scene.addItem(edge_item_delaunay);
                    }
                    //also for transformed points in Bregman geometry
                    QLineF edge_bregman;
                    if(bregman){
                        if(!alpha_complex->isPeriodic()){
                            edge_bregman = QLineF(QPointF(transformed_data_points->at(simplex.getVertex(0)).getX_double(),transformed_data_points->at(simplex.getVertex(0)).getY_double()), QPointF(transformed_data_points->at(simplex.getVertex(1)).getX_double(),transformed_data_points->at(simplex.getVertex(1)).getY_double()));
                        }else{
                            int periodic_size = alpha_complex->getPeriodicSize();
                            Offset offset0_bregman = simplex.getOffset(0);
                            Offset offset1_bregman = simplex.getOffset(1);
                            QPointF point0_bregman = QPointF(transformed_data_points->at(simplex.getVertex(0)).getX_double()+offset0_bregman.getX()*periodic_size,transformed_data_points->at(simplex.getVertex(0)).getY_double()+offset0_bregman.getY()*periodic_size);
                            QPointF point1_bregman = QPointF(transformed_data_points->at(simplex.getVertex(1)).getX_double()+offset1_bregman.getX()*periodic_size,transformed_data_points->at(simplex.getVertex(1)).getY_double()+offset1_bregman.getY()*periodic_size);
                            edge_bregman = QLineF(point0_bregman,point1_bregman);
                        }
                    }
                    
                    //convex hull edges
                    if((simplex.getCofacetIndices().size())==1){
                        QGraphicsLineItem* edge_item_convex_hull = scene.addLine(edge,QPen(Qt::black, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)); 
                        edge_item_convex_hull->setZValue(38); //z-value   
                        convex_hull_items.add(edge_item_convex_hull);
                        scene.addItem(edge_item_convex_hull);
                    }

                    //define graphics item and add to group
                    if(!alpha_complex->isExcluded(simplex.getIndex())){
                        //Alpha
                        QGraphicsLineItem* edge_item;
                        
                        edge_item = scene.addLine(edge,QPen(color_alpha_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)); 
                        edge_item->setZValue(39); //z-value   
                        alpha_items.add(alpha_complex->getAlpha2Simplex(simplex.getIndex()),edge_item); //add (together with alpha^2 value as key) to group of alpha complex
                        scene.addItem(edge_item); //add to scene 
                        
                        if(bregman){
                            //also for transformed points in Bregman geometry
                            QGraphicsLineItem* edge_item_bregman;
                            edge_item_bregman = scene.addLine(edge_bregman,QPen(color_alpha_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)); 
                            edge_item_bregman->setZValue(39); //z-value 
                            alpha_items_bregman.add(alpha_complex->getAlpha2Simplex(simplex.getIndex()),edge_item_bregman); //add (together with alpha^2 value as key) to group of alpha complex
                            scene.addItem(edge_item_bregman); //add to scene 
                        }
                    }
                    //Wrap
                    if(wrap_complex->isInComplex(simplex.getIndex())){
                        QGraphicsLineItem* edge_item_wrap = scene.addLine(edge,QPen(color_wrap_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));       
                        edge_item_wrap->setZValue(40); //z-value        
                        wrap_items.add(wrap_complex->getAlpha2Simplex(simplex.getIndex()),edge_item_wrap); //add (together with alpha^2 value as key) to group of wrap complex
                        scene.addItem(edge_item_wrap); //add to scene
                        
                        if(bregman){
                            //also for transformed points in Bregman geometry
                            QGraphicsLineItem* edge_item_wrap_bregman = scene.addLine(edge_bregman,QPen(color_wrap_dark, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));       
                            edge_item_wrap_bregman->setZValue(40); //z-value        
                            wrap_items_bregman.add(wrap_complex->getAlpha2Simplex(simplex.getIndex()),edge_item_wrap_bregman); //add (together with alpha^2 value as key) to group of wrap complex
                            scene.addItem(edge_item_wrap_bregman); //add to scene
                        }
                    }
                }else if(simplex.getDim()==0){
                    //points for alpha and wrap complex 
                    
                    QPointF p = QPointF(input_data_points[simplex.getVertex(0)].getX_double(),input_data_points[simplex.getVertex(0)].getY_double()); //cast to Qt Point
                    QPointF p_bregman = QPointF(transformed_data_points->at(simplex.getVertex(0)).getX_double(),transformed_data_points->at(simplex.getVertex(0)).getY_double()); //cast to Qt Point
                    if(!(alpha_complex->isExcluded(simplex.getIndex()))){
                        QGraphicsEllipseItem * ell_alpha = scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                        Qt::NoPen, QBrush(color_points,Qt::SolidPattern));
                        ell_alpha->setZValue(49); //z-value (whether to draw in back or front of scene)
                        alpha_items.add(alpha_complex->getAlpha2Simplex(simplex.getIndex()),ell_alpha);
                        scene.addItem(ell_alpha);
                        
                        if(bregman){
                            QGraphicsEllipseItem * ell_alpha_bregman = scene.addEllipse(p_bregman.x()-rad, p_bregman.y()-rad, rad*2.0, rad*2.0, 
                            Qt::NoPen, QBrush(color_points,Qt::SolidPattern));
                            ell_alpha_bregman->setZValue(49); //z-value (whether to draw in back or front of scene)
                            alpha_items_bregman.add(alpha_complex->getAlpha2Simplex(simplex.getIndex()),ell_alpha_bregman);
                            scene.addItem(ell_alpha_bregman);
                        }
                    }
                    if(wrap_complex->isInComplex(simplex.getIndex())){
                        QGraphicsEllipseItem * ell_wrap = scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                        Qt::NoPen, QBrush(color_points,Qt::SolidPattern));
                        ell_wrap->setZValue(50); //z-value (whether to draw in back or front of scene)
                        wrap_items.add(wrap_complex->getAlpha2Simplex(simplex.getIndex()),ell_wrap);
                        scene.addItem(ell_wrap);
                        
                        if(bregman){
                            QGraphicsEllipseItem * ell_wrap_bregman = scene.addEllipse(p_bregman.x()-rad, p_bregman.y()-rad, rad*2.0, rad*2.0, 
                            Qt::NoPen, QBrush(color_points,Qt::SolidPattern));
                            ell_wrap_bregman->setZValue(50); //z-value (whether to draw in back or front of scene)
                            wrap_items_bregman.add(wrap_complex->getAlpha2Simplex(simplex.getIndex()),ell_wrap_bregman);
                            scene.addItem(ell_wrap_bregman);
                        }
                    }
                }
            }
       }
      }
    
   }else{ //3-dim 
       opengl_widget->setPoints(alpha_complex->getPeriodicSize(),alpha_complex->isWeighted(),bregman,&input_data_points,getNumDataPoints(),transformed_data_points,simplices,wrap_complex->getIntervalIndices(),
               wrap_complex->getFiltration(),wrap_complex->getFiltrationIndexGreaterThanAlpha2(),
               wrap_complex->getNumPointsFiltration(),wrap_complex->getNumEdgesFiltration(),wrap_complex->getNumTrianglesFiltration(),wrap_complex->getNumTetrahedraFiltration(),
               alpha_complex->getFiltration(),alpha_complex->getFiltrationIndexGreaterThanAlpha2(),
               alpha_complex->getNumPointsFiltration(),alpha_complex->getNumEdgesFiltration(),alpha_complex->getNumTrianglesFiltration(),alpha_complex->getNumTetrahedraFiltration(),
               alpha_complex->getFiltrationValues(),wrap_complex->getFiltrationValues());  
   }
    
    
    //draw directed graph of intervals to second drawing plane, if not too large
    //only draw intervals containing a maximal simplex (2-dim: triangle, 3-dim: tetrahedron)
    //--------------------------------------------------------------------------------------
    std::vector<Interval>* intervals = wrap_complex->getIntervals();
    rad=12.; //radius for circles representing nodes = intervals
    int num_max_intervals;
    if(!dim3){
        num_max_intervals=alpha_complex->getNumTrianglesFull();
    }else{
        num_max_intervals=alpha_complex->getNumTetrahedraFull();
    }
   if(num_max_intervals<=intervalgraph_maxsize){
    intervalgraph_drawn=true;
    //draw nodes as vertices of regular polygon
    double polygon_radius = 2*rad*num_max_intervals/M_PI;
    for(int i=0; i<simplices->size(); i++){
        Simplex* max_simplex = &(simplices->at(i));;
        if((dim3 && max_simplex->getDim()==3)|| (!dim3 && max_simplex->getDim()==2)){
            if(!max_simplex->isDeleted()){
                int interval_index = wrap_complex->getIntervalIndex(max_simplex->getIndex());
                Interval interval = intervals->at(interval_index);

                //compute angle
                double angle = 2*M_PI/num_max_intervals *  i;      
                double x = polygon_radius * std::cos(angle);
                double y = polygon_radius * std::sin(angle);
                //draw black if singular interval, otherwise gray
                QColor color;        
                if(interval.isSingular()){
                    color = Qt::black;
                }else{
                    color = Qt::lightGray;
                }
                //position text depending on text length (number of digits)
                double text_offset = 0.6;
                int fontsize0=8;
                if(i>9){
                    text_offset = 0.85;
                }
                if(i>99){
                    text_offset = 1.05;
                    fontsize0=8;
                }
                //draw circle
                QGraphicsEllipseItem * ell = second_scene.addEllipse(x-rad, y-rad, rad*2.0, rad*2.0,QPen(color),QBrush(Qt::white, Qt::SolidPattern));
                ell->setZValue(5);
                //add text
                QGraphicsTextItem *text = second_scene.addText(QString::number(interval_index),QFont("Arial",fontsize0));
                text->setPos(x-rad*text_offset,y-rad*0.95);
                text->setDefaultTextColor(color);
                text->setZValue(10);

                //draw arcs from predecessors
                QPointF end = QPointF(x,y); //start at edge node
                std::set<int> predecessors = interval.getPredecessors();
                for(std::set<int>::iterator it = predecessors.begin(); it != predecessors.end(); ++it){
                    int predecessor_interval_index = (*it);
                    if(predecessor_interval_index < num_max_intervals){
                    //only draw arcs to intervals containing a maximal simplex
                        double angle_predecessor = 2*M_PI/num_max_intervals *  predecessor_interval_index;      
                        double x_predecessor = polygon_radius * std::cos(angle_predecessor);
                        double y_predecessor = polygon_radius * std::sin(angle_predecessor);
                        QPointF start = QPointF(x_predecessor,y_predecessor);
                        QLineF line(start,end);
                        QGraphicsLineItem* line_item = second_scene.addLine(line,QPen(color, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));    
                        line_item->setZValue(1); //low z value -> circles are drawn above it

                        //draw arrow head
                        QPointF line_vec = end-start;
                        line_vec = line_vec*0.6; //do not draw arrow at the end but a bit sooner because of nodes
                        QPointF arrow_tip = start + line_vec;
                        double line_length = std::sqrt(line_vec.x()*line_vec.x()+line_vec.y()*line_vec.y());
                        //compute point at base of arrowhead
                        double t_base = (rad/2) / (std::tan(M_PI/4)*line_length);
                        QPointF arrow_base = arrow_tip - t_base * line_vec;
                        //compute normal vector
                        QPointF normal = QPointF(-line_vec.y(),line_vec.x());
                        double t_normal = (rad/2) / (2*line_length);
                        QPointF arrow_leftstart = arrow_base + t_normal * normal;
                        QPointF arrow_rightstart = arrow_base - t_normal * normal;           
                        QLineF arrow_left(arrow_leftstart,arrow_tip);
                        QLineF arrow_right(arrow_rightstart,arrow_tip);
                        QGraphicsLineItem* line_item_left = second_scene.addLine(arrow_left,QPen(color, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
                        QGraphicsLineItem* line_item_right = second_scene.addLine(arrow_right,QPen(color, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
                        line_item_left->setZValue(20); //draw above nodes
                        line_item_right->setZValue(20); //draw above nodes
                    }
                }
            }
        }
    }
   }else{ //interval graph is not drawn
       intervalgraph_drawn=false; //disable button
   }  
    Q_EMIT(intervalGraphDrawn(intervalgraph_drawn));
    
    //draw (alpha) persistence diagram of Wrap and Alpha complex to third drawing plane
    //-----------------------------------------------------------------------    
    //scale everything to [0,100]
    double alpha_range_length, alpha_min, alpha_max;
    if(!(isWeighted()||use_radius2)){
        alpha_min = std::sqrt(CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = std::sqrt(CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }else{
        alpha_min = (CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = (CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }
    //colors
    double penwidth=0.02;
    QColor color6 = QColor(200,200,200,100); //transparent gray
    
    //draw coordinate axes and diagonal (need to transform coordinates to [x,100-y])
    QLineF line = QLineF(QPointF(0,100-0),QPointF(0,100-100));
    QGraphicsLineItem* line_item1 = third_scene.addLine(line,QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));    
    line_item1->setZValue(1); //low z value -> points are drawn above it
    line = QLineF(QPointF(0,100-0),QPointF(100,100-0));
    QGraphicsLineItem* line_item2 = third_scene.addLine(line,QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));    
    line_item2->setZValue(1); 
    line = QLineF(QPointF(0,100-0),QPointF(100,100-100));
    QGraphicsLineItem* line_item3 = third_scene.addLine(line,QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));    
    line_item3->setZValue(1);  
    //label axes
    QGraphicsTextItem* text1 = third_scene.addText(QString::number(alpha_min),QFont("Arial",5));
    text1->setPos(-15,103);
    QGraphicsTextItem* text2 = third_scene.addText(QString::number(alpha_max,'f',2),QFont("Arial",5));
    text2->setPos(103,103);
    QGraphicsTextItem* text3 = third_scene.addText(QString::number(alpha_max,'f',2),QFont("Arial",5));
    text3->setPos(-25,-20);
    
    //draw persistence diagram for alpha and wrap complex
    drawPersistenceDiag(true,true,false,std::vector<bool>(wrap_complex->getFiltrationFull()->size(),true),-1);
    drawPersistenceDiag(false,true,false,std::vector<bool>(alpha_complex->getFiltrationFull()->size(),true),-1);
    drawPersistenceDiag(true,false,true,std::vector<bool>(wrap_complex->getFiltrationFull()->size(),true),-1); //with status information
    drawPersistenceDiag(false,false,true,std::vector<bool>(alpha_complex->getFiltrationFull()->size(),true),-1);
    //if persistence was adapted, also draw original persistence diagram and connections
    if(alpha_complex->getWrapComplex()->wasPersistenceDiagramAdapted()){
        drawPersistenceDiag(true,false,false,std::vector<bool>(wrap_complex->getFiltrationFull()->size(),true),-1);
        drawPersistenceDiag(false,false,false,std::vector<bool>(alpha_complex->getFiltrationFull()->size(),true),-1);
        
        std::vector<std::pair<PersistencePair,std::pair<exact,exact> > >* manipulated_points_alpha = alpha_complex->getManipulatedPersistencePoints();
        std::vector<std::pair<PersistencePair,std::pair<exact,exact> > >* manipulated_points_wrap = alpha_complex->getWrapComplex()->getManipulatedPersistencePoints();
        Filtration* filtration_alpha = alpha_complex->getFiltrationFull();
        Filtration* filtration_wrap = wrap_complex->getFiltrationFull();
        for(int i=0; i<manipulated_points_alpha->size(); i++){
            std::pair<PersistencePair,std::pair<exact,exact> > manipulated_point = manipulated_points_alpha->at(i);
            std::pair<double,double> start, end;
            if(alpha_complex->isWeighted()||use_radius2){
                start = std::make_pair(manipulated_point.first.getBirth2_double(filtration_alpha),manipulated_point.first.getDeath2_double(filtration_alpha));
                end = std::make_pair(CGAL::to_double(manipulated_point.second.first),CGAL::to_double(manipulated_point.second.second));
            }else{
                start = std::make_pair(manipulated_point.first.getBirth_double(filtration_alpha),manipulated_point.first.getDeath_double(filtration_alpha));
                if(manipulated_point.second.first<0){
                    end = std::make_pair(-1,-1);
                }else{
                    if(manipulated_point.second.second<0){
                        end = std::make_pair(std::sqrt(CGAL::to_double(manipulated_point.second.first)),-1);
                    }else{
                        end = std::make_pair(std::sqrt(CGAL::to_double(manipulated_point.second.first)),std::sqrt(CGAL::to_double(manipulated_point.second.second)));
                    }
                }
            }
            drawPersistenceDiagConnection(start,end,false);
        }
        for(int i=0; i<manipulated_points_wrap->size(); i++){
            std::pair<PersistencePair,std::pair<exact,exact> > manipulated_point = manipulated_points_wrap->at(i);
            std::pair<double,double> start, end;
            if(alpha_complex->isWeighted()||use_radius2){
                start = std::make_pair(manipulated_point.first.getBirth2_double(filtration_wrap),manipulated_point.first.getDeath2_double(filtration_wrap));
                end = std::make_pair(CGAL::to_double(manipulated_point.second.first),CGAL::to_double(manipulated_point.second.second));
            }else{
                start = std::make_pair(manipulated_point.first.getBirth_double(filtration_wrap),manipulated_point.first.getDeath_double(filtration_wrap));
                if(manipulated_point.second.first<0){
                    end = std::make_pair(-1,-1);
                }else{
                    if(manipulated_point.second.second<0){
                        end = std::make_pair(std::sqrt(CGAL::to_double(manipulated_point.second.first)),-1);
                    }else{
                        end = std::make_pair(std::sqrt(CGAL::to_double(manipulated_point.second.first)),std::sqrt(CGAL::to_double(manipulated_point.second.second)));
                    }
                }
            }
            drawPersistenceDiagConnection(start,end,true);
        }
    }
    
    double alpha = getAlpha2Current();
    if(!(isWeighted()||use_radius2)){
        alpha = std::sqrt(getAlpha2Current());
    }
    persistence_diag_subcomplex_box = third_scene.addRect(0,0,(alpha-alpha_min)*100./alpha_range_length,100-(alpha-alpha_min)*100./alpha_range_length,
            QPen(color6,penwidth),QBrush(color6,Qt::SolidPattern));
    persistence_diag_subcomplex_box->setZValue(0);
    
    //draw statistics plots (for whole filtration) in fourth window
    //-----------------------------------------------------------------------
   if(getNumDataPoints()>0){
    //set up axes, scale everything to [0,100], i.e. multiply by 100/max
    //draw coordinate axes  (need to transform coordinates to [x,100-y])
    line = QLineF(QPointF(0,100-0),QPointF(0,100-100));
    QGraphicsLineItem* line_item = fourth_scene.addLine(line,QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));    
    line_item->setZValue(1); //low z value -> points are drawn above it
    line = QLineF(QPointF(0,100-0),QPointF(100,100-0));
    line_item = fourth_scene.addLine(line,QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));    
    line_item->setZValue(1); 
    //label axes
    text1 = fourth_scene.addText(QString::number(alpha_min),QFont("Arial",5));
    text1->setPos(0,103);
    text2 = fourth_scene.addText(QString::number(alpha_max,'f',2),QFont("Arial",5));
    text2->setPos(103,103);
    
    //define plot titles depending on dimension
    statistics_plots_titles.push_back(QString("# Alpha edges"));
    statistics_plots_titles.push_back(QString("# Alpha triangles"));
    if(dim3) statistics_plots_titles.push_back(QString("# Alpha tetrahedra"));
    statistics_plots_titles.push_back(QString("# Wrap edges"));
    statistics_plots_titles.push_back(QString("# Wrap triangles"));
    if(dim3) statistics_plots_titles.push_back(QString("# Wrap tetrahedra"));
    statistics_plots_titles.push_back(QString("# neg critical edges"));
    statistics_plots_titles.push_back(QString("# pos critical edges"));
    statistics_plots_titles.push_back(QString("# neg critical triangles"));
    if(dim3) statistics_plots_titles.push_back(QString("# pos critical triangles"));
    if(dim3) statistics_plots_titles.push_back(QString("# critical tetrahedra"));
    statistics_plots_titles.push_back(QString("Betti 0"));
    statistics_plots_titles.push_back(QString("Betti 1"));
    if(dim3) statistics_plots_titles.push_back(QString("Betti 2"));
    
    //draw vertical line to indicate current filtration value
    double current_alpha;
    if(!(isWeighted()||use_radius2)){
        current_alpha = std::sqrt(alpha_complex->getAlpha2_double());
    }else{
        current_alpha = alpha_complex->getAlpha2_double();
    }
    if(current_alpha > alpha_max)
    {
        current_alpha = alpha_max*1.05;
    }
    if(alpha_range_length>0){
        line = QLineF(QPointF((current_alpha-alpha_min)*100./alpha_range_length,100-0),QPointF((current_alpha-alpha_min)*100./alpha_range_length,100-100));
    }else{
        line = QLineF(QPointF(0,100-0),QPointF(0,100-100));
    }
    statistics_plots_currentvalue = fourth_scene.addLine(line,QPen(Qt::red, 0.3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    statistics_plots_currentvalue->setZValue(2);  
    
   //draw all plots
    for(int j=0; j<statistics_plots_titles.size(); j++){
        Filtration* filtration;
        std::vector<int>* data_filtration;
        std::vector<bool>* included;
        std::unordered_map<int,std::pair<exact,int> >* inclusion_values_and_counter;
        //get data (for each title)
        if(statistics_plots_titles[j]==QString("# Alpha edges")){
            data_filtration = alpha_complex->getNumEdgesFiltration();
            filtration = alpha_complex->getFiltration();
            included = alpha_complex->getIncluded();
            inclusion_values_and_counter = alpha_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# Alpha triangles")){
            data_filtration = alpha_complex->getNumTrianglesFiltration();
            filtration = alpha_complex->getFiltration();
            included = alpha_complex->getIncluded();
            inclusion_values_and_counter = alpha_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# Alpha tetrahedra")){
            data_filtration = alpha_complex->getNumTetrahedraFiltration();
            filtration = alpha_complex->getFiltration();
            included = alpha_complex->getIncluded();
            inclusion_values_and_counter = alpha_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# Wrap edges")){
            data_filtration = wrap_complex->getNumEdgesFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# Wrap triangles")){
            data_filtration = wrap_complex->getNumTrianglesFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# Wrap tetrahedra")){
            data_filtration = wrap_complex->getNumTetrahedraFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# neg critical edges")){
            data_filtration = wrap_complex->getNumCriticalEdgesNegFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# pos critical edges")){
            data_filtration = wrap_complex->getNumCriticalEdgesPosFiltration(); 
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# neg critical triangles")){
            data_filtration = wrap_complex->getNumCriticalTrianglesNegFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# pos critical triangles")){
            data_filtration = wrap_complex->getNumCriticalTrianglesPosFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("# critical tetrahedra")){
            data_filtration = wrap_complex->getNumCriticalTetrahedraFiltration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("Betti 0")){
            data_filtration = wrap_complex->getBetti0Filtration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("Betti 1")){
            data_filtration = wrap_complex->getBetti1Filtration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }else if(statistics_plots_titles[j]==QString("Betti 2")){
            data_filtration = wrap_complex->getBetti2Filtration();
            filtration = wrap_complex->getFiltration();
            included = wrap_complex->getIncluded();
            inclusion_values_and_counter = wrap_complex->getInclusionValuesAndCounter();
        }
        //draw path for plot
        QPainterPath plot;
        QPainterPath plot_log;
        plot.moveTo(0,100); //start at origin 
        plot_log.moveTo(0,100); 
        //use max alpha critical for Wrap complex as x-axis limit for both Wrap and Alpha data
        int filtration_index_max = filtration->getIndexGreaterThanAlpha2(wrap_complex->getMaxAlpha2(),alpha_complex->getSimplices(),alpha_complex->getCritical(),included,inclusion_values_and_counter);
        double max_data = 1.;
        if(data_filtration->size()>=filtration_index_max){
            //find data maximum
            if(statistics_plots_titles[j]==QString("Betti 0")){
                max_data = getNumDataPoints();
            }else if(statistics_plots_titles[j]==QString("Betti 1") || statistics_plots_titles[j]==QString("Betti 2")){
                for(int i=0; i<filtration_index_max; i++){
                    if((data_filtration->at(i))>max_data){
                        max_data = (data_filtration->at(i));
                    }
                }
            }else{
                max_data = data_filtration->at(filtration_index_max-1);
            }
            //draw statistics over filtration values
            if(alpha_range_length>0){
                for(int i=1; i<filtration_index_max; i++){
                    double filtration_value;
                    if(!(isWeighted()||use_radius2)){
                        filtration_value = std::sqrt(CGAL::to_double(filtration->getElement(i).first));
                    }else{
                        filtration_value = CGAL::to_double(filtration->getElement(i).first);
                    } 
                    plot.lineTo((filtration_value-alpha_min)*100./alpha_range_length,100.-data_filtration->at(i)*100./max_data);
                    plot_log.lineTo((filtration_value-alpha_min)*100./alpha_range_length,100.-100.*std::log(1.+data_filtration->at(i))/std::log(1.+max_data));
                }
            }
        }
        //add graphics items to scene and lists
        //axis label
        QGraphicsTextItem* yaxis_label = fourth_scene.addText(QString::number(max_data),QFont("Arial",5)); //y-axis label
        yaxis_label->setPos(-25,-20);
        statistics_plots_axislabel.push_back(yaxis_label);
        //plot path
        QGraphicsPathItem* path_item = fourth_scene.addPath(plot,QPen(Qt::blue, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        path_item->setZValue(3); 
        statistics_plots.push_back(path_item);
        QGraphicsPathItem* path_log_item = fourth_scene.addPath(plot_log,QPen(Qt::blue, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        path_item->setZValue(3); 
        statistics_plots_log.push_back(path_log_item);
    }
   }
    displayDrawing();
    
    Q_EMIT(plotsChanged());
    
    Q_EMIT(sceneDrawn());
}

//draw persistence diagram to third scene, for wrap or alpha complex, adapted or original
void Geometry::drawPersistenceDiag(bool wrap, bool adapted, bool fill_by_status, std::vector<bool> draw_pair_of_birth_simplex, int highlight_point_birth_index)
{               
    std::vector<PersistencePair>* persistence_pairs;
    Filtration* filtration;
    //for wrap complex
    if(wrap){
        if(adapted){
            persistence_pairs = wrap_complex->getPersistencePairsCurrent();
            filtration = wrap_complex->getFiltration();
        }else{
            persistence_pairs = wrap_complex->getPersistencePairsOriginal();
            filtration = wrap_complex->getFiltrationFull();
        }
    //for Alpha complex
    }else{ 
        if(adapted){
            persistence_pairs = alpha_complex->getPersistencePairsCurrent();
            filtration = alpha_complex->getFiltration();
        }else{
            persistence_pairs = alpha_complex->getPersistencePairsOriginal();
            filtration = alpha_complex->getFiltrationFull();
        }
    }
    
    //scale everything to [0,100]
    double alpha_range_length, alpha_min, alpha_max;
    if(!(isWeighted()||use_radius2)){
        alpha_min = std::sqrt(CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = std::sqrt(CGAL::to_double(wrap_complex->getMaxAlpha2()));
        //use given maximum coordinate value to zoom in
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }else{
        alpha_min = (CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = (CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }
    double min_pers_drawn = persdiag_minpers_rel * alpha_range_length;
    //colors
    double rad = pointsize_persdiag;
    double penwidth=0.02;
    //QColor color1 = QColor(0,0,0,100); //transparent black
    QColor color2 = QColor(255,0,0,100); //transparent red
    QColor color3 = QColor(0,0,255,100); //transparent blue
    QColor color4 = QColor(0,150,0,100); //transparent green
    QColor color5 = QColor(255,120,0,100); //transparent orange
    //QColor color6 = QColor(200,200,200,100); //transparent gray
    
    //drawing takes very long if there are a lot persistence pairs, set a threshold
    //if we have too many pairs only those with persistence above a certain threshold (relative to maximal alpha^2 of Wrap complex) are drawn
    if(wrap){
        persDiag_wrap_fulldrawn = true;
    }else{
        persDiag_alpha_fulldrawn = true;
    }
    if(persistence_pairs->size()>persdiagfull_maxpairs){
        //too many pairs, only draw those with persistence above threshold min_pers_drawn*max
        if(wrap){
            persDiag_wrap_fulldrawn = true;
        }else{
            persDiag_alpha_fulldrawn = true;
        }
    }
    
   if(alpha_range_length>0){
    //iterate through persistence pairs of (full) complex, draw points for persistence diagram
    for(int i=0; i<persistence_pairs->size(); i++){
        PersistencePair persistence_pair = persistence_pairs->at(i);
        if(persistence_pair.getDim()>=0){ //do not draw point for (-1)-pair
            if(draw_pair_of_birth_simplex[persistence_pair.getBirthIndexFiltration()]){
                double birth_draw;
                if(!(isWeighted()||use_radius2)){
                    birth_draw = persistence_pair.getBirth_double(filtration);
                }else{
                    birth_draw = persistence_pair.getBirth2_double(filtration);
                }
                int status = alpha_complex->getStatusOfCorrespondingPersistencePair(persistence_pair.getBirthIndexFiltration());
                //for every pair add point(s) to the persistence diagram item group, will be filtered by current alpha^2 value, specify lower (and upper) bound
                if(persistence_pair.isInfinite()){
                    //infinite pair (birth, - ) 
                    //add "infinite" point (drawn at upper edge of diagram) with lower bound
                    double inf = 105.; //"infinite"
                    QPointF p((birth_draw-alpha_min)*100./alpha_range_length,100-inf);
                    //draw point and add to filtered item group
                    QGraphicsEllipseItem * point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                            QPen(color2,penwidth), QBrush(color2,Qt::SolidPattern));
                    point->setZValue(3);
                    if(wrap){
                        if(adapted){
                            persistence_diag_wrap_items_adapted.add(point);
                        }else if(fill_by_status){
                            persistence_diag_wrap_items_status.add(point);
                        }else{
                            persistence_diag_wrap_items_original.add(point);
                        }
                    }else{
                        if(adapted){
                            persistence_diag_alpha_items_adapted.add(point);
                        }else if(fill_by_status){
                            persistence_diag_alpha_items_status.add(point);
                        }else{
                            persistence_diag_alpha_items_original.add(point);
                        }   
                    }  
                    third_scene.addItem(point);
                }else{
                    //finite pair (birth, death)
                    double death_draw;
                     if(!(isWeighted()||use_radius2)){
                        death_draw = persistence_pair.getDeath_double(filtration);
                    }else{
                        death_draw = persistence_pair.getDeath2_double(filtration);
                    }
                    //only draw if persistence is above threshold
                    if((!persistence_pair.hasZeroPersistence(filtration) || (fill_by_status && status ==1)) && (persDiag_alpha_fulldrawn || (death_draw-birth_draw)>min_pers_drawn)){
                        if(adapted){
                            //add highlighted point with lower and upper bound for subcomplex
                            double inf = 105.;
                            QPointF p((birth_draw-alpha_min)*100./alpha_range_length,100-(death_draw-alpha_min)*100./alpha_range_length);
                            QGraphicsEllipseItem * point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                            QPen(color2,penwidth), QBrush(color2,Qt::SolidPattern));
                            point->setZValue(3);
                            if(wrap){
                                persistence_diag_wrap_items_subcomplex.add(persistence_pair.getBirth2(filtration),persistence_pair.getDeath2(filtration),point); 
                            }else{
                                persistence_diag_alpha_items_subcomplex.add(persistence_pair.getBirth2(filtration),persistence_pair.getDeath2(filtration),point);
                            }
                            third_scene.addItem(point);
                        }
                        //add colored point for full persistence diagram
                        QPointF p = QPointF((birth_draw-alpha_min)*100./alpha_range_length,100-(death_draw-alpha_min)*100./alpha_range_length);
                        QColor color = color3; //blue
                        int zvalue = 2;
                        if(persistence_pair.getDim()==2){
                            color = color4; //green
                        }else if(persistence_pair.getDim()==0){
                            color = color5; //orange
                        }
                        //highlight main persistence pair (dependences shown for this one, last element of given vector) 
                        if(highlight_point_birth_index>=0 && persistence_pair.getBirthIndexFiltration()==highlight_point_birth_index){
                            color = Qt::black;
                            rad = 1.5*pointsize_persdiag;
                        }
                        QGraphicsItem *point;
                        if(!fill_by_status || status == 0){ //normal point, past
                            point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                            QPen(color,penwidth*5), QBrush(color,Qt::SolidPattern));
                        }else if(status == 1){ //presence
                            point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                            QPen(color,penwidth*40), QBrush(color,Qt::NoBrush));
                        }else{ //future
                            point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                            QPen(color,penwidth*40,Qt::DotLine), QBrush(color,Qt::NoBrush));
                        }
                        rad = pointsize_persdiag;
                        point->setZValue(zvalue);     
                        if(wrap){
                            if(adapted){
                                persistence_diag_wrap_items_adapted.add(point);
                            }else if(fill_by_status){
                                persistence_diag_wrap_items_status.add(point);
                            }else{
                                persistence_diag_wrap_items_original.add(point);
                            }
                        }else{
                            if(adapted){
                                persistence_diag_alpha_items_adapted.add(point);
                            }else if(fill_by_status){
                                persistence_diag_alpha_items_status.add(point);
                            }else{
                                persistence_diag_alpha_items_original.add(point);
                            }   
                        }  
                        third_scene.addItem(point);
                    }
                }
            }
        }
    } 
   }
}

//draw connecting arrow between points in old and new persistence diagram
void Geometry::drawPersistenceDiagConnection(std::pair<double,double> start, std::pair<double,double> end, bool wrap)
{
    QColor color = Qt::gray;
    //scale everything to [0,100]
    double alpha_range_length, alpha_min, alpha_max;
    if(!(isWeighted()||use_radius2)){
        alpha_min = std::sqrt(CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = std::sqrt(CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }else{
        alpha_min = (CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = (CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }
    
    QPointF start_point = QPointF((start.first-alpha_min)*100./alpha_range_length,100-(start.second-alpha_min)*100./alpha_range_length);
    if(end.first>=0 && end.first != end.second){
        QPointF end_point;
        if(end.second>=0){
            //move to finite point
            end_point = QPointF((end.first-alpha_min)*100./alpha_range_length,100-(end.second-alpha_min)*100./alpha_range_length);
        }else{
            //move to infinite point
            end_point = QPointF((end.first-alpha_min)*100./alpha_range_length,100-105);
        }
        QLineF line = QLineF(start_point,end_point);
        QGraphicsLineItem* line_item = third_scene.addLine(line,QPen(color, 1., Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        line_item->setZValue(5);
        if(wrap){
            persistence_diag_connections_wrap.add(line_item);
        }else{
            persistence_diag_connections_alpha.add(line_item);
        }
        third_scene.addItem(line_item);
        //create arrow head
        QLineF head1, head2;
        head1.setP1(end_point); //set origin
        head2.setP1(end_point);
        head1.setAngle(line.angle()-150); //set angle
        head2.setAngle(line.angle()+150);
        head1.setLength(pointsize_persdiag*1.5); //set length
        head2.setLength(pointsize_persdiag*1.5);
        QGraphicsLineItem* head1_item = third_scene.addLine(head1,QPen(color, 1., Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        QGraphicsLineItem* head2_item = third_scene.addLine(head2,QPen(color, 1., Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        head1_item->setZValue(5);
        head2_item->setZValue(5);
        if(wrap){
            persistence_diag_connections_wrap.add(head1_item);
            persistence_diag_connections_wrap.add(head2_item);
        }else{
            persistence_diag_connections_alpha.add(head1_item);
            persistence_diag_connections_alpha.add(head2_item);
        }
        third_scene.addItem(head1_item);
        third_scene.addItem(head2_item);
        
    }else{
        //point is deleted
        double off = pointsize_persdiag*1.5;
        QLineF cross1 = QLineF(QPointF(start_point.x()-off,start_point.y()-off),QPointF(start_point.x()+off,start_point.y()+off));
        QLineF cross2 = QLineF(QPointF(start_point.x()-off,start_point.y()+off),QPointF(start_point.x()+off,start_point.y()-off));
        QGraphicsLineItem* cross1_item = third_scene.addLine(cross1,QPen(color, 1., Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        QGraphicsLineItem* cross2_item = third_scene.addLine(cross2,QPen(color, 1., Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        cross1_item->setZValue(5);
        cross2_item->setZValue(5);
        if(wrap){
            persistence_diag_connections_wrap.add(cross1_item);
            persistence_diag_connections_wrap.add(cross2_item);
        }else{
            persistence_diag_connections_alpha.add(cross1_item);
            persistence_diag_connections_alpha.add(cross2_item);
        }
        third_scene.addItem(cross1_item);
        third_scene.addItem(cross2_item);
    }
}

//display currently drawn scene, respecting bool variables
void Geometry::displayDrawing()
{
    clearHighlightedItems();
    
    //draw, set visibility of graphics items
    drawDelaunay(delaunay_bool);  //show/hide Delaunay triangulation
    drawAlpha(alpha_bool); //show/hide Alpha complex
    drawWrap(wrap_bool); //show/hide Wrap complex
    showLabelsIntervals(labels_intervals_bool); //show/hide triangle labels
    showLabelsPoints(labels_points_bool); //show/hide points labels
    showWeightedPointSpheres(weighted_point_spheres_bool); //weighted points as spheres
    //filter persistence diagram
    changePersDiag(persDiag_wrap_bool,persDiag_adapted_bool,persDiag_status_bool);
    
    if(dim3){
        update();
    }
}

//redraw scene with new parameters
void Geometry::setDrawingParameters(double pointsize_, double pointsize_input_, double linewidth_, double labelsize_, double pointsize_persdiag_, double maxvalue_persdiag_, int color_delaunay_by_radius_, bool draw)
{
    pointsize_persdiag=pointsize_persdiag_;
    maxvalue_plots=maxvalue_persdiag_;
    opengl_widget->setMaxValueColorGradient(maxvalue_plots);
    color_delaunay_by_radius = color_delaunay_by_radius_;
    opengl_widget->setColorByRadius((color_delaunay_by_radius>0));
        
    if(!dim3){ //2-dim
        pointsize=pointsize_;
        pointsize_input=pointsize_input_;
        linewidth=linewidth_;
        labelsize=labelsize_;
    }else{ //3-dim
        opengl_widget->setDrawingParameters(pointsize_, pointsize_input_, linewidth_, labelsize_, false);
    }
    if(draw){
        drawScene();
    }
}

//export points to given file
void Geometry::exportPointsToFile(std::string filename){
    std::ofstream myfile(filename.c_str());
        
    if(!dim3){
        myfile << "2" << std::endl;
    }else{
        myfile << "3" << std::endl;
    }
    //write point coordinates to file
    for(int i=0;i<input_data_points.size();i++){
        DataPoint point = input_data_points[i];
        if(!point.isDeleted()){
            myfile << point.getX() << " " << point.getY();
            if(dim3){
                myfile << " " << point.getZ();
            }
            if(alpha_complex->isWeighted()){
                myfile << " " << point.getWeight();
            }
            myfile << std::endl;
        }
    }
}

//generate point set from Poisson point process in window [0,size]^2 with given parameter, repeat given number of times, optionally draw last trial, optionally periodic point set (on flat torus), optionally in Bregman geometry, perturbed point set and more
void Geometry::generatePointsPPP(double window_size, double lambda, bool fixed_number, int ntrials, bool weighted, double min_weight, double max_weight, bool draw, bool periodic, bool dim3_, bool on_standard_simplex_, bool uniform_for_fisher, bool compute_persistence_and_statistics, std::string test, double test_value)
{
    dim3=dim3_||on_standard_simplex_;
    on_standard_simplex=on_standard_simplex_;
    
    double time1 = getCPUTime();  
    
    //Initialize
    double volume = window_size*window_size; //2-dimensional window
    if(on_standard_simplex){
        volume = std::sqrt(3)/2.; //standard 2-simplex
        window_size = 1;
        periodic = false;
    }else if(dim3){
        volume = volume * window_size; //3-dimensional window
    }
    
    //seed random number generator
    std::mt19937 mrand(std::time(0));
    //std::default_random_engine generator; //pseudo-random number generator
    
    //number of points in window is random Poisson variable with parameter lambda*volume
    std::poisson_distribution<int> poisson_dist(lambda*volume);
    //points are uniformly distributed in the window, weights are uniformly distributed in given interval
    std::uniform_real_distribution<double> uniform_dist(0.,1.);
    //we need standard normal distribution for sampling on sphere
    std::normal_distribution<> normal_dist{0,1};
    
    std::vector<DataPoint> generated_points;
    
    //open output file
    std::ostringstream ss_size, ss_ntrials;
    ss_size << window_size;
    ss_ntrials << ntrials;
    std::string s = "../output/output";
    if(!dim3){
        s = s + "2";
    }else{
        s = s + "3";
    }
    s = s + "_" + getCurrentDate() + "_" + ss_size.str() + "_" + ss_ntrials.str();
    if(periodic){
        s = s + "_p";
    }
    s=s+".txt";
    char const* filename = s.c_str();
    std::ofstream myfile(filename);

    std::cout << s << std::endl;
    
    //redirect std::cout output to file
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
    std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile   
    
    if(fixed_number){
        std::cout  << "RANDOM POINTS" << std::endl;
    }else{
        std::cout << "POISSON POINT PROCESS" << std::endl;
    }
    
    
    std::cout <<  std::setw(8) << std::right << "# trials" << ", " << std::setw(3) << std::right << "dim" << ", ";
    if(fixed_number){
        std::cout << std::setw(10) << std::right << "# points" << ", ";
    }else{
        std::cout << std::setw(11) << std::right << "window size" << ", " << std::setw(10) << std::right << "lambda" << ", " ;
    }
    std::cout << std::setw(8) << std::right << "periodic" << std::endl;
    std::cout <<  std::setw(8) << std::right << ntrials << ", ";
    if(on_standard_simplex){
        std::cout << std::setw(3) << std::right << "2-simplex";
    }else if(dim3){
        std::cout << std::setw(3) << std::right << "3";
    }else{
        std::cout << std::setw(3) << std::right << "2";
    }
    if(fixed_number){
        std::cout << ", " << std::setw(10) << std::right << lambda << ", ";
    }else{
        std::cout << ", " << std::setw(11) << std::right << window_size << ", " << std::setw(10) << std::right << lambda << ", ";
    }
    std::cout << std::setw(8) << std::right << periodic << std::endl;
    
    int count_invalid = 0;
    bool repeated = false;
    
    //generate points, compute complexes, repeat 
    for(int t = 0; t<ntrials; t++){      
        std::cout << std::endl;
        if(!repeated){
            std::cout << "TRIAL, " << t << std::endl;
        }
        repeated = false;
        int num_data_points;
        if(fixed_number){
            num_data_points = lambda;
        }else{
            num_data_points = poisson_dist(mrand);
        }
        
        for(int i = 0; i<num_data_points; i++){
            if(!dim3){
                double randx, randy;
                if(!uniform_for_fisher){
                    randx = uniform_dist(mrand)*window_size; 
                    randy = uniform_dist(mrand)*window_size;
                }else{
                    randx = std::pow(uniform_dist(mrand)*std::sqrt(window_size),2); 
                    randy = std::pow(uniform_dist(mrand)*std::sqrt(window_size),2);
                }
                generated_points.push_back(DataPoint(randx,randy));
            }else{
                exact randx,randy,randz;
                if(!on_standard_simplex){
                    if(!uniform_for_fisher){
                        randx = uniform_dist(mrand)*window_size; 
                        randy = uniform_dist(mrand)*window_size;
                        randz = uniform_dist(mrand)*window_size;
                    }else{
                        randx = std::pow(uniform_dist(mrand)*std::sqrt(window_size),2); 
                        randy = std::pow(uniform_dist(mrand)*std::sqrt(window_size),2);
                        randz = std::pow(uniform_dist(mrand)*std::sqrt(window_size),2);
                    }
                }else{
                    //uniformly sample on standard 2-simplex
                    if(!uniform_for_fisher){
                        //split the interval [0,1] into 3 parts {0,split1,split2,1}
                        exact split1 = uniform_dist(mrand);
                        exact split2 = uniform_dist(mrand);
                        if(split1>split2){
                            exact tmp = split2;
                            split2 = split1;
                            split1 = tmp;
                        }
                        //take 1-split2, split2-split1, split1 as coordinates
                        randx = 1-split2;
                        randy = split2-split1;
                        randz = split1;
                    //uniformly sample on corresponding Fisher sphere (positive part of sphere of radius sqrt(2))
                    }else{
                        randx=-1;
                        randy=-1;
                        randz=-1;
                        //only use points in positive orthant
                        while(randx<0 || randy<0 || randz<0){
                            //get coordinates sampled with standard normal distribution
                            randx = normal_dist(mrand);
                            randy = normal_dist(mrand);
                            randz = normal_dist(mrand);
                        }
                        //normalize point to sqrt(2)-sphere (*sqrt(2)/norm(v)) and further to standard 2-simplex (.^2                                                                    /2)
                        exact norm2 = randx*randx+randy*randy+randz*randz;
                        randx=randx*randx/norm2;
                        randy=randy*randy/norm2;
                        //randz=randz*randz /norm2;
                        randz=1-randx-randy; //points should lie EXACTLY on simplex
                    }
                }
                generated_points.push_back(DataPoint(false,randx,randy,randz));
            }          
            if(weighted){
                double weight = uniform_dist(mrand)*(max_weight-min_weight)+min_weight;
                generated_points[generated_points.size()-1].setWeight(weight);
            }
        }
        int periodic_size=-1;
        if(periodic){
            periodic_size=window_size;
        }       
        clearPoints(); //clear old points (properly deleted)
        bool valid = false;
        if(test == "bregman_halfEuclidean" || test == "bregman_Shannon" || test == "bregman_conjugateShannon"){
            bregman = true;
            Q_EMIT(bregmanChanged(true));
            if(on_standard_simplex){
                bregman_complexes->setOnStandardSimplex(on_standard_simplex);
            }
            
            if(test == "bregman_halfEuclidean"){
                bregman_complexes->setType("halfEuclidean");
            }else if(test == "bregman_Shannon"){
                bregman_complexes->setType("Shannon");
            }else if(test == "bregman_conjugateShannon"){
                bregman_complexes->setType("conjugateShannon");
            }
            clearScene();
            input_data_points = generated_points;
            if(input_data_points.size()>0){
                dim3 = (input_data_points[0].getDim()==3);
            }  
            
            valid = bregman_complexes->computeBregmanDelaunay(&input_data_points,false,input_data_points.size(),periodic_size,dim3,compute_persistence_and_statistics,true);
            
            
            //use Bregman complexes as Alpha and Wrap complex
            alpha_complex = (Alpha_Complex*)bregman_complexes;
            wrap_complex = alpha_complex->getWrapComplex();
            
            if(compute_persistence_and_statistics){
                alpha_complex->computeFiltrationStatistics(false,false);
                alpha_complex->getWrapComplex()->computeFiltrationStatistics(false,false);
            }
            
  
        }else{
            valid = setPoints(generated_points,false,periodic_size,weighted,"",compute_persistence_and_statistics);
        }

        
        //for periodic point sets the result might be invalid
        if(valid){

            //TEST
            //exportPointsToFile("../output/output/points.txt");

            if(test == "holes_paper"){
                
                alpha_complex->computeResultsPaperHoles(std::cout);
                
            }else if(test == "perturb_filtration"){
                //perturb with Gaussian noise with standard deviation of test_value*window_size
                std::normal_distribution<> gaussian_dist{0,test_value*window_size};
                std::vector<exact>* filtration_values = alpha_complex->getFiltrationValues();
                std::vector<exact> perturbed_filtration;
                for(int i=0; i<filtration_values->size(); i++){
                    //we add Gaussian noise
                    double noise = gaussian_dist(mrand);
                    perturbed_filtration.push_back(filtration_values->at(i)+noise);
                }
                
                //store old values to compare
                std::vector<bool> old_critical(alpha_complex->getCritical()->begin(),alpha_complex->getCritical()->end());
                std::vector<exact> old_filtration_values(filtration_values->begin(),filtration_values->end());
                std::vector<std::vector<int> > old_interval_faces(alpha_complex->getIntervalFaces()->begin(),alpha_complex->getIntervalFaces()->end());
                
                //reconstruct intervals, adjust filtration values
                
                //without reconsidering blocked intervals
                exact threshold = alpha_complex->computeIntervalsFromNoisyFiltration(&perturbed_filtration,-1,weighted,false,false,false,true,true,false); 
                //with reconsidering blocked intervals
                alpha_complex->computeIntervalsFromNoisyFiltration(&perturbed_filtration,-1,weighted,false,false,true,true,true,false); 
                
                //compare reconstructed and original intervals
                int count_changes = 0;
                for(int i=0; i<alpha_complex->getSimplices()->size(); i++){
                    std::vector<int> reconstructed_interval_faces_i = alpha_complex->getIntervalFaces()->at(i);
                    bool same_interval_faces = (reconstructed_interval_faces_i.size() == old_interval_faces[i].size());
                    std::sort(reconstructed_interval_faces_i.begin(),reconstructed_interval_faces_i.end());
                    std::sort(old_interval_faces[i].begin(),old_interval_faces[i].end());
                    if(same_interval_faces){
                        for(int j=0; j<old_interval_faces[i].size(); j++){
                            if(old_interval_faces[i][j]!=reconstructed_interval_faces_i[j]){
                                same_interval_faces = false;
                                break;
                            }
                        }
                    }
                    if(old_critical[i]!=alpha_complex->getCritical()->at(i) || !same_interval_faces){
                        count_changes++;
                        (alpha_complex->getSimplices()->at(i)).printShort();
                        std::cout << "value " << old_filtration_values[i] << " -> " << alpha_complex->getFiltrationValues()->at(i) << ", critical " << old_critical[i] << " -> " << alpha_complex->getCritical()->at(i);
                        std::cout << ", interval_faces ";
                        for(int j=0; j<old_interval_faces[i].size(); j++){
                            std::cout << old_interval_faces[i][j] << " ";
                        }
                        std::cout << "-> ";
                        for(int j=0; j<reconstructed_interval_faces_i.size(); j++){
                            std::cout << reconstructed_interval_faces_i[j] << " ";
                        };
                        std::cout << std::endl;
                    }
                }
                std::cout << std::endl;
                std::cout << "threshold: " << threshold << std::endl;
                std::cout << count_changes << "/" << alpha_complex->getSimplices()->size() << " (" << ((double)count_changes/(double)alpha_complex->getSimplices()->size())*100. << "%) simplices changed" << std::endl;
            
            }else if(test=="dynamic_runtime"){    
                
                //dynamic point addition and deletion

                double randx = uniform_dist(mrand)*window_size; 
                double randy = uniform_dist(mrand)*window_size;
                double randz = uniform_dist(mrand)*window_size;
                std::cout << "ADD RANDOM POINT" << std::endl;
                addPoint(randx,randy,randz,false,true,false);
                std::cout << "DELETE POINT 0" << std::endl;
                deletePoint(0,false,true,false);
                
                
            }else{
                
                wrap_complex->computeIntervalStatisticsPaper();
                 
            }

                
                //TEST export points
                //exportPointsToFile(s);
                
                 //TEST compare output strings (formerly used for opening of holes sequentially or simultaneously)
                /*std::stringstream ss1, ss2; 
                alpha_complex->openHoles(openhole_dim,2,true,false);
                wrap_complex->printFull(false, std::cout);
                wrap_complex->printFull(false, ss2);
                if(ss1.str()!=ss2.str()){
                    std::cout << "different result!" << std::endl;
                }*/
               
            
        }else{ //if invalid
            count_invalid++;
            if(count_invalid<ntrials*2){ //avoid infinite loop
                t--; //repeat trial
                repeated = true;
            }
        }        
        generated_points.clear();       
    }
    
    double time2 = getCPUTime();
    
    myfile.close(); //close output file
    std::cout.rdbuf(coutbuf); //reset to standard output again
    
    std::cout << "Elapsed time: " << time2-time1 << std::endl; 
    if(periodic && count_invalid>0){
        std::cerr << "ERROR - # invalid trials: " << count_invalid;
        if(count_invalid>=2*ntrials){
            std::cout << " - CANCELLED" << std::endl;
            std::cout << "--> PLEASE USE A LARGER WINDOW SIZE OR LAMBDA! <--";
            draw = false;
        }
        std::cout << std::endl;
    }
    
     //draw last trial (set drawing parameters relative to window size for 2D)
    if(draw){
        if(!dim3){
            setDrawingParameters(window_size*0.01,window_size*0.005,window_size*0.01,labelsize,pointsize_persdiag,maxvalue_plots,color_delaunay_by_radius,true);
        }else{
            drawScene();
        }
        if(bregman){
            drawBregmanOnInputPoints(draw_on_input_points);
        }
    }else{
        emitInitialSignals();
        if(!dim3){
            update();
        }else{
            opengl_widget->clear();
            opengl_widget->update();
        }
    }
    
    if(!dim3 && !bregman){
        //display specified window 
        QRectF rect(0,0,window_size,window_size);
        if(periodic){
            rect = QRectF(0,0,1.2*window_size,1.2*window_size);
        }
        setSceneRect(rect);
        fitInView(sceneRect(), Qt::KeepAspectRatio);
    }else{
        fitView();
    }
}

//add point to point set
bool Geometry::addPoint(double x, double y, double z, bool recompute, bool test, bool printstats)
{
    //no dynamic update for weighted points
    if(alpha_complex->isWeighted() || bregman) {
        recompute = true;
    }

    
    clearHighlightedItems();
    
    if(getNumDataPoints()>0){
        DataPoint last_point = input_data_points.back();
        if(last_point.isSame(x,y,z)){
            std::cout << "This point was just added! Please choose a new one." << std::endl;
            return false;
        }
    }
    if(alpha_complex->isPeriodic()){
        int periodic_size = alpha_complex->getPeriodicSize();
        if(x<0 || x>=periodic_size || y<0 || y>=periodic_size || z<0 || z>=periodic_size){
            std::cout << "This point is outside of periodic domain! Please choose one that is inside. The domain size is " << periodic_size <<"." << std::endl;
            return false;
        }
    }
    //add point to list of data_points
    int new_index = input_data_points.size();
    if(free_indices_data_points.size()==0){
        if(!dim3){
            input_data_points.push_back(DataPoint(x,y));
        }else{
            input_data_points.push_back(DataPoint(false,x,y,z));
        }
    }else{
        //fill empty space (previously deleted point) in point list
        new_index = free_indices_data_points.back();
        free_indices_data_points.pop_back();
        if(!dim3){
            input_data_points[new_index]=DataPoint(x,y);
        }else{
            input_data_points[new_index]=DataPoint(false,x,y,z);
        }
    }
    last_point_inserted=new_index;
    
    transformed_data_points = &input_data_points;
    bregman = false;
    Q_EMIT(bregmanChanged(false));
    
    bool infos = false;
    bool compute_persistence_and_statistics = true;
    
    if(!test){
        
        //update complexes
        if(recompute){
            //recompute from scratch 
            recomputeWithUpdatedPoints(true,infos,compute_persistence_and_statistics);
        }else{
            //recompute by local modifications 
            alpha_complex->addPoint(&input_data_points,new_index,infos,printstats,compute_persistence_and_statistics,compute_persistence_and_statistics); 
        }
        //redraw everything 
        drawScene(); //draw complexes, interval graph and persistence diagrams
        //emit signals 
        emitInitialSignals();
        
    }else{
        //TEST
        //first add point by local updates, then recompute, output running times
        alpha_complex->addPoint(&input_data_points,new_index,false,printstats,false,false); 
        recomputeWithUpdatedPoints(false,false,false);
    }
    
    return true;
}

//delete point from point set
bool Geometry::deletePoint(int data_point_index, bool recompute, bool test, bool printstats)
{
    //no dynamic update for weighted points
    if(alpha_complex->isWeighted() || bregman) {
        recompute = true;
    }
    
    clearHighlightedItems();
    
    if(data_point_index>=0 && data_point_index<input_data_points.size()){
        if(input_data_points[data_point_index].isDeleted()){
            std::cout << "This point was already deleted! Choose another." << std::endl;
            return false;
        }

        //deleting point from list (formally, invalidate)
        DataPoint old_point = input_data_points[data_point_index]; //keep old point for now
        //set empty/deleted point at given index
        input_data_points[data_point_index] = DataPoint(); 
        free_indices_data_points.push_back(data_point_index);
        
        transformed_data_points = &input_data_points;
        bregman = false;
        Q_EMIT(bregmanChanged(false));
        
        if(!test){
            
            bool infos = false;
            bool compute_persistence_and_statistics = true;
            if(recompute){
                //recompute from scratch 
                recomputeWithUpdatedPoints(true,infos,compute_persistence_and_statistics);
            }else{
                //update by local modifications
                alpha_complex->updateDataPointsPtr(&input_data_points);
                alpha_complex->deletePoint(old_point,data_point_index,infos,printstats,compute_persistence_and_statistics,compute_persistence_and_statistics);
            }

            //redraw everything 
            drawScene(); //draw complexes, interval graph and persistence diagrams
            //emit signals 
            emitInitialSignals();
        
        }else{
            //TEST 
            //first add by local updates
            alpha_complex->deletePoint(old_point,data_point_index,false,printstats,false,false); 
            //then try recomputing
            recomputeWithUpdatedPoints(false,false,false);
        }
        
        last_point_inserted=input_data_points.size()-1;
        while(last_point_inserted>=0 && input_data_points[last_point_inserted].isDeleted()){
            last_point_inserted--;
        }

        return true;
    }else{
        std::cout << "Please choose valid point index (0-" << input_data_points.size() << ") to delete point! (Bad input: " << data_point_index << ")" << std::endl;
        return false;
    }
}

//move existing point
bool Geometry::movePoint(int data_point_index, double x, double y, double z, bool recompute)
{
    if(data_point_index>=0 && data_point_index<input_data_points.size()){
        if(input_data_points[data_point_index].isSame(x,y,z)){
            std::cout << "This point has already the desired position. Choose other location to move to." << std::endl;
            return false;
        }
        if(input_data_points.back().isSame(x,y,z)){
            std::cout << "A point that was just added is already in the desired position. Choose other location to move to." << std::endl;
            return false;
        }
        if(input_data_points[data_point_index].isDeleted()){
            std::cout << "This point was deleted! Choose another." << std::endl;
            return false;
        }
        if(recompute){
            //compute from scratch
            
            //change coordinates of point of given index
            input_data_points[data_point_index].moveTo(x,y,z);
            //recompute from scratch 
            recomputeWithUpdatedPoints(true,true,true);
        }else{
            //update by local modifications
            
            //delete point first, then insert it elsewhere, index should stay the same as it is the last one inserted in free_indices_points
            bool valid = deletePoint(data_point_index,recompute,false,false);
            if(!valid){
                return false;
            }
            valid = addPoint(x,y,z,recompute,false,false);
            if(!valid){
                return false;
            }
        }
        
        return true;
    }else{
        std::cout << "Please choose valid point index (0-" << input_data_points.size() << ") to move point! (Bad input: " << data_point_index << ")" << std::endl;
        return false;
    }
}

//test running time of local vs global point operation for incrementally adding all points
void Geometry::testIncrementalPointAddition(std::vector<DataPoint> input_data_points, int periodic)
{
    if(input_data_points.size()>3){
        setPoints(std::vector<DataPoint>(input_data_points.begin(), input_data_points.begin()+4),false,periodic,false,"",false);
        for(int i=5; i<input_data_points.size(); i++){
            std::cout << "ADD POINT, "  << i << std::endl;
            addPoint(input_data_points[i].getX_double(),input_data_points[i].getY_double(),input_data_points[i].getZ_double(),false,true,true);
        }
    }
}

 //compare dynamic point insertion and deletion to static computation
void Geometry::testDynamicRuntime(double window_size, double lambda, int num_trials, bool periodic, bool dim3, bool draw)
{
    generatePointsPPP(window_size,lambda,false,num_trials,false, -1,-1, draw, periodic, dim3, false, false, false, "dynamic_runtime",-1);
}

//run trials to get data for statistics on ppp
void Geometry::computeStatisticsPPP(double window_size, double lambda, int num_trials, bool periodic, bool dim3, bool draw)
{
    generatePointsPPP(window_size,lambda,false,num_trials,false, -1,-1, draw, periodic, dim3, false, false, false, "",-1);
}

void Geometry::undoHoleOperations()
{
    //undo all hole operations
    alpha_complex->undoHoleOperations(true, true);
    wrap_complex->undoHoleOperations(true,true);
    
    //redraw everything 
    drawScene(); //draw complexes, interval graph and persistence diagrams
    //emit signals 
    emitInitialSignals();
}

//perform hole operation on given persistence pair or above/below given bound
void Geometry::holeOperation(bool lock, bool un, int dim, int pers_pair_num, bool subcomplex, double persistence_bound, bool lowerbound, bool printinfo)
{
    double time1 = getCPUTime();
    
    //perform operation
    alpha_complex->holeOperation(lock,un,dim,pers_pair_num,subcomplex,persistence_bound,lowerbound,printinfo,true, true, true);
    wrap_complex->holeOperation(lock,un,dim,pers_pair_num,subcomplex,persistence_bound,lowerbound,printinfo,true, true, true);
    
    std::cout << "Elapsed time: " << getCPUTime()-time1 << std::endl; 
    
    //redraw everything 
    drawScene(); //draw complexes, interval graph and persistence diagrams 
    //emit signals 
    emitInitialSignals(); 
}

//recompute intervals (-> Wrap complex) with prescribed imprecision threshold, as with noisy filtration values
void Geometry::recomputeWrapWithThreshold(double threshold, bool printinfo)
{
   //TEST print output to file
    /*std::string s = "../output/output_recompute.txt";
    char const* filename = s.c_str();
    std::ofstream myfile(filename);
    std::cout << s << std::endl;
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
    std::cout.rdbuf(myfile.rdbuf()); //redirect std::cout to myfile*/  
    
    //store old values to compare
    std::vector<bool> old_critical(alpha_complex->getCritical()->begin(),alpha_complex->getCritical()->end());
    std::vector<exact> old_filtration_values(alpha_complex->getFiltrationValues()->begin(),alpha_complex->getFiltrationValues()->end());
    std::vector<std::vector<int> > old_interval_faces(alpha_complex->getIntervalFaces()->begin(),alpha_complex->getIntervalFaces()->end());
    std::vector<bool> old_in_wrap(wrap_complex->getInComplex()->begin(),wrap_complex->getInComplex()->end());

    //reconstruct intervals, adjust filtration values
    alpha_complex->computeIntervalsFromNoisyFiltration(alpha_complex->getFiltrationValues(),threshold,isWeighted(),false,false,true,true,true,printinfo);

    //compare reconstructed and original intervals
    int count_changes_interval = 0;
    int count_changes_inwrap = 0;
    for(int i=0; i<alpha_complex->getSimplices()->size(); i++){
        std::vector<int> reconstructed_interval_faces_i = alpha_complex->getIntervalFaces()->at(i);
        bool same_interval_faces = (reconstructed_interval_faces_i.size() == old_interval_faces[i].size());
        std::sort(reconstructed_interval_faces_i.begin(),reconstructed_interval_faces_i.end());
        std::sort(old_interval_faces[i].begin(),old_interval_faces[i].end());
        if(same_interval_faces){
            for(int j=0; j<old_interval_faces[i].size(); j++){
                if(old_interval_faces[i][j]!=reconstructed_interval_faces_i[j]){
                    same_interval_faces = false;
                    break;
                }
            }
        }
        if(old_critical[i]!=alpha_complex->getCritical()->at(i) || !same_interval_faces){
            count_changes_interval++;
            if(printinfo){
                (alpha_complex->getSimplices()->at(i)).printShort();
                std::cout << "value " << old_filtration_values[i] << " -> " << alpha_complex->getFiltrationValues()->at(i) << ", critical " << old_critical[i] << " -> " << alpha_complex->getCritical()->at(i);
                std::cout << ", interval_faces ";
                for(int j=0; j<old_interval_faces[i].size(); j++){
                    std::cout << old_interval_faces[i][j] << " ";
                }
                std::cout << "-> ";
                for(int j=0; j<reconstructed_interval_faces_i.size(); j++){
                    std::cout << reconstructed_interval_faces_i[j] << " ";
                };
                std::cout << std::endl;
            }
        }
        if(old_in_wrap[i]!=wrap_complex->isInComplex(i)){
            count_changes_inwrap++;
        }
    }
    if(printinfo){
        std::cout << std::endl;
    }
    std::cout << "threshold: " << threshold << std::endl;
    std::cout << count_changes_interval << "/" << alpha_complex->getSimplices()->size() << " (" << (count_changes_interval)*100./(alpha_complex->getSimplices()->size()*1.) << " %) interval for simplex has changed" << std::endl;
    std::cout << count_changes_inwrap << "/" << alpha_complex->getSimplices()->size() << " (" << (count_changes_inwrap)*100./(alpha_complex->getSimplices()->size()*1.) << " %) changes whether simplex in full Wrap complex" << std::endl;
    
    //TEST
    //myfile.close(); //close output file
    //std::cout.rdbuf(coutbuf); //reset to standard output again
    
    drawScene();
}

//delete all simplices for which circumcenter is outside of domain (positive quadrant/orthant or standard simplex), only works for unweighted points in Euclidean or Fisher geometry
void Geometry::computeCircumcentersMaxSimplices(std::string type, bool restrict_to_domain, bool print)
{
    if(type==""){
        alpha_complex->computeCircumcentersMaxSimplices(restrict_to_domain,print);
    }else if(type=="fisher"||type=="bregman_Shannon_dual"||type=="bregman_Shannon_primal"){
        bregman_complexes->computeCircumcentersMaxSimplicesBregman(restrict_to_domain,print);
    }else{
        return; //no culling in other cases
    }
    
    if(restrict_to_domain){
        alpha_complex->computeFiltrationStatistics(false,false);
        alpha_complex->getWrapComplex()->computeFiltrationStatistics(false,false);

        //draw and emit signals for interactive output
        //-----------------------------------------------
        //draw complexes, interval graph and persistence diagrams
        drawScene(); 
        if(bregman){
            drawBregmanOnInputPoints(draw_on_input_points);
        }

        //emit signals 
        emitInitialSignals();  
    }
}

//update alpha complex for new alpha value from slider
void Geometry::updateAlpha(double new_alpha)
{
    double new_alpha2 = new_alpha;
    if(!(isWeighted()||use_radius2)){
        new_alpha2 = new_alpha*new_alpha;
        if(new_alpha<0){
            new_alpha2 = -new_alpha2;
        }
    }
    if(new_alpha2 != alpha_complex->getAlpha2_double()){
        double alpha_range_length, alpha_min, alpha_max;
        if(!(isWeighted()||use_radius2)){
            alpha_min = std::sqrt(CGAL::to_double(wrap_complex->getMinAlpha2()));
            alpha_max = std::sqrt(CGAL::to_double(wrap_complex->getMaxAlpha2()));
            if(maxvalue_plots>0){
                alpha_max = maxvalue_plots;
            }
            alpha_range_length = alpha_max-alpha_min;
        }else{
            alpha_min = (CGAL::to_double(wrap_complex->getMinAlpha2()));
            alpha_max = (CGAL::to_double(wrap_complex->getMaxAlpha2()));
            if(maxvalue_plots>0){
                alpha_max = maxvalue_plots;
            }
            alpha_range_length = alpha_max-alpha_min;
        }
        alpha_complex->setAlpha2(new_alpha2);
        wrap_complex->setAlpha2(new_alpha2);

        if(dim3){
            opengl_widget->updateAlpha(wrap_complex->getFiltrationIndexGreaterThanAlpha2(),alpha_complex->getFiltrationIndexGreaterThanAlpha2());
        }

        if(!full_bool){ 
            //redraw alpha complex
            if(alpha_bool || wrap_bool || union_of_balls_bool){
                if(!dim3){
                    updateVisibleGeometry();
                }else{
                    if(alpha_bool){
                        drawAlpha(alpha_bool);
                    }
                    if(wrap_bool){
                        drawWrap(wrap_bool);
                    } 
                }
            }
        }

        //redraw persistence diagram
        changePersDiag(persDiag_wrap_bool,persDiag_adapted_bool,persDiag_status_bool);

        //move line in statistics plots
        if(new_alpha > alpha_max)
        {
            new_alpha = alpha_max*1.05;
        }
        if(alpha_range_length>0){
            statistics_plots_currentvalue->setLine((new_alpha-alpha_min)*100./alpha_range_length,100,(new_alpha-alpha_min)*100./alpha_range_length,0);
        }

        //emit signals for interactive output
        //-----------------------------------------------
        if(!full_bool){
            Q_EMIT(numEdgesAlphaChanged(QString::number(alpha_complex->getNumEdges())));
            Q_EMIT(numTrianglesAlphaChanged(QString::number(alpha_complex->getNumTriangles())));
            Q_EMIT(numEdgesWrapChanged(QString::number(wrap_complex->getNumEdges())));
            Q_EMIT(numTrianglesWrapChanged(QString::number(wrap_complex->getNumTriangles()))); 
            Q_EMIT(numCritEdgesPosChanged(QString::number(wrap_complex->getNumCriticalEdgesPos())));
            Q_EMIT(numCritEdgesNegChanged(QString::number(wrap_complex->getNumCriticalEdgesNeg())));
            Q_EMIT(numCritTrianglesNegChanged(QString::number(wrap_complex->getNumCriticalTrianglesNeg())));
            Q_EMIT(betti0Changed(QString::number(wrap_complex->getBetti0())));
            Q_EMIT(betti1Changed(QString::number(wrap_complex->getBetti1())));    
            if(dim3){
                Q_EMIT(numTetrahedraAlphaChanged(QString::number(alpha_complex->getNumTetrahedra())));
                Q_EMIT(numTetrahedraWrapChanged(QString::number(wrap_complex->getNumTetrahedra()))); 
                Q_EMIT(numCritTrianglesPosChanged(QString::number(wrap_complex->getNumCriticalTrianglesPos())));
                Q_EMIT(numCritTetrahedraChanged(QString::number(wrap_complex->getNumCriticalTetrahedra())));
                Q_EMIT(betti2Changed(QString::number(wrap_complex->getBetti2()))); 
            }
        }

        if(!dim3){
            update();
        }
        
        Q_EMIT(alphaChanged());
    }
}

//ensure that interactive output shown at start
void Geometry::emitInitialSignals()
{
    Q_EMIT(dimChanged(dim3));
    Q_EMIT(numPointsChanged(QString::number(getNumDataPoints())));
    Q_EMIT(numEdgesDelaunayChanged(QString::number(alpha_complex->getNumEdgesFull())));
    Q_EMIT(numTrianglesDelaunayChanged(QString::number(alpha_complex->getNumTrianglesFull())));
    Q_EMIT(maxPersistence0Changed(QString::number(wrap_complex->getMaxPersistence0())));
    Q_EMIT(maxPersistence1Changed(QString::number(wrap_complex->getMaxPersistence1())));
    if(dim3){
        Q_EMIT(numTetrahedraDelaunayChanged(QString::number(alpha_complex->getNumTetrahedraFull())));
        Q_EMIT(maxPersistence2Changed(QString::number(wrap_complex->getMaxPersistence2())));
    }
    if(!full_bool){
        Q_EMIT(numEdgesAlphaChanged(QString::number(alpha_complex->getNumEdges())));
        Q_EMIT(numTrianglesAlphaChanged(QString::number(alpha_complex->getNumTriangles())));
        Q_EMIT(numEdgesWrapChanged(QString::number(wrap_complex->getNumEdges())));
        Q_EMIT(numTrianglesWrapChanged(QString::number(wrap_complex->getNumTriangles())));     
        Q_EMIT(numIntervalsChanged(QString::number(wrap_complex->getNumIntervals())));
        Q_EMIT(numCritEdgesPosChanged(QString::number(wrap_complex->getNumCriticalEdgesPos())));
        Q_EMIT(numCritEdgesNegChanged(QString::number(wrap_complex->getNumCriticalEdgesNeg())));
        Q_EMIT(numCritTrianglesNegChanged(QString::number(wrap_complex->getNumCriticalTrianglesNeg())));
        Q_EMIT(betti0Changed(QString::number(wrap_complex->getBetti0())));
        Q_EMIT(betti1Changed(QString::number(wrap_complex->getBetti1())));
        if(dim3){
            Q_EMIT(numTetrahedraAlphaChanged(QString::number(alpha_complex->getNumTetrahedra())));
            Q_EMIT(numTetrahedraWrapChanged(QString::number(wrap_complex->getNumTetrahedra()))); 
            Q_EMIT(numCritTrianglesPosChanged(QString::number(wrap_complex->getNumCriticalTrianglesPos())));
            Q_EMIT(numCritTetrahedraChanged(QString::number(wrap_complex->getNumCriticalTetrahedra())));
            Q_EMIT(betti2Changed(QString::number(wrap_complex->getBetti2()))); 
        }
    }else{
        Q_EMIT(numEdgesAlphaChanged(QString::number(alpha_complex->getNumEdgesFull())));
        Q_EMIT(numTrianglesAlphaChanged(QString::number(alpha_complex->getNumTrianglesFull())));
        Q_EMIT(numEdgesWrapChanged(QString::number(wrap_complex->getNumEdgesFull())));
        Q_EMIT(numTrianglesWrapChanged(QString::number(wrap_complex->getNumTrianglesFull()))); 
        Q_EMIT(numCritEdgesPosChanged(QString::number(wrap_complex->getNumCriticalEdgesPosFull())));
        Q_EMIT(numCritEdgesNegChanged(QString::number(wrap_complex->getNumCriticalEdgesNegFull())));
        Q_EMIT(numCritTrianglesNegChanged(QString::number(wrap_complex->getNumCriticalTrianglesNegFull())));
        Q_EMIT(betti0Changed(QString::number(wrap_complex->getBetti0Full())));
        Q_EMIT(betti1Changed(QString::number(wrap_complex->getBetti1Full()))); 
        if(dim3){
            Q_EMIT(numTetrahedraAlphaChanged(QString::number(alpha_complex->getNumTetrahedraFull())));
            Q_EMIT(numTetrahedraWrapChanged(QString::number(wrap_complex->getNumTetrahedraFull()))); 
            Q_EMIT(numCritTrianglesPosChanged(QString::number(wrap_complex->getNumCriticalTrianglesPosFull())));
            Q_EMIT(numCritTetrahedraChanged(QString::number(wrap_complex->getNumCriticalTetrahedraFull())));
            Q_EMIT(betti2Changed(QString::number(wrap_complex->getBetti2Full()))); 
        }
    }
    if(persDiag_wrap_bool){
        Q_EMIT(showMessagePersDiag(!persDiag_wrap_fulldrawn));
    }else{
        Q_EMIT(showMessagePersDiag(!persDiag_alpha_fulldrawn));
    }
    Q_EMIT(intervalGraphDrawn(intervalgraph_drawn));
    Q_EMIT(plotsChanged());
}

//show/hide Delaunay triangulation
void Geometry::drawDelaunay(bool checked)
{
    delaunay_bool = checked;
    updateVisibleGeometry();
}

//show/hide convex hull
void Geometry::drawConvexHull(bool checked)
{
    convex_hull_bool = checked;
    updateVisibleGeometry();
}

//show/hide union of balls
void Geometry::drawUnionOfBalls(bool checked)
{
    union_of_balls_bool = checked;
    if(!dim3){ //2-dim
        updateVisibleGeometry();
    }
}

//show/hide interval labels (2D: triangle labels, 3D: tetrahedron labels)
void Geometry::showLabelsIntervals(bool checked)
{
    labels_intervals_bool = checked;
    if(!dim3){
         updateVisibleGeometry();
    }else{
        opengl_widget->showLabelsIntervals(checked);
    }
}

//show/hide point labels 
void Geometry::showLabelsPoints(bool checked)
{
    labels_points_bool = checked;
    if(!dim3){
        updateVisibleGeometry();
    }else{
        opengl_widget->showLabelsPoints(checked);
    }
}

//show weighted points as spheres
void Geometry::showWeightedPointSpheres(bool checked)
{
    weighted_point_spheres_bool = checked;
    if(!dim3){
        updateVisibleGeometry();
    }else{
        opengl_widget->showWeightedPointsSpheres(checked);
    }
}

//show/hide Wrap complex
void Geometry::drawWrap(bool checked)
{
    wrap_bool = checked;
    if(!dim3){ //2-dim
        updateVisibleGeometry();
    }else{ //3-dim
        opengl_widget->drawWrap(wrap_bool);
    }
}

//show/hide Alpha complex
void Geometry::drawAlpha(bool checked)
{
    alpha_bool = checked;
    if(!dim3){ //2-dim
        updateVisibleGeometry();
    }else{ //3-dim
        opengl_widget->drawAlpha(alpha_bool);       
    }    
}

//draw full Wrap and Alpha complex
void Geometry::drawFull(bool checked)
{
    full_bool = checked;
    changePersDiag(persDiag_wrap_bool,persDiag_adapted_bool,persDiag_status_bool); //update persistence diagram
    
    if(!dim3){ //2-dim
        //update drawing
        updateVisibleGeometry();
    }else{ //3-dim
        opengl_widget->drawFull(full_bool);
    }
    
    //update statistics output
    if(!full_bool){
        Q_EMIT(numEdgesAlphaChanged(QString::number(alpha_complex->getNumEdges())));
        Q_EMIT(numTrianglesAlphaChanged(QString::number(alpha_complex->getNumTriangles())));
        Q_EMIT(numEdgesWrapChanged(QString::number(wrap_complex->getNumEdges())));
        Q_EMIT(numTrianglesWrapChanged(QString::number(wrap_complex->getNumTriangles()))); 
        Q_EMIT(numCritEdgesPosChanged(QString::number(wrap_complex->getNumCriticalEdgesPos())));
        Q_EMIT(numCritEdgesNegChanged(QString::number(wrap_complex->getNumCriticalEdgesNeg())));
        Q_EMIT(numCritTrianglesNegChanged(QString::number(wrap_complex->getNumCriticalTrianglesNeg())));
        Q_EMIT(betti0Changed(QString::number(wrap_complex->getBetti0())));
        Q_EMIT(betti1Changed(QString::number(wrap_complex->getBetti1())));   
        if(dim3){
            Q_EMIT(numTetrahedraAlphaChanged(QString::number(alpha_complex->getNumTetrahedra())));
            Q_EMIT(numTetrahedraWrapChanged(QString::number(wrap_complex->getNumTetrahedra()))); 
            Q_EMIT(numCritTrianglesPosChanged(QString::number(wrap_complex->getNumCriticalTrianglesPos())));
            Q_EMIT(numCritTetrahedraChanged(QString::number(wrap_complex->getNumCriticalTetrahedra())));
            Q_EMIT(betti2Changed(QString::number(wrap_complex->getBetti2()))); 
        }
    }else{
        Q_EMIT(numEdgesAlphaChanged(QString::number(alpha_complex->getNumEdgesFull())));
        Q_EMIT(numTrianglesAlphaChanged(QString::number(alpha_complex->getNumTrianglesFull())));
        Q_EMIT(numEdgesWrapChanged(QString::number(wrap_complex->getNumEdgesFull())));
        Q_EMIT(numTrianglesWrapChanged(QString::number(wrap_complex->getNumTrianglesFull()))); 
        Q_EMIT(numCritEdgesPosChanged(QString::number(wrap_complex->getNumCriticalEdgesPosFull())));
        Q_EMIT(numCritEdgesNegChanged(QString::number(wrap_complex->getNumCriticalEdgesNegFull())));
        Q_EMIT(numCritTrianglesNegChanged(QString::number(wrap_complex->getNumCriticalTrianglesNegFull())));
        Q_EMIT(betti0Changed(QString::number(wrap_complex->getBetti0Full())));
        Q_EMIT(betti1Changed(QString::number(wrap_complex->getBetti1Full()))); 
        if(dim3){
            Q_EMIT(numTetrahedraAlphaChanged(QString::number(alpha_complex->getNumTetrahedraFull())));
            Q_EMIT(numTetrahedraWrapChanged(QString::number(wrap_complex->getNumTetrahedraFull()))); 
            Q_EMIT(numCritTrianglesPosChanged(QString::number(wrap_complex->getNumCriticalTrianglesPosFull())));
            Q_EMIT(numCritTetrahedraChanged(QString::number(wrap_complex->getNumCriticalTetrahedraFull())));
            Q_EMIT(betti2Changed(QString::number(wrap_complex->getBetti2Full()))); 
        }
    }
}

//update visibility of groups of simplices depending on bools (Wrap, Alpha, Delaunay, Bregman, ...)
void Geometry::updateVisibleGeometry()
{
     if(draw_on_input_points || !bregman){
        input_points_items.setVisible(true);  
        labels_intervals_items.setVisible(labels_intervals_bool);
        labels_points_items.setVisible(labels_points_bool);
        delaunay_items.setVisible(delaunay_bool); 
        if(wrap_bool){
            if(full_bool){
                wrap_items.setVisible(true);
            }else{
                wrap_items.filterAlpha(alpha_complex->getAlpha2_double());
            }
        }else{
            wrap_items.setVisible(false);
        }
        if(alpha_bool){
            if(full_bool){
                alpha_items.setVisible(true);
            }else{
                alpha_items.filterAlpha(alpha_complex->getAlpha2_double());
            }
        }else{
            alpha_items.setVisible(false);
        }  
        convex_hull_items.setVisible(convex_hull_bool);
        if(union_of_balls_bool){
            union_of_balls_items.setVisible(true);
            int index_points = 0;
            for(int i=0; i<union_of_balls_items.size(); i++){
                union_of_balls_items.getItem(i)->setScale(std::sqrt(alpha_complex->getAlpha2_double()));
                while(input_data_points[index_points].isDeleted()){
                    index_points++;
                }
                union_of_balls_items.getItem(i)->setPos(input_data_points[index_points].getX_double()-std::sqrt(alpha_complex->getAlpha2_double()),input_data_points[index_points].getY_double()-std::sqrt(alpha_complex->getAlpha2_double()));
                index_points++;
            }
        }else{
            union_of_balls_items.setVisible(false);
        }
        transformed_points_items.setVisible(false);
        labels_intervals_items_bregman.setVisible(false);
        labels_points_items_bregman.setVisible(false);
        weighted_point_spheres_items.setVisible(weighted_point_spheres_bool && !bregman);
        delaunay_items_bregman.setVisible(false);
        wrap_items_bregman.setVisible(false);
        alpha_items_bregman.setVisible(false);
    }else{
        input_points_items.setVisible(false);  
        labels_intervals_items.setVisible(false);
        labels_points_items.setVisible(false);
        delaunay_items.setVisible(false); 
        wrap_items.setVisible(false);
        alpha_items.setVisible(false);
        convex_hull_items.setVisible(false);
        union_of_balls_items.setVisible(false);
        transformed_points_items.setVisible(true);
        labels_intervals_items_bregman.setVisible(labels_intervals_bool);
        labels_points_items_bregman.setVisible(labels_points_bool);
        weighted_point_spheres_items.setVisible(weighted_point_spheres_bool);
        delaunay_items_bregman.setVisible(delaunay_bool);
        if(wrap_bool){
            if(full_bool){
                wrap_items_bregman.setVisible(true);
            }else{
                wrap_items_bregman.filterAlpha(alpha_complex->getAlpha2_double());
            }
        }else{
            wrap_items_bregman.setVisible(false);
        }
        if(alpha_bool){
            if(full_bool){
                alpha_items_bregman.setVisible(true);
            }else{
                alpha_items_bregman.filterAlpha(alpha_complex->getAlpha2_double());
            }
        } else{
            alpha_items_bregman.setVisible(false);
        }
        if(!dim3){
            old_point->setVisible(false);
            new_point->setVisible(false);
        }
    }
    update();
}

//show persistence diagram for Alpha or Wrap complex depending on boolean, original, current or original with current status
void Geometry::changePersDiag(bool persDiag_wrap_bool_, bool adapted, bool status){
    
    persDiag_wrap_bool = persDiag_wrap_bool_;
    persDiag_adapted_bool = adapted;
    persDiag_status_bool = status;
    
    //nothing was drawn
    if(persistence_diag_alpha_items_adapted.size()==0){
        return;
    }

    double alpha_range_length, alpha_min, alpha_max, alpha;
    if(!(isWeighted()||use_radius2)){
        alpha_min = std::sqrt(CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = std::sqrt(CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
        alpha = std::sqrt(getAlpha2Current());
    }else{
        alpha_min = (CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = (CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
        alpha = getAlpha2Current();
    }
    if(persistence_diag_wrap_items_adapted.size()>0){
        persistence_diag_subcomplex_box->setRect(0,0,(alpha-alpha_min)*100./alpha_range_length,100-(alpha-alpha_min)*100./alpha_range_length);
    }
    if(persDiag_wrap_bool){
        highlighted_persistence_points_wrap.setVisible(true);
        highlighted_persistence_points_alpha.setVisible(false);
        if(adapted||(!alpha_complex->getWrapComplex()->wasPersistenceDiagramAdapted()&&!status)){
            //adapted persistence diagram (or original if nothing was adapted)
            persistence_diag_wrap_items_adapted.setVisible(true);
            persistence_diag_wrap_items_original.setVisible(false);
            persistence_diag_wrap_items_status.setVisible(false);
        }else if(status){
            persistence_diag_wrap_items_adapted.setVisible(false);
            persistence_diag_wrap_items_original.setVisible(false);
            persistence_diag_wrap_items_status.setVisible(true);
        }else{
            persistence_diag_wrap_items_original.setVisible(true);
            persistence_diag_wrap_items_adapted.setVisible(false);
            persistence_diag_wrap_items_status.setVisible(false);
        }
        persistence_diag_alpha_items_original.setVisible(false);
        persistence_diag_alpha_items_adapted.setVisible(false);
        persistence_diag_alpha_items_status.setVisible(false);
        if(persDiag_subcomplex_bool){
            if(adapted){
                persistence_diag_wrap_items_subcomplex.filterAlpha(alpha_complex->getAlpha2_double()); 
            }else{
                persistence_diag_wrap_items_subcomplex.setVisible(false); 
            }
            persistence_diag_subcomplex_box->setVisible(true);
        }else{
            persistence_diag_wrap_items_subcomplex.setVisible(false); 
            persistence_diag_subcomplex_box->setVisible(false);
        }
        persistence_diag_alpha_items_subcomplex.setVisible(false);
        highlighted_items_wrap.setVisible(true);
        if(full_bool){
            highlighted_items_filtered_wrap.setVisible(true);
        }else{
            highlighted_items_filtered_wrap.filterAlpha(alpha_complex->getAlpha2_double());
        }
        highlighted_items_alpha.setVisible(false);
        highlighted_items_filtered_alpha.setVisible(false);
        Q_EMIT(showMessagePersDiag(!persDiag_wrap_fulldrawn));
    }else{
        highlighted_persistence_points_wrap.setVisible(false);
        highlighted_persistence_points_alpha.setVisible(true);
        if(adapted||(!alpha_complex->getWrapComplex()->wasPersistenceDiagramAdapted()&&!status)){
             //adapted persistence diagram (or original if nothing was adapted)
            persistence_diag_alpha_items_adapted.setVisible(true);
            persistence_diag_alpha_items_original.setVisible(false);
            persistence_diag_alpha_items_status.setVisible(false);
        }else if(status){
            persistence_diag_alpha_items_adapted.setVisible(false);
            persistence_diag_alpha_items_original.setVisible(false);
            persistence_diag_alpha_items_status.setVisible(true);
        }else{
            persistence_diag_alpha_items_original.setVisible(true);
            persistence_diag_alpha_items_adapted.setVisible(false);
            persistence_diag_alpha_items_status.setVisible(false);
        }
        persistence_diag_wrap_items_original.setVisible(false);
        persistence_diag_wrap_items_adapted.setVisible(false);
        persistence_diag_wrap_items_status.setVisible(false);
        if(persDiag_subcomplex_bool){
            if(adapted){
                persistence_diag_alpha_items_subcomplex.filterAlpha(alpha_complex->getAlpha2_double()); 
            }else{
                persistence_diag_alpha_items_subcomplex.setVisible(false); 
            }
            persistence_diag_subcomplex_box->setVisible(true);
        }else{
            persistence_diag_alpha_items_subcomplex.setVisible(false); 
            persistence_diag_subcomplex_box->setVisible(false);
        }
        persistence_diag_wrap_items_subcomplex.setVisible(false);
        highlighted_items_alpha.setVisible(true);
        if(full_bool){
            highlighted_items_filtered_alpha.setVisible(true);
        }else{
            highlighted_items_filtered_alpha.filterAlpha(alpha_complex->getAlpha2_double());
        }
        highlighted_items_wrap.setVisible(false);
        highlighted_items_filtered_wrap.setVisible(false);
        Q_EMIT(showMessagePersDiag(!persDiag_alpha_fulldrawn));
    }  
    if(alpha_complex->getWrapComplex()->wasPersistenceDiagramAdapted() && persDiag_connections_bool){
        if(persDiag_wrap_bool){
            persistence_diag_connections_wrap.setVisible(true);
            persistence_diag_connections_alpha.setVisible(false);
        }else{
            persistence_diag_connections_wrap.setVisible(false);
            persistence_diag_connections_alpha.setVisible(true);
        }
    }else{
        persistence_diag_connections_wrap.setVisible(false);
        persistence_diag_connections_alpha.setVisible(false);
    }
    if(persDiag_status_bool){ //do not highlight in status persistence diagram
        highlighted_persistence_points_wrap.setVisible(false);
        highlighted_persistence_points_alpha.setVisible(false);
        highlighted_items_filtered_wrap.setVisible(false);
        highlighted_items_filtered_alpha.setVisible(false);
    }
    update();
    third_scene.update();
}

//show/hide box and red points to highlight current subcomplex in persistence diagram
void Geometry::changePersDiagSubcomplex(bool highlight_subcomplex){
    persDiag_subcomplex_bool = highlight_subcomplex;
    changePersDiag(persDiag_wrap_bool,persDiag_adapted_bool,persDiag_status_bool);
}

//whether to display connections between old and new persistence diagram after hole manipulations
void Geometry::showPersDiagConnections(bool checked){
    persDiag_connections_bool = checked;
}

//change drawing of Bregman-Delaunay complexes between input points and transformed points 
void Geometry::drawBregmanOnInputPoints(bool checked){
    draw_on_input_points = checked;
    if(!dim3){
        updateVisibleGeometry();
        fitView();
    }else{
        opengl_widget->drawBregmanOnTransformedPoints(bregman && !draw_on_input_points);
    }
}

//show statistic plot of given title
void Geometry::changeStatisticPlot(QString title)
{
    for(int i=0; i<statistics_plots_titles.size(); i++){
        if(title==statistics_plots_titles[i]){
            if(!plots_logscale){
                statistics_plots[i]->setVisible(true);
                statistics_plots_log[i]->setVisible(false);
            }else{
                statistics_plots_log[i]->setVisible(true);
                statistics_plots[i]->setVisible(false);
            }
            statistics_plots_axislabel[i]->setVisible(true);
        }else{
            statistics_plots[i]->setVisible(false);
            statistics_plots_log[i]->setVisible(false);
            statistics_plots_axislabel[i]->setVisible(false);
        }
    }
}

//insert new point by mouse click (left))
void Geometry::mousePressEvent(QMouseEvent *mouseEvent)
{
      if (mouseEvent->button() == Qt::LeftButton)
      {
          //get position of mouse click
          QPointF p = mapToScene(mouseEvent->pos());
          //add point
          bool recompute = false;
          if(input_data_points.size()<4){
              recompute = true;
          }
          addPoint(p.x(),p.y(),0.,recompute,false,false);
      }
}

//track coordinates of mouse cursor
void Geometry::mouseMoveEvent(QMouseEvent *mouseEvent){
    QPointF q = mapToScene(mouseEvent->pos());
    std::stringstream ss;
    ss << std::setprecision(4) << q.x() << ", " << std::setprecision(4) << q.y();
    Q_EMIT(mouseCoordsChanged(QString::fromStdString(ss.str())));
}

//update drawings of old point
void Geometry::updateOldPoint(bool show, int index)
{
    if(show && index>=0 && index<=getMaxPointIndex() && !input_data_points[index].isDeleted()){
        double oldx = input_data_points[index].getX_double();
        double oldy = input_data_points[index].getY_double();
        double oldz = input_data_points[index].getZ_double();
        if(!dim3){
            old_point->setPos(oldx,oldy);
            old_point->show();
        }else{
            opengl_widget->updateOldPoint(show,oldx,oldy,oldz);
        }
    }else{
        if(!dim3){
            old_point->hide();
        }else{
            opengl_widget->updateOldPoint(show,-1,-1,-1);
        }
    }
}

//update drawings of new point
void Geometry:: updateNewPoint(bool show, double x, double y, double z)
{
    if(show){
        if(!dim3){
            new_point->setPos(x,y);
            new_point->show();
        }else{
            opengl_widget->updateNewPoint(show,x,y,z);
        }
    }else{
        if(!dim3){
            new_point->hide();
        }else{
            opengl_widget->updateNewPoint(show,-1,-1,-1);
        }
    }
}

//get ranking-th persistence pair of given dim, ordered by decreasing persistence
PersistencePair Geometry::getPersistencePairOfDim(int dim, int ranking, bool subcomplex, bool lock, bool un, bool wrap)
{
    
    PersistencePair persistence_pair;
    if(wrap){
        if(!subcomplex){ 
            persistence_pair = alpha_complex->getWrapComplex()->getPersistencePairOfDim(dim,ranking);
        }else{
            if(lock && !un){ //lock
                //only take persistence pairs with future birth
                persistence_pair = alpha_complex->getWrapComplex()->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),false,true,true);
            }else if(!lock && !un){ //fill
                //only take persistence pairs which are not dead yet
                persistence_pair = alpha_complex->getWrapComplex()->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),false,false,true);
            }else if(lock && un){ //unlock
                //only take persistence pairs which are born already
                persistence_pair = alpha_complex->getWrapComplex()->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),true,false,false);
            }else if(!lock && un){ //unfill
                //only take persistence pairs which are already dead
                persistence_pair = alpha_complex->getWrapComplex()->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),true,true,false);
            }
        }
    }else{
        if(!subcomplex){ 
            persistence_pair = alpha_complex->getPersistencePairOfDim(dim,ranking);
        }else{
            if(lock && !un){ //lock
                //only take persistence pairs with future birth
                persistence_pair = alpha_complex->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),false,true,true);
            }else if(!lock && !un){ //fill
                ///only take persistence pairs which are not dead yet
                persistence_pair = alpha_complex->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),false,false,true);
            }else if(lock && un){ //unlock
                //only take persistence pairs which are born already
                persistence_pair = alpha_complex->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),true,false,false);
            }else if(!lock && un){ //unfill
                //only take persistence pairs which are already dead
                persistence_pair = alpha_complex->getPersistencePairOfDimStatus(dim,ranking,alpha_complex->getAlpha2_double(),true,true,false);
            }
        }
    }
    return persistence_pair;
}

double Geometry::getPersistenceValueOfDim(int dim, int ranking, bool subcomplex, bool lock, bool un)
{
    PersistencePair pair = getPersistencePairOfDim(dim,ranking,subcomplex,lock,un,false); 
    if((isWeighted()||use_radius2)){
        return pair.getPersistence2_double(alpha_complex->getFiltrationFull());
    }else{
        return pair.getPersistence_double(alpha_complex->getFiltrationFull());
    }
}
        

//highlight canonical cycle (lock), chain (fill), cocycle (unfill) or cochain (unlock) of ranking-th persistence pair of given dimension
// mode: true = highlight, false = only draw these simplices, subcomplex true: operation for current subcomplex
void Geometry::highlightChain(int dim, int pers_pair_ranking, bool lock, bool un, bool highlight_mode, bool subcomplex, QColor color1, QColor color2)
{
    clearHighlightedItems();
    
    int main_simplex_index_wrap, main_simplex_index_alpha;
    std::vector<int> simplices_to_highlight_indices_wrap, simplices_to_highlight_indices_alpha;
    
    Filtration* wrap_filtration_full = wrap_complex->getFiltrationFull();
    Filtration* alpha_filtration_full = alpha_complex->getFiltrationFull();
    
    PersistencePair persistence_pair_wrap = getPersistencePairOfDim(dim,pers_pair_ranking,subcomplex,lock,un,true);
    PersistencePair persistence_pair_alpha = getPersistencePairOfDim(dim,pers_pair_ranking,subcomplex,lock,un,false);
    //check if pair was found
    if(persistence_pair_wrap.getBirthIndexFiltration()<0){
        return;
    }
    main_simplex_index_wrap = -1; 
    main_simplex_index_alpha = -1;
    bool column = true;
    if(lock && !un){ //cycle (lock)
        main_simplex_index_wrap = persistence_pair_wrap.getBirthIndexFiltration();
        main_simplex_index_alpha = persistence_pair_alpha.getBirthIndexFiltration();
        column = true;
    }else if(!lock && !un){ //chain (fill)
        main_simplex_index_wrap = persistence_pair_wrap.getDeathIndexFiltration();
        main_simplex_index_alpha = persistence_pair_alpha.getDeathIndexFiltration();
        column = true;
    }else if(!lock && un){ //cocycle (unfill)
        main_simplex_index_wrap = persistence_pair_wrap.getDeathIndexFiltration();
        main_simplex_index_alpha = persistence_pair_alpha.getDeathIndexFiltration();
        column = false;
    }else if(lock && un){ //cochain (unlock)
        main_simplex_index_wrap = persistence_pair_wrap.getBirthIndexFiltration();
        main_simplex_index_alpha = persistence_pair_alpha.getBirthIndexFiltration();
        column = false;
    }else{
        return;
    }
    simplices_to_highlight_indices_wrap = alpha_complex->getWrapComplex()->getCanonicalCycleChain(main_simplex_index_wrap,column);
    simplices_to_highlight_indices_alpha = alpha_complex->getCanonicalCycleChain(main_simplex_index_alpha,column);
    
    int main_simplex_wrap = wrap_filtration_full->getElement(main_simplex_index_wrap).second;
    int main_simplex_alpha = alpha_filtration_full->getElement(main_simplex_index_alpha).second;
    std::vector<int> simplices_to_highlight_wrap;
    std::vector<int> simplices_to_highlight_alpha;
    for(int i=0; i<simplices_to_highlight_indices_wrap.size(); i++){
        int simplex = wrap_filtration_full->getElement(simplices_to_highlight_indices_wrap[i]).second;
        if(simplex != main_simplex_wrap){
            simplices_to_highlight_wrap.push_back(simplex);
        }
    }
    for(int i=0; i<simplices_to_highlight_indices_alpha.size(); i++){
        int simplex = alpha_filtration_full->getElement(simplices_to_highlight_indices_alpha[i]).second;
        if(simplex != main_simplex_alpha){
            simplices_to_highlight_alpha.push_back(simplex);
        }
    }
    std::vector<int> simplices_to_highlight2_wrap;
    std::vector<int> simplices_to_highlight2_alpha;
    simplices_to_highlight2_wrap.push_back(main_simplex_wrap);
    simplices_to_highlight2_alpha.push_back(main_simplex_alpha);

    //highlight simplices (2d or 3d)
    highlightSimplices(&simplices_to_highlight_wrap, &simplices_to_highlight2_wrap, highlight_mode, true, color1, color2);
    highlightSimplices(&simplices_to_highlight_alpha, &simplices_to_highlight2_alpha, highlight_mode, false, color1, color2);
    
    
    //highlight dependent persistence points
    if(subcomplex){
        
        std::vector<PersistencePair> dependent_pairs_wrap;
        std::vector<PersistencePair> dependent_pairs_alpha;
        if(!un){
            dependent_pairs_wrap = alpha_complex->getWrapComplex()->getDependencesBoundary(main_simplex_index_wrap,false);
            dependent_pairs_alpha = alpha_complex->getDependencesBoundary(main_simplex_index_alpha,false);
        }else{
            dependent_pairs_wrap = alpha_complex->getWrapComplex()->getDependencesBoundary(main_simplex_index_wrap,true);
            dependent_pairs_alpha = alpha_complex->getDependencesBoundary(main_simplex_index_alpha,true);
        }
        if(lock && !un){ //lock
            //BB_T
            std::vector<PersistencePair> more = alpha_complex->getWrapComplex()->getDependences(main_simplex_index_wrap,true,true,true);
            dependent_pairs_wrap.insert(dependent_pairs_wrap.end(),more.begin(),more.end());
            more = alpha_complex->getDependences(main_simplex_index_alpha,true,true,true);
            dependent_pairs_alpha.insert(dependent_pairs_alpha.end(),more.begin(),more.end());
            //DB_T
            more = alpha_complex->getWrapComplex()->getDependences(main_simplex_index_wrap,false,true,true);
            dependent_pairs_wrap.insert(dependent_pairs_wrap.end(),more.begin(),more.end());
            more = alpha_complex->getDependences(main_simplex_index_alpha,false,true,true);
            dependent_pairs_alpha.insert(dependent_pairs_alpha.end(),more.begin(),more.end());
        }else if(!lock && !un){ //fill
            //DD_T
            std::vector<PersistencePair> more = alpha_complex->getWrapComplex()->getDependences(main_simplex_index_wrap,false,false,true);
            dependent_pairs_wrap.insert(dependent_pairs_wrap.end(),more.begin(),more.end());
            more = alpha_complex->getDependences(main_simplex_index_alpha,false,false,true);
            dependent_pairs_alpha.insert(dependent_pairs_alpha.end(),more.begin(),more.end());
        }else if(lock && un){ //unlock
            //BB
            std::vector<PersistencePair> more = alpha_complex->getWrapComplex()->getDependences(main_simplex_index_wrap,true,true,false);
            dependent_pairs_wrap.insert(dependent_pairs_wrap.end(),more.begin(),more.end());
            more = alpha_complex->getDependences(main_simplex_index_alpha,true,true,false);
            dependent_pairs_alpha.insert(dependent_pairs_alpha.end(),more.begin(),more.end());
        }else if(!lock && un){ //unfill
            //DD
            std::vector<PersistencePair> more = alpha_complex->getWrapComplex()->getDependences(main_simplex_index_wrap,false,false,false);
            dependent_pairs_wrap.insert(dependent_pairs_wrap.end(),more.begin(),more.end());
            more = alpha_complex->getDependences(main_simplex_index_alpha,false,false,false);
            dependent_pairs_alpha.insert(dependent_pairs_alpha.end(),more.begin(),more.end());
            //DB
            more = alpha_complex->getWrapComplex()->getDependences(main_simplex_index_wrap,false,true,false);
            dependent_pairs_wrap.insert(dependent_pairs_wrap.end(),more.begin(),more.end());
            more = alpha_complex->getDependences(main_simplex_index_alpha,false,true,false);
            dependent_pairs_alpha.insert(dependent_pairs_alpha.end(),more.begin(),more.end());
        }
        for(int i=0; i<dependent_pairs_wrap.size(); i++){
            highlightPersistencePoint(dependent_pairs_wrap[i],color1,true);
        }
        std::sort(dependent_pairs_alpha.begin(),dependent_pairs_alpha.end());
        dependent_pairs_alpha.erase(std::unique(dependent_pairs_alpha.begin(),dependent_pairs_alpha.end()),dependent_pairs_alpha.end());
        std::sort(dependent_pairs_wrap.begin(),dependent_pairs_wrap.end());
        dependent_pairs_wrap.erase(std::unique(dependent_pairs_wrap.begin(),dependent_pairs_wrap.end()),dependent_pairs_wrap.end());
        for(int i=0; i<dependent_pairs_alpha.size(); i++){
            highlightPersistencePoint(dependent_pairs_alpha[i],color1,false);
        }
    }
    //highlight main point in persistence diagram
    highlightPersistencePoint(persistence_pair_wrap, color2, true);
    highlightPersistencePoint(persistence_pair_alpha, color2, false);

    changePersDiag(persDiag_wrap_bool,persDiag_adapted_bool,persDiag_status_bool);
    update();
}

//highlight persistence pairs that will be manipulated for hole operation with persistence bound
void Geometry::highlightPersistencePairsWithBoundStatus(int dim, double persistence_bound, bool lowerbound, bool lock, bool un, bool subcomplex, QColor color)
{
    clearHighlightedItems();
    
    std::vector<PersistencePair*> persistence_pairs;
    if(!subcomplex){
        persistence_pairs = alpha_complex->getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,alpha_complex->getAlpha2_double(),false,false,false);
    }else{
        if(lock && !un){ //lock
            //only take persistence pairs with future birth
            persistence_pairs = alpha_complex->getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,alpha_complex->getAlpha2_double(),false,true,true);
        }else if(!lock && !un){ //fill
            //only take persistence pairs which are not dead
            persistence_pairs = alpha_complex->getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,alpha_complex->getAlpha2_double(),false,false,true);
        }else if(lock && un){ //unlock
            //only take persistence pairs which are born already
            persistence_pairs = alpha_complex->getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,alpha_complex->getAlpha2_double(),true,false,false);
        }else if(!lock && un){ //unfill
            //only take persistence pairs which are already dead
            persistence_pairs = alpha_complex->getPersistencePairsWithBoundStatus(dim,persistence_bound,lowerbound,alpha_complex->getAlpha2_double(),true,true,false);
        }
    }
    
    for(int i=0; i<persistence_pairs.size(); i++){
        highlightPersistencePoint(*persistence_pairs[i], color, false);
    }
    
    update();
}

//highlight two sets of simplices in different colors, mode: true = highlight, false = only draw these simplices
void Geometry::highlightSimplices(std::vector<int>* simplices_to_highlight1, std::vector<int>* simplices_to_highlight2, bool highlight_mode, bool wrap, QColor color1, QColor color2)
{
    if(!dim3){
        if(!highlight_mode){ //only draw highlighted simplices
            delaunay_items.setVisible(false);
            alpha_items.setVisible(false); 
            wrap_items.setVisible(false);
        }else{ //normally draw complexes
            displayDrawing();
        }
        
        std::vector<Simplex>* simplices = alpha_complex->getSimplices();
        
        double rad = pointsize;
        QColor color;
        
        for(int i=0; i<simplices_to_highlight1->size()+simplices_to_highlight2->size(); i++){
            int simplex_dim, simplex_index;
            if(i<simplices_to_highlight1->size()){
                simplex_index = simplices_to_highlight1->at(i);
                simplex_dim = (simplices->at(simplex_index)).getDim();
                color = color1;
            }else{
                simplex_index = simplices_to_highlight2->at(i-simplices_to_highlight1->size());
                simplex_dim = (simplices->at(simplex_index)).getDim();
                color = color2;
            }
            
            Simplex simplex = alpha_complex->getSimplices()->at(simplex_index);
            
            if(simplex_dim==0){
                DataPoint point = input_data_points[simplex.getVertex(0)];
                if(bregman && !draw_on_input_points){
                    point = transformed_data_points->at(simplex.getVertex(0));
                }
                QPointF p = QPointF(point.getX_double(),point.getY_double()); //cast to Qt Point
                //draw Ellipse with specified pen
                QGraphicsEllipseItem * ell = scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                    Qt::NoPen, QBrush(color,Qt::SolidPattern));
                ell->setZValue(51); 
                if(highlight_mode){
                    if(wrap){
                        highlighted_items_filtered_wrap.add(alpha_complex->getWrapComplex()->getAlpha2Simplex(simplex_index),ell);
                    }else{
                        highlighted_items_filtered_alpha.add(alpha_complex->getAlpha2Simplex(simplex_index),ell);
                    }
                    scene.addItem(ell);
                }else{
                    if(wrap){
                        highlighted_items_wrap.add(-1,ell);
                    }else{
                        highlighted_items_alpha.add(-1,ell);
                    }
                    scene.addItem(ell);
                }
            }else if(simplex_dim==1){
                
                QLineF line;
                if(!alpha_complex->isPeriodic()){
                    line = QLineF(QPointF(input_data_points[simplex.getVertex(0)].getX_double(),input_data_points[simplex.getVertex(0)].getY_double()), QPointF(input_data_points[simplex.getVertex(1)].getX_double(),input_data_points[simplex.getVertex(1)].getY_double()));
                    if(bregman && !draw_on_input_points){
                        line = QLineF(QPointF(transformed_data_points->at(simplex.getVertex(0)).getX_double(),transformed_data_points->at(simplex.getVertex(0)).getY_double()), QPointF(transformed_data_points->at(simplex.getVertex(1)).getX_double(),input_data_points[simplex.getVertex(1)].getY_double()));
                    }
                }else{
                    int periodic_size = alpha_complex->getPeriodicSize();
                    Offset offset0 = simplex.getOffset(0);
                    Offset offset1 = simplex.getOffset(1);
                    QPointF point0 = QPointF(input_data_points[simplex.getVertex(0)].getX_double()+offset0.getX()*periodic_size,input_data_points[simplex.getVertex(0)].getY_double()+offset0.getY()*periodic_size);
                    QPointF point1 = QPointF(input_data_points[simplex.getVertex(1)].getX_double()+offset1.getX()*periodic_size,input_data_points[simplex.getVertex(1)].getY_double()+offset1.getY()*periodic_size);
                    if(bregman && !draw_on_input_points){
                        point0 = QPointF(transformed_data_points->at(simplex.getVertex(0)).getX_double()+offset0.getX()*periodic_size,transformed_data_points->at(simplex.getVertex(0)).getY_double()+offset0.getY()*periodic_size);
                        point1 = QPointF(transformed_data_points->at(simplex.getVertex(1)).getX_double()+offset1.getX()*periodic_size,transformed_data_points->at(simplex.getVertex(1)).getY_double()+offset1.getY()*periodic_size);
                    }
                    line = QLineF(point0,point1);
                }
                
                QGraphicsLineItem* edge_item;
                edge_item = scene.addLine(line,QPen(color, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));   
                edge_item->setZValue(41);    
                if(highlight_mode){
                    if(wrap){
                        highlighted_items_filtered_wrap.add(alpha_complex->getWrapComplex()->getAlpha2Simplex(simplex_index),edge_item);
                    }else{
                        highlighted_items_filtered_alpha.add(alpha_complex->getAlpha2Simplex(simplex_index),edge_item);
                    }
                    scene.addItem(edge_item);
                }else{
                    if(wrap){
                        highlighted_items_wrap.add(-1,edge_item);
                    }else{
                        highlighted_items_alpha.add(-1,edge_item);
                    }
                    scene.addItem(edge_item);
                }
                
            }else if(simplex_dim==2){
                //save as Qt polygon
                QPolygonF triangle;
                //get triangle points
                std::vector<DataPoint> tri_points;
                if(!(bregman && !draw_on_input_points)){
                    tri_points.push_back(input_data_points[simplex.getVertex(0)]);
                    tri_points.push_back(input_data_points[simplex.getVertex(1)]);
                    tri_points.push_back(input_data_points[simplex.getVertex(2)]);
                }else{
                    tri_points.push_back(transformed_data_points->at(simplex.getVertex(0)));
                    tri_points.push_back(transformed_data_points->at(simplex.getVertex(1)));
                    tri_points.push_back(transformed_data_points->at(simplex.getVertex(2)));
                }
                //if periodic triangulation, need to draw points with offset
                if(alpha_complex->isPeriodic()){
                    int periodic_size = alpha_complex->getPeriodicSize();
                    for(int i=0; i<3; i++){
                        Offset offset = simplex.getOffset(i);
                        tri_points[i]=DataPoint(tri_points[i].getX_double()+offset.getX()*periodic_size,tri_points[i].getY_double()+offset.getY()*periodic_size);
                    }
                }
                triangle << QPointF(tri_points[0].getX_double(),tri_points[0].getY_double()) << QPointF(tri_points[1].getX_double(),tri_points[1].getY_double()) << QPointF(tri_points[2].getX_double(),tri_points[2].getY_double());

                //define graphics item and add to group
                //Delaunay
                QGraphicsPolygonItem* triangle_item = scene.addPolygon(triangle,QPen(color, linewidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin), QBrush(color, Qt::SolidPattern));             
                triangle_item->setZValue(31);    
                if(highlight_mode){
                    if(wrap){
                        highlighted_items_filtered_wrap.add(alpha_complex->getWrapComplex()->getAlpha2Simplex(simplex_index),triangle_item);
                    }else{
                        highlighted_items_filtered_alpha.add(alpha_complex->getAlpha2Simplex(simplex_index),triangle_item);
                    }
                    scene.addItem(triangle_item);
                }else{
                    if(wrap){
                        highlighted_items_wrap.add(-1,triangle_item);
                    }else{
                        highlighted_items_alpha.add(-1,triangle_item);
                    }
                    scene.addItem(triangle_item);
                }
            }
        }    
    }else{
        if(!wrap){ 
            if(!bregman || draw_on_input_points){
                opengl_widget->highlightSimplices(simplices_to_highlight1,simplices_to_highlight2,highlight_mode,wrap,color1,color2,
                    &input_data_points,alpha_complex->getSimplices());
            }else{
                opengl_widget->highlightSimplices(simplices_to_highlight1,simplices_to_highlight2,highlight_mode,wrap,color1,color2,
                    transformed_data_points,alpha_complex->getSimplices());
            }
        }
    }
}

//highlight point in persistence diagram of wrap or alpha complex
void Geometry::highlightPersistencePoint(PersistencePair persistence_pair, QColor color, bool wrap)
{
    //drawing parameters
    double rad = pointsize_persdiag*1.5;
    double penwidth=0.02;
    QColor color_transparent = QColor(color.red(),color.green(),color.blue(),100);
    QPen pen(color_transparent); pen.setWidth(penwidth);
    
    //determine range of axes in diagram
    double alpha_range_length, alpha_min, alpha_max;
    if(!(isWeighted()||use_radius2)){
        alpha_min = std::sqrt(CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = std::sqrt(CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }else{
        alpha_min = (CGAL::to_double(wrap_complex->getMinAlpha2()));
        alpha_max = (CGAL::to_double(wrap_complex->getMaxAlpha2()));
        if(maxvalue_plots>0){
            alpha_max = maxvalue_plots;
        }
        alpha_range_length = alpha_max-alpha_min;
    }
    Filtration* filtration;
    if(wrap){
        filtration = alpha_complex->getWrapComplex()->getFiltrationFull();
    }else{
        filtration = alpha_complex->getFiltrationFull();
    }
    //get birth value
    double birth_draw;
    if(!(isWeighted()||use_radius2)){
        birth_draw = persistence_pair.getBirth_double(filtration);
    }else{
        birth_draw = persistence_pair.getBirth2_double(filtration);
    }
    //draw point
    if(persistence_pair.isInfinite()){
        //infinite pair (birth, - ) 
        //add "infinite" point (drawn at upper edge of diagram) with lower bound
        double inf = 105.; //"infinite"
        QPointF p((birth_draw-alpha_min)*100./alpha_range_length,100-inf);
        //draw point and add to filtered item group
        QGraphicsEllipseItem* point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
            pen, QBrush(color,Qt::SolidPattern));
        point->setZValue(3);
        if(wrap){
            highlighted_persistence_points_wrap.add(point);  
        }else{
            highlighted_persistence_points_alpha.add(point);  
        }
        third_scene.addItem(point);
    }else{
        //finite pair (birth, death)
        double death_draw;
         if(!(isWeighted()||use_radius2)){
            death_draw = persistence_pair.getDeath_double(filtration);
        }else{
            death_draw = persistence_pair.getDeath2_double(filtration);
        }
        //do not draw points on diagonal
        if(!persistence_pair.hasZeroPersistence(filtration)){
            QPointF p = QPointF((birth_draw-alpha_min)*100./alpha_range_length,100-(death_draw-alpha_min)*100./alpha_range_length);
            int zvalue = 2;
            QGraphicsEllipseItem* point = third_scene.addEllipse(p.x()-rad, p.y()-rad, rad*2.0, rad*2.0, 
                pen, QBrush(color,Qt::SolidPattern));
            point->setZValue(zvalue);
            if(wrap){
                highlighted_persistence_points_wrap.add(point);  
            }else{
                highlighted_persistence_points_alpha.add(point);  
            }  
            third_scene.addItem(point);
        }
    }
}

//clear highlighted items when exiting highlight window
void Geometry::clearHighlightedItems()
{
    highlighted_persistence_points_wrap.clear();
    highlighted_persistence_points_alpha.clear();
    if(!dim3){
        highlighted_items_filtered_wrap.clear();
        highlighted_items_wrap.clear(); 
        highlighted_items_filtered_alpha.clear();
        highlighted_items_alpha.clear(); 
    }else{
        opengl_widget->clearHighlighted();
    }
    update();
    
}

void Geometry::updatePersistenceDiagramStatusSelection(std::vector<bool> draw_dim, std::vector<bool> draw_status, int highlight_dependences_of_simplex, bool lock, bool un, bool recursive, bool original_status)
{
    persistence_diag_alpha_items_status.clear();
    
    Filtration* filtration = alpha_complex->getFiltrationFull();
    std::vector<bool> draw_pair_of_birth_simplex(filtration->size(),false);
    if(highlight_dependences_of_simplex>=0){
        std::vector<PersistencePair> dependent_pairs = alpha_complex->getDependentPairsOperation(highlight_dependences_of_simplex, lock, un, recursive, original_status);
        for(int i=0; i<dependent_pairs.size(); i++){
            int simplex_index = dependent_pairs[i].getBirthIndexFiltration();
            int dim = dependent_pairs[i].getDim();
            if(dim>=0){
                int status = alpha_complex->getStatusOfCorrespondingPersistencePair(simplex_index);
                if(draw_dim[dim]&&draw_status[status]){
                    draw_pair_of_birth_simplex[simplex_index]=true;
                }
            }
        }
        PersistencePair main_pair = alpha_complex->getPersistencePairForSimplex(highlight_dependences_of_simplex,lock);
        draw_pair_of_birth_simplex[main_pair.getBirthIndexFiltration()] = true;
        highlight_dependences_of_simplex = main_pair.getBirthIndexFiltration();
    }else{
        for(int i=1; i<(filtration->size());i++){
            //only work with birth simplices (representing a pair)
            if(alpha_complex->isSimplexPositive(i)){
                int dim = filtration->getElementDim(i,alpha_complex->getSimplices());
                int status = alpha_complex->getStatusOfCorrespondingPersistencePair(i);
                if(draw_dim[dim]&&draw_status[status]){
                    draw_pair_of_birth_simplex[i]=true;
                }
            }
        }
    }
    drawPersistenceDiag(false,false,true,draw_pair_of_birth_simplex,highlight_dependences_of_simplex);
    
}

//highlight simplices in the current (full) complex that do not belong to the complex stored in the given file
void Geometry::highlightDifferentSimplices(QString filename)
{
    
    std::vector<std::vector<std::vector<int> > > simplices_input_byvertex0(input_data_points.size());
    
    std::ifstream ifs(qPrintable(filename));
    std::string line;
    std::getline(ifs, line); //skip first line
    std::string simplex_str;
    while(!ifs.eof()){
        //skip first three entries in line, comma-separated
        getline(ifs, line,',');
        getline(ifs, line,',');
        getline(ifs, line,',');
        //get entry containing simplex vertices
        getline(ifs,simplex_str,'\n');
        //parse with empty space
        std::istringstream streamLine(simplex_str);
        std::vector<int> simplex_vec;
        int vertex = 0;
        for(; streamLine >> vertex;){
            simplex_vec.push_back(vertex);
        }
        simplices_input_byvertex0[simplex_vec[0]].push_back(simplex_vec);
    }
    
    std::vector<Simplex>* simplices_ptr = alpha_complex->getSimplices();
    
    std::vector<bool> simplex_different = std::vector<bool>(simplices_ptr->size(),true);
    for(int i=0; i<simplices_input_byvertex0.size(); i++){
        if(simplices_input_byvertex0[i].size()>0){
            int vertex0 = simplices_input_byvertex0[i][0][0];
            Simplex simplex_vertex0 = simplices_ptr->at(alpha_complex->getDataPointSimplexIndex(vertex0));
            std::set<int> cofaces_vertex0 = getCofacesIndices(simplex_vertex0.getIndex(),simplices_ptr);
            //iterate over all simplices with same vertex0
            for(int j=0; j<simplices_input_byvertex0[i].size(); j++){
                //check if input simplex is contained in current set of simplices
                for(std::set<int>::iterator it = cofaces_vertex0.begin(); it != cofaces_vertex0.end(); ++it){
                    Simplex coface = simplices_ptr->at(*it);
                    if(coface.sameSimplex(simplices_input_byvertex0[i][j])){
                        simplex_different[*it]=false;
                        break;
                    }
                }
            }
        }
    }
    
    std::vector<int> simplices_to_highlight_dim1;
    std::vector<int> simplices_to_highlight_dim2;
    for(int i=0; i<simplices_ptr->size(); i++){
        if(simplex_different[i]){
            if(simplices_ptr->at(i).getDim()==1){
                simplices_to_highlight_dim1.push_back(i);
            }else if(simplices_ptr->at(i).getDim()==2){
                simplices_to_highlight_dim2.push_back(i);
            }
        }
    }
    highlightSimplices(&simplices_to_highlight_dim1,&simplices_to_highlight_dim2,true,false,color_old_point,color_new_point);
    
}

//highlight p-tree (death simplices) of given dimension
void Geometry::highlightPTree(int tree_dim, QColor color)
{
    std::vector<int> ptree_simplices;
    Filtration* alpha_filtration = alpha_complex->getFiltrationFull();
    std::vector<Simplex>* simplices_ptr = alpha_complex->getSimplices();
    for(int i=0; i<alpha_filtration->size(); i++){
        if((alpha_filtration->getElementDim(i,simplices_ptr))==tree_dim){
            int simplex_index = alpha_filtration->getElementIndex(i);
            if(!(alpha_complex->isSimplexPositive(i))){
                ptree_simplices.push_back(simplex_index);
            }
        }
    }
    std::vector<int> empty_vec(0);
    highlightSimplices(&ptree_simplices,&empty_vec,true,false,color,color);
}

//fit graphics view to current scene, window size given
double Geometry::fitView(int drawing_window_size)
{
    if(!dim3){
        QRectF rect;
        if(drawing_window_size>0){
            //set scene rectangle according to given window size
            rect = QRectF(0,0,drawing_window_size,drawing_window_size);
            if(alpha_complex->isPeriodic()){
                rect = QRectF(0,0,1.2*drawing_window_size,1.2*drawing_window_size);
            }
        }else{
            //compute bounding rectangle of point cloud (max and min coordinate)
            if(input_data_points.size()>0){
                double xmin, xmax, ymin, ymax;
                xmin = input_data_points[0].getX_double();
                ymin = input_data_points[0].getY_double();
                if(bregman && !draw_on_input_points){
                    xmin = transformed_data_points->at(0).getX_double();
                    ymin = transformed_data_points->at(0).getY_double();
                }
                xmax = xmin;
                ymax = ymin;
                for(int i=1; i<input_data_points.size(); i++){
                    if(!bregman || draw_on_input_points){
                        if((input_data_points[i].getX_double())<xmin){
                            xmin=(input_data_points[i].getX_double());
                        }
                        if((input_data_points[i].getX_double())>xmax){
                            xmax=(input_data_points[i].getX_double());
                        }
                        if((input_data_points[i].getY_double())<ymin){
                            ymin=(input_data_points[i].getY_double());
                        }
                        if((input_data_points[i].getY_double())>ymax){
                            ymax=(input_data_points[i].getY_double());
                        }
                    }else{
                        if((transformed_data_points->at(i).getX_double())<xmin){
                            xmin=(transformed_data_points->at(i).getX_double());
                        }
                        if((transformed_data_points->at(i).getX_double())>xmax){
                            xmax=(transformed_data_points->at(i).getX_double());
                        }
                        if((transformed_data_points->at(i).getY_double())<ymin){
                            ymin=(transformed_data_points->at(i).getY_double());
                        }
                        if((transformed_data_points->at(i).getY_double())>ymax){
                            ymax=(transformed_data_points->at(i).getY_double());
                        }
                    }
                }
                double width = xmax-xmin;
                double height = ymax-ymin;
                if(width==0){
                    width=1;
                }
                if(height==0){
                    height=1;
                }
                rect = QRectF(xmin-0.05*width,ymin-0.05*height,width*1.1,height*1.1);
            }else{
                rect = QRectF(0,0,1,1);
            }
        }   
        setSceneRect(rect); 
        //fit view
        fitInView(sceneRect(), Qt::KeepAspectRatio);
        return std::max(rect.width(),rect.height());
    }else{
        opengl_widget->fitView();
    }
    return -1;
}

//fit graphics view to current scene, window size not given
double Geometry::fitView()
{
    return fitView(-1);
}

//fit view for third scene (persistence diagram)
void Geometry::fitViewThirdScene()
{
    QRectF rect(-30,-30 ,160, 160);   
    third_drawing_plane.setSceneRect(rect); 
    third_drawing_plane.fitInView(third_drawing_plane.sceneRect(), Qt::KeepAspectRatio);
}

//fit view for fourth scene (filtration statistics)
void Geometry::fitViewFourthScene()
{
    QRectF rect(-30,-30 ,160, 160);   
    fourth_drawing_plane.setSceneRect(rect); 
    fourth_drawing_plane.fitInView(fourth_drawing_plane.sceneRect(), Qt::KeepAspectRatio);
}

//set default colors for drawing
void Geometry::setDefaultColors()
{
    color_points = Qt::red;
    color_delaunay_points = Qt::darkGray;
    color_delaunay_dark = Qt::darkGray;
    color_delaunay_light = Qt::lightGray;
    color_alpha_dark = QColor(0,50,150);
    color_alpha_light = QColor(150,190,255);
    color_wrap_dark = Qt::darkGreen;
    color_wrap_light = Qt::green;
    //color_wrap_light = QColor(150,255,150); //light green
    color_old_point = QColor(255,180,0); //orange
    color_new_point = Qt::black;
    color_weighted_points_pos = Qt::black;
    color_weighted_points_neg = Qt::red;
}

//print list of simplices with point indices and radius^2 values
void Geometry::printSimplices(std::ostream& output)
{
    std::vector<Simplex>* simplices = alpha_complex->getSimplices();
    output << "dimension, Alpha radius^2, Wrap radius^2, vertex1 vertex2 ..." << std::endl;
    for(int i=0; i<simplices->size(); i++){
        Simplex simplex = simplices->at(i);
        if(simplex.getDim()>=0){
            double wrap_radius2 = -1;
            if(wrap_complex->isInComplex(i)){
                wrap_radius2 = CGAL::to_double(wrap_complex->getAlpha2Simplex(i));
            }
            output << simplex.getDim() << ", " << CGAL::to_double(alpha_complex->getAlpha2Simplex(i)) << ", " << wrap_radius2 << ", ";
            for(int j=0; j<simplex.getDim()+1; j++){
                output << simplex.getVertex(j);
                if(j<simplex.getDim()){
                    output << " ";
                }
            }   
            output << std::endl;
        }
    }
}